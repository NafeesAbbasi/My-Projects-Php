// NL
var __editLanguage =
  {
  101  : "Nieuw",
  102  : "Open",
  103  : "Afdrukken",
  104  : "Knippen",
  105  : "Kopi�ren",
  106  : "Plakken",
  107  : "Ongedaan maken",
  108  : "Herhalen",
  109  : "Link Invoegen",
  110  : "Afbeelding Invoegen",
  111  : "Tabel Invoegen",
  112  : "Regel Invoegen",
  113  : "Zoeken",
  114  : "Help",
  115  : "Speciale Karakters Invoegen",
  116  : "Datum Invoegen",
  117  : "Tijdstip Invoegen",
  118  : "Opslaan",
  119  : "Opslaan Als",

  201  : "Vet",
  202  : "Cursief",
  203  : "Onderstrepen",
  204  : "Superscript",
  205  : "Subscript",
  206  : "Links uitlijnen",
  207  : "Centreren",
  208  : "Rechts uitlijnen",
  209  : "Uitvullen",
  210  : "Nummering",
  211  : "Opsommingstekens",
  212  : "Insprong vergroten",
  213  : "Insprong verkleinen",
  214  : "Formatering verwijderen",
  215  : "Tekstkleur instellen",
  216  : "Achtergrondkleur instellen",
  217  : "Tekstkleur kiezen",
  218  : "Achtergrondkleur kiezen",

  301  : "Label invoegen",
  302  : "Button invoegen",
  303  : "Input invoegen",
  304  : "Checkbox invoegen",
  305  : "Radio invoegen",
  306  : "Combobox invoegen",
  307  : "Listbox invoegen",
  308  : "Text Area invoegen",
  309  : "Subpagina invoegen",
  310  : "Container invoegen",
  311  : "Absolute positionering",
  312  : "Tekstmodule invoegen",
  313  : "Nieuwe tekstmodule",
  314  : "Actieve tekstmodule verwijderen",

  401  : "Edit",
  402  : "HTML",
  403  : "Preview",
  404  : "Page break",
  405  : "Alles selecteren",
  406  : "Formulier invoegen",
  407  : "Anchor invoegen",
  408  : "Plakken uit Word",
  409  : "Marquee",
  410  : "Hidden field invoegen",
  411  : "Spellingchecker",

  501  : " Tabel",
  502  : "Annuleren",

  601  : "Meer kleuren...",

  701  : "Lokale afbeelding",
  702  : "Web afbeelding",
  703  : "Server afbeelding",
  704  : "Afbeelding uploaden",

  801  : "Rij vooraan invoegen",
  802  : "Rij achteraan invoegen",
  803  : "Rij verwijderen",
  804  : "Kolom vooraan invoegen",
  805  : "Kolom achteraan invoegen",
  806  : "Kolom verwijderen",
  807  : "Cel verwijderen",
  808  : "Cel samenvoegen",
  809  : "Cel splitsen",
  810  : "Cel eigenschappen...",
  811  : "Tabel eigenschappen...",
  812  : "Pagina eigenschappen...",
  813  : "Afbeelding eigenschappen...",
  814  : "Naar de voorgrond brengen",
  815  : "Container eigenschappen...",
  816  : "Subpagina eigenschappen...",
  817  : "Label eigenschappen...",

  900  : "Annuleren",
  901  : "Tabel Eigenschappen",
  902  : "Percent",
  903  : "Absoluut",
  904  : "Cel spati�ring:",
  905  : "Randbreedte:",
  906  : "Cel padding:",
  907  : "Randkleur:",
  908  : "Breedte:",
  909  : "Achtergrondkleur:",
  911  : "Cel Eigenschappen",
  912  : "Hor. aligneren:",
  913  : "Hoogte:",
  914  : "Vert. aligneren:",
  915  : "Wrap:",
  916  : "Randkleur:",
  917  : "Achtergrondkleur:",
  918  : "Breedte:",
  921  : "Afbeelding Eigenschappen",
  922  : "Breedte:",
  923  : "Vert. aligneren:",
  924  : "Hoogte:",
  925  : "Randbreedte:",
  926  : "Titel:",
  927  : "Randkleur:",
  931  : "Label Eigenschappen",
  932  : "Breedte:",
  933  : "Randkkleur:",
  934  : "Hoogte:",
  935  : "Achtergrondkleur:",
  941  : "Container Eigenschappen",
  942  : "Breedte:",
  943  : "Randkleur:",
  944  : "Hoogte:",
  945  : "Achtergrondkleur:",
  951  : "Pagina Eigenschappen",
  952  : "Paginarand:",
  953  : "Achtergrondkleur:",
  961  : "Kleur kiezen",
  971  : "Subpagina Eigenschappen",
  972  : "Breedte:",
  973  : "Hoogte:",
  974  : "URL:",
  981  : "Web afbeelding invoegen",
  982  : "Voer een URL in:",
  991  : "Zoeken/Vervangen",
  992  : "Zoek:",
  993  : "Vervang:",
  994  : "Zoeken",
  995  : "Vervangen",
  996  : "Gelieve uw zoekcriterium op te geven!",
  997  : "Einde van het document. De search werd niet teruggevonden. Zoekopdracht herhalen vanaf het begin!",
  998  : "Gelieve uw zoekcriterium op te geven!",

  1001  : "Open/Opslaan dialoog",
  1002  : "Open",
  1003  : "Opslaan",
  1004  : "Open",
  1005  : "Bestandsnaam:",
  1006  : "Preview:",

  1101  : "Anchor cre�ren/bewerken",
  1102  : "Anchor naam:",
  1103  : "Anchor eigenschappen",
  1104  : "Anchor verwijderen",

  1201  : "Form eigenschappen",
  1202  : "Anchor naam:",
  1203  : "Form eigenschappen",
  1204  : "Form verwijderen",

  1301  : "Input eigenschappen",
  1302  : "Select eigenschappen",
  1303  : "Grootte",
  1304  : "Kleur",

  2000  : "Naar de achtergrond brengen",
  2001  : "Todo",

  119  : "Bewaren Als",
  3000  : "Links naar rechts",
  3001  : "Rechts naar links",
  3002  : "Alinea invoegen",
  3003  : "Tabel highlight",
  3004  : "Hor. Aligneren:",
  3005  : "Upload invoegen",
  3006  : "Gelieve een tekstblok te selecteren !",
  3007  : "Naam:",
  3008  : "Tekstmodule aanmaken",
  3009  : "Gelieve een geldige tekstmodulenaam te gebruiken!",
  3010  : "Upload document",
  3011  : "Upload",
  3012  : "Gelieve eerst tekst te selecteren!",
  3013  : "Set RETURN modus",
  3014  : "Stylesheet kiezen",
  3015  : "Formaat kiezen",
  3016  : "Lettertype kiezen",
  3017  : "Lettergrootte kiezen",
  3018  : "Bestand",
  3019  : "Aanpassen",
  3020  : "Invoegen",
  3021  : "Formaat",
  3022  : "Formulieren",
  3023  : "Link eigenschappen",
  3024  : "Link verwijderen",
  3025  : "Pad:",
  3026  : "Type:",
  3027  : "Url:",
  3028  : "Target:",
  3029  : "Titel:",
  3030  : "Document kiezen",
  3031  : "Input Eigenschappen",
  3032  : "Select Eigenschappen",
  3033  : "Textarea Eigenschappen",

  962  : "ID:",
  963  : "Name:",
  964  : "ID/Name",
  899  : "OK",
  1007  : "Path:",
  3100  : "Make editable",
  3101  : "No color",

  3201  : "Upload",
  3202  : "File Upload",
  3203  : "Server Path:",
  3204  : "File:",

  3210  : "Name",
  3211  : "Size",
  3212  : "Type",
  3213  : "Changed",

  // 4.0  
  3300 : "Save local",
  3301 : "Set non editable",
  3302 : "Flash properties",
  3303 : "Insert Flash",
  3304 : "Insert Media",
  3305 : "Play:",
  3306 : "Loop:",
  3307 : "Menu:",
  3308 : "Quality:",
  3309 : "Parameter",
  3310 : "Convert Row(td <--> th)",
  3311 : "Please select a file !",
  3312 : "Remove Table",
  3313 : "New with template",
  3314 : "Print Preview",
  3315 : "Remove style/class",
  3316 : "Remove formatting",
  3317 : "Remove empty tags",
  3318 : "Remove Word specific",
  3319 : "Zoom",
  3320 : "Select text snippet",
  3321 : "Source:",
  3322 : "Float:",
  3323 : "Title:",
  3324 : "Action:",
  3325 : "Target:",
  3326 : "Name:",
  3327 : "Form",
  3328 : "Name",
  3329 : "Value:",
  3330 : "The selected text already includes a link !",
  3331 : "No link found !",
  3332 : "Textarea properties",
  3333 : "Alignment",
  3334 : "Span",
  3335 : "Row span:",
  3336 : "Col span:",
  3337 : "Class",
  3338 : "Class:",
  3339 : "Url",
  3340 : "Paste Text",
  3341 : "Source:",
  3342 : "Checked:",
  3343 : "Type:",
  3344 : "Text:",
  3345 : "Link",
  3346 : "Title",
  3347 : "Title:",
  3348 : "Charset",
  3349 : "Charset:",
  3350 : "Description",
  3351 : "Description:",
  3352 : "Author",
  3353 : "Author:",
  3354 : "Keywords",
  3355 : "Keywords:",
  3356 : "Text:",
  3357 : "Color:",
  3358 : "B-Color:",
  3359 : "Add",
  3360 : "Edit",
  3361 : "Delete",
  3362 : "Options",
  3363 : "Spacing",
  3364 : "Border",
  3365 : "Border:",
  3366 : "Border:",
  3367 : "Font",
  3368 : "Family:",
  3369 : "Size:",
  3370 : "Color:",
  3371 : "Type",
  3372 : "Strike through",
  3373 : "Overline",
  3374 : "Caps",
  3375 : "Border",
  3376 : "Full:",
  3377 : "Color",
  3378 : "Backcolor:",
  3379 : "Margin",
  3380 : "Padding",
  3381 : "Alignment",
  3382 : "Spacing",
  3383 : "Letters",
  3384 : "Height:",
  3385 : "Text flow",
  3386 : "Direction:",
  3387 : "Hor./Vert.:",
  3388 : "Display",
  3389 : "Display:",
  3390 : "Visibilty",
  3391 : "Position",
  3392 : "Position:",
  3393 : "Overflow:",
  3394 : "Float:",
  3395 : "Update",
  3396 : "Remove span",
  3397 : "Full size",
  3398 : "Page attributes",
  3399 : "Class",
  3400 : "Style",
  3401 : "Attributes",
  3402 : "Font",
  3403 : "Background",
  3404 : "Border",
  3405 : "Margin",
  3406 : "Text",
  3407 : "Layout",
  3408 : "Backcolor",
  3409 : "Backcolor:",
  3410 : "Column span",
  3411 : "Row span",
  3412 : "Add cell",
  3413 : "Anchor:",

  4000 : "Width:",
  4001 : "Height:",
  4002 : "Left:",
  4003 : "Top:",
  4004 : "Bottom:",
  4005 : "Right:",
  4006 : "Size",
  4007 : "Misc",
  4008 : "Standard",
  4009 : "Alignment",
  4010 : "Vertical:",
  4011 : "Horizontal:",
  4012 : "Value:",

  5000 : ""
  }
  
function getLanguageString(language,id)
{
  return __editLanguage[id];
}
