// PT
var __editLanguage =
  {
  101  : "Novo",
  102  : "Abrir",
  103  : "Imprimir",
  104  : "Recortar",
  105  : "Copiar",
  106  : "Colar",
  107  : "Desfazer",
  108  : "Refazer",
  109  : "Inserir Link",
  110  : "Inserir Imagem",
  111  : "Inserir Tabela",
  112  : "Inserir R�gua",
  113  : "Procurar",
  114  : "Ajuda",
  115  : "Inserir Caracteres Especiais",
  116  : "Inserir Data",
  117  : "Inserir Hora",
  118  : "Salvar",

  201  : "Negrito",
  202  : "It�lico",
  203  : "Sublinhado",
  204  : "Sobrescrito",
  205  : "Subscrito",
  206  : "Alinhar � Esquerda",
  207  : "Centralizar",
  208  : "Alinhar � Direita",
  209  : "Justificar",
  210  : "Numera��o",
  211  : "Marcadores",
  212  : "Diminuir Recuo",
  213  : "Aumentar Recuo",
  214  : "Remover formata��o",
  215  : "Cor do Texto",
  216  : "Cor do Plano de Fundo",
  217  : "Selecione a Cor do Texto",
  218  : "Selecione a Cor do Plano de Fundo",

  301  : "Inserir label",
  302  : "Inserir Bot�o",
  303  : "Inserir input",
  304  : "Inserir checkbox",
  305  : "Inserir radio",
  306  : "Inserir combobox",
  307  : "Inserir listbox",
  308  : "Inserir �rea de texto",
  309  : "Inserir sub page",
  310  : "Inserir container",
  311  : "Posi��o Absoluta",
  312  : "Inserir m�dulo de texto",
  313  : "Novo m�dulo de Texto",
  314  : "Remover m�dulo de Texto Ativo",

  401  : "Editar",
  402  : "HTML",
  403  : "Visualizar",
  404  : "Quebra de P�gina",
  405  : "Selecionar Tudo",
  406  : "Inserir Formul�rio",
  407  : "Inserir Link",
  408  : "Colar do Word",
  409  : "Marca",
  410  : "Inserir Campo Oculto",
  411  : "Verificar Ortografia",

  501  : "Tabela",
  502  : "Cancelar",

  601  : "Mais Cores...",

  701  : "Inserir imagem local",
  702  : "Inserir imagem  da web",
  703  : "Inserir imagem do servidor",
  704  : "Enviar imagem ao servidor",

  801  : "Inserir linha acima",
  802  : "Inserir linha abaixo",
  803  : "Excluir linha",
  804  : "Inserir coluna � esquerda",
  805  : "Inserir coluna � direita",
  806  : "Deletar coluna",
  807  : "Delete c�lula",
  808  : "Mesclar c�lulas",
  809  : "Dividir c�lulas",
  810  : "Propriedades da  c�lula...",
  811  : "Propriedades da Tabela...",
  812  : "Propriedades da P�gina...",
  813  : "Propriedades da Imagem...",
  814  : "Definir como primeiro plano",
  815  : "Propriedades do Container...",
  816  : "Propriedades da Sub P�gina...",
  817  : "Propriedades do Label...",

  900  : "Cancelar",
  901  : "Propriedades da Tabela",
  902  : "Porcentagem",
  903  : "Absoluto",
  904  : "Espa�amento da C�lula:",
  905  : "Tamanho da Borda:",
  906  : "Enchimento da C�lula:",
  907  : "Cor da Borda:",
  908  : "Largura:",
  909  : "Cor do Plano de Fundo:",
  911  : "Propriedades da C�lula",
  912  : "Alinhamento Horizontal:",
  913  : "Altura:",
  914  : "Alinhamento Vertical:",
  915  : "Quebra:",
  916  : "Cor da Borda:",
  917  : "Cor do Plano de Fundo:",
  918  : "Largura:",
  921  : "Propriedades da Imagem",
  922  : "Largura:",
  923  : "Alinhamento Vertical:",
  924  : "Altura:",
  925  : "Largura da Borda:",
  926  : "T�tulo:",
  927  : "Cor da Borda:",
  931  : "Propriedades do Label",
  932  : "Width:",
  933  : "Border color:",
  934  : "Height:",
  935  : "Background color:",
  941  : "Container Properties",
  942  : "Largura:",
  943  : "Cor da Borda:",
  944  : "Altura:",
  945  : "Cor de Fundo:",
  951  : "Propriedades da P�gina",
  952  : "Margem:",
  953  : "Cor do Plano de Fundo:",
  961  : "Selecione uma Cor",
  971  : "Propriedades da Sub P�gina",
  972  : "Largura:",
  973  : "Altura:",
  974  : "URL:",
  981  : "Inserir imagem da web",
  982  : "Insira a URL:",
  991  : "Localizar/Substituir",
  992  : "Lozalizar:",
  993  : "Substituir:",
  994  : "Lozalizar",
  995  : "Substituir",
  996  : "Por favor, insira uma palavra !",
  997  : "O documento chegou ao fim ou a palavra n�o foi encontrada.  A busca continuar� no in�cio do documento.",
  998  : "Por favor, insira uma palavra para substituir !",

  1001  : "Abrir/Salvar",
  1002  : "Abrir",
  1003  : "Salvar",
  1004  : "Abrir",
  1005  : "Nome do Arquivo:",
  1006  : "Visualizar:",

  1101  : "Criar/editar link",
  1102  : "Nome do link:",
  1103  : "Propriedades do link",
  1104  : "Remover link",

  1201  : "Propriedades do formul�rio",
  1202  : "Nome do link:",
  1203  : "Propriedades do formul�rio",
  1204  : "Remover formul�rio",

  1301  : "Propriedades do Input",
  1302  : "Propriedades da Select",
  1303  : "Tamanho",
  1304  : "Cor",

  2000  : "Definir como plano de fundo",
  2001  : "Todo",

  // new in 3.0
  // TODO
  119  : "Save as",
  3000  : "Left to right",
  3001  : "Right to left",
  3002  : "Insert Paragraph",
  3003  : "Table highlight",
  3004  : "Hor. Alignment:",
  3005  : "Upload and Insert",
  3006  : "Please select a text range first !",
  3007  : "Name:",
  3008  : "Create text module",
  3009  : "Please enter a valid text module name !",
  3010  : "Upload document",
  3011  : "Upload",
  3012  : "Please select a text first !",
  3013  : "Set RETURN mode",
  3014  : "Select Style Sheet",
  3015  : "Select Format",
  3016  : "Select Font",
  3017  : "Select Fontsize",
  3018  : "File",
  3019  : "Edit",
  3020  : "Insert",
  3021  : "Format",
  3022  : "Forms",
  3023  : "Link properties",
  3024  : "Remove Link",
  3025  : "Path:",
  3026  : "Type:",
  3027  : "Url:",
  3028  : "Target:",
  3029  : "Title:",
  3030  : "Select document",
  3031  : "Input Properties",
  3032  : "Select Properties",
  3033  : "Textarea Properties",

  // new in 3.1
  962  : "ID:",
  963  : "Name:",
  964  : "ID/Name",
  899  : "OK",
  1007  : "Path:",
  3100  : "Make editable",
  3101  : "No color",

  3201  : "Upload",
  3202  : "File Upload",
  3203  : "Server Path:",
  3204  : "File:",

  3210  : "Name",
  3211  : "Size",
  3212  : "Type",
  3213  : "Changed",

  3300 : "Save local",
  3301 : "Set non editable",
  3302 : "Flash properties",
  3303 : "Insert Flash",
  3304 : "Insert Media",
  3305 : "Play:",
  3306 : "Loop:",
  3307 : "Menu:",
  3308 : "Quality:",
  3309 : "Parameter",
  3310 : "Convert Row(td <--> th)",
  3311 : "Please select a file !",
  3312 : "Remove Table",
  3313 : "New with template",
  3314 : "Print Preview",
  3315 : "Remove style/class",
  3316 : "Remove formatting",
  3317 : "Remove empty tags",
  3318 : "Remove Word specific",
  3319 : "Zoom",
  3320 : "Select text snippet",
  3321 : "Source:",
  3322 : "Float:",
  3323 : "Title:",
  3324 : "Action:",
  3325 : "Target:",
  3326 : "Name:",
  3327 : "Form",
  3328 : "Name",
  3329 : "Value:",
  3330 : "The selected text already includes a link !",
  3331 : "No link found !",
  3332 : "Textarea properties",
  3333 : "Alignment",
  3334 : "Span",
  3335 : "Row span:",
  3336 : "Col span:",
  3337 : "Class",
  3338 : "Class:",
  3339 : "Url",
  3340 : "Paste Text",
  3341 : "Source:",
  3342 : "Checked:",
  3343 : "Type:",
  3344 : "Text:",
  3345 : "Link",
  3346 : "Title",
  3347 : "Title:",
  3348 : "Charset",
  3349 : "Charset:",
  3350 : "Description",
  3351 : "Description:",
  3352 : "Author",
  3353 : "Author:",
  3354 : "Keywords",
  3355 : "Keywords:",
  3356 : "Text:",
  3357 : "Color:",
  3358 : "B-Color:",
  3359 : "Add",
  3360 : "Edit",
  3361 : "Delete",
  3362 : "Options",
  3363 : "Spacing",
  3364 : "Border",
  3365 : "Border:",
  3366 : "Border:",
  3367 : "Font",
  3368 : "Family:",
  3369 : "Size:",
  3370 : "Color:",
  3371 : "Type",
  3372 : "Strike through",
  3373 : "Overline",
  3374 : "Caps",
  3375 : "Border",
  3376 : "Full:",
  3377 : "Color",
  3378 : "Backcolor:",
  3379 : "Margin",
  3380 : "Padding",
  3381 : "Alignment",
  3382 : "Spacing",
  3383 : "Letters",
  3384 : "Height:",
  3385 : "Text flow",
  3386 : "Direction:",
  3387 : "Hor./Vert.:",
  3388 : "Display",
  3389 : "Display:",
  3390 : "Visibilty",
  3391 : "Position",
  3392 : "Position:",
  3393 : "Overflow:",
  3394 : "Float:",
  3395 : "Update",
  3396 : "Remove span",
  3397 : "Full size",
  3398 : "Page attributes",
  3399 : "Class",
  3400 : "Style",
  3401 : "Attributes",
  3402 : "Font",
  3403 : "Background",
  3404 : "Border",
  3405 : "Margin",
  3406 : "Text",
  3407 : "Layout",
  3408 : "Backcolor",
  3409 : "Backcolor:",
  3410 : "Column span",
  3411 : "Row span",
  3412 : "Add cell",
  3413 : "Anchor:",

  4000 : "Width:",
  4001 : "Height:",
  4002 : "Left:",
  4003 : "Top:",
  4004 : "Bottom:",
  4005 : "Right:",
  4006 : "Size",
  4007 : "Misc",
  4008 : "Standard",
  4009 : "Alignment",
  4010 : "Vertical:",
  4011 : "Horizontal:",
  4012 : "Value:",

  5000 : ""
  }
  
function getLanguageString(language,id)
{
  return __editLanguage[id];
}
