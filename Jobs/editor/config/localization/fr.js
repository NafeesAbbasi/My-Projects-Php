// FR
var __editLanguage =
  {
  101  : "Nouveau",
  102  : "Ouvrir",
  103  : "Imprimer",
  104  : "Couper",
  105  : "Copier",
  106  : "Coller",
  107  : "Annuler",
  108  : "R�p�ter",
  109  : "Ins�rer un lien",
  110  : "Ins�rer une image",
  111  : "Ins�rer un tableau",
  112  : "Ins�rer une ligne",
  113  : "Rechercher",
  114  : "Aide",
  115  : "Ins�rer un caract�re sp�cial",
  116  : "Ins�rer la date",
  117  : "Ins�rer l'heure",
  118  : "Enregistrer",

  201  : "Gras",
  202  : "Italique",
  203  : "Soulign�",
  204  : "Exposant",
  205  : "Indice",
  206  : "Align� � gauche",
  207  : "Centr�",
  208  : "Align� � droite",
  209  : "Justifier",
  210  : "Num�rotation",
  211  : "Puces",
  212  : "Augmenter le retrait",
  213  : "Diminuer le retrait",
  214  : "Enlever le format",
  215  : "Couleur du texte",
  216  : "Couleur de fond",
  217  : "Choisir la couleur du texte",
  218  : "Choisir la couleur de fond",

  301  : "Ins�rer une �tiquette",
  302  : "Ins�rer un bouton",
  303  : "Ins�rer un champ texte",
  304  : "Ins�rer une case � cocher",
  305  : "Ins�rer un bouton radio",
  306  : "Ins�rer une boite de s�lection",
  307  : "Ins�rer un menu d�roulant",
  308  : "Ins�rer une zone de texte",
  309  : "Ins�rer une sous-page",
  310  : "Ins�rer un conteneur",
  311  : "Position absolue",
  312  : "Ins�rer un module texte",
  313  : "Nouveau module texte",
  314  : "Enlever le module texte",

  401  : "Edition",
  402  : "HTML",
  403  : "Pr�visualisation",
  404  : "Saut de page",
  405  : "Tout s�lectionner",
  406  : "Ajouter un formulaire",
  407  : "Ins�rer une Ancre",
  408  : "Coller depuis Word",
  409  : "D�filement",
  410  : "Ins�rer un champ cach�",
  411  : "Correcteur orthographique",

  501  : " Tableau",
  502  : "Annuler",

  601  : "Autres couleurs...",

  701  : "Ins�rer une image locale",
  702  : "Ins�rer une image web",
  703  : "Ins�rer une image du serveur",
  704  : "T�l�charger une image locale",

  801  : "Ins�rer une ligne avant",
  802  : "Ins�rer une ligne apr�s",
  803  : "Effacer la ligne",
  804  : "Ins�rer une colonne avant",
  805  : "Ins�rer une colonne apr�s",
  806  : "Effacer la colonne",
  807  : "Effacer la cellule",
  808  : "Marge de la cellule",
  809  : "Diviser la cellule",
  810  : "Propri�t�s de la cellule...",
  811  : "Propri�t�s du tableau...",
  812  : "Propri�t�s de la page...",
  813  : "Propri�t�s de l'image...",
  814  : "Placer en avant-plan",
  815  : "Propri�t�s du conteneur...",
  816  : "Propri�t�s de la sous-page...",
  817  : "Propri�t�s de l'�tiquette...",

  900  : "Annuler",
  901  : "Propri�t�s du tableau",
  902  : "Pourcent",
  903  : "Absolu",
  904  : "Espacement externe de la cellule:",
  905  : "Largeur de la bordure:",
  906  : "Espacement interne de la cellule:",
  907  : "Couleur de la bordure:",
  908  : "Largeur:",
  909  : "Couleur de fond:",
  911  : "Propri�t�s de la cellule",
  912  : "Alignement horizontal:",
  913  : "Hauteur:",
  914  : "Alignement vertical:",
  915  : "Saut de ligne:",
  916  : "Couleur de la bordure:",
  917  : "Couleur de fond:",
  918  : "Largeur:",
  921  : "Propri�t� de l'image",
  922  : "Largeur:",
  923  : "Alignement vertical:",
  924  : "Hauteur:",
  925  : "Largeur de la bordure:",
  926  : "Titre:",
  927  : "Couleur de la bordure:",
  931  : "Propri�t� de l'�tiquette",
  932  : "Largeur:",
  933  : "Couleur de la bordure:",
  934  : "Hauteur:",
  935  : "Couleur de fond:",
  941  : "Propri�t�s du conteneur",
  942  : "Largeur:",
  943  : "Couleur de la bordure:",
  944  : "Hauteur:",
  945  : "Couleur de fond:",
  951  : "Propri�t� de la page",
  952  : "Marge:",
  953  : "Couleur de fond:",
  961  : "Choisissez la couleur",
  971  : "Propri�t� de la sous-page",
  972  : "Largeur:",
  973  : "Hauteur:",
  974  : "URL:",
  981  : "Ins�rer une image WEB",
  982  : "Entrer l'URL:",
  991  : "Chercher/Remplacer",
  992  : "Chercher:",
  993  : "Remplacer:",
  994  : "Chercher",
  995  : "Remplacer",
  996  : "Entrez un mot-clef !",
  997  : "La fin du document est atteinte ou le mot-clef n'a pas �t� trouv�. La recherche continue au d�but du document !",
  998  : "Entrez une expression de remplacement!",

  1001  : "Ouvrier/Enregistrer",
  1002  : "Ouvrir",
  1003  : "Enregistrer",
  1004  : "Ouvrir",
  1005  : "Nom du fichier:",
  1006  : "Pr�visualisation:",

  1101  : "Cr�er/Editer ancre",
  1102  : "Nom de l'ancre:",
  1103  : "Propri�t� de l'ancre",
  1104  : "Enlever l'ancre",

  1201  : "Propri�t�s du formulaire",
  1202  : "Nom de l'ancre:",
  1203  : "Propri�t�s du formulaire",
  1204  : "Enlever le formulaire",

  1301  : "Propri�t�s de la saisie",
  1302  : "Propri�t�s du menu",
  1303  : "Taille",
  1304  : "Couleur",

  2000  : "Placer en arri�re-plan",
  2001  : "Style",

  119  : "Enregistrer sous",
  3000  : "De gauche � droite",
  3001  : "De droite � gauche",
  3002  : "Ins�rer un paragraphe",
  3003  : "Faire ressortir la table",
  3004  : "Alignement horizontal:",
  3005  : "T�l�charger et ins�rer",
  3006  : "S�lectionnez d'abord une partie de texte !",
  3007  : "Nom:",
  3008  : "Cr�er un module de texte",
  3009  : "Entrez un nom de module de texte valide !",
  3010  : "T�l�charger document",
  3011  : "T�l�charger",
  3012  : "S�lectionner d'abord un texte !",
  3013  : "Choisir le mode RETOUR LIGNE",
  3014  : "S�lectionner la feuille de style",
  3015  : "S�lectionner le format",
  3016  : "S�lectionner la police",
  3017  : "S�lectionner la taille de la police",
  3018  : "Fichier",
  3019  : "Edition",
  3020  : "Insertion",
  3021  : "Format",
  3022  : "Formulaires",
  3023  : "Propri�t�s du lien",
  3024  : "Enlever le lien",
  3025  : "Chemin d'acc�s:",
  3026  : "Type:",
  3027  : "URL:",
  3028  : "Cible:",
  3029  : "Titre:",
  3030  : "S�lectionner le document",
  3031  : "Propri�t�s de la case de saisie",
  3032  : "Propri�t�s du menu d�roulant",
  3033  : "Propri�t�s du champ texte",

  962  : "ID:",
  963  : "Nom:",
  964  : "ID/Nom",
  899  : "OK",
  1007  : "Chemin:",
  3100  : "Rendre �ditable",
  3101  : "Pas de couleur",

  3201  : "T�l�charger",
  3202  : "T�l�charger le fichier",
  3203  : "Chemin du serveur:",
  3204  : "Fichier:",

  3210  : "Nom",
  3211  : "Taille",
  3212  : "Type",
  3213  : "Modifi�",

  3300  : "Sauvegarder localement",
  3301  : "Partie non �ditable",
  3302  : "Propri�t�s fichier Flash",
  3303  : "Ins�rer fichier Flash",
  3304  : "Ins�rer fichier Media",
  3305  : "Lecture:",
  3306  : "Boucle:",
  3307  : "Menu:",
  3308  : "Qualit�:",
  3309  : "Param�tre",
  3310  : "Convertir ligne(td &lt,--&gt, th)",
  3311  : "S�lectionnez un fichier, svp !",
  3312  : "Enlever tableau",
  3313  : "Nouveau avec calibrage",
  3314  : "Imprimer aper�u",
  3315  : "Enlever style/class",
  3316  : "Enlever le formattage",
  3317  : "Enlever les balises vides",
  3318  : "Enlever le balisage Word",
  3319  : "Zoom",
  3320  : "S�lectionnez un module texte",

  3321 : "Source:",
  3322 : "Float:",
  3323 : "Title:",
  3324 : "Action:",
  3325 : "Target:",
  3326 : "Name:",
  3327 : "Form",
  3328 : "Name",
  3329 : "Value:",
  3330 : "The selected text already includes a link !",
  3331 : "No link found !",
  3332 : "Textarea properties",
  3333 : "Alignment",
  3334 : "Span",
  3335 : "Row span:",
  3336 : "Col span:",
  3337 : "Class",
  3338 : "Class:",
  3339 : "Url",
  3340 : "Paste Text",
  3341 : "Source:",
  3342 : "Checked:",
  3343 : "Type:",
  3344 : "Text:",
  3345 : "Link",
  3346 : "Title",
  3347 : "Title:",
  3348 : "Charset",
  3349 : "Charset:",
  3350 : "Description",
  3351 : "Description:",
  3352 : "Author",
  3353 : "Author:",
  3354 : "Keywords",
  3355 : "Keywords:",
  3356 : "Text:",
  3357 : "Color:",
  3358 : "B-Color:",
  3359 : "Add",
  3360 : "Edit",
  3361 : "Delete",
  3362 : "Options",
  3363 : "Spacing",
  3364 : "Border",
  3365 : "Border:",
  3366 : "Border:",
  3367 : "Font",
  3368 : "Family:",
  3369 : "Size:",
  3370 : "Color:",
  3371 : "Type",
  3372 : "Strike through",
  3373 : "Overline",
  3374 : "Caps",
  3375 : "Border",
  3376 : "Full:",
  3377 : "Color",
  3378 : "Backcolor:",
  3379 : "Margin",
  3380 : "Padding",
  3381 : "Alignment",
  3382 : "Spacing",
  3383 : "Letters",
  3384 : "Height:",
  3385 : "Text flow",
  3386 : "Direction:",
  3387 : "Hor./Vert.:",
  3388 : "Display",
  3389 : "Display:",
  3390 : "Visibilty",
  3391 : "Position",
  3392 : "Position:",
  3393 : "Overflow:",
  3394 : "Float:",
  3395 : "Update",
  3396 : "Remove span",
  3397 : "Full size",
  3398 : "Page attributes",
  3399 : "Class",
  3400 : "Style",
  3401 : "Attributes",
  3402 : "Font",
  3403 : "Background",
  3404 : "Border",
  3405 : "Margin",
  3406 : "Text",
  3407 : "Layout",
  3408 : "Backcolor",
  3409 : "Backcolor:",
  3410 : "Column span",
  3411 : "Row span",
  3412 : "Add cell",
  3413 : "Anchor:",

  4000 : "Width:",
  4001 : "Height:",
  4002 : "Left:",
  4003 : "Top:",
  4004 : "Bottom:",
  4005 : "Right:",
  4006 : "Size",
  4007 : "Misc",
  4008 : "Standard",
  4009 : "Alignment",
  4010 : "Vertical:",
  4011 : "Horizontal:",
  4012 : "Value:",

  5000 : ""
}
  
function getLanguageString(language,id)
{
  return __editLanguage[id];
}
