//------------------------------------------------------------------------------------------------------
// configuration file for pinEdit context menu creation
//------------------------------------------------------------------------------------------------------

// creates the context menu
function CreateContextMenu(tag,objMenu,userCode)
{
  var url = __editGetEditorUrl() + "design/image/" + design + "/";
  var activeObject = null;
  try {
    activeObject = editGetActiveObject().className;
  } catch(Error) {}
  // is there a class ?
  var removeClass = activeObject ? true:false;

  try {  
    // check for link in hierarchy
    var isLink = false;
    if(tag != "A") {
      var parent = editGetActiveObject().parentNode;
      while(parent) {
        if(parent.tagName.toLowerCase() == "a") {
          isLink = true;
          break;
        }
        parent = parent.parentNode;
      }
    }  
  } catch(Error) {}
  
  if(browser.ie) {
    // add cut,copy,paste and detect if something is selected
    if(editHasSelection()) {
      objMenu.add(new MenuItem(getLanguageString(language,104),url + "cut.gif","","CUT"));
    } else {
      var item = new MenuItem(getLanguageString(language,104),url + "cut.gif","","CUT");
      item.enabled = false;
      objMenu.add(item);
    }
    if(editHasSelection()) {
      objMenu.add(new MenuItem(getLanguageString(language,105),url + "copy.gif","","COPY"));
    } else {
      var item = new MenuItem(getLanguageString(language,105),url + "copy.gif","","COPY");
      item.enabled = false;
      objMenu.add(item);
    }
    objMenu.add(new MenuItem(getLanguageString(language,106),url + "paste.gif","","PASTE"));
    objMenu.add(new MenuSeparator());
  }
    
  if(tag == "BODY") {
    objMenu.add(new MenuItem(getLanguageString(language,812),url + "properties.gif","","BODY"));
  }
  if(tag == "IMAGE") {
    objMenu.add(new MenuItem(getLanguageString(language,814) ,url + "foreground.gif","","FOREGROUND"));
    objMenu.add(new MenuItem(getLanguageString(language,2000) ,url + "background.gif","","BACKGROUND"));
    objMenu.add(new MenuSeparator());
    objMenu.add(new MenuItem(getLanguageString(language,813),url + "properties.gif","","IMAGE"));
    if(isLink) {
      objMenu.add(new MenuSeparator());
      objMenu.add(new MenuItem(getLanguageString(language,3024),url + "link.gif","","REMOVELINK"));
      objMenu.add(new MenuItem(getLanguageString(language,3023),url + "properties.gif","","LINK"));
    }
  }
  if(tag == "SPAN") {
    objMenu.add(new MenuItem(getLanguageString(language,814) ,url + "foreground.gif","","FOREGROUND"));
    objMenu.add(new MenuItem(getLanguageString(language,2000) ,url + "background.gif","","BACKGROUND"));
    if(removeClass) {
      objMenu.add(new MenuSeparator());
      objMenu.add(new MenuItem(getLanguageString(language,3418) ,"","","REMOVECLASS"));
    }
    objMenu.add(new MenuSeparator());
    objMenu.add(new MenuItem(getLanguageString(language,815),url + "properties.gif","","DIV"));
    if(isLink) {
      objMenu.add(new MenuSeparator());
      objMenu.add(new MenuItem(getLanguageString(language,3024),url + "link.gif","","REMOVELINK"));
      objMenu.add(new MenuItem(getLanguageString(language,3023),url + "properties.gif","","LINK"));
    }
  }
  if(tag == "DIV") {
    objMenu.add(new MenuItem(getLanguageString(language,814) ,url + "foreground.gif","","FOREGROUND"));
    objMenu.add(new MenuItem(getLanguageString(language,2000) ,url + "background.gif","","BACKGROUND"));
    if(removeClass) {
      objMenu.add(new MenuSeparator());
      objMenu.add(new MenuItem(getLanguageString(language,3418) ,"","","REMOVECLASS"));
    }
    objMenu.add(new MenuSeparator());
    objMenu.add(new MenuItem(getLanguageString(language,815),url + "properties.gif","","DIV"));
    if(isLink) {
      objMenu.add(new MenuSeparator());
      objMenu.add(new MenuItem(getLanguageString(language,3024),url + "link.gif","","REMOVELINK"));
      objMenu.add(new MenuItem(getLanguageString(language,3023),url + "properties.gif","","LINK"));
    }
  }
  if(tag == "IFRAME") {
    objMenu.add(new MenuItem(getLanguageString(language,814) ,url + "foreground.gif","","FOREGROUND"));
    objMenu.add(new MenuItem(getLanguageString(language,2000) ,url + "background.gif","","BACKGROUND"));
    objMenu.add(new MenuSeparator());
    objMenu.add(new MenuItem(getLanguageString(language,816),url + "properties.gif","","IFRAME"));
  }

  if(tag == "P") {
    objMenu.add(new MenuItem(getLanguageString(language,814) ,url + "foreground.gif","","FOREGROUND"));
    objMenu.add(new MenuItem(getLanguageString(language,2000) ,url + "background.gif","","BACKGROUND"));
    if(removeClass) {
      objMenu.add(new MenuSeparator());
      objMenu.add(new MenuItem(getLanguageString(language,3418) ,"","","REMOVECLASS"));
    }
    objMenu.add(new MenuSeparator());
    objMenu.add(new MenuItem(getLanguageString(language,817),url + "properties.gif","","P"));
    if(isLink) {
      objMenu.add(new MenuSeparator());
      objMenu.add(new MenuItem(getLanguageString(language,3024),url + "link.gif","","REMOVELINK"));
      objMenu.add(new MenuItem(getLanguageString(language,3023),url + "properties.gif","","LINK"));
    }
  }

  if(tag == "ANCHOR") {
    objMenu.add(new MenuItem(getLanguageString(language,1104) ,url + "remove.gif","","REMOVEANCHOR"));
    objMenu.add(new MenuSeparator());
    objMenu.add(new MenuItem(getLanguageString(language,1103),url + "properties.gif","","ANCHOR"));
  }
  if(tag == "FORM") {
    objMenu.add(new MenuItem(getLanguageString(language,1204) ,url + "remove.gif","","REMOVEFORM"));
    if(removeClass) {
      objMenu.add(new MenuSeparator());
      objMenu.add(new MenuItem(getLanguageString(language,3418) ,"","","REMOVECLASS"));
    }
    objMenu.add(new MenuSeparator());
    objMenu.add(new MenuItem(getLanguageString(language,1203),url + "properties.gif","","FORM"));
  }
  if(tag == "INPUT") {
    objMenu.add(new MenuItem(getLanguageString(language,814) ,url + "foreground.gif","","FOREGROUND"));
    objMenu.add(new MenuItem(getLanguageString(language,2000) ,url + "background.gif","","BACKGROUND"));
    if(removeClass) {
      objMenu.add(new MenuSeparator());
      objMenu.add(new MenuItem(getLanguageString(language,3418) ,"","","REMOVECLASS"));
    }
    objMenu.add(new MenuSeparator());
    objMenu.add(new MenuItem(getLanguageString(language,3031),url + "properties.gif","","INPUT"));
  }
  if(tag == "SELECT") {
    objMenu.add(new MenuItem(getLanguageString(language,814) ,url + "foreground.gif","","FOREGROUND"));
    objMenu.add(new MenuItem(getLanguageString(language,2000) ,url + "background.gif","","BACKGROUND"));
    if(removeClass) {
      objMenu.add(new MenuSeparator());
      objMenu.add(new MenuItem(getLanguageString(language,3418) ,"","","REMOVECLASS"));
    }
    objMenu.add(new MenuSeparator());
    objMenu.add(new MenuItem(getLanguageString(language,3032),url + "properties.gif","","SELECT"));
  }
  if(tag == "TEXTAREA") {
    objMenu.add(new MenuItem(getLanguageString(language,814) ,url + "foreground.gif","","FOREGROUND"));
    objMenu.add(new MenuItem(getLanguageString(language,2000) ,url + "background.gif","","BACKGROUND"));
    if(removeClass) {
      objMenu.add(new MenuSeparator());
      objMenu.add(new MenuItem(getLanguageString(language,3418) ,"","","REMOVECLASS"));
    }
    objMenu.add(new MenuSeparator());
    objMenu.add(new MenuItem(getLanguageString(language,3033),url + "properties.gif","","TEXTAREA"));
  }
  if(tag == "TABLE") {
		if(browser.ns) {
			objMenu.add(new MenuItem(getLanguageString(language,3312) ,url + "remove.gif","","REMOVETABLE"));
			objMenu.add(new MenuSeparator());
		}
    if(removeClass) {
      objMenu.add(new MenuSeparator());
      objMenu.add(new MenuItem(getLanguageString(language,3418) ,"","","REMOVECLASS"));
    }
		objMenu.add(new MenuItem(getLanguageString(language,811),url + "properties.gif","","TABLE"));
  }
  if(tag == "TD") {
    objMenu.add(new MenuItem(getLanguageString(language,801),url + "insertrow.gif","","INSERTROWB"));
    objMenu.add(new MenuItem(getLanguageString(language,802),url + "insertrow.gif","","INSERTROWA"));
    objMenu.add(new MenuItem(getLanguageString(language,803),"","","DELETEROW"));
    objMenu.add(new MenuSeparator());
    objMenu.add(new MenuItem(getLanguageString(language,804),url + "insertcol.gif","","INSERTCOLB"));
    objMenu.add(new MenuItem(getLanguageString(language,805),url + "insertcol.gif","","INSERTCOLA"));
    objMenu.add(new MenuItem(getLanguageString(language,806),"","","DELETECOL"));
    objMenu.add(new MenuSeparator());
    objMenu.add(new MenuItem(getLanguageString(language,807),"","","DELETECELL"));
    objMenu.add(new MenuItem(getLanguageString(language,3412),"","","ADDCELL"));
    objMenu.add(new MenuSeparator());
    objMenu.add(new MenuItem(getLanguageString(language,3410),"","","COLSPAN"));
    objMenu.add(new MenuItem(getLanguageString(language,809),"","","DIVIDECELL"));
    objMenu.add(new MenuSeparator());
    objMenu.add(new MenuItem(getLanguageString(language,3411),"","","ROWSPAN"));
    objMenu.add(new MenuSeparator());
    objMenu.add(new MenuItem(getLanguageString(language,3310),"","","CONVERTROW"));
    objMenu.add(new MenuSeparator());
    if(removeClass) {
      objMenu.add(new MenuSeparator());
      objMenu.add(new MenuItem(getLanguageString(language,3418) ,"","","REMOVECLASS"));
    }
    objMenu.add(new MenuItem(getLanguageString(language,810),url + "properties.gif","","TD"));
    objMenu.add(new MenuSeparator());
    objMenu.add(new MenuItem(getLanguageString(language,811),url + "properties.gif","","TDTABLE"));
  }
  if(tag == "A") {
    objMenu.add(new MenuItem(getLanguageString(language,3024),url + "link.gif","","REMOVELINK"));
    objMenu.add(new MenuSeparator());
    objMenu.add(new MenuItem(getLanguageString(language,3023),url + "properties.gif","","LINK"));
  }
  if(tag == "FLASH") {
    objMenu.add(new MenuItem(getLanguageString(language,3302),url + "properties.gif","","FLASH"));
  }
}

// after item click
function onContextMenuItemClick(key)
{
  if(key == "FOREGROUND")
    editSetForeground();
  if(key == "BACKGROUND")
    editSetBackground();

  if(key == "BODY")
    editProperties(4);
  if(key == "IMAGE")
    editProperties(3);
  if(key == "DIV")
    editProperties(5);
  if(key == "IFRAME")
    editProperties(6);
  if(key == "P")
    editProperties(7);
  if(key == "REMOVEANCHOR")
    __editRemoveAnchor();
  if(key == "ANCHOR")
    editProperties(8);
  if(key == "REMOVEFORM")
    __editRemoveForm();
  if(key == "FORM")
    editProperties(9);
  if(key == "INPUT")
    editProperties(10);
  if(key == "SELECT")
    editProperties(11);
  if(key == "TEXTAREA")
    editProperties(13);
  if(key == "TABLE")
    editProperties(0);
  if(key == "TDTABLE") {
    var curObject = editGetActiveObject();
    var parent = curObject;
    while(parent.tagName.toUpperCase() != "TABLE") {
      parent = parent.parentNode;
    }
    editSetActiveObject(parent);
    editProperties(0);
  }
  if(key == "REMOVETABLE")
    __editRemoveTable();

  if(key == "INSERTROWB")
    __editInsertRow(0);
  if(key == "INSERTROWA")
    __editInsertRow(1);
  if(key == "DELETEROW")
    __editDeleteRow();
  if(key == "INSERTCOLB")
    __editInsertColumn(0);
  if(key == "INSERTCOLA")
    __editInsertColumn(1);
  if(key == "DELETECOL")
    __editDeleteColumn();
  if(key == "DELETECELL")
    __editDeleteCell();
  if(key == "ADDCELL")
    __editAddCell();
  if(key == "COLSPAN")
    __editColSpan();
  if(key == "DIVIDECELL")
    __editDivideCell();
  if(key == "ROWSPAN")
    __editRowSpan();
  if(key == "CONVERTROW")
		__editConvertRow();
  if(key == "TD")
    editProperties(2);

  if(key == "EDITABLE")
    editSetEditable(true);
  if(key == "FLASH")
    editProperties(12);
  if(key == "LINK")
    editLink();
  if(key == "REMOVELINK")
    editRemoveLink();

  if(key == "CUT")
    editCut();
  if(key == "COPY")
    editCopy();
  if(key == "PASTE")
    editPaste();

  if(key == "REMOVECLASS")
    editSetStyle("STANDARD");

	__editHidePopup();
}


