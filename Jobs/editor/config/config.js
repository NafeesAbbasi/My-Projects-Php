//----------------------------------------------------------------------------------------
// pinEdit global settings
//----------------------------------------------------------------------------------------

// select design
var design = "Office2003";      // ""                   Standard like Office 2000
                                // "Office"             Like Visual Studio
                                // "Office2003"         Like Office 2003
                                // "Office2003S"        Like Office 2003 silver
                                // This string corresponds with the last characters of the
                                // stylesheet rules in editor/design/style/pinEdit.css

// design of the tabs
var globalTabDesign = "1";      // 1 = Windows XP


// set language
var language = "EN";            // supported
                                // EN = english
                                // DE = german
                                // ES = spanish
                                // FR = french
                                // PT = Portugese
                                // GR = greek
                                // NL = dutch

// select technology for dialog functions like open/save dialog
var globalTechnology   = "php"; // supported
                                // asp - use server side asp technology
                                // jsp - use server side jsp technology
                                // php - use server side PHP technology

//--------------------------------------------------------------------------------------------------------
// COMPATIBILITY 3.5
//--------------------------------------------------------------------------------------------------------
var globalCompatibilityMode = false;

//--------------------------------------------------------------------------------------------------------
// COMMON URL
//--------------------------------------------------------------------------------------------------------
// URL of root (context). Can be http://www.mydomain.com/
// Must end with /
var globalRootUrl  = "";

// physical path of root (context)
var globalRootPath = PATH+"/admin/editor/";

// URL of editor folder: Must be in the same domain as the files that are opened per URL
// Must end with /
// If not specified the editor folder must be in web application root
var globalEditorUrl = WWWROOT+"/admin/editor/";


//--------------------------------------------------------------------------------------------------------
// VIEWS
//--------------------------------------------------------------------------------------------------------
// menu
var globalMenuVisible = false;

// hierarchy bar parameter
var globalHierarchyBarVisible = false;
var globalHierarchyBarSelectionColor = "#FFFFA0";
var globalHierarchyBarHighlight = true;

// attribute area
var globalAttributeAreaVisible = false;
var globalAttributeAreaUrl = "dialogs/attribute.html";
var globalAttributeAreaWidth = "230px";

// document view
var globalDocumentView = false;


//--------------------------------------------------------------------------------------------------------
// ABSOLUTE URL/PATH
//--------------------------------------------------------------------------------------------------------
// specifies the location of the image folder anywhere on the server
// the folder must be accessable by a valid URL
// these settings are also used for image upload
var globalImageUrlAbsolute     = WWWROOT+"/documentSetting/emailer/";
var globalImagePathAbsolute    = PATH+"/documentSetting/emailer/";

// specifies the location of the document folder anywhere on the server
// the folder must be accessable by a valid URL
// these settings are also used for doc upload
var globalDocumentUrlAbsolute     = WWWROOT+"/admin/editor/"; 
var globalDocumentPathAbsolute    = PATH+"/admin/editor/"; 

// specifies the location of the template folder anywhere on the server
// the folder must be accessable by a valid URL
var globalTemplateUrlAbsolute     = WWWROOT+"/admin/editor/";
var globalTemplatePathAbsolute    = PATH+"/admin/editor/";


//--------------------------------------------------------------------------------------------------------
// RELATIVE IMAGES/LINKS
//--------------------------------------------------------------------------------------------------------
// if set, the function editGetHtmlRel()/editGetHtmlBodyRel() returns content with all images/href path relative to given path
// ie. http://www.myweb.com/mysite/
// needed for globalSaveMode=2 or 3
var globalImagePathRelative = ""; 
var globalLinkPathRelative  = ""; 

// set the document directory within the web root(this directory is displayed when you open documents via dialog)
// don't use a / at the begining or end
var globalDocDir       = "data/files";

// set the image directory within the web root(this directory is displayed when you open images via dialog)
// don't use a / at the begining or end
var globalImageDir     = "data/images";


//--------------------------------------------------------------------------------------------------------
// ADAPTERS
//--------------------------------------------------------------------------------------------------------
// set target file, where html content is posted
// enter here your file with absolute or relative path
// in relativ mode, the location must be relativ to 'editor' directory
// Java: if you want to use the servlet and the java adapter
//var target = "../servlet/Edit";
// if you want to use a jsp file

//var target = "adapter/saveadapter.php";
var target = PATH+"/modules/emailer/design.php";

// set target file, that is called to load or save text modules
// enter here your file with absolute or relative path
// in relativ mode, the location must be relativ to 'editor' directory
// implemented only for Java and .Net
var targetTextModule = "adapter/textmoduleadapter.php";


// set target file, that is called to get intellisense data
// enter here your file with absolute or relative path
// in relativ mode, the location must be relativ to 'editor' directory
// implemented only for Java and .Net
var targetIntellisense = "adapter/intellisenseadapter.php";


//--------------------------------------------------------------------------------------------------------
// SAVE
//--------------------------------------------------------------------------------------------------------
// defines the save mode
// 0 = Absolute All HTML
// 1 = Absolute Body HTML
// 2 = Relative All HTML
// 3 = Relative Body HTML
// 4 = XHTML
// 5 = XHtml Body
var globalSaveMode = 0;

// this content is passed in variable "custom" to the save adapter
var globalSaveValue = "";



//--------------------------------------------------------------------------------------------------------
// MISC
//--------------------------------------------------------------------------------------------------------
// defines, what is visible in HTML mode
var globalHTMLMode = "1";
                      // 1 = Standard ( editing the html body )
                      // 2 = Extended ( editing the entire document -> head and body)

// defines if after a RETURN a <br> or a paragraph is inserted
var globalIsBR = false; // enter <p>

// Needed for XP SP2 to increase height of dialogs
// For SP2 set this parameter to 15
var globalDialogOffset = 0;

// if set to true, Control-V is disabled and an event occurs in config_event.js
var globalControlVDisabled = false;

// key code for intellisense ("" if disabled)
var globalISKey = "";

// these values are set dynamically during startup dependant of design
// global toolbar background color for all toolbars
var globalToolbarColor = "ButtonFace";

// global toolbar border
var globalToolbarBorder = true;

// defines the popup width
// please increase this value when the context menu strings are too large
var globalPopupWidth = 200;

// colors for syntax highlighting
var globalTagColor       = "#0000ff";
var globalTagNameColor   = "#800000";
var globalAttributeColor = "red";
var globalStyleColor     = "#0000ff";

// if true, the language is automatically detected
var globalLanguageDetect = false;

// the extended mode has syntax coloring
var globalHTMLModeExtended = true;

// the toolbar status is updated when a key is pressed
// for very slow systems set this value to false
var globalUpdateToolbarWithKey = true;

// highligh color for table highlighting
var globalHighlightColor = "#FF0000";

// if tb parameter is used (Toolbar auto config)
var globalToolbarHeight      = 27;

// sets focus to editor after editWrite
var globalAutoFocus          = true;

// the doc type for XHTML
var globalXHtmlDocType = "<?xml version=\"1.0\"?><!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">";

// filter (file extensions) for dialogs for mode OPEN, IMAGE, LINK
var globalDialogFilterDoc   = "html;htm;txt;php;";
var globalDialogFilterImage = "gif;jpeg;jpg;png;";
var globalDialogFilterLink  = "html;htm;txt;doc;pdf;"; 

// if color is set then an non editable region is colored
var globalEditableColor = "#c0c0c0";

// please set the license key code here
var globalSpellLicenseKey = "";
// defines the spell checker page ( relative to editor folder)
// for ASP.Net/ASP it should be spell.aspx
// for JSP it should be spell.jsp
// for PHP it depends if ASP.Net or Java is used
var globalSpellPage = "spell/spell.jsp";
// defines the absolute physical path to the spell checkers dictionary file (.dict)
var globalSpellDictPath = "";
// defines the absolute physical path to the spell checkers user dictionary file (Word custom)
var globalSpellUserDictPath = "";
// creates pretty format in HTML view
var globalPrettyFormat = true;

// the first row of the inserted table is a <hr>
var globalFirstTableRowIsHeader = true;

// defines if pinToolbar is used or not
var globalSingleMode = false;

// anchor is displayed as symbol
var globalDisplaySymbolAnchor = true;

// TBODY tag is displayed
var globalShowTBodyTag = true;

// In HTML area the lines are not wrapped when longer than editor window
// IE only
var globalHTMLAreaWrapLine = false;

// an anchor symbol is displayed when an anchor is available
var globalDisplaySymbolAnchor = true;

// IE only: lines are wrapped at the editors right margin
var globalHTMLAreaWrapLine = false;

// the default name of editors page. 
//The file name can be renamed to pinEdit.jsp or pinEdit.php when this parameter is changed, too
var globalEditorPageName = "pinEdit.php";

// debug flag
var globalDebug = true;

//--------------------------------------------------------------------------------------------------------
// pinEditSpell
//--------------------------------------------------------------------------------------------------------
// please set the license key code here
var globalSpellLicenseKey = "";
// defines the spell checker page ( relative to editor folder)
// for ASP.Net/ASP it should be spell.aspx
// for JSP it should be spell.jsp
// for PHP it depends if ASP.Net or Java is used
var globalSpellPage = "add-on/spell/spell.jsp";
// defines the absolute physical path to the spell checkers dictionary file (.dict)
var globalSpellDictPath = "";
// defines the absolute physical path to the spell checkers user dictionary file (Word)
var globalSpellUserDictPath = "";



//--------------------------------------------------------------------------------------------------------
// pinEditPDF
//--------------------------------------------------------------------------------------------------------
// this parameters defines the client interface only
// when specifying standard then some of client attributes are not available
// Options: "standard" or "professional" 
var globalPDFVersion = "professional";
// please set the license key code here
var globalPDFLicenseKey = "";
// points to the pdf service page, by default: http://localhost/pinEditPDF/pdf.aspx
var globalPDFPage = "http://ip/pinEditPDF/pdf.aspx";
// defines the output path where the pdf files are created
// please use / instead of \
var globalPDFOutputPath = "";
// defines the output url where the pdf files are created
var globalPDFOutputUrl = "";
// define file name. if not defined the file name is generated automatically
var globalPDFFileName = "";// defines paper size
//  1   Letter [8.5 x 11 in]
//  5   Legal [8.5 x 14 in]
//      Executive [7.25 x 10.5 in]
//      Ledger [355.6 x 215.9 mm]
//      Tabloid [11 x 17 in]
//      Screen [10 x 7.5 in]
//  8   A3 [297 x 420 mm]
//  9   A4 [210 x 297 mm]
//      A5 [148 x 210 mm]
//  256 Custom page size
var globalPDFPaperSize = "1";
// Orientation:
// 1  Portrait
// 2  Landscape
var globalPDFOrientation = "1";
// Resolution in dpi
// 75,150,300,600,1200,2400
var globalPDFResolution = "600";
// Document settings
var globalPDFTitle = "";
var globalPDFAuthor = "";
var globalPDFSubject = "";
var globalPDFCreator = "";
var globalPDFKeyWords = "";
// header/footer 
var globalPDFHeader = "";
var globalPDFFooter = "";
// 0 = false
// 1 = true
var globalPDFJPEGCompression = 1;
// unit for all margins and heights
// 1 = inches
// 2 = mm
// 3 = cm
var globalPDFUnit = 1;
// margin
var globalPDFMarginLeft  = 0;
var globalPDFMarginTop   = 0;
var globalPDFMarginRight = 0;
var globalPDFMarginBottom= 0;
var globalPDFHeaderHeight= 0;
var globalPDFFooterHeight= 0;
// misc
var globalPDFConvertForms = 0;
var globalPDFConvertLinks = 0;
var globalPDFOwnerPassword = "";
var globalPDFUserPassword = "";
