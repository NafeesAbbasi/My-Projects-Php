//------------------------------------------------------------------------------------------------------
// configuration file for pinEdit toolbar creation
//------------------------------------------------------------------------------------------------------

// variables for the used combo boxes
var cmbZoom;
var cmbFormat;
var cmbFont;
var cmbFontSize;
var cmbStyle;
var cmbTextModule;

var btnEdit;
var btnHTML;
var btnPreview;

//------------------------------------------------------------------------------------------------
// create toolbars auto
// Config is:
// T means Toolbar Top
// B means Toolbar Bottom
//
// T-1:2:3:4:5:6:7;T-10:11:12;B-45:46
//
// NEW       1
// OPEN      2
// SAVE      3
// SEARCH    4
// PRINT     5
// SPELL     64
// CUT       6
// COPY      7
// PASTE     8
// PASTEWORD 61
// UNDO      9
// REDO      10
// LINK      11
// ANCHOR    60
// IMAGE     12
// TABLE     13
// RULE      14
// CHAR      15
// DATE      16
// TIME      17
// MARQUEE   62
// PAGEBREAK 57
// ZOOM      18
// REMOVE    37
// SELALL    58

// STYLE     19
// FORMAT    56
// FONT      20
// FONTSIZE  21
// BOLD      22
// ITALIC    23
// UNDERLINE 24
// SUPER     25
// SUB       26
// LEFT      27
// CENTER    28
// RIGHT     29
// BLOCK     30
// ORDERED   31
// UNORDERED 32
// INDENT    33
// OUTDENT   34
// COLOR     35
// BACKCOLOR 36

// FORM      59
// LABEL     38
// BUTTON    39
// INPUT     40
// CHECKBOX  41
// RADIO     42
// COMBO     43
// LIST      44
// AREA      45
// HIDDEN    63
// CONTAINER 46
// SUBPAGE   47
// ABSOLUTE  48
// TMLIST    49
// TMADD     50
// TMNEW     51
// TMREMOVE  52
// EDIT      53
// HTML      54
// PREVIEW   55

// SAVEAS    65
// UPLOAD    66
// PARAGRAPH 67
// RETURNMODE68
// HIGHLIGHT 69
// Splash    70
// MEDIA     71
// EDITABLE  72
// SAVELOC   73
// TEMPLATE  74

// next: 70

// HELP      99

//toolbar=
//T01740203657304056466SE06070861SE0910SE1160121314151617625767707172SE99;
//T195620212223242526SE27282930SE31323334SE3536;
//T59383940414243444563SE4647SE48SE49505152SE3768695818;
//B535455

//?toolbar=T01740203657304056466SE06070861SE0910SE1160121314151617625767707172SE99;T195620212223242526SE27282930SE31323334SE3536;T59383940414243444563SE4647SE48SE49505152SE3768695818;B535455
//------------------------------------------------------------------------------------------------
function CreateToolbarsAuto(config,idTop,idBottom)
{
  var objToolbarsTop = null;
  var objToolbarsBottom = null;
  var hasTop = false;
  var hasBottom = false;
  var hasFont = false;
  var hasFontSize = false;
  var hasZoom = false;
  var hasTM = false;
  var hasEdit = false;
  var hasFormat = false;
  var hasButton = false;
  var hasStyle = false;

  var aToolbars = config.split(";");

  for(var i=0;i<aToolbars.length;i++) {
    var where = aToolbars[i].substring(0,1);
    if(where == "T") {
      if(objToolbarsTop == null) {
        objToolbarsTop = new Toolbars(idTop);
        objToolbarsTop.backcolor = globalToolbarColor;
        hasTop = true;
      }
      currentToolbar = objToolbarsTop;
    } else if( where == "B")  {
      if(objToolbarsBottom == null) {
        objToolbarsBottom = new Toolbars(idBottom);
        objToolbarsBottom.backcolor = globalToolbarColor;
        hasBottom = true;
      }
      currentToolbar = objToolbarsBottom;
    } else {
      alert("Unknown toolbar key: " + where);
      return;
    }
    var buttons = aToolbars[i].substring(1,aToolbars[i].length);
    var aItems = new Array(buttons.length/2);
    for(var k=0;k<aItems.length;k++) {
      aItems[k] = buttons.substring(k*2,(k*2) +2);
    }
    var objToolbar = new Toolbar();
    objToolbar.design = design;
    objToolbar.border = globalToolbarBorder;
    try {
      objToolbar.height = globalToolbarHeight;
    } catch(error) {}
    objToolbar.add(new Separator("design/image/" + design + "/tbbegin.gif"));
    hasButton = false;
    for(var j=0;j<aItems.length;j++) {
      if(aItems[j] == "19")
        hasStyle = true;
      if(aItems[j] == "20")
        hasFont = true;
      if(aItems[j] == "21")
        hasFontSize = true;
      if(aItems[j] == "18")
        hasZoom = true;
      if(aItems[j] == "49")
        hasTM = true;
      if(aItems[j] == "53")
        hasEdit = true;
      if(aItems[j] == "56")
        hasFormat = true;
      if(addItem(objToolbar,aItems[j]))
        hasButton = true;
    }
    if(design =="Office2003")
      objToolbar.add(new Separator("design/image/" + design + "/tbend.gif"));
    if(hasButton)
      currentToolbar.add(objToolbar);
  }
  if(hasTop) {
    objToolbarsTop.create();
  } else {
    objToolbarsTop = new Toolbars(idTop);
    objToolbarsTop.hide();
  }

  if(hasBottom) {
    objToolbarsBottom.create();
  } else {
    objToolbarsBottom = new Toolbars(idBottom);
    objToolbarsBottom.hide();
  }

  if(hasEdit){
    btnEdit.setStatus(true);
  }

  if(hasStyle) {
    if(browser.ns) {
      cmbStyle.add("Standard","STANDARD");
    }
  }

  if(hasFormat) {
    if(browser.ie) {
      cmbFormat.add(new ComboItem("Normal","","","NORMAL","Normal"));
      cmbFormat.add(new ComboItem("<h1>Heading 1</h1>","","","<h1>","Heading 1"));
      cmbFormat.add(new ComboItem("<h2>Heading 2</h2>","","","<h2>","Heading 2"));
      cmbFormat.add(new ComboItem("<h3>Heading 3</h3>","","","<h3>","Heading 3"));
      cmbFormat.add(new ComboItem("<h4>Heading 4</h4>","","","<h4>","Heading 4"));
      cmbFormat.add(new ComboItem("<h5>Heading 5</h5>","","","<h5>","Heading 5"));
      cmbFormat.add(new ComboItem("<h6>Heading 6</h6>","","","<h6>","Heading 6"));
      cmbFormat.add(new ComboItem("<address>Address</address>","","","<address>","Address"));
      cmbFormat.add(new ComboItem("<dir>Directory List</dir>","","","<dir>","Directory List"));
      cmbFormat.add(new ComboItem("<pre>Formatted</pre>","","","<pre>","Formatted"));
      cmbFormat.add(new ComboItem("<menu>Menu List</menu>","","","<menu>","Menu List"));
      editSetFormatObject(cmbFormat);
    } else {
      cmbFormat.add("Normal","NORMAL");
      cmbFormat.add("Heading 1","<h1>");
      cmbFormat.add("Heading 2","<h2>");
      cmbFormat.add("Heading 3","<h3>");
      cmbFormat.add("Heading 4","<h4>");
      cmbFormat.add("Heading 5","<h5>");
      cmbFormat.add("Heading 6","<h6>");
      cmbFormat.add("Address","<address>");
      cmbFormat.add("Directory List","<dir>");
      cmbFormat.add("Formatted","<pre>");
      cmbFormat.add("Menu List","<menu>");    
    }
  }

  if(hasFont) {
    if(browser.ie) {
      cmbFont.add(new ComboItem("<FONT face='Arial'>Arial</font>","","","Arial"));
      cmbFont.add(new ComboItem("<FONT face='Courier'>Courier</font>","","","Courier"));
      cmbFont.add(new ComboItem("<FONT face='Courier New'>Courier New</font>","","","Courier New"));
      cmbFont.add(new ComboItem("<FONT face='Tahoma'>Tahoma</font>","","","Tahoma"));
      cmbFont.add(new ComboItem("<FONT face='Times New Roman'>Times New Roman</font>","","","Times New Roman"));
      cmbFont.add(new ComboItem("<FONT face='Verdana'>Verdana</font>","","","Verdana"));
    } else {
      cmbFont.add("Arial","ARIAL");
      cmbFont.add("Courier","COURIER");
      cmbFont.add("Courier New","COURIER NEW");
      cmbFont.add("Tahoma","TAHOMA");
      cmbFont.add("Times New Roman","TIMES NEW ROMAN");
      cmbFont.add("Verdana","VERDANA");    
    }
  }
  if(hasFontSize) {
    if(browser.ie) {
      cmbFontSize.add(new ComboItem("<FONT size=1>8</FONT>","","","1","8"));
      cmbFontSize.add(new ComboItem("<FONT size=2>10</FONT>","","","2","10"));
      cmbFontSize.add(new ComboItem("<FONT size=3>12</FONT>","","","3","12"));
      cmbFontSize.add(new ComboItem("<FONT size=4>14</FONT>","","","4","14"));
      cmbFontSize.add(new ComboItem("<FONT size=5>18</FONT>","","","5","18"));
      cmbFontSize.add(new ComboItem("<FONT size=6>24</FONT>","","","6","24"));
      cmbFontSize.add(new ComboItem("<FONT size=7>36</FONT>","","","7","36"));
    } else {
      cmbFontSize.add("8","1");
      cmbFontSize.add("10","2");
      cmbFontSize.add("12","3");
      cmbFontSize.add("14","4");
      cmbFontSize.add("18","5");
      cmbFontSize.add("24","6");
      cmbFontSize.add("36","7");    
    }
  }

  if(hasZoom) {
    // not available in IE5
    if(!browser.ie5 && !browser.ns) {
      // now add the zoom values
      cmbZoom.add("10%","10%");
      cmbZoom.add("50%","50%");
      cmbZoom.add("100%","100%");
      cmbZoom.add("150%","150%");
      cmbZoom.add("200%","200%");
      cmbZoom.setSelected("100%");
    }
  }

  if(hasTM) {
    // read available text modules from server
    editGetTextModule(cmbTextModule.id);
  }

  return objToolbarsTop;
}

function addItem(objToolbar, item)
{
    if(item == "SE") {
      objToolbar.add(new Separator("design/image/" + design + "/separator.gif"));
      return false;
    }
    if(item == "01") {
      objToolbar.add(new Button("","","design/image/" + design + "/neu.gif","editNew()",getLanguageString(language,101),design,""));
      return true;
    }
    if(item == "02") {
      objToolbar.add(new Button("","","design/image/" + design + "/open.gif","editOpen(1)",getLanguageString(language,102),design,""));
      return true;
    }
    if(item == "03") {
      objToolbar.add(new Button("","","design/image/" + design + "/save.gif","saveCommon()",getLanguageString(language,118),design,""));
      return true;
    }
    if(item == "04") {
      objToolbar.add(new Button("","","design/image/" + design + "/search.gif","editSearch()",getLanguageString(language,113),design,""));
      return true;
    }
    if(item == "05") {
      objToolbar.add(new Button("","","design/image/" + design + "/print.gif","editPrint()",getLanguageString(language,103),design,""));
      return true;
    }
    if(item == "06") {
      objToolbar.add(new Button("","","design/image/" + design + "/cut.gif","editCut()",getLanguageString(language,104),design,""));
      return true;
    }
    if(item == "07") {
      objToolbar.add(new Button("","","design/image/" + design + "/copy.gif","editCopy()",getLanguageString(language,105),design,""));
      return true;
    }
    if(item == "08") {
      objToolbar.add(new Button("","","design/image/" + design + "/paste.gif","editPaste()",getLanguageString(language,106),design,""));
      return true;
    }
    if(item == "09") {
      objToolbar.add(new Button("","","design/image/" + design + "/undo.gif","editUndo()",getLanguageString(language,107),design,""));
      return true;
    }
    if(item == "10") {
      objToolbar.add(new Button("","","design/image/" + design + "/redo.gif","editRedo()",getLanguageString(language,108),design,""));
      return true;
    }
    if(item == "11") {
      objToolbar.add(new Button("","","design/image/" + design + "/link.gif","editLink()",getLanguageString(language,109),design,""));
      return true;
    }
    if(item == "12") {
      var objImage = new MenuButton("menu_image.html","design/image/" + design + "/image.gif","onMenuImageClicked",getLanguageString(language,110),design,"");
      objImage.width = "150";
      if(browser.ns)
        objImage.height = "48";
      else
        objImage.height = "93";
      objToolbar.add(objImage);
      return true;
    }
    if(item == "13") {
      objToolbar.add(new TableButton("design/image/" + design + "/table.gif","onCreateTable",getLanguageString(language,111),design,""));
      return true;
    }
    if(item == "14") {
      objToolbar.add(new Button("","","design/image/" + design + "/rule.gif","editInsertObject('RULE')",getLanguageString(language,112),design,""));
      return true;
    }
    if(item == "15") {
      var objChar = new MenuButton("char.html","design/image/" + design + "/char.gif","onMenuCharClicked",getLanguageString(language,115),design,"");
      objChar.width = "78";
      objChar.height = "90";
      objToolbar.add(objChar);
      return true;
    }
    if(item == "16") {
      objToolbar.add(new Button("","","design/image/" + design + "/date.gif","editInsertDate()",getLanguageString(language,116),design,""));
      return true;
    }
    if(item == "17") {
      objToolbar.add(new Button("","","design/image/" + design + "/time.gif","editInsertTime()",getLanguageString(language,117),design,""));
      return true;
    }
    if(item == "18") {
      if(!browser.ie5 && !browser.ns) {
        cmbZoom = new Combo("changeZoom()");
        objToolbar.add(cmbZoom);
        return true;
      }
    }
    if(item == "99") {
      objToolbar.add(new Button("","","design/image/" + design + "/help.gif","onHelp()",getLanguageString(language,114),design,""));
      return true;
    }

    if(item == "19") {
      if(browser.ie) {
        cmbStyle = new StyleCombo("onStyleChanged",getLanguageString(language,3014),design,"");
        cmbStyle.width = "100";
        cmbStyle.popupwidth = "200";
        cmbStyle.popupheight = "120";
        cmbStyle.displayValue = "tag1";
        cmbStyle.add(new ComboItem("Standard","","","STANDARD","Standard"));
      } else {
        cmbStyle = new Combo("changeStyle()");
      }
      objToolbar.add(cmbStyle);
      objToolbar.add(new Distance(3));
      // needed for automatic fill
      editSetStyleSheetObject(cmbStyle);
      return true;
    }
    if(item == "56") {
      if(browser.ie) {
        cmbFormat = new StyleCombo("onFormatChanged",getLanguageString(language,3015),design,"");
        cmbFormat.width = "100";
        cmbFormat.popupwidth = "200";
        cmbFormat.popupheight = "130";
        cmbFormat.displayValue = "tag1";
      } else {
        cmbFormat = new Combo("changeFormat()");
      }
      objToolbar.add(cmbFormat);
      objToolbar.add(new Distance(3));
      return true;
    }
    if(item == "20") {
      if(browser.ie) {
        cmbFont = new StyleCombo("onFontChanged",getLanguageString(language,3016),design,"");
        cmbFont.width = "114";
        cmbFont.popupwidth = "120";
        cmbFont.popupheight = "136";
        cmbFont.displayValue = "value";
      } else {
        cmbFont = new Combo("changeFont()");
      }
      objToolbar.add(cmbFont);
      objToolbar.add(new Distance(3));
      return true;
    }
    if(item == "21") {
      if(browser.ie) {
        cmbFontSize = new StyleCombo("onFontSizeChanged",getLanguageString(language,3017),design,"");
        cmbFontSize.width = "35";
        cmbFontSize.popupwidth = "85";
        cmbFontSize.popupheight = "130";
        cmbFontSize.displayValue = "tag1";
      } else {
        cmbFontSize = new Combo("changeFontSize()");
      }
      objToolbar.add(cmbFontSize);
      objToolbar.add(new Distance(3));
      return true;
    }
    if(item == "22") {
      objToolbar.add(new Button("","","design/image/" + design + "/bold.gif","editBold()",getLanguageString(language,201),design,"BOLD"));
      return true;
    }
    if(item == "23") {
      objToolbar.add(new Button("","","design/image/" + design + "/italic.gif","editItalic()",getLanguageString(language,202),design,"ITALIC"));
      return true;
    }
    if(item == "24") {
      objToolbar.add(new Button("","","design/image/" + design + "/underline.gif","editUnderline()",getLanguageString(language,203),design,"UNDERLINE"));
      return true;
    }
    if(item == "25") {
      objToolbar.add(new Button("","","design/image/" + design + "/superscript.gif","editSuperscript()",getLanguageString(language,204),design,"SUPERSCRIPT"));
      return true;
    }
    if(item == "26") {
      objToolbar.add(new Button("","","design/image/" + design + "/subscript.gif","editSubscript()",getLanguageString(language,205),design,"SUBSCRIPT"));
      return true;
    }
    if(item == "27") {
      objToolbar.add(new Button("","","design/image/" + design + "/left.gif","editJustifyLeft()",getLanguageString(language,206),design,"JUSTIFYLEFT"));
      return true;
    }
    if(item == "28") {
      objToolbar.add(new Button("","","design/image/" + design + "/center.gif","editJustifyCenter()",getLanguageString(language,207),design,"JUSTIFYCENTER"));
      return true;
    }
    if(item == "29") {
      objToolbar.add(new Button("","","design/image/" + design + "/right.gif","editJustifyRight()",getLanguageString(language,208),design,"JUSTIFYRIGHT"));
      return true;
    }
    if(item == "30") {
      objToolbar.add(new Button("","","design/image/" + design + "/block.gif","editJustifyFull()",getLanguageString(language,209),design,"JUSTIFYFULL"));
      return true;
    }
    if(item == "31") {
      objToolbar.add(new Button("","","design/image/" + design + "/orderedlist.gif","editOrderedList()",getLanguageString(language,210),design,"INSERTORDEREDLIST"));
      return true;
    }
    if(item == "32") {
      objToolbar.add(new Button("","","design/image/" + design + "/unorderedlist.gif","editUnorderedList()",getLanguageString(language,211),design,"INSERTUNORDEREDLIST"));
      return true;
    }
    if(item == "33") {
      objToolbar.add(new Button("","","design/image/" + design + "/indent.gif","editIndent()",getLanguageString(language,212),design,"INDENT"));
      return true;
    }
    if(item == "34") {
      objToolbar.add(new Button("","","design/image/" + design + "/outdent.gif","editOutdent()",getLanguageString(language,213),design,"OUTDENT"));
      return true;
    }
    if(item == "35") {
      objToolbar.add(new ColorSelector("design/image/" + design + "/color.gif","design/image/colorselect.gif","onSetTextColor",getLanguageString(language,215),getLanguageString(language,217),design,""));
      return true;
    }
    if(item == "36") {
      objToolbar.add(new ColorSelector("design/image/" + design + "/backcolor.gif","design/image/colorselect.gif","onSetBackgroundColor",getLanguageString(language,216),getLanguageString(language,218),design,""));
      return true;
    }
    if(item == "37") {
      var objClean = new MenuButton("menu_clean.html","design/image/" + design + "/remove.gif","onMenuCleanClicked",getLanguageString(language,214),design,"");
      objClean.width = "200";
      objClean.height = "93";
      objToolbar.add(objClean);
      return true;
    }
    if(item == "38") {
      objToolbar.add(new Button("","","design/image/" + design + "/label.gif","editInsertObject('LABEL')",getLanguageString(language,301),design,""));
      return true;
    }
    if(item == "39") {
      objToolbar.add(new Button("","","design/image/" + design + "/button.gif","editInsertObject('BUTTON')",getLanguageString(language,302),design,""));
      return true;
    }
    if(item == "40") {
      objToolbar.add(new Button("","","design/image/" + design + "/input.gif","editInsertObject('INPUT')",getLanguageString(language,303),design,""));
      return true;
    }
    if(item == "41") {
      objToolbar.add(new Button("","","design/image/" + design + "/checkbox.gif","editInsertObject('CHECK')",getLanguageString(language,304),design,""));
      return true;
    }
    if(item == "42") {
      objToolbar.add(new Button("","","design/image/" + design + "/radio.gif","editInsertObject('OPTION')",getLanguageString(language,305),design,""));
      return true;
    }
    if(item == "43") {
      objToolbar.add(new Button("","","design/image/" + design + "/combobox.gif","editInsertObject('COMBO')",getLanguageString(language,306),design,""));
      return true;
    }
    if(item == "44") {
      objToolbar.add(new Button("","","design/image/" + design + "/listbox.gif","editInsertObject('LISTBOX')",getLanguageString(language,307),design,""));
      return true;
    }
    if(item == "45") {
      objToolbar.add(new Button("","","design/image/" + design + "/textarea.gif","editInsertObject('AREA')",getLanguageString(language,308),design,""));
      return true;
    }
    if(item == "46") {
      objToolbar.add(new Button("","","design/image/" + design + "/div.gif","editInsertObject('DIV')",getLanguageString(language,310),design,""));
      return true;
    }
    if(item == "47") {
      objToolbar.add(new Button("","","design/image/" + design + "/iframe.gif","editInsertObject('IFRAME')",getLanguageString(language,309),design,""));
      return true;
    }
    if(item == "48") {
      if(!browser.ie5 && !browser.ns) {
        objToolbar.add(new Button("","","design/image/" + design + "/position.gif","editAbsolute()",getLanguageString(language,311),design,""));
        return true;
      }
    }
    if(item == "49") {
      cmbTextModule = new Combo("");
      objToolbar.add(cmbTextModule);
      return true;
    }
    if(item == "50") {
      objToolbar.add(new Button("","","design/image/" + design + "/tm_add.gif","editInsertTextModule('" + cmbTextModule.id + "')",getLanguageString(language,312),design,""));
      return true;
    }
    if(item == "51") {
      objToolbar.add(new Button("","","design/image/" + design + "/tm_create.gif","editSetTextModule('" + cmbTextModule.id + "')",getLanguageString(language,313),design,""));
      return true;
    }
    if(item == "52") {
      objToolbar.add(new Button("","","design/image/" + design + "/tm_remove.gif","editRemoveTextModule('" + cmbTextModule.id + "')",getLanguageString(language,314),design,""));
      return true;
    }

    if(item == "53") {
      //objToolbar.action = "onButtonClick";
      btnEdit = new Button(getLanguageString(language,401),"","design/image/" + design + "/edit.gif","onButtonClick('EDIT')",getLanguageString(language,401),design,"");
      objToolbar.add(btnEdit);
      return true;
    }
    if(item == "54") {
      btnHTML = new Button(getLanguageString(language,402),"","design/image/" + design + "/html.gif","onButtonClick('HTML')",getLanguageString(language,402),design,"");
      objToolbar.add(btnHTML);
      return true;
    }
    if(item == "55") {
      btnPreview = new Button(getLanguageString(language,403),"","design/image/" + design + "/preview.gif","onButtonClick('PREVIEW')",getLanguageString(language,403),design,"");
      objToolbar.add(btnPreview);
      return true;
    }

    if(item == "57") {
      objToolbar.add(new Button("","","design/image/" + design + "/pagebreak.gif","editInsertObject('PAGEBREAK')",getLanguageString(language,404),design,""));
      return true;
    }
    if(item == "58") {
      objToolbar.add(new Button("","","design/image/" + design + "/selectall.gif","editSelectAll()",getLanguageString(language,405),design,""));
      return true;
    }
    if(item == "59") {
      objToolbar.add(new Button("","","design/image/" + design + "/form.gif","editInsertObject('FORM')",getLanguageString(language,406),design,""));
      return true;
    }
    if(item == "60") {
      objToolbar.add(new Button("","","design/image/" + design + "/anchor.gif","editInsertObject('ANCHOR')",getLanguageString(language,407),design,""));
      return true;
    }
    if(item == "61") {
      objToolbar.add(new Button("","","design/image/" + design + "/pasteword.gif","editPasteWord()",getLanguageString(language,408),design,""));
      return true;
    }
    if(item == "62") {
      objToolbar.add(new Button("","","design/image/" + design + "/marquee.gif","editInsertObject('MARQUEE')",getLanguageString(language,409),design,""));
      return true;
    }
    if(item == "63") {
      objToolbar.add(new Button("","","design/image/" + design + "/hidden.gif","editInsertObject('HIDDEN')",getLanguageString(language,410),design,""));
      return true;
    }
    if(item == "64") {
      objToolbar.add(new Button("","","design/image/" + design + "/spell.gif","__onSpell()",getLanguageString(language,411),design,""));
      return true;
    }

    if(item == "65") {
      objToolbar.add(new Button("","","design/image/" + design + "/saveas.gif","editSaveDialog()",getLanguageString(language,119),design,""));
      return true;
    }
    if(item == "66") {
      var objUpload = new MenuButton("menu_upload.html","design/image/" + design + "/upload.gif","onMenuUploadClicked",getLanguageString(language,3011),design,"");
      objUpload.width = "170";
      objUpload.height = "48";
      objToolbar.add(objUpload);
      return true;
    }
    if(item == "67") {
      objToolbar.add(new Button("","","design/image/" + design + "/paragraph.gif","editParagraph()",getLanguageString(language,3002),design,""));
      return true;
    }
    if(item == "68") {
      // set RETURN mode dependant of settings
      if(editGetBr())
        btnReturnMode = new Button("","","design/image/" + design + "/bron.gif","onReturnMode()",getLanguageString(language,3013),design,"");
      else
        btnReturnMode = new Button("","","design/image/" + design + "/broff.gif","onReturnMode()",getLanguageString(language,3013),design,"");
      objToolbar.add(btnReturnMode);
      return true;
    }
    if(item == "69") {
      objToolbar.add(new Button("","","design/image/" + design + "/tablehigh.gif","editTableHighlight()",getLanguageString(language,3003),design,""));
      return true;
    }
    if(item == "70") {
      objToolbar.add(new Button("","","design/image/" + design + "/flash.gif","editOpen(4)",getLanguageString(language,3303),design,""));
      return true;
    }
    if(item == "71") {
      objToolbar.add(new Button("","","design/image/" + design + "/media.gif","editOpen(5)",getLanguageString(language,3304),design,""));
      return true;
    }
    if(item == "72") {
      objToolbar.add(new Button("","","design/image/" + design + "/edit.gif","editSetEditable(false)",getLanguageString(language,3301),design,""));
      return true;
    }
    if(item == "73") {
      objToolbar.add(new Button("","","design/image/" + design + "/savelocal.gif","editSaveLocal()",getLanguageString(language,3300),design,""));
      return true;
    }
    if(item == "74") {
      objToolbar.add(new Button("","","design/image/" + design + "/template.gif","editOpen(6)",getLanguageString(language,3313),design,""));
      return true;
    }
}

// used also in config_toolbar_auto
function onStyleChanged(text,value,tag1,tag2)
{
  editSetStyle(value);
}

