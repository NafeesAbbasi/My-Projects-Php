//------------------------------------------------------------------------------------------------------
// configuration file for pinEdit toolbar creation
//------------------------------------------------------------------------------------------------------

// variables for the used combo boxes
var cmbZoom;
var cmbFont;
var cmbFontSize;
var cmbStyleSheet;
var cmbFormat;
var btnColor;
var btnBackColor;
var btnEdit;
var btnHTML;
var btnPreview;
var btnReturnMode;

//------------------------------------------------------------------------------------------------
// create the toolbar on top
//------------------------------------------------------------------------------------------------
function CreateToolbarsTop(id, userCode)
{
  // userCode is a string value that allows an individual customizing
  if(userCode == "") {
    // create the main toolbar collection
    var objToolbars = new Toolbars(id);
    // define toolbar settings
    objToolbars.backcolor = globalToolbarColor; // defined in config.js

    //-----------------------------------------------------------------------------------
    // create the main toolbar
    //-----------------------------------------------------------------------------------
    var objToolbar1 = new Toolbar();
    objToolbar1.design = design;
    objToolbar1.border = globalToolbarBorder;
    objToolbar1.height = browser.ns ? 29:27;

    objToolbar1.add(new Separator("design/image/" + design + "/tbbegin.gif"));

    objToolbar1.add(new Button("","","design/image/" + design + "/neu.gif","editNew()",getLanguageString(language,101),design,""));

    objToolbar1.add(new Button("","","design/image/" + design + "/open.gif","editFullSize()",getLanguageString(language,102),design,""));

    objToolbar1.add(new Button("","","design/image/" + design + "/open.gif","editOpen(1)",getLanguageString(language,102),design,""));
    objToolbar1.add(new Button("","","design/image/" + design + "/save.gif","saveCommon()",getLanguageString(language,118),design,""));
    objToolbar1.add(new Button("","","design/image/" + design + "/saveas.gif","editSaveDialog()",getLanguageString(language,119),design,""));
    objToolbar1.add(new Button("","","design/image/" + design + "/savelocal.gif","editSaveLocal()",getLanguageString(language,3300),design,""));

    objToolbar1.add(new Button("","","design/image/" + design + "/search.gif","editSearch()",getLanguageString(language,113),design,""));
    objToolbar1.add(new Button("","","design/image/" + design + "/print.gif","editPrint()",getLanguageString(language,103),design,""));
    if(browser.ns)
      objToolbar1.add(new Button("","","design/image/" + design + "/preview.gif","editPreview()","Print Preview",design,""));
    objToolbar1.add(new Button("","","design/image/" + design + "/spell.gif","__onSpell()",getLanguageString(language,411),design,""));
    var objUpload = new MenuButton("menu_upload.html","design/image/" + design + "/upload.gif","onMenuUploadClicked",getLanguageString(language,3011),design,"");
    objUpload.width = "170";
    objUpload.height = "48";
    objToolbar1.add(objUpload);

    objToolbar1.add(new Separator("design/image/" + design + "/separator.gif"));
    objToolbar1.add(new Button("","","design/image/" + design + "/cut.gif","editCut()",getLanguageString(language,104),design,""));
    objToolbar1.add(new Button("","","design/image/" + design + "/copy.gif","editCopy()",getLanguageString(language,105),design,""));
    objToolbar1.add(new Button("","","design/image/" + design + "/paste.gif","editPaste()",getLanguageString(language,106),design,""));
    if(!browser.ns)
      objToolbar1.add(new Button("","","design/image/" + design + "/pasteword.gif","editPasteWord()",getLanguageString(language,408),design,""));
    objToolbar1.add(new Separator("design/image/" + design + "/separator.gif"));
    objToolbar1.add(new Button("","","design/image/" + design + "/undo.gif","editUndo()",getLanguageString(language,107),design,""));
    objToolbar1.add(new Button("","","design/image/" + design + "/redo.gif","editRedo()",getLanguageString(language,108),design,""));
    //objToolbar1.add(new Separator("design/image/" + design + "/separator.gif"));

    // optional: divide the toolbar in 2 toolbars
    if(design =="Office2003") {
      objToolbar1.add(new Separator("design/image/" + design + "/tbend.gif"));
      objToolbar1.add(new Distance(3,true));
      objToolbar1.add(new Separator("design/image/" + design + "/tbbegin.gif"));
    } else {
      objToolbar1.add(new Separator("design/image/" + design + "/separator.gif"));
    }

    objToolbar1.add(new Button("","","design/image/" + design + "/link.gif","editLink()",getLanguageString(language,109),design,""));
    objToolbar1.add(new Button("","","design/image/" + design + "/anchor.gif","editInsertObject('ANCHOR')",getLanguageString(language,407),design,""));

    var objImage = new MenuButton("menu_image.html","design/image/" + design + "/image.gif","onMenuImageClicked",getLanguageString(language,110),design,"");
    objImage.width = "150";
    if(browser.ns)
      objImage.height = "72";
    else
      objImage.height = "96";
    objToolbar1.add(objImage);

    objToolbar1.add(new TableButton("design/image/" + design + "/table.gif","onCreateTable",getLanguageString(language,111),design,""));
    objToolbar1.add(new Button("","","design/image/" + design + "/rule.gif","editInsertObject('RULE')",getLanguageString(language,112),design,""));
    var objChar = new MenuButton("char.html","design/image/" + design + "/char.gif","onMenuCharClicked",getLanguageString(language,115),design,"");
    objChar.width = "78";
    objChar.height = "100";
    objToolbar1.add(objChar);

    objToolbar1.add(new Button("","","design/image/" + design + "/date.gif","editInsertDate()",getLanguageString(language,116),design,""));
    objToolbar1.add(new Button("","","design/image/" + design + "/time.gif","editInsertTime()",getLanguageString(language,117),design,""));
    objToolbar1.add(new Button("","","design/image/" + design + "/marquee.gif","editInsertObject('MARQUEE')",getLanguageString(language,409),design,""));
    objToolbar1.add(new Button("","","design/image/" + design + "/pagebreak.gif","editInsertObject('PAGEBREAK')",getLanguageString(language,404),design,""));
    objToolbar1.add(new Button("","","design/image/" + design + "/paragraph.gif","editParagraph()",getLanguageString(language,3002),design,""));
    objToolbar1.add(new Button("","","design/image/" + design + "/edit.gif","editSetEditable(false)",getLanguageString(language,3301),design,""));
    objToolbar1.add(new Button("","","design/image/" + design + "/flash.gif","editOpen(4)",getLanguageString(language,3303),design,""));
    objToolbar1.add(new Button("","","design/image/" + design + "/media.gif","editOpen(5)",getLanguageString(language,3304),design,""));

    objToolbar1.add(new Separator("design/image/" + design + "/separator.gif"));
    objToolbar1.add(new Button("","","design/image/" + design + "/help.gif","onHelp()",getLanguageString(language,114),design,""));

    if(design =="Office2003")
      objToolbar1.add(new Separator("design/image/Office2003/tbend.gif"));

    // add toolbar to collection
    objToolbars.add(objToolbar1);

    //-----------------------------------------------------------------------------------
    // create formatting toolbar
    //-----------------------------------------------------------------------------------
    var objToolbar2 = new Toolbar();
    objToolbar2.design = design;
    objToolbar2.border = globalToolbarBorder;
    objToolbar2.height = browser.ns ? 29:27;
    // add the members of the toolbar
    objToolbar2.add(new Separator("design/image/" + design + "/tbbegin.gif"));

    if(!browser.ns) {
      cmbStyle = new StyleCombo("onStyleChanged",getLanguageString(language,3014),design,"");
      cmbStyle.width = "100";
      cmbStyle.popupwidth = "200";
      cmbStyle.popupheight = "120";
      cmbStyle.displayValue = "tag1";
      cmbStyle.add(new ComboItem("Standard","","","STANDARD","Standard"));
      // needed for automatic fill
      editSetStyleSheetObject(cmbStyle);

      objToolbar2.add(cmbStyle);

      objToolbar2.add(new Distance(3));

      cmbFormat = new StyleCombo("onFormatChanged",getLanguageString(language,3015),design,"");
      cmbFormat.width = "100";
      cmbFormat.popupwidth = "200";
      cmbFormat.popupheight = "130";
      cmbFormat.displayValue = "tag1";
      cmbFormat.add(new ComboItem("Normal","","","NORMAL","Normal"));
      cmbFormat.add(new ComboItem("<h1>Heading 1</h1>","","","<h1>","Heading 1"));
      cmbFormat.add(new ComboItem("<h2>Heading 2</h2>","","","<h2>","Heading 2"));
      cmbFormat.add(new ComboItem("<h3>Heading 3</h3>","","","<h3>","Heading 3"));
      cmbFormat.add(new ComboItem("<h4>Heading 4</h4>","","","<h4>","Heading 4"));
      cmbFormat.add(new ComboItem("<h5>Heading 5</h5>","","","<h5>","Heading 5"));
      cmbFormat.add(new ComboItem("<h6>Heading 6</h6>","","","<h6>","Heading 6"));
      cmbFormat.add(new ComboItem("<address>Address</address>","","","<address>","Address"));
      cmbFormat.add(new ComboItem("<dir>Directory List</dir>","","","<dir>","Directory List"));
      cmbFormat.add(new ComboItem("<pre>Formatted</pre>","","","<pre>","Formatted"));
      cmbFormat.add(new ComboItem("<menu>Menu List</menu>","","","<menu>","Menu List"));
      // needed for changes format styles
      editSetFormatObject(cmbFormat);
      objToolbar2.add(cmbFormat);

      objToolbar2.add(new Distance(3));

      cmbFont = new StyleCombo("onFontChanged",getLanguageString(language,3016),design,"");
      cmbFont.width = "114";
      cmbFont.popupwidth = "120";
      cmbFont.popupheight = "136";
      cmbFont.displayValue = "value";
      cmbFont.add(new ComboItem("<FONT face='Arial'>Arial</font>","","","Arial"));
      cmbFont.add(new ComboItem("<FONT face='Courier'>Courier</font>","","","Courier"));
      cmbFont.add(new ComboItem("<FONT face='Courier New'>Courier New</font>","","","Courier New"));
      cmbFont.add(new ComboItem("<FONT face='Tahoma'>Tahoma</font>","","","Tahoma"));
      cmbFont.add(new ComboItem("<FONT face='Times New Roman'>Times New Roman</font>","","","Times New Roman"));
      cmbFont.add(new ComboItem("<FONT face='Verdana'>Verdana</font>","","","Verdana"));
      objToolbar2.add(cmbFont);

      objToolbar2.add(new Distance(3));

      cmbFontSize = new StyleCombo("onFontSizeChanged",getLanguageString(language,3017),design,"");
      cmbFontSize.width = "35";
      cmbFontSize.popupwidth = "85";
      cmbFontSize.popupheight = "200";
      cmbFontSize.displayValue = "tag1";
      cmbFontSize.add(new ComboItem("<FONT size=1>10</FONT>","","","1","10"));
      cmbFontSize.add(new ComboItem("<FONT size=2>12</FONT>","","","2","12"));
      cmbFontSize.add(new ComboItem("<FONT size=3>16</FONT>","","","3","16"));
      cmbFontSize.add(new ComboItem("<FONT size=4>18</FONT>","","","4","18"));
      cmbFontSize.add(new ComboItem("<FONT size=5>24</FONT>","","","5","24"));
      cmbFontSize.add(new ComboItem("<FONT size=6>32</FONT>","","","6","32"));
      cmbFontSize.add(new ComboItem("<FONT size=7>48</FONT>","","","7","48"));
      objToolbar2.add(cmbFontSize);
    } else {
      cmbStyle = new Combo("changeStyle()");
      objToolbar2.add(cmbStyle);
      // stylesheets are filled dynamicly
      editSetStyleSheetObject(cmbStyle);
      cmbFormat = new Combo("changeFormat()");
      objToolbar2.add(cmbFormat);
      cmbFont = new Combo("changeFont()");
      objToolbar2.add(cmbFont);
      cmbFontSize = new Combo("changeFontSize()");
      objToolbar2.add(cmbFontSize);
    }
    objToolbar2.add(new Button("","","design/image/" + design + "/bold.gif","editBold()",getLanguageString(language,201),design,"BOLD"));
    objToolbar2.add(new Button("","","design/image/" + design + "/italic.gif","editItalic()",getLanguageString(language,202),design,"ITALIC"));
    objToolbar2.add(new Button("","","design/image/" + design + "/underline.gif","editUnderline()",getLanguageString(language,203),design,"UNDERLINE"));
    objToolbar2.add(new Button("","","design/image/" + design + "/superscript.gif","editSuperscript()",getLanguageString(language,204),design,"SUPERSCRIPT"));
    objToolbar2.add(new Button("","","design/image/" + design + "/subscript.gif","editSubscript()",getLanguageString(language,205),design,"SUBSCRIPT"));
    objToolbar2.add(new Separator("design/image/" + design + "/separator.gif"));
    objToolbar2.add(new Button("","","design/image/" + design + "/left.gif","editJustifyLeft()",getLanguageString(language,206),design,"JUSTIFYLEFT"));
    objToolbar2.add(new Button("","","design/image/" + design + "/center.gif","editJustifyCenter()",getLanguageString(language,207),design,"JUSTIFYCENTER"));
    objToolbar2.add(new Button("","","design/image/" + design + "/right.gif","editJustifyRight()",getLanguageString(language,208),design,"JUSTIFYRIGHT"));
    objToolbar2.add(new Button("","","design/image/" + design + "/block.gif","editJustifyFull()",getLanguageString(language,209),design,"JUSTIFYFULL"));
    objToolbar2.add(new Separator("design/image/" + design + "/separator.gif"));
    objToolbar2.add(new Button("","","design/image/" + design + "/orderedlist.gif","editOrderedList()",getLanguageString(language,210),design,"INSERTORDEREDLIST"));
    objToolbar2.add(new Button("","","design/image/" + design + "/unorderedlist.gif","editUnorderedList()",getLanguageString(language,211),design,"INSERTUNORDEREDLIST"));
    objToolbar2.add(new Button("","","design/image/" + design + "/indent.gif","editIndent()",getLanguageString(language,212),design,"INDENT"));
    objToolbar2.add(new Button("","","design/image/" + design + "/outdent.gif","editOutdent()",getLanguageString(language,213),design,"OUTDENT"));
    objToolbar2.add(new Separator("design/image/" + design + "/separator.gif"));
    btnColor = new ColorSelector("design/image/" + design + "/color.gif","design/image/colorselect.gif","onSetTextColor",getLanguageString(language,215),getLanguageString(language,217),design,"");
    objToolbar2.add(btnColor);
    btnBackColor = new ColorSelector("design/image/" + design + "/backcolor.gif","design/image/colorselect.gif","onSetBackgroundColor",getLanguageString(language,216),getLanguageString(language,218),design,"");
    objToolbar2.add(btnBackColor);
    if(design =="Office2003")
      objToolbar2.add(new Separator("design/image/Office2003/tbend.gif"));

    // add toolbar to collection
    objToolbars.add(objToolbar2);


    //-----------------------------------------------------------------------------------
    // create formular toolbar
    //-----------------------------------------------------------------------------------
    var objToolbar3 = new Toolbar();
    objToolbar3.design = design;
    objToolbar3.border = globalToolbarBorder;
    objToolbar3.add(new Separator("design/image/" + design + "/tbbegin.gif"));
    objToolbar3.add(new Button("","","design/image/" + design + "/form.gif","editInsertObject('FORM')",getLanguageString(language,406),design,""));
    objToolbar3.add(new Button("","","design/image/" + design + "/label.gif","editInsertObject('LABEL')",getLanguageString(language,301),design,""));
    objToolbar3.add(new Button("","","design/image/" + design + "/button.gif","editInsertObject('BUTTON')",getLanguageString(language,302),design,""));
    objToolbar3.add(new Button("","","design/image/" + design + "/input.gif","editInsertObject('INPUT')",getLanguageString(language,303),design,""));
    objToolbar3.add(new Button("","","design/image/" + design + "/checkbox.gif","editInsertObject('CHECK')",getLanguageString(language,304),design,""));
    objToolbar3.add(new Button("","","design/image/" + design + "/radio.gif","editInsertObject('OPTION')",getLanguageString(language,305),design,""));
    objToolbar3.add(new Button("","","design/image/" + design + "/combobox.gif","editInsertObject('COMBO')",getLanguageString(language,306),design,""));
    objToolbar3.add(new Button("","","design/image/" + design + "/listbox.gif","editInsertObject('LISTBOX')",getLanguageString(language,307),design,""));
    objToolbar3.add(new Button("","","design/image/" + design + "/textarea.gif","editInsertObject('AREA')",getLanguageString(language,308),design,""));
    objToolbar3.add(new Button("","","design/image/" + design + "/hidden.gif","editInsertObject('HIDDEN')",getLanguageString(language,410),design,""));
    objToolbar3.add(new Separator("design/image/" + design + "/separator.gif"));
    objToolbar3.add(new Button("","","design/image/" + design + "/div.gif","editInsertObject('DIV')",getLanguageString(language,310),design,""));
    objToolbar3.add(new Button("","","design/image/" + design + "/iframe.gif","editInsertObject('IFRAME')",getLanguageString(language,309),design,""));
    if(!browser.ie5) {
      objToolbar3.add(new Separator("design/image/" + design + "/separator.gif"));
      objToolbar3.add(new Button("","","design/image/" + design + "/position.gif","editAbsolute()",getLanguageString(language,311),design,""));
    }

    // optional: divide the toolbar in 2 toolbars
    if(design =="Office2003") {
      objToolbar3.add(new Separator("design/image/" + design + "/tbend.gif"));
      objToolbar3.add(new Distance(3,true));
      objToolbar3.add(new Separator("design/image/" + design + "/tbbegin.gif"));
    } else {
      objToolbar3.add(new Separator("design/image/" + design + "/separator.gif"));
    }

    //-----------------------------------------------------------------------------------
    // create text modules toolbar
    //-----------------------------------------------------------------------------------
    //objToolbar3.add(new Separator("design/image/" + design + "/separator.gif"));
    var cmbTextModule = new Combo("");
    objToolbar3.add(cmbTextModule);
    objToolbar3.add(new Button("","","design/image/tm_add.gif","editInsertTextModule('" + cmbTextModule.id + "')",getLanguageString(language,312),design,""));
    objToolbar3.add(new Button("","","design/image/tm_create.gif","editSetTextModule('" + cmbTextModule.id + "')",getLanguageString(language,313),design,""));
    objToolbar3.add(new Button("","","design/image/tm_remove.gif","editRemoveTextModule('" + cmbTextModule.id + "')",getLanguageString(language,314),design,""));

    // optional: divide the toolbar in 2 toolbars
    if(design =="Office2003") {
      objToolbar3.add(new Separator("design/image/" + design + "/tbend.gif"));
      objToolbar3.add(new Distance(3,true));
      objToolbar3.add(new Separator("design/image/" + design + "/tbbegin.gif"));
    } else {
      objToolbar3.add(new Separator("design/image/" + design + "/separator.gif"));
    }


    //objToolbar1.add(new Separator("design/image/" + design + "/separator.gif"));
    var objClean = new MenuButton("menu_clean.html","design/image/" + design + "/remove.gif","onMenuCleanClicked",getLanguageString(language,214),design,"");
    objClean.width = "200";
    objClean.height = "93";
    objToolbar3.add(objClean);
    // set RETURN mode dependant of settings
    if(editGetBr())
      btnReturnMode = new Button("","","design/image/" + design + "/bron.gif","onReturnMode()",getLanguageString(language,3013),design,"");
    else
      btnReturnMode = new Button("","","design/image/" + design + "/broff.gif","onReturnMode()",getLanguageString(language,3013),design,"");
    objToolbar3.add(btnReturnMode);

    objToolbar3.add(new Button("","","design/image/" + design + "/tablehigh.gif","editTableHighlight()",getLanguageString(language,3003),design,""));
    objToolbar3.add(new Button("","","design/image/" + design + "/selectall.gif","editSelectAll()",getLanguageString(language,405),design,""));
    //objToolbar1.add(new Button("","","design/image/" + design + "/ltr.gif","editSetDirection('ltr')",getLanguageString(language,3000),design,""));
    //objToolbar1.add(new Button("","","design/image/" + design + "/rtl.gif","editSetDirection('rtl')",getLanguageString(language,3001),design,""));
    objToolbar3.add(new Separator("design/image/" + design + "/separator.gif"));

    if(!browser.ie5) {
      cmbZoom = new Combo("changeZoom()");
      objToolbar3.add(cmbZoom);
    }

    if(design =="Office2003")
      objToolbar3.add(new Separator("design/image/Office2003/tbend.gif"));

    // add toolbar to collection
    objToolbars.add(objToolbar3);

    // now create toolbar
    objToolbars.create();

    if(browser.ns) {
      cmbStyle.add("Standard","STANDARD");

      cmbFormat.add("Normal","NORMAL");
      cmbFormat.add("Heading 1","<h1>");
      cmbFormat.add("Heading 2","<h2>");
      cmbFormat.add("Heading 3","<h3>");
      cmbFormat.add("Heading 4","<h4>");
      cmbFormat.add("Heading 5","<h5>");
      cmbFormat.add("Heading 6","<h6>");
      cmbFormat.add("Address","<address>");
      cmbFormat.add("Directory List","<dir>");
      cmbFormat.add("Formatted","<pre>");
      cmbFormat.add("Menu List","<menu>");

      // now add the fonts
      cmbFont.add("Arial","ARIAL");
      cmbFont.add("Courier","COURIER");
      cmbFont.add("Courier New","COURIER NEW");
      cmbFont.add("Tahoma","TAHOMA");
      cmbFont.add("Times New Roman","TIMES NEW ROMAN");
      cmbFont.add("Verdana","VERDANA");

      // now add the font size
      cmbFontSize.add("8","1");
      cmbFontSize.add("10","2");
      cmbFontSize.add("12","3");
      cmbFontSize.add("14","4");
      cmbFontSize.add("18","5");
      cmbFontSize.add("24","6");
      cmbFontSize.add("36","7");
    }

    if(!browser.ie5  && !browser.ns) {
      // now add the zoom values
      cmbZoom.add("10%","10%");
      cmbZoom.add("50%","50%");
      cmbZoom.add("100%","100%");
      cmbZoom.add("150%","150%");
      cmbZoom.add("200%","200%");
      cmbZoom.setSelected("100%");
    }

    try {
      // read available text modules from server
      editGetTextModule(cmbTextModule.id);
    } catch(Error) {}
  }


  //---------------------------------------------------------------------------------------------------
  // this is a sample for customizing the toolbar
  //---------------------------------------------------------------------------------------------------
  if(userCode == "USER") {
    var objToolbars = new Toolbars(id);
    // define toolbar settings
    objToolbars.backcolor = globalToolbarColor;

    var objToolbar1 = new Toolbar();
    objToolbar1.design = design;
    objToolbar1.border = globalToolbarBorder;
    objToolbar1.height = browser.ns ? 29:27;

    objToolbar1.add(new Separator("design/image/" + design + "/tbbegin.gif"));
    // add a button
    objToolbar1.add(new Button("","","design/image/" + design + "/save.gif","alert('Save button clicked')",'My Save button',design,""));
    // add a separator
    objToolbar1.add(new Separator("design/image/" + design + "/separator.gif"));
    // add a style combo
    cmbDemo = new StyleCombo("onMyStyleCombo",'MyStyleCombo',design,"");
    cmbDemo.width = "100";
    cmbDemo.popupwidth = "200";
    cmbDemo.popupheight = "130";
    cmbDemo.displayValue = "tag1";
    cmbDemo.add(new ComboItem("<font color='red'>Html 1</font>","","","TEXT1","Display text1"));
    cmbDemo.add(new ComboItem("<font color='blue'>Html 2</font>","","","TEXT2","Display text2"));
    objToolbar1.add(cmbDemo);

    objToolbar1.add(new Separator("design/image/" + design + "/tbend.gif"));

    objToolbars.add(objToolbar1);
    objToolbars.create();
  }

  return objToolbars;
}

//--------------------------------------------------------------------------------------------------------------------
// create the bottom toolbar
//--------------------------------------------------------------------------------------------------------------------
function CreateToolbarsBottom(id, userCode)
{
  // create the main toolbar collection
  var objToolbars = new Toolbars(id);
  // define toolbar color
  objToolbars.backcolor = globalToolbarColor;

  //-----------------------------------------------------------------------------------
  // create toolbar
  //-----------------------------------------------------------------------------------
  var objToolbar1 = new Toolbar();
  objToolbar1.design = design;
  objToolbar1.border = globalToolbarBorder;
  // we want set the button status of the mode buttons
  objToolbar1.add(new Separator("design/image/" + design + "/tbbegin.gif"));
  btnEdit = new Button(getLanguageString(language,401),"","design/image/" + design + "/edit.gif","onButtonClick('EDIT')",getLanguageString(language,401),design,"");
  objToolbar1.add(btnEdit);
  btnHTML = new Button(getLanguageString(language,402),"","design/image/" + design + "/html.gif","onButtonClick('HTML')",getLanguageString(language,402),design,"");
  objToolbar1.add(btnHTML);
  btnPreview = new Button(getLanguageString(language,403),"","design/image/" + design + "/preview.gif","onButtonClick('PREVIEW')",getLanguageString(language,403),design,"");
  objToolbar1.add(btnPreview);

  if(design =="Office2003")
    objToolbar1.add(new Separator("design/image/Office2003/tbend.gif"));

	// add toolbar to collection
  objToolbars.add(objToolbar1);

  // now create toolbar
  objToolbars.create();
  btnEdit.setStatus(true);
}

//--------------------------------------------------------------------------------------------------------------------
// react on combo changes
//--------------------------------------------------------------------------------------------------------------------
// used also in config_toolbar_auto
function onStyleChanged(text,value,tag1,tag2)
{
  editSetStyle(value);
}

// used also in config_toolbar_auto
function onFormatChanged(text,value,tag1,tag2)
{
  if (value == 'NORMAL') {
    editFormat('Normal');
  } else {
    editFormat(value);
  }
}

// used also in config_toolbar_auto
function onFontChanged(text,value,tag1,tag2)
{
  editFont(value);
}

// used also in config_toolbar_auto
function onFontSizeChanged(text,value,tag1,tag2)
{
  editFontSize(value);
}

// used also in config_toolbar_auto
function changeZoom()
{
  editZoom(cmbZoom.getSelected());
}

// used also in config_toolbar_auto
function changeStyle()
{
  editSetStyle(cmbStyle.getSelected());
}

// needed for Mozilla
// used also in config_toolbar_auto
function changeFormat()
{
  var value = cmbFormat.getSelected();
  if (value == 'NORMAL') {
    editFormat('Normal');
    editRemoveFormat();
  } else {
    editFormat(value);
  }
}

// used also in config_toolbar_auto
function changeFont()
{
  editFont(cmbFont.getSelected());
}

// used also in config_toolbar_auto
function changeFontSize()
{
  editFontSize(cmbFontSize.getSelected());
}

//--------------------------------------------------------------------------------------------------------------------
// these functions are called after click on a button
//--------------------------------------------------------------------------------------------------------------------
// used also in config_toolbar_auto
function onReturnMode()
{
  if(editGetBr()) {
    editSetBr(false);
    btnReturnMode.setImage("design/image/" + design + "/broff.gif");
  } else {
    editSetBr(true);
    btnReturnMode.setImage("design/image/" + design + "/bron.gif");
  }
}

// used also in config_toolbar_auto
function onCreateTable(row, col)
{
  editCreateTable(row,col);
}

// click on mode buttons
// we set the pressed state
// used also in config_toolbar_auto
function onButtonClick(tag)
{
	editSetMode(tag);
	editSetActiveMode(tag);

//	if(tag == "EDIT" && browser.ns)
//		setTimeout("update()",10);
}

function update()
{
	CreateToolbarsTop("toolbartop","");
	//CreateToolbarsAuto("T01020304","toolbartop","toolbarbottom");
}

// if a text color is set
// used also in config_toolbar_auto
function onSetTextColor(color)
{
  editColor(color)
}

// if a text backgroundcolor is set
// used also in config_toolbar_auto
function onSetBackgroundColor(color)
{
  editBackColor(color);
}

// if the image menu is clicked
// used also in config_toolbar_auto
function onMenuImageClicked(tag)
{
  if(tag == "LOCAL")        editInsertObject('IMAGE');
  if(tag == "WEB")          editInsertObject('IMAGEWEB');
  if(tag == "SERVER")       editOpen(3);
  if(tag == "UPLOADINSERT") editUpload("","",0,true);
}

// used also in config_toolbar_auto
function onMenuUploadClicked(tag)
{
  if(tag == "UPLOAD")     editUpload("","",0);
  if(tag == "UPLOADDOC")  editUpload("","",1);
}

// if the clean menu is clicked
// used also in config_toolbar_auto
function onMenuCleanClicked(tag)
{
  if(tag == "STYLE")  editClean(0);
  if(tag == "FORMAT") editClean(1);
  if(tag == "TAG")    editClean(2);
  if(tag == "ALL")    editClean(3);
}

// if one of the special character button is clicked
// used also in config_toolbar_auto
function onMenuCharClicked(tag)
{
  editInsertHtml(tag)
}

// used also in config_toolbar_auto
function saveCommon()
{
	if(__currentURL == "")
		editSaveDialog();
	else
		editSave();

  // do some internal things...
  // after saving, the loaded page is displayed without pinEdit
  // this is part of integration mode (see integration sample)
  if(__mode == "I") {
    document.location.href = __currentURL;
  }
}

// help function
// used also in config_toolbar_auto
function onHelp()
{
  var left = screen.width/2 - 400/2;
  var top = screen.height/2 - 600/2;
  window.open(__editGetEditorUrl() + "userhelp.html","","left=" + left + ",top=" + top + ",height=600,width=400,resizable=1,status=0,scrollbars=1");
}


//----------------------------------------------------
function onMyStyleCombo(text,value,tag1,tag2)
{
  alert("Item clicked: " + value);
}


// Used for internal purpose: DO NOT MODIFY
var __globalWinControlValue = "";
function Preview()
{
  __globalWinControlValue = "PREVIEW";
 document.title = __globalWinControlValue;
}

function RaiseControlEvent(event)
{
 // __globalWinControlValue = event;
 document.title = ":" + event;
}


// works for selection
function removeAllFormatSelection()
{
	editClean(1);
	var html = editGetSelectedHtml();

  getDoc().selection.clear();
  html = html.replace(/<h1[^>]+>/gi, "<p>");
  html = html.replace(/<\/h1>/gi, "</p>");
  html = html.replace(/<h2[^>]+>/gi, "<p>");
  html = html.replace(/<\/h2>/gi, "</p>");
  html = html.replace(/<h3[^>]+>/gi, "<p>");
  html = html.replace(/<\/h3>/gi, "</p>");
  html = html.replace(/<h4[^>]+>/gi, "<p>");
  html = html.replace(/<\/h4>/gi, "</p>");
  html = html.replace(/<h5[^>]+>/gi, "<p>");
  html = html.replace(/<\/h5>/gi, "</p>");
  html = html.replace(/<h6[^>]+>/gi, "<p>");
  html = html.replace(/<\/h6>/gi, "</p>");
  html = html.replace(/<address[^>]+>/gi, "<p>");
  html = html.replace(/<\/address>/gi, "</p>");
  html = html.replace(/<menu[^>]+>/gi, "<p>");
  html = html.replace(/<\/menu>/gi, "</p>");
  html = html.replace(/<pre[^>]+>/gi, "<p>");
  html = html.replace(/<\/pre>/gi, "</p>");
  html = html.replace(/<dir[^>]+>/gi, "<p>");
  html = html.replace(/<\/dir>/gi, "</p>");
  editInsertHtml(html);
	editClean(0);
}

// this works for the whole document, not for selection
function removeAllFormat()
{
	var html = editGetHtmlBody();
  html = html.replace(/<h1>/gi, "<p>");
  html = html.replace(/<h1[^>]+>/gi, "<p>");
  html = html.replace(/<\/h1>/gi, "</p>");
  html = html.replace(/<h2>/gi, "<p>");
  html = html.replace(/<h2[^>]+>/gi, "<p>");
  html = html.replace(/<\/h2>/gi, "</p>");
  html = html.replace(/<h3>/gi, "<p>");
  html = html.replace(/<h3[^>]+>/gi, "<p>");
  html = html.replace(/<\/h3>/gi, "</p>");
  html = html.replace(/<h4>/gi, "<p>");
  html = html.replace(/<h4[^>]+>/gi, "<p>");
  html = html.replace(/<\/h4>/gi, "</p>");
  html = html.replace(/<h5>/gi, "<p>");
  html = html.replace(/<h5[^>]+>/gi, "<p>");
  html = html.replace(/<\/h5>/gi, "</p>");
  html = html.replace(/<h6>/gi, "<p>");
  html = html.replace(/<h6[^>]+>/gi, "<p>");
  html = html.replace(/<\/h6>/gi, "</p>");
  html = html.replace(/<address>/gi, "<p>");
  html = html.replace(/<address[^>]+>/gi, "<p>");
  html = html.replace(/<\/address>/gi, "</p>");
  html = html.replace(/<menu>/gi, "<p>");
  html = html.replace(/<menu[^>]+>/gi, "<p>");
  html = html.replace(/<\/menu>/gi, "</p>");
  html = html.replace(/<pre>/gi, "<p>");
  html = html.replace(/<pre[^>]+>/gi, "<p>");
  html = html.replace(/<\/pre>/gi, "</p>");
  html = html.replace(/<dir>/gi, "<p>");
  html = html.replace(/<dir[^>]+>/gi, "<p>");
  html = html.replace(/<\/dir>/gi, "</p>");
  editSetBodyHtml(html);
  editRemoveAttribute("lang");
	editClean(0);

	var rng = getDoc().body.createTextRange();
	rng.select();
	editClean(1);
	editSetCursorHome();
}
