function getLanguageString(language,id)
{
	alert(language+id)
  if(language=="EN") {
    if(id=='101') return "New";
    if(id=='102') return "Open";
    if(id=='103') return "Print";
    if(id=='104') return "Cut";
    if(id=='105') return "Copy";
    if(id=='106') return "Paste";
    if(id=='107') return "Undo";
    if(id=='108') return "Redo";
    if(id=='109') return "Insert Link";
    if(id=='110') return "Insert Image";
    if(id=='111') return "Insert Table";
    if(id=='112') return "Insert Rule";
    if(id=='113') return "Search";
    if(id=='114') return "Help";
    if(id=='115') return "Insert special characters";
    if(id=='116') return "Insert Date";
    if(id=='117') return "Insert Time";
    if(id=='118') return "Save";

    if(id=='201') return "Bold";
    if(id=='202') return "Italic";
    if(id=='203') return "Underline";
    if(id=='204') return "Superscript";
    if(id=='205') return "Subscript";
    if(id=='206') return "Justify left";
    if(id=='207') return "Justify center";
    if(id=='208') return "Justify right";
    if(id=='209') return "Justify full";
    if(id=='210') return "Ordered list";
    if(id=='211') return "Unordered list";
    if(id=='212') return "Indent";
    if(id=='213') return "Outdent";
    if(id=='214') return "Remove format";
    if(id=='215') return "Set text color";
    if(id=='216') return "Set font background color";
    if(id=='217') return "Select text color";
    if(id=='218') return "Select font background color";
    if(id=='219') return "Set background color";
    if(id=='220') return "Select background color";

    if(id=='301') return "Insert label";
    if(id=='302') return "Insert button";
    if(id=='303') return "Insert input";
    if(id=='304') return "Insert checkbox";
    if(id=='305') return "Insert radio";
    if(id=='306') return "Insert combobox";
    if(id=='307') return "Insert listbox";
    if(id=='308') return "Insert text area";
    if(id=='309') return "Insert sub page";
    if(id=='310') return "Insert container";
    if(id=='311') return "Absolute position";
    /*if(id=='312') return "Insert text snippet";
    if(id=='313') return "New text snippet";
    if(id=='314') return "Remove active text snippet";*/
	if(id=='312') return "Insert Signature";
    if(id=='313') return "New Signature";
    if(id=='314') return "Remove Active Signature";

    if(id=='401') return "Edit";
    if(id=='402') return "HTML";
    if(id=='403') return "Preview";
    if(id=='404') return "Page break";
    if(id=='405') return "Select all";
    if(id=='406') return "Insert form";
    if(id=='407') return "Insert Anchor";
    if(id=='408') return "Paste from Word";
    if(id=='409') return "Marquee";
    if(id=='410') return "Insert hidden field";
    if(id=='411') return "Spell checker";

    if(id=='501') return " Table";
    if(id=='502') return "Cancel";

    if(id=='601') return "More colors...";

    if(id=='701') return "Insert local image";
    if(id=='702') return "Insert web image";
    if(id=='703') return "Insert server image";
    if(id=='704') return "Upload local image";
	if(id=='705') return "Insert image from library";
	if(id=='706') return "Insert background from library";
	if(id=='707') return "Insert emoticons from library";
	if(id=='713') return "Insert gif animations from library";
	if(id=='708') return "Happy";
	if(id=='709') return "Insert sound from library";
	if(id=='710') return "Insert Tags";
	if(id=='711') return "Replace Image";
	if(id=='712') return "Smile";
	if(id=='714') return "Insert Audio Recorder";

    if(id=='801') return "Insert row before";
    if(id=='802') return "Insert row after";
    if(id=='803') return "Delete row";
    if(id=='804') return "Insert column before";
    if(id=='805') return "Insert column after";
    if(id=='806') return "Delete column";
    if(id=='807') return "Delete cell";
    if(id=='808') return "Merge cell";
    if(id=='809') return "Divide cell";
    if(id=='810') return "Cell properties...";
    if(id=='811') return "Table properties...";
    if(id=='812') return "Page properties...";
    if(id=='813') return "Image properties...";
    if(id=='814') return "Set to foreground";
    if(id=='815') return "Container properties...";
    if(id=='816') return "Sub Page properties...";
    if(id=='817') return "Label properties...";

    if(id=='900') return "Cancel";
    if(id=='901') return "Table Properties";
    if(id=='902') return "Percent";
    if(id=='903') return "Absolute";
    if(id=='904') return "Cell spacing:";
    if(id=='905') return "Border width:";
    if(id=='906') return "Cell padding:";
    if(id=='907') return "Border color:";
    if(id=='908') return "Width:";
    if(id=='909') return "Background color:";
    if(id=='911') return "Cell Properties";
    if(id=='912') return "Hor. alignment:";
    if(id=='913') return "Height:";
    if(id=='914') return "Vert. alignment:";
    if(id=='915') return "Wrap:";
    if(id=='916') return "Border color:";
    if(id=='917') return "Background color:";
    if(id=='918') return "Width:";
    if(id=='921') return "Image Properties";
    if(id=='922') return "Width:";
    if(id=='923') return "Vert. alignment:";
    if(id=='924') return "Height:";
    if(id=='925') return "Border Width:";
    if(id=='926') return "Title:";
    if(id=='927') return "Border color:";
    if(id=='931') return "Label Properties";
    if(id=='932') return "Width:";
    if(id=='933') return "Border color:";
    if(id=='934') return "Height:";
    if(id=='935') return "Background color:";
    if(id=='941') return "Container Properties";
    if(id=='942') return "Width:";
    if(id=='943') return "Border color:";
    if(id=='944') return "Height:";
    if(id=='945') return "Back color:";
    if(id=='951') return "Page Properties";
    if(id=='952') return "Margin:";
    if(id=='953') return "Background color:";
    if(id=='961') return "Select Color";
    if(id=='971') return "Sub Page Properties";
    if(id=='972') return "Width:";
    if(id=='973') return "Height:";
    if(id=='974') return "URL:";
    if(id=='981') return "Insert web image";
    if(id=='982') return "Enter URL:";
    if(id=='991') return "Search/Replace";
    if(id=='992') return "Search:";
    if(id=='993') return "Replace:";
    if(id=='994') return "Search";
    if(id=='995') return "Replace";
    if(id=='996') return "Please enter a search string !";
    if(id=='997') return "Document end reached or search string not found. Search continues at document start !";
    if(id=='998') return "Please enter a replace string !";

    if(id=='1001') return "Open/Save dialog";
    if(id=='1002') return "Open";
    if(id=='1003') return "Save";
    if(id=='1004') return "Open";
    if(id=='1005') return "File name:";
    if(id=='1006') return "Preview:";

    if(id=='1101') return "Create/edit anchor";
    if(id=='1102') return "Anchor name:";
    if(id=='1103') return "Anchor properties";
    if(id=='1104') return "Remove anchor";

    if(id=='1201') return "Form properties";
    if(id=='1202') return "Anchor name:";
    if(id=='1203') return "Form properties";
    if(id=='1204') return "Remove form";

    if(id=='1301') return "Input properties";
    if(id=='1302') return "Select properties";
    if(id=='1303') return "Size";
    if(id=='1304') return "Color";

    if(id=='2000') return "Set to background";
    if(id=='2001') return "Style";

    // new in 3.0
    if(id=='119') return "Save as";
    if(id=='3000') return "Left to right";
    if(id=='3001') return "Right to left";
    if(id=='3002') return "Insert Paragraph";
    if(id=='3003') return "Table highlight";
    if(id=='3004') return "Hor. Alignment:";
    if(id=='3005') return "Upload and Insert";
    if(id=='3006') return "Please follow below steps to create new signature:\n1. Enter your signature in editor.\n2. Select the text from editor.\n3. Click the New Signature icon.";
    if(id=='3007') return "Name:";
    if(id=='3008') return "Create text module";
    if(id=='3009') return "Please avoid to enter space and special characters (%^&*$ etc.)!";
    if(id=='3010') return "Upload document";
    if(id=='3011') return "Upload";
    if(id=='3012') return "Please select text first !";
    if(id=='3013') return "Set RETURN mode";
    if(id=='3014') return "Select Style Sheet";
    if(id=='3015') return "Select Format";
    if(id=='3016') return "Select Font";
    if(id=='3017') return "Select Fontsize";
    if(id=='3018') return "File";
    if(id=='3019') return "Edit";
    if(id=='3020') return "Insert";
    if(id=='3021') return "Format";
    if(id=='3022') return "Forms";
    if(id=='3023') return "Link properties";
    if(id=='3024') return "Remove Link";
    if(id=='3025') return "Path:";
    if(id=='3026') return "Type:";
    if(id=='3027') return "Url:";
    if(id=='3028') return "Target:";
    if(id=='3029') return "Title:";
    if(id=='3030') return "Select document";
    if(id=='3031') return "Input Properties";
    if(id=='3032') return "Select Properties";
    if(id=='3033') return "Textarea Properties";
    if(id=='3033') return "Textarea Properties";
    // new in 3.5
    if(id=='962') return "ID:";
    if(id=='963') return "Name:";
    if(id=='964') return "ID/Name";
    if(id=='899') return "OK";
    if(id=='1007') return "Path:";
    if(id=='3100') return "Make editable";
    if(id=='3101') return "No color";

    if(id=='3201') return "Upload";
    if(id=='3202') return "File Upload";
    if(id=='3203') return "Server Path:";
    if(id=='3204') return "File:";

    if(id=='3210') return "Name";
    if(id=='3211') return "Size";
    if(id=='3212') return "Type";
    if(id=='3213') return "Changed";

    if(id=='3300') return "Save local";
    if(id=='3301') return "Set non editable";
	if(id=='3414') return "Set editable";
    if(id=='3302') return "Flash properties";
    if(id=='3303') return "Insert Flash";
    if(id=='3304') return "Insert Media";
    if(id=='3305') return "Play:";
    if(id=='3306') return "Loop:";
    if(id=='3307') return "Menu:";
    if(id=='3308') return "Quality:";
    if(id=='3309') return "Parameter";
    if(id=='3310') return "Convert Row(td &lt;--&gt; th)";
    if(id=='3311') return "Please select a file !";
    if(id=='3312') return "Remove Table";
    if(id=='3313') return "New with template";
    if(id=='3314') return "Print Preview";
    if(id=='3315') return "Remove style/class";
    if(id=='3316') return "Remove formatting";
    if(id=='3317') return "Remove empty tags";
    if(id=='3318') return "Remove Word specific";
    if(id=='3319') return "Zoom";
    //if(id=='3320') return "Select text snippet";
	if(id=='3320') return "Select Signature";
    if(id=='3321') return "Source:";
    if(id=='3322') return "Float:";

    return "Missing"
  }

  if(language=="DE") {
    if(id=='101') return "Neu";
    if(id=='102') return "�ffnen";
    if(id=='103') return "Drucken";
    if(id=='104') return "Ausschneiden";
    if(id=='105') return "Kopieren";
    if(id=='106') return "Einf�gen";
    if(id=='107') return "R�ckg�ngig";
    if(id=='108') return "Wiederherstellen";
    if(id=='109') return "Link einf�gen";
    if(id=='110') return "Bild einf�gen";
    if(id=='111') return "Tabelle einf�gen";
    if(id=='112') return "Linie einf�gen";
    if(id=='113') return "Suche";
    if(id=='114') return "Hilfe";
    if(id=='115') return "Sonderzeichen einf�gen";
    if(id=='116') return "Datum einf�gen";
    if(id=='117') return "Zeit einf�gen";
    if(id=='118') return "Speichern";

    if(id=='201') return "Fett";
    if(id=='202') return "Kursiv";
    if(id=='203') return "Unterstrichen";
    if(id=='204') return "Superscript";
    if(id=='205') return "Subscript";
    if(id=='206') return "Linksb�ndig";
    if(id=='207') return "Zentriert";
    if(id=='208') return "Rechtsb�ndig";
    if(id=='209') return "Block";
    if(id=='210') return "Numerierung";
    if(id=='211') return "Aufz�hlungszeichen";
    if(id=='212') return "Einzug verkleinern";
    if(id=='213') return "Einzug vergr��ern";
    if(id=='214') return "Format entfernen";
    if(id=='215') return "Textfarbe setzen";
    if(id=='216') return "Hintergrundfarbe setzen";
    if(id=='217') return "Textfarbe ausw�hlen";
    if(id=='218') return "Hintergrundfarbe ausw�hlen";

    if(id=='301') return "Text einf�gen";
    if(id=='302') return "Button einf�gen";
    if(id=='303') return "Eingabefeld einf�gen";
    if(id=='304') return "Checkbox einf�gen";
    if(id=='305') return "Radio einf�gen";
    if(id=='306') return "Combobox einf�gen";
    if(id=='307') return "Listbox einf�gen";
    if(id=='308') return "Textarea einf�gen";
    if(id=='309') return "Unterseite einf�gen";
    if(id=='310') return "Container einf�gen";
    if(id=='311') return "Absolute Position";
    if(id=='312') return "Textbaustein einf�gen";
    if(id=='313') return "Neuen Textbaustein anlegen";
    if(id=='314') return "Aktiven Textbaustein l�schen";

    if(id=='401') return "Bearbeiten";
    if(id=='402') return "HTML";
    if(id=='403') return "Vorschau";
    if(id=='404') return "Seitenumbruch";
    if(id=='405') return "Alles markieren";
    if(id=='406') return "Formular einf�gen";
    if(id=='407') return "Anker einf�gen";
    if(id=='408') return "Von Word einf�gen";
    if(id=='409') return "Laufband";
    if(id=='410') return "Unsichtbares Feld einf�gen";
    if(id=='411') return "Rechtschreibpr�fung";

    if(id=='501') return " Tabelle";
    if(id=='502') return "Abbrechen";

    if(id=='601') return "Weitere Farben...";

    if(id=='701') return "Lokales Bild einf�gen";
    if(id=='702') return "Bild aus Web einf�gen";
    if(id=='703') return "Bild vom Server einf�gen";
    if(id=='704') return "Bild auf Server laden";

    if(id=='801') return "Zeile einf�gen (davor)";
    if(id=='802') return "Zeile einf�gen (danach)";
    if(id=='803') return "Zeile l�schen";
    if(id=='804') return "Spalte einf�gen (davor)";
    if(id=='805') return "Spalte einf�gen (danach)";
    if(id=='806') return "Spalte l�schen";
    if(id=='807') return "Zelle l�schen";
    if(id=='808') return "Zelle verbinden";
    if(id=='809') return "Zelle aufteilen";
    if(id=='810') return "Zellen Eigenschaften...";
    if(id=='811') return "Tabellen Eigenschaften...";
    if(id=='812') return "Seiten Eigenschaften...";
    if(id=='813') return "Bild Eigenschaften...";
    if(id=='814') return "In den Vordergrund";
    if(id=='815') return "Container Eigenschaften...";
    if(id=='816') return "Sub Page Eigenschaften...";
    if(id=='817') return "Text Eigenschaften...";

    if(id=='900') return "Abbrechen";
    if(id=='901') return "Tabellen Eigenschaften";
    if(id=='902') return "Prozent";
    if(id=='903') return "Absolut";
    if(id=='904') return "Cell spacing:";
    if(id=='905') return "Rahmenbreite:";
    if(id=='906') return "Cell padding:";
    if(id=='907') return "Rahmenfarbe:";
    if(id=='908') return "Breite:";
    if(id=='909') return "Hintergrundfarbe:";
    if(id=='911') return "Zellen Eigenschaften";
    if(id=='912') return "Hor. Ausrichtung:";
    if(id=='913') return "H�he:";
    if(id=='914') return "Vert. Ausrichtung:";
    if(id=='915') return "Umbruch:";
    if(id=='916') return "Rahmenfarbe:";
    if(id=='917') return "Hintergrundfarbe:";
    if(id=='918') return "Breite:";
    if(id=='921') return "Bild Eigenschaften";
    if(id=='922') return "Breite:";
    if(id=='923') return "Vert. Ausrichtung:";
    if(id=='924') return "H�he:";
    if(id=='925') return "Rahmenbreite:";
    if(id=='926') return "Titel:";
    if(id=='927') return "Rahmenfarbe:";
    if(id=='931') return "Text Eigenschaften";
    if(id=='932') return "Breite:";
    if(id=='933') return "Rahmenfarbe:";
    if(id=='934') return "H�he:";
    if(id=='935') return "Hintergrundfarbe:";
    if(id=='941') return "Container Eigenschaften";
    if(id=='942') return "Breite:";
    if(id=='943') return "Rahmenfarbe:";
    if(id=='944') return "H�he:";
    if(id=='945') return "Hintergrund:";
    if(id=='951') return "Seiten Eigenschaften";
    if(id=='952') return "Rand:";
    if(id=='953') return "Hintergrundfarbe:";
    if(id=='961') return "Farbe ausw�hlen";
    if(id=='971') return "Sub Page Eigenschaften";
    if(id=='972') return "Breite:";
    if(id=='973') return "H�he:";
    if(id=='974') return "URL:";
    if(id=='981') return "Bild vom Web einf�gen";
    if(id=='982') return "URL eingeben:";
    if(id=='991') return "Suchen/Ersetzen";
    if(id=='992') return "Suchen:";
    if(id=='993') return "Ersetzen:";
    if(id=='994') return "Suchen";
    if(id=='995') return "Ersetzen";
    if(id=='996') return "Bitte geben Sie einen Suchbegriff ein !";
    if(id=='997') return "Das Ende des Dokuments wurde erreicht oder der Suchbegriff wurde nicht gefunden. Die Suche wird am Dokumentanfang fortgesetzt !";
    if(id=='998') return "Bitte geben Sie einen Ersetzungsbegriff ein !";

    if(id=='1001') return "�ffnen/Speichern Dialog";
    if(id=='1002') return "�ffnen";
    if(id=='1003') return "Speichern";
    if(id=='1004') return "�ffnen";
    if(id=='1005') return "Dateiname:";
    if(id=='1006') return "Vorschau:";

    if(id=='1101') return "Anker anlegen/bearbeiten";
    if(id=='1102') return "Anker Name:";
    if(id=='1103') return "Anker Eigenschaften";
    if(id=='1104') return "Anker l�schen";

    if(id=='1201') return "Formular Eigenschaften";
    if(id=='1202') return "Anker Name:";
    if(id=='1203') return "Formular Eigenschaften";
    if(id=='1204') return "Formular l�schen";

    if(id=='1301') return "Feld Eigenschaften";
    if(id=='1302') return "Auswahlelement Eigenschaften";
    if(id=='1303') return "Gr��e";
    if(id=='1304') return "Farbe";

    if(id=='2000') return "In den Hintergrund";
    if(id=='2001') return "Formatvorlage";

    // new in 3.0
    if(id=='119') return "Speichern unter";
    if(id=='3000') return "Von links nach rechts";
    if(id=='3001') return "Von rechts nach links";
    if(id=='3002') return "Absatz einf�gen";
    if(id=='3003') return "Tabelle markieren";
    if(id=='3004') return "Hor. Ausrichtung:";
    if(id=='3005') return "Hochladen und Einf�gen";
    if(id=='3006') return "Bitte zuerst einen Bereich selektieren !";
    if(id=='3007') return "Name:";
    if(id=='3008') return "Textmodul anlegen";
    if(id=='3009') return "Bitte einen g�ltigen Namen eingeben !";
    if(id=='3010') return "Dokument auf Server laden";
    if(id=='3011') return "Auf Server laden";
    if(id=='3012') return "Bitte zuerst den Linktext selektieren !";
    if(id=='3013') return "RETURN Modus setzen";
    if(id=='3014') return "Formatvorlage ausw�hlen";
    if(id=='3015') return "Format ausw�hlen";
    if(id=='3016') return "Schriftart ausw�hlen";
    if(id=='3017') return "Schriftgr��e ausw�hlen";
    if(id=='3018') return "Datei";
    if(id=='3019') return "Bearbeiten";
    if(id=='3020') return "Einf�gen";
    if(id=='3021') return "Format";
    if(id=='3022') return "Formulare";
    if(id=='3023') return "Link Eigenschaften";
    if(id=='3024') return "Link l�schen";
    if(id=='3025') return "Pfad:";
    if(id=='3026') return "Typ:";
    if(id=='3027') return "Url:";
    if(id=='3028') return "Ziel:";
    if(id=='3029') return "Titel:";
    if(id=='3030') return "Dokument ausw�hlen";
    if(id=='3031') return "Eingabefeld Eigenschaften";
    if(id=='3032') return "Auswahlelement Eigenschaften";
    if(id=='3033') return "Textarea Eigenschaften";

    // new in 3.5
    if(id=='962') return "ID:";
    if(id=='963') return "Name:";
    if(id=='964') return "ID/Name";
    if(id=='899') return "OK";
    if(id=='1007') return "Pfad:";
    if(id=='3100') return "Editierbar";
    if(id=='3101') return "Keine";

    if(id=='3201') return "Upload";
    if(id=='3202') return "Datei Upload";
    if(id=='3203') return "Server Pfad:";
    if(id=='3204') return "Datei:";

    if(id=='3210') return "Name";
    if(id=='3211') return "Gr��e";
    if(id=='3212') return "Typ";
    if(id=='3213') return "Ge�ndert am";

    if(id=='3300') return "Lokal speichern";
    if(id=='3301') return "Nicht editierbar";
    if(id=='3302') return "Flash Eigenschaften";
    if(id=='3303') return "Flash einf�gen";
    if(id=='3304') return "Media einf�gen";
    if(id=='3305') return "Play:";
    if(id=='3306') return "Loop:";
    if(id=='3307') return "Menu:";
    if(id=='3308') return "Quality:";
    if(id=='3309') return "Parameter";
    if(id=='3310') return "Zeile konvertieren (td &lt;--&gt; th)";
    if(id=='3311') return "Bitte w�hlen Sie eine Datei aus !";
    if(id=='3312') return "Tabelle l�schen";
    if(id=='3313') return "Neu mit Template";
    if(id=='3314') return "Druckvorschau";
    if(id=='3315') return "Style/class Attribute entfernen";
    if(id=='3316') return "Formatierung entfernen";
    if(id=='3317') return "Leere Tags entfernen";
    if(id=='3318') return "Word spezifische Tags entfernen";
    if(id=='3319') return "Zoom";
    if(id=='3320') return "Textbaustein ausw�hlen";
    if(id=='3321') return "URL:";
    if(id=='3322') return "Textflu�:";

    return "Missing"
  }

  // spanish
  if(language=="ES") {
    if(id=='101') return "Nuevo";
    if(id=='102') return "Abrir";
    if(id=='103') return "Imprimir";
    if(id=='104') return "Cortar";
    if(id=='105') return "Copiar";
    if(id=='106') return "Pegar";
    if(id=='107') return "Deshacer";
    if(id=='108') return "Rehacer";
    if(id=='109') return "Insertar Enlace";
    if(id=='110') return "Insertar Imagen";
    if(id=='111') return "Insertar Tabla";
    if(id=='112') return "Insertar Regla";
    if(id=='113') return "Buscar";
    if(id=='114') return "Ayuda";
    if(id=='115') return "Insertar caracteres especiales";
    if(id=='116') return "Insertar Fecha";
    if(id=='117') return "Insertar Hora";
    if(id=='118') return "Guardar";

    if(id=='201') return "Negrita";
    if(id=='202') return "Cursiva";
    if(id=='203') return "Subrayado";
    if(id=='204') return "Super�ndice";
    if(id=='205') return "Sub�ndice";
    if(id=='206') return "Alinear a la izquierda";
    if(id=='207') return "Centrar";
    if(id=='208') return "Alinear a la derecha";
    if(id=='209') return "Justificar";
    if(id=='210') return "Lista ordenada";
    if(id=='211') return "Lista no ordenada";
    if(id=='212') return "Aumentar sangr�a";
    if(id=='213') return "Disminuir sangr�a";
    if(id=='214') return "Eliminar formato";
    if(id=='215') return "Asignar el color del texto";
    if(id=='216') return "Asignar el color de fondo";
    if(id=='217') return "Elegir el color del texto";
    if(id=='218') return "Elegir el color de fondo";

    if(id=='301') return "Insertar etiqueta";
    if(id=='302') return "Insertar bot�n";
    if(id=='303') return "Insertar campo de entrada";
    if(id=='304') return "Insertar casilla de verificaci�n";
    if(id=='305') return "Insertar casilla de opci�n";
    if(id=='306') return "Insertar cuadro combinado";
    if(id=='307') return "Insertar cuadro de lista";
    if(id=='308') return "Insertar �rea de texto";
    if(id=='309') return "Insertar subp�gina";
    if(id=='310') return "Insertar contenedor";
    if(id=='311') return "Posici�n absoluta";
    if(id=='312') return "Insertar m�dulo de texto";
    if(id=='313') return "Nuevo m�dulo de texto";
    if(id=='314') return "Eliminar el m�dulo de texto activo";

    if(id=='401') return "Editar";
    if(id=='402') return "HTML";
    if(id=='403') return "Vista preliminar";
    if(id=='404') return "Salto de p�gina";
    if(id=='405') return "Seleccionarlo todo";
    if(id=='406') return "Insertar formulario";
    if(id=='407') return "Insertar Anchor";
    if(id=='408') return "Pegar desde Word";
    if(id=='409') return "Marquee";
    if(id=='410') return "Insertar campo oculto";
    if(id=='411') return "Ortograf�a";

    if(id=='501') return " Tabla";
    if(id=='502') return "Cancelar";

    if(id=='601') return "M�s colores...";

    if(id=='701') return "Insertar imagen local";
    if(id=='702') return "Insertar imagen web";
    if(id=='703') return "Insert imagen de servidor";
    if(id=='704') return "Subir imagen local";

    if(id=='801') return "Insertar fila en la parte superior";
    if(id=='802') return "Insertar fila en la parte inferior";
    if(id=='803') return "Borrar fila";
    if(id=='804') return "Insertar columna a la izquierda";
    if(id=='805') return "Insertar columna a la derecha";
    if(id=='806') return "Borrar columna";
    if(id=='807') return "Borrar celda";
    if(id=='808') return "Combinar celdas";
    if(id=='809') return "Dividir celda";
    if(id=='810') return "Propiedades de la celda...";
    if(id=='811') return "Propiedades de la tabla...";
    if(id=='812') return "Propiedades de la p�gina...";
    if(id=='813') return "Propiedades de la imagen...";
    if(id=='814') return "Asignar al primer plano";
    if(id=='815') return "Propiedades del contenedor...";
    if(id=='816') return "Propiedades de la subp�gina...";
    if(id=='817') return "Propiedades de la etiqueta...";

    if(id=='900') return "Cancelar";
    if(id=='901') return "Propiedades de la tabla";
    if(id=='902') return "Porcentaje";
    if(id=='903') return "Absoluto";
    if(id=='904') return "Espaciado de celdas:";
    if(id=='905') return "Anchura del borde:";
    if(id=='906') return "Separaci�n de celdas:";
    if(id=='907') return "Color del borde:";
    if(id=='908') return "Anchura:";
    if(id=='909') return "Color del fondo:";
    if(id=='911') return "Propiedades de la celda";
    if(id=='912') return "Alineaci�n horiz.:";
    if(id=='913') return "Altura:";
    if(id=='914') return "Alineaci�n Vert.:";
    if(id=='915') return "Ajustar:";
    if(id=='916') return "Color del borde:";
    if(id=='917') return "Color de fondo:";
    if(id=='918') return "Anchura:";
    if(id=='921') return "Propiedades de la imagen";
    if(id=='922') return "Anchura:";
    if(id=='923') return "Alineaci�n Vert.:";
    if(id=='924') return "Altura:";
    if(id=='925') return "Anchura del borde:";
    if(id=='926') return "T�tulo:";
    if(id=='927') return "Color del borde:";
    if(id=='931') return "Propiedades de la etiqueta";
    if(id=='932') return "Anchura:";
    if(id=='933') return "Color del borde:";
    if(id=='934') return "Altura:";
    if(id=='935') return "Color de fondo:";
    if(id=='941') return "Propiedades del contenedor";
    if(id=='942') return "Anchura:";
    if(id=='943') return "Color del borde:";
    if(id=='944') return "Altura:";
    if(id=='945') return "Color de fondo:";
    if(id=='951') return "Propiedades de la p�gina";
    if(id=='952') return "Margen:";
    if(id=='953') return "Color del fondo:";
    if(id=='961') return "Seleccionar color";
    if(id=='971') return "Propiedades de la subp�gina";
    if(id=='972') return "Anchura:";
    if(id=='973') return "Altura:";
    if(id=='974') return "URL:";
    if(id=='981') return "Insertar im�gen web";
    if(id=='982') return "Entrar URL:";
    if(id=='991') return "Buscar/Reemplazar";
    if(id=='992') return "Buscar:";
    if(id=='993') return "Reemplazar:";
    if(id=='994') return "Buscar";
    if(id=='995') return "Reemplazar";
    if(id=='996') return "�Por favor, entre el texto a buscar!";
    if(id=='997') return "�Se ha llegado al final del documento o no se ha encontrado el texto buscado. Se contin�a la b�squeda desde el principio del documento!";
    if(id=='998') return "�Por favor, entre el texto de reemplazo!";

    if(id=='1001') return "Di�logo Abrir/Guardar";
    if(id=='1002') return "Abrir";
    if(id=='1003') return "Guardar";
    if(id=='1004') return "Abrir";
    if(id=='1005') return "Nombre de archivo:";
    if(id=='1006') return "Vista preliminar:";

    if(id=='1101') return "Crear/editar anclaje";
    if(id=='1102') return "Nombre del anclaje:";
    if(id=='1103') return "Propiedades del anclaje";
    if(id=='1104') return "Eliminar anclaje";

    if(id=='1201') return "Propiedades del formulario";
    if(id=='1202') return "Nombre del anclaje:";
    if(id=='1203') return "Propiedades del formulario";
    if(id=='1204') return "Eliminar formulario";

    if(id=='1301') return "Entrar propiedades";
    if(id=='1302') return "Seleccionar propiedades";
    if(id=='1303') return "Medida";
    if(id=='1304') return "Color";

    if(id=='2000') return "Asignar como fondo";
    if(id=='2001') return "Todo";

    // new in 3.0
    // TODO
    if(id=='119') return "Save as";
    if(id=='3000') return "Left to right";
    if(id=='3001') return "Right to left";
    if(id=='3002') return "Insert Paragraph";
    if(id=='3003') return "Table highlight";
    if(id=='3004') return "Hor. Alignment:";
    if(id=='3005') return "Upload and Insert";
    if(id=='3006') return "Please select a text range first !";
    if(id=='3007') return "Name:";
    if(id=='3008') return "Create text module";
    if(id=='3009') return "Please enter a valid text module name !";
    if(id=='3010') return "Upload document";
    if(id=='3011') return "Upload";
    if(id=='3012') return "Please select a text first !";
    if(id=='3013') return "Set RETURN mode";
    if(id=='3014') return "Select Style Sheet";
    if(id=='3015') return "Select Format";
    if(id=='3016') return "Select Font";
    if(id=='3017') return "Select Fontsize";
    if(id=='3018') return "File";
    if(id=='3019') return "Edit";
    if(id=='3020') return "Insert";
    if(id=='3021') return "Format";
    if(id=='3022') return "Forms";
    if(id=='3023') return "Link properties";
    if(id=='3024') return "Remove Link";
    if(id=='3025') return "Path:";
    if(id=='3026') return "Type:";
    if(id=='3027') return "Url:";
    if(id=='3028') return "Target:";
    if(id=='3029') return "Title:";
    if(id=='3030') return "Select document";
    if(id=='3031') return "Input Properties";
    if(id=='3032') return "Select Properties";
    if(id=='3033') return "Textarea Properties";

    // new in 3.1
    if(id=='962') return "ID:";
    if(id=='963') return "Name:";
    if(id=='964') return "ID/Name";
    if(id=='899') return "OK";
    if(id=='1007') return "Path:";
    if(id=='3100') return "Make editable";
    if(id=='3101') return "No color";

    if(id=='3201') return "Upload";
    if(id=='3202') return "File Upload";
    if(id=='3203') return "Server Path:";
    if(id=='3204') return "File:";

    if(id=='3210') return "Name";
    if(id=='3211') return "Size";
    if(id=='3212') return "Type";
    if(id=='3213') return "Changed";

    return "Missing";
  }

  if(language=="FR") {
    if(id=='101') return "Nouveau";
    if(id=='102') return "Ouvrir";
    if(id=='103') return "Imprimer";
    if(id=='104') return "Couper";
    if(id=='105') return "Copier";
    if(id=='106') return "Coller";
    if(id=='107') return "Annuler";
    if(id=='108') return "R�p�ter";
    if(id=='109') return "Ins�rer un lien";
    if(id=='110') return "Ins�rer une image";
    if(id=='111') return "Ins�rer un tableau";
    if(id=='112') return "Ins�rer une ligne";
    if(id=='113') return "Rechercher";
    if(id=='114') return "Aide";
    if(id=='115') return "Ins�rer un caract�re sp�cial";
    if(id=='116') return "Ins�rer la date";
    if(id=='117') return "Ins�rer l'heure";
    if(id=='118') return "Enregistrer";

    if(id=='201') return "Gras";
    if(id=='202') return "Italique";
    if(id=='203') return "Soulign�";
    if(id=='204') return "Exposant";
    if(id=='205') return "Indice";
    if(id=='206') return "Align� � gauche";
    if(id=='207') return "Centr�";
    if(id=='208') return "Align� � droite";
    if(id=='209') return "Justifier";
    if(id=='210') return "Num�rotation";
    if(id=='211') return "Puces";
    if(id=='212') return "Augmenter le retrait";
    if(id=='213') return "Diminuer le retrait";
    if(id=='214') return "Enlever le format";
    if(id=='215') return "Couleur du texte";
    if(id=='216') return "Couleur de fond";
    if(id=='217') return "Choisir la couleur du texte";
    if(id=='218') return "Choisir la couleur de fond";

    if(id=='301') return "Ins�rer une �tiquette";
    if(id=='302') return "Ins�rer un bouton";
    if(id=='303') return "Ins�rer un champ texte";
    if(id=='304') return "Ins�rer une case � cocher";
    if(id=='305') return "Ins�rer un bouton radio";
    if(id=='306') return "Ins�rer une boite de s�lection";
    if(id=='307') return "Ins�rer un menu d�roulant";
    if(id=='308') return "Ins�rer une zone de texte";
    if(id=='309') return "Ins�rer une sous-page";
    if(id=='310') return "Ins�rer un conteneur";
    if(id=='311') return "Position absolue";
    if(id=='312') return "Ins�rer un module texte";
    if(id=='313') return "Nouveau module texte";
    if(id=='314') return "Enlever le module texte";

    if(id=='401') return "Edition";
    if(id=='402') return "HTML";
    if(id=='403') return "Pr�visualisation";
    if(id=='404') return "Saut de page";
    if(id=='405') return "Tout s�lectionner";
    if(id=='406') return "Ajouter un formulaire";
    if(id=='407') return "Ins�rer une Ancre";
    if(id=='408') return "Coller depuis Word";
    if(id=='409') return "D�filement";
    if(id=='410') return "Ins�rer un champ cach�";
    if(id=='411') return "Correcteur orthographique";

    if(id=='501') return " Tableau";
    if(id=='502') return "Annuler";

    if(id=='601') return "Autres couleurs...";

    if(id=='701') return "Ins�rer une image locale";
    if(id=='702') return "Ins�rer une image web";
    if(id=='703') return "Ins�rer une image du serveur";
    if(id=='704') return "T�l�charger une image locale";

    if(id=='801') return "Ins�rer une ligne avant";
    if(id=='802') return "Ins�rer une ligne apr�s";
    if(id=='803') return "Effacer la ligne";
    if(id=='804') return "Ins�rer une colonne avant";
    if(id=='805') return "Ins�rer une colonne apr�s";
    if(id=='806') return "Effacer la colonne";
    if(id=='807') return "Effacer la cellule";
    if(id=='808') return "Marge de la cellule";
    if(id=='809') return "Diviser la cellule";
    if(id=='810') return "Propri�t�s de la cellule...";
    if(id=='811') return "Propri�t�s du tableau...";
    if(id=='812') return "Propri�t�s de la page...";
    if(id=='813') return "Propri�t�s de l'image...";
    if(id=='814') return "Placer en avant-plan";
    if(id=='815') return "Propri�t�s du conteneur...";
    if(id=='816') return "Propri�t�s de la sous-page...";
    if(id=='817') return "Propri�t�s de l'�tiquette...";

    if(id=='900') return "Annuler";
    if(id=='901') return "Propri�t�s du tableau";
    if(id=='902') return "Pourcent";
    if(id=='903') return "Absolu";
    if(id=='904') return "Espacement externe de la cellule:";
    if(id=='905') return "Largeur de la bordure:";
    if(id=='906') return "Espacement interne de la cellule:";
    if(id=='907') return "Couleur de la bordure:";
    if(id=='908') return "Largeur:";
    if(id=='909') return "Couleur de fond:";
    if(id=='911') return "Propri�t�s de la cellule";
    if(id=='912') return "Alignement horizontal:";
    if(id=='913') return "Hauteur:";
    if(id=='914') return "Alignement vertical:";
    if(id=='915') return "Saut de ligne:";
    if(id=='916') return "Couleur de la bordure:";
    if(id=='917') return "Couleur de fond:";
    if(id=='918') return "Largeur:";
    if(id=='921') return "Propri�t� de l'image";
    if(id=='922') return "Largeur:";
    if(id=='923') return "Alignement vertical:";
    if(id=='924') return "Hauteur:";
    if(id=='925') return "Largeur de la bordure:";
    if(id=='926') return "Titre:";
    if(id=='927') return "Couleur de la bordure:";
    if(id=='931') return "Propri�t� de l'�tiquette";
    if(id=='932') return "Largeur:";
    if(id=='933') return "Couleur de la bordure:";
    if(id=='934') return "Hauteur:";
    if(id=='935') return "Couleur de fond:";
    if(id=='941') return "Propri�t�s du conteneur";
    if(id=='942') return "Largeur:";
    if(id=='943') return "Couleur de la bordure:";
    if(id=='944') return "Hauteur:";
    if(id=='945') return "Couleur de fond:";
    if(id=='951') return "Propri�t� de la page";
    if(id=='952') return "Marge:";
    if(id=='953') return "Couleur de fond:";
    if(id=='961') return "Choisissez la couleur";
    if(id=='971') return "Propri�t� de la sous-page";
    if(id=='972') return "Largeur:";
    if(id=='973') return "Hauteur:";
    if(id=='974') return "URL:";
    if(id=='981') return "Ins�rer une image WEB";
    if(id=='982') return "Entrer l'URL:";
    if(id=='991') return "Chercher/Remplacer";
    if(id=='992') return "Chercher:";
    if(id=='993') return "Remplacer:";
    if(id=='994') return "Chercher";
    if(id=='995') return "Remplacer";
    if(id=='996') return "Entrez un mot-clef !";
    if(id=='997') return "La fin du document est atteinte ou le mot-clef n'a pas �t� trouv�. La recherche continue au d�but du document !";
    if(id=='998') return "Entrez une expression de remplacement!";

    if(id=='1001') return "Ouvrier/Enregistrer";
    if(id=='1002') return "Ouvrir";
    if(id=='1003') return "Enregistrer";
    if(id=='1004') return "Ouvrir";
    if(id=='1005') return "Nom du fichier:";
    if(id=='1006') return "Pr�visualisation:";

    if(id=='1101') return "Cr�er/Editer ancre";
    if(id=='1102') return "Nom de l'ancre:";
    if(id=='1103') return "Propri�t� de l'ancre";
    if(id=='1104') return "Enlever l'ancre";

    if(id=='1201') return "Propri�t�s du formulaire";
    if(id=='1202') return "Nom de l'ancre:";
    if(id=='1203') return "Propri�t�s du formulaire";
    if(id=='1204') return "Enlever le formulaire";

    if(id=='1301') return "Propri�t�s de la saisie";
    if(id=='1302') return "Propri�t�s du menu";
    if(id=='1303') return "Taille";
    if(id=='1304') return "Couleur";

    if(id=='2000') return "Placer en arri�re-plan";
    if(id=='2001') return "Style";

    // new in 3.0
    if(id=='119') return "Enregistrer sous";
    if(id=='3000') return "De gauche � droite";
    if(id=='3001') return "De droite � gauche";
    if(id=='3002') return "Ins�rer un paragraphe";
    if(id=='3003') return "Faire ressortir la table";
    if(id=='3004') return "Alignement horizontal:";
    if(id=='3005') return "T�l�charger et ins�rer";
    if(id=='3006') return "S�lectionnez d'abord une partie de texte !";
    if(id=='3007') return "Nom:";
    if(id=='3008') return "Cr�er un module de texte";
    if(id=='3009') return "Entrez un nom de module de texte valide !";
    if(id=='3010') return "T�l�charger document";
    if(id=='3011') return "T�l�charger";
    if(id=='3012') return "S�lectionner d'abord un texte !";
    if(id=='3013') return "Choisir le mode RETOUR LIGNE";
    if(id=='3014') return "S�lectionner la feuille de style";
    if(id=='3015') return "S�lectionner le format";
    if(id=='3016') return "S�lectionner la police";
    if(id=='3017') return "S�lectionner la taille de la police";
    if(id=='3018') return "Fichier";
    if(id=='3019') return "Edition";
    if(id=='3020') return "Insertion";
    if(id=='3021') return "Format";
    if(id=='3022') return "Formulaires";
    if(id=='3023') return "Propri�t�s du lien";
    if(id=='3024') return "Enlever le lien";
    if(id=='3025') return "Chemin d'acc�s:";
    if(id=='3026') return "Type:";
    if(id=='3027') return "URL:";
    if(id=='3028') return "Cible:";
    if(id=='3029') return "Titre:";
    if(id=='3030') return "S�lectionner le document";
    if(id=='3031') return "Propri�t�s de la case de saisie";
    if(id=='3032') return "Propri�t�s du menu d�roulant";
    if(id=='3033') return "Propri�t�s du champ texte";

    // new in 3.5
    // TODO
    if(id=='962') return "ID:";
    if(id=='963') return "Name:";
    if(id=='964') return "ID/Name";
    if(id=='899') return "OK";
    if(id=='1007') return "Path:";
    if(id=='3100') return "Make editable";
    if(id=='3101') return "No color";

    if(id=='3201') return "Upload";
    if(id=='3202') return "File Upload";
    if(id=='3203') return "Server Path:";
    if(id=='3204') return "File:";

    if(id=='3210') return "Name";
    if(id=='3211') return "Size";
    if(id=='3212') return "Type";
    if(id=='3213') return "Changed";

    if(id=='3300') return "Save local";
    if(id=='3301') return "Set non editable";
    if(id=='3302') return "Flash properties";
    if(id=='3303') return "Insert Flash";
    if(id=='3304') return "Insert Media";
    if(id=='3305') return "Play:";
    if(id=='3306') return "Loop:";
    if(id=='3307') return "Menu:";
    if(id=='3308') return "Quality:";
    if(id=='3309') return "Parameter";
    if(id=='3310') return "Convert Row(td &lt;--&gt; th)";
    if(id=='3311') return "Please select a file !";
    if(id=='3312') return "Remove Table";
    if(id=='3313') return "New with template";
    if(id=='3314') return "Print Preview";
    if(id=='3315') return "Remove style/class";
    if(id=='3316') return "Remove formatting";
    if(id=='3317') return "Remove empty tags";
    if(id=='3318') return "Remove Word specific";
    if(id=='3319') return "Zoom";
    if(id=='3320') return "Select text snippet";

    return "Manquant"
  }


  // hebrew
  if(language=="HE") {
    if(id=='101') return "���";
    if(id=='102') return "���";
    if(id=='103') return "����";
    if(id=='104') return "����";
    if(id=='105') return "����";
    if(id=='106') return "����";
    if(id=='107') return "��� �����";
    if(id=='108') return "��� ���";
    if(id=='109') return "����� �����";
    if(id=='110') return "����� �����";
    if(id=='111') return "����� ����";
    if(id=='112') return "����� ��";
    if(id=='113') return "�����";
    if(id=='114') return "����";
    if(id=='115') return "����� ����� �������";
    if(id=='116') return "����� �����";
    if(id=='117') return "����� ���";
    if(id=='118') return "����";
    if(id=='119') return "����� �����";
    if(id=='120') return "����� �����";
    if(id=='121') return "����� ���� ����";

    if(id=='201') return "�����";
    if(id=='202') return "����";
    if(id=='203') return "�� �����";
    if(id=='204') return "��� ����";
    if(id=='205') return "��� ����";
    if(id=='206') return "��� �����";
    if(id=='207') return "��� �����";
    if(id=='208') return "��� �����";
    if(id=='209') return "����";
    if(id=='210') return "����� �������";
    if(id=='211') return "����� �� �����";
    if(id=='212') return "���� �����";
    if(id=='213') return "���� �����";
    if(id=='214') return "��� �����";
    if(id=='215') return "��� ��� ����";
    if(id=='216') return "��� ��� ����";
    if(id=='217') return "��� ��� ����";
    if(id=='218') return "��� ��� ����";

    if(id=='301') return "���� ����";
    if(id=='302') return "���� �����";
    if(id=='303') return "���� ��� ���";
    if(id=='304') return "���� ���� �����";
    if(id=='305') return "���� ����� ����";
    if(id=='306') return "���� ���� �����";
    if(id=='307') return "���� ����� �����";
    if(id=='308') return "���� ����� �����";
    if(id=='309') return "���� ���� �����";
    if(id=='310') return "���� ����";
    if(id=='311') return "����� �����";
    if(id=='312') return "���� ���� ����";
    if(id=='313') return "���� ���� ���";
    if(id=='314') return "��� ���� ���� ����";

    if(id=='401') return "�����";
    if(id=='402') return "��� ����";
    if(id=='403') return "����� ������";
    if(id=='404') return "���� ����";
    if(id=='405') return "��� ���";
    if(id=='406') return "���� ����";
    if(id=='407') return "���� ����";
    if(id=='408') return "���� �����";
    if(id=='409') return "Marquee";
    if(id=='410') return "���� ��� ����";
    if(id=='411') return "����� ����";

    if(id=='501') return "����";
    if(id=='502') return "�����";

    if(id=='601') return "����� ������...";

    if(id=='701') return "���� ����� ������ ������";
    if(id=='702') return "���� ����� ���� ��������";
    if(id=='703') return "���� ����� �����";
    if(id=='704') return "���� ����� �� ����";

    if(id=='801') return "���� ���� ����";
    if(id=='802') return "���� ���� ����";
    if(id=='803') return "��� ����";
    if(id=='804') return "���� ����� ����";
    if(id=='805') return "���� ����� ����";
    if(id=='806') return "��� �����";
    if(id=='807') return "��� ��";
    if(id=='808') return "��� ����";
    if(id=='809') return "��� ����";
    if(id=='810') return "������� ��...";
    if(id=='811') return "������� ����...";
    if(id=='812') return "������� ����...";
    if(id=='813') return "������� �����...";
    if(id=='814') return "���� ����";
    if(id=='815') return "������� ����...";
    if(id=='816') return "������� ���� �����...";
    if(id=='817') return "������� ����...";

    if(id=='900') return "����";
    if(id=='901') return "������� ����";
    if(id=='902') return "�������";
    if(id=='903') return "�����";
    if(id=='904') return "���� ��� ����:";
    if(id=='905') return "���� ������:";
    if(id=='906') return "���� ����:";
    if(id=='907') return "��� ����:";
    if(id=='908') return "����:";
    if(id=='909') return "��� ���:";
    if(id=='911') return "������� ��";
    if(id=='912') return "���� �����:";
    if(id=='913') return "����:";
    if(id=='914') return "���� ����:";
    if(id=='915') return "�����:";
    if(id=='916') return "��� ����:";
    if(id=='917') return "��� ���:";
    if(id=='918') return "����:";
    if(id=='921') return "������� �����";
    if(id=='922') return "����:";
    if(id=='923') return "���� ����:";
    if(id=='924') return "����:";
    if(id=='925') return "���� ����:";
    if(id=='926') return "�����:";
    if(id=='927') return "��� ����:";
    if(id=='931') return "������� ����";
    if(id=='932') return "����:";
    if(id=='933') return "��� ����:";
    if(id=='934') return "����:";
    if(id=='935') return "��� ���:";
    if(id=='941') return "������� ����";
    if(id=='942') return "����:";
    if(id=='943') return "��� ����:";
    if(id=='944') return "����:";
    if(id=='945') return "��� �����:";
    if(id=='951') return "������� ����";
    if(id=='952') return "����:";
    if(id=='953') return "��� ���:";
    if(id=='961') return "��� ���";
    if(id=='971') return "������� ���� �����";
    if(id=='972') return "����:";
    if(id=='973') return "����:";
    if(id=='974') return "URL:";
    if(id=='981') return "���� ����� �����";
    if(id=='982') return "���� URL:";
    if(id=='991') return "�����/�����";
    if(id=='992') return "�����:";
    if(id=='993') return "�����:";
    if(id=='994') return "�����";
    if(id=='995') return "�����";
    if(id=='996') return "��� ���� ������ ������ !";
    if(id=='997') return "��� �����, �� �������� �� �����, ������ ����� ���� ��� !";
    if(id=='998') return "��� ���� ������ ������ !";

    if(id=='1001') return "�����/�����";
    if(id=='1002') return "�����";
    if(id=='1003') return "�����";
    if(id=='1004') return "�����";
    if(id=='1005') return "�� ����:";
    if(id=='1006') return "����� ������:";

    if(id=='1101') return "���/���� ����";
    if(id=='1102') return "�� ����:";
    if(id=='1103') return "������� ����";
    if(id=='1104') return "��� ����";

    if(id=='1201') return "������� ����";
    if(id=='1202') return "�� ����:";
    if(id=='1203') return "������� ����";
    if(id=='1204') return "��� ����";

    if(id=='1301') return "������� ���";
    if(id=='1302') return "������� �����";
    if(id=='1303') return "����";
    if(id=='1304') return "���";

    if(id=='2000') return "���� ����";
    if(id=='2001') return "Todo";

    // new in 3.0
    // TODO
    if(id=='119') return "Save as";
    if(id=='3000') return "Left to right";
    if(id=='3001') return "Right to left";
    if(id=='3002') return "Insert Paragraph";
    if(id=='3003') return "Table highlight";
    if(id=='3004') return "Hor. Alignment:";
    if(id=='3005') return "Upload and Insert";
    if(id=='3006') return "Please select a text range first !";
    if(id=='3007') return "Name:";
    if(id=='3008') return "Create text module";
    if(id=='3009') return "Please enter a valid text module name !";
    if(id=='3010') return "Upload document";
    if(id=='3011') return "Upload";
    if(id=='3012') return "Please select a text first !";
    if(id=='3013') return "Set RETURN mode";
    if(id=='3014') return "Select Style Sheet";
    if(id=='3015') return "Select Format";
    if(id=='3016') return "Select Font";
    if(id=='3017') return "Select Fontsize";
    if(id=='3018') return "File";
    if(id=='3019') return "Edit";
    if(id=='3020') return "Insert";
    if(id=='3021') return "Format";
    if(id=='3022') return "Forms";
    if(id=='3023') return "Link properties";
    if(id=='3024') return "Remove Link";
    if(id=='3025') return "Path:";
    if(id=='3026') return "Type:";
    if(id=='3027') return "Url:";
    if(id=='3028') return "Target:";
    if(id=='3029') return "Title:";
    if(id=='3030') return "Select document";
    if(id=='3031') return "Input Properties";
    if(id=='3032') return "Select Properties";
    if(id=='3033') return "Textarea Properties";

    // new in 3.1
    if(id=='962') return "ID:";
    if(id=='963') return "Name:";
    if(id=='964') return "ID/Name";
    if(id=='899') return "OK";
    if(id=='1007') return "Path:";
    if(id=='3100') return "Make editable";
    if(id=='3101') return "No color";

    if(id=='3201') return "Upload";
    if(id=='3202') return "File Upload";
    if(id=='3203') return "Server Path:";
    if(id=='3204') return "File:";

    if(id=='3210') return "Name";
    if(id=='3211') return "Size";
    if(id=='3212') return "Type";
    if(id=='3213') return "Changed";

    return "�� �����"
  }

  // dutch
  if(language=="NL") {
    if(id=='101') return "Nieuw";
    if(id=='102') return "Open";
    if(id=='103') return "Afdrukken";
    if(id=='104') return "Knippen";
    if(id=='105') return "Kopi�ren";
    if(id=='106') return "Plakken";
    if(id=='107') return "Ongedaan maken";
    if(id=='108') return "Herhalen";
    if(id=='109') return "Link Invoegen";
    if(id=='110') return "Afbeelding Invoegen";
    if(id=='111') return "Tabel Invoegen";
    if(id=='112') return "Regel Invoegen";
    if(id=='113') return "Zoeken";
    if(id=='114') return "Help";
    if(id=='115') return "Speciale Karakters Invoegen";
    if(id=='116') return "Datum Invoegen";
    if(id=='117') return "Tijdstip Invoegen";
    if(id=='118') return "Opslaan";
    if(id=='119') return "Opslaan Als";

    if(id=='201') return "Vet";
    if(id=='202') return "Cursief";
    if(id=='203') return "Onderstrepen";
    if(id=='204') return "Superscript";
    if(id=='205') return "Subscript";
    if(id=='206') return "Links uitlijnen";
    if(id=='207') return "Centreren";
    if(id=='208') return "Rechts uitlijnen";
    if(id=='209') return "Uitvullen";
    if(id=='210') return "Nummering";
    if(id=='211') return "Opsommingstekens";
    if(id=='212') return "Insprong vergroten";
    if(id=='213') return "Insprong verkleinen";
    if(id=='214') return "Formatering verwijderen";
    if(id=='215') return "Tekstkleur instellen";
    if(id=='216') return "Achtergrondkleur instellen";
    if(id=='217') return "Tekstkleur kiezen";
    if(id=='218') return "Achtergrondkleur kiezen";

    if(id=='301') return "Label invoegen";
    if(id=='302') return "Button invoegen";
    if(id=='303') return "Input invoegen";
    if(id=='304') return "Checkbox invoegen";
    if(id=='305') return "Radio invoegen";
    if(id=='306') return "Combobox invoegen";
    if(id=='307') return "Listbox invoegen";
    if(id=='308') return "Text Area invoegen";
    if(id=='309') return "Subpagina invoegen";
    if(id=='310') return "Container invoegen";
    if(id=='311') return "Absolute positionering";
    if(id=='312') return "Tekstmodule invoegen";
    if(id=='313') return "Nieuwe tekstmodule";
    if(id=='314') return "Actieve tekstmodule verwijderen";

    if(id=='401') return "Edit";
    if(id=='402') return "HTML";
    if(id=='403') return "Preview";
    if(id=='404') return "Page break";
    if(id=='405') return "Alles selecteren";
    if(id=='406') return "Formulier invoegen";
    if(id=='407') return "Anchor invoegen";
    if(id=='408') return "Plakken uit Word";
    if(id=='409') return "Marquee";
    if(id=='410') return "Hidden field invoegen";
    if(id=='411') return "Spellingchecker";

    if(id=='501') return " Tabel";
    if(id=='502') return "Annuleren";

    if(id=='601') return "Meer kleuren...";

    if(id=='701') return "Lokale afbeelding";
    if(id=='702') return "Web afbeelding";
    if(id=='703') return "Server afbeelding";
    if(id=='704') return "Afbeelding uploaden";

    if(id=='801') return "Rij vooraan invoegen";
    if(id=='802') return "Rij achteraan invoegen";
    if(id=='803') return "Rij verwijderen";
    if(id=='804') return "Kolom vooraan invoegen";
    if(id=='805') return "Kolom achteraan invoegen";
    if(id=='806') return "Kolom verwijderen";
    if(id=='807') return "Cel verwijderen";
    if(id=='808') return "Cel samenvoegen";
    if(id=='809') return "Cel splitsen";
    if(id=='810') return "Cel eigenschappen...";
    if(id=='811') return "Tabel eigenschappen...";
    if(id=='812') return "Pagina eigenschappen...";
    if(id=='813') return "Afbeelding eigenschappen...";
    if(id=='814') return "Naar de voorgrond brengen";
    if(id=='815') return "Container eigenschappen...";
    if(id=='816') return "Subpagina eigenschappen...";
    if(id=='817') return "Label eigenschappen...";

    if(id=='900') return "Annuleren";
    if(id=='901') return "Tabel Eigenschappen";
    if(id=='902') return "Percent";
    if(id=='903') return "Absoluut";
    if(id=='904') return "Cel spati�ring:";
    if(id=='905') return "Randbreedte:";
    if(id=='906') return "Cel padding:";
    if(id=='907') return "Randkleur:";
    if(id=='908') return "Breedte:";
    if(id=='909') return "Achtergrondkleur:";
    if(id=='911') return "Cel Eigenschappen";
    if(id=='912') return "Hor. aligneren:";
    if(id=='913') return "Hoogte:";
    if(id=='914') return "Vert. aligneren:";
    if(id=='915') return "Wrap:";
    if(id=='916') return "Randkleur:";
    if(id=='917') return "Achtergrondkleur:";
    if(id=='918') return "Breedte:";
    if(id=='921') return "Afbeelding Eigenschappen";
    if(id=='922') return "Breedte:";
    if(id=='923') return "Vert. aligneren:";
    if(id=='924') return "Hoogte:";
    if(id=='925') return "Randbreedte:";
    if(id=='926') return "Titel:";
    if(id=='927') return "Randkleur:";
    if(id=='931') return "Label Eigenschappen";
    if(id=='932') return "Breedte:";
    if(id=='933') return "Randkkleur:";
    if(id=='934') return "Hoogte:";
    if(id=='935') return "Achtergrondkleur:";
    if(id=='941') return "Container Eigenschappen";
    if(id=='942') return "Breedte:";
    if(id=='943') return "Randkleur:";
    if(id=='944') return "Hoogte:";
    if(id=='945') return "Achtergrondkleur:";
    if(id=='951') return "Pagina Eigenschappen";
    if(id=='952') return "Paginarand:";
    if(id=='953') return "Achtergrondkleur:";
    if(id=='961') return "Kleur kiezen";
    if(id=='971') return "Subpagina Eigenschappen";
    if(id=='972') return "Breedte:";
    if(id=='973') return "Hoogte:";
    if(id=='974') return "URL:";
    if(id=='981') return "Web afbeelding invoegen";
    if(id=='982') return "Voer een URL in:";
    if(id=='991') return "Zoeken/Vervangen";
    if(id=='992') return "Zoek:";
    if(id=='993') return "Vervang:";
    if(id=='994') return "Zoeken";
    if(id=='995') return "Vervangen";
    if(id=='996') return "Gelieve uw zoekcriterium op te geven!";
    if(id=='997') return "Einde van het document. De search werd niet teruggevonden. Zoekopdracht herhalen vanaf het begin!";
    if(id=='998') return "Gelieve uw zoekcriterium op te geven!";

    if(id=='1001') return "Open/Opslaan dialoog";
    if(id=='1002') return "Open";
    if(id=='1003') return "Opslaan";
    if(id=='1004') return "Open";
    if(id=='1005') return "Bestandsnaam:";
    if(id=='1006') return "Preview:";

    if(id=='1101') return "Anchor cre�ren/bewerken";
    if(id=='1102') return "Anchor naam:";
    if(id=='1103') return "Anchor eigenschappen";
    if(id=='1104') return "Anchor verwijderen";

    if(id=='1201') return "Form eigenschappen";
    if(id=='1202') return "Anchor naam:";
    if(id=='1203') return "Form eigenschappen";
    if(id=='1204') return "Form verwijderen";

    if(id=='1301') return "Input eigenschappen";
    if(id=='1302') return "Select eigenschappen";
    if(id=='1303') return "Grootte";
    if(id=='1304') return "Kleur";

    if(id=='2000') return "Naar de achtergrond brengen";
    if(id=='2001') return "Todo";

    // new in 3.0
    if(id=='119') return "Bewaren Als";
    if(id=='3000') return "Links naar rechts";
    if(id=='3001') return "Rechts naar links";
    if(id=='3002') return "Alinea invoegen";
    if(id=='3003') return "Tabel highlight";
    if(id=='3004') return "Hor. Aligneren:";
    if(id=='3005') return "Upload invoegen";
    if(id=='3006') return "Gelieve een tekstblok te selecteren !";
    if(id=='3007') return "Naam:";
    if(id=='3008') return "Tekstmodule aanmaken";
    if(id=='3009') return "Gelieve een geldige tekstmodulenaam te gebruiken!";
    if(id=='3010') return "Upload document";
    if(id=='3011') return "Upload";
    if(id=='3012') return "Gelieve eerst tekst te selecteren!";
    if(id=='3013') return "Set RETURN modus";
    if(id=='3014') return "Stylesheet kiezen";
    if(id=='3015') return "Formaat kiezen";
    if(id=='3016') return "Lettertype kiezen";
    if(id=='3017') return "Lettergrootte kiezen";
    if(id=='3018') return "Bestand";
    if(id=='3019') return "Aanpassen";
    if(id=='3020') return "Invoegen";
    if(id=='3021') return "Formaat";
    if(id=='3022') return "Formulieren";
    if(id=='3023') return "Link eigenschappen";
    if(id=='3024') return "Link verwijderen";
    if(id=='3025') return "Pad:";
    if(id=='3026') return "Type:";
    if(id=='3027') return "Url:";
    if(id=='3028') return "Target:";
    if(id=='3029') return "Titel:";
    if(id=='3030') return "Document kiezen";
    if(id=='3031') return "Input Eigenschappen";
    if(id=='3032') return "Select Eigenschappen";
    if(id=='3033') return "Textarea Eigenschappen";

    // new in 3.1
    if(id=='962') return "ID:";
    if(id=='963') return "Name:";
    if(id=='964') return "ID/Name";
    if(id=='899') return "OK";
    if(id=='1007') return "Path:";
    if(id=='3100') return "Make editable";
    if(id=='3101') return "No color";

    if(id=='3201') return "Upload";
    if(id=='3202') return "File Upload";
    if(id=='3203') return "Server Path:";
    if(id=='3204') return "File:";

    if(id=='3210') return "Name";
    if(id=='3211') return "Size";
    if(id=='3212') return "Type";
    if(id=='3213') return "Changed";

    return "Missing"
  }

  // greek
  if(language == "GR") {
    if(id=='101') return "\u039D\u03AD\u03BF";
    if(id=='102') return "'\u0391\u03BD\u03BF\u03B9\u03B3\u03BC\u03B1";
    if(id=='103') return "\u0395\u03BA\u03C4\u03CD\u03C0\u03C9\u03C3\u03B7";
    if(id=='104') return "\u0391\u03C0\u03BF\u03BA\u03BF\u03C0\u03AE";
    if(id=='105') return "\u0391\u03BD\u03C4\u03B9\u03B3\u03C1\u03B1\u03C6\u03AE";
    if(id=='106') return "\u0395\u03C0\u03B9\u03BA\u03CC\u03BB\u03BB\u03B7\u03C3\u03B7";
    if(id=='107') return "\u0391\u03BD\u03AC\u03BA\u03BB\u03B7\u03C3\u03B7 \u03C4\u03B5\u03BB\u03B5\u03C5\u03C4\u03B1\u03AF\u03B1\u03C2 \u03B5\u03BD\u03AD\u03C1\u03B3\u03B5\u03B9\u03B1\u03C2";
    if(id=='108') return "\u0395\u03C0\u03B1\u03BD\u03AC\u03BB\u03B7\u03C8\u03B7 \u03C4\u03B5\u03BB\u03B5\u03C5\u03C4\u03B1\u03AF\u03B1\u03C2 \u03B5\u03BD\u03AD\u03C1\u03B3\u03B5\u03B9\u03B1\u03C2";
    if(id=='109') return "\u0395\u03B9\u03C3\u03B1\u03B3\u03C9\u03B3\u03AE \u03C3\u03C5\u03BD\u03B4\u03AD\u03C3\u03BC\u03BF\u03C5";
    if(id=='110') return "\u0395\u03B9\u03C3\u03B1\u03B3\u03C9\u03B3\u03AE \u03B5\u03B9\u03BA\u03CC\u03BD\u03B1\u03C2";
    if(id=='111') return "\u0395\u03B9\u03C3\u03B1\u03B3\u03C9\u03B3\u03AE \u03C0\u03AF\u03BD\u03B1\u03BA\u03B1";
    if(id=='112') return "\u0395\u03B9\u03C3\u03B1\u03B3\u03C9\u03B3\u03AE \u03BF\u03C1\u03B9\u03B6\u03CC\u03BD\u03C4\u03B9\u03B1\u03C2 \u03B3\u03C1\u03B1\u03BC\u03BC\u03AE\u03C2";
    if(id=='113') return "\u0391\u03BD\u03B1\u03B6\u03AE\u03C4\u03B7\u03C3\u03B7";
    if(id=='114') return "\u0392\u03BF\u03AE\u03B8\u03B5\u03B9\u03B1";
    if(id=='115') return "\u0395\u03B9\u03C3\u03B1\u03B3\u03C9\u03B3\u03AE \u03B5\u03B9\u03B4\u03B9\u03BA\u03CE\u03BD \u03C7\u03B1\u03C1\u03B1\u03BA\u03C4\u03AE\u03C1\u03C9\u03BD";
    if(id=='116') return "\u0395\u03B9\u03C3\u03B1\u03B3\u03C9\u03B3\u03AE \u03B7\u03BC\u03B5\u03C1\u03BF\u03BC\u03B7\u03BD\u03AF\u03B1\u03C2";
    if(id=='117') return "\u0395\u03B9\u03C3\u03B1\u03B3\u03C9\u03B3\u03AE \u03CE\u03C1\u03B1\u03C2";
    if(id=='118') return "\u0391\u03C0\u03BF\u03B8\u03AE\u03BA\u03B5\u03C5\u03C3\u03B7";

    if(id=='201') return "\u0388\u03BD\u03C4\u03BF\u03BD\u03BF \u03BA\u03B5\u03AF\u03BC\u03B5\u03BD\u03BF";
    if(id=='202') return "\u03A0\u03BB\u03AC\u03B3\u03B9\u03BF \u03BA\u03B5\u03AF\u03BC\u03B5\u03BD\u03BF";
    if(id=='203') return "\u03A5\u03C0\u03BF\u03B3\u03C1\u03B1\u03BC\u03BC\u03B9\u03C3\u03BC\u03AD\u03BD\u03BF \u03BA\u03B5\u03AF\u03BC\u03B5\u03BD\u03BF";
    if(id=='204') return "Superscript";
    if(id=='205') return "Subscript";
    if(id=='206') return "\u0391\u03C1\u03B9\u03C3\u03C4\u03B5\u03C1\u03AE \u03B5\u03C5\u03B8\u03C5\u03B3\u03C1\u03AC\u03BC\u03BC\u03B9\u03C3\u03B7 \u03BA\u03B5\u03B9\u03BC\u03AD\u03BD\u03BF\u03C5";
    if(id=='207') return "\u0388\u03C5\u03B8\u03C5\u03B3\u03C1\u03AC\u03BC\u03BC\u03B9\u03C3\u03B7 \u03BA\u03B5\u03B9\u03BC\u03AD\u03BD\u03BF\u03C5 \u03C3\u03C4\u03BF \u03BA\u03AD\u03BD\u03C4\u03C1\u03BF";
    if(id=='208') return "\u0388\u03C5\u03B8\u03C5\u03B3\u03C1\u03AC\u03BC\u03BC\u03B9\u03C3\u03B7 \u03BA\u03B5\u03B9\u03BC\u03AD\u03BD\u03BF\u03C5 \u03C3\u03C4\u03B1 \u03B4\u03B5\u03BE\u03B9\u03AC";
    if(id=='209') return "\u03A0\u03BB\u03AE\u03C1\u03B7\u03C2 \u03B5\u03C5\u03B8\u03C5\u03B3\u03C1\u03AC\u03BC\u03BC\u03B9\u03C3\u03B7 \u03BA\u03B5\u03B9\u03BC\u03AD\u03BD\u03BF\u03C5";
    if(id=='210') return "\u0391\u03C1\u03B9\u03B8\u03BC\u03B7\u03C4\u03B9\u03BA\u03AE \u03BB\u03AF\u03C3\u03C4\u03B1";
    if(id=='211') return "\u039C\u03B7-\u03B1\u03C1\u03B9\u03B8\u03BC\u03B7\u03C4\u03B9\u03BA\u03AE \u03BB\u03AF\u03C3\u03C4\u03B1";
    if(id=='212') return "\u039C\u03B5\u03C4\u03B1\u03BA\u03AF\u03BD\u03B7\u03C3\u03B7 \u03BA\u03B5\u03B9\u03BC\u03AD\u03BD\u03BF\u03C5 \u03AD\u03BD\u03B1 \u03B5\u03C0\u03AF\u03C0\u03B5\u03B4\u03BF \u03C0\u03C1\u03BF\u03C2 \u03C4\u03B1 \u03BC\u03AD\u03C3\u03B1";
    if(id=='213') return "\u039C\u03B5\u03C4\u03B1\u03BA\u03AF\u03BD\u03B7\u03C3\u03B7 \u03BA\u03B5\u03B9\u03BC\u03AD\u03BD\u03BF\u03C5 \u03AD\u03BD\u03B1 \u03B5\u03C0\u03AF\u03C0\u03B5\u03B4\u03BF \u03C0\u03C1\u03BF\u03C2 \u03C4\u03B1 \u03AD\u03BE\u03C9";
    if(id=='214') return "\u0391\u03C6\u03B1\u03AF\u03C1\u03B5\u03C3\u03B7 \u03BC\u03BF\u03C1\u03C6\u03BF\u03C0\u03BF\u03AF\u03B7\u03C3\u03B7\u03C2";
    if(id=='215') return "\u039F\u03C1\u03B9\u03C3\u03BC\u03CC\u03C2 \u03C7\u03C1\u03CE\u03BC\u03B1\u03C4\u03BF\u03C2 \u03BA\u03B5\u03B9\u03BC\u03AD\u03BD\u03BF\u03C5";
    if(id=='216') return "\u039F\u03C1\u03B9\u03C3\u03BC\u03CC\u03C2 \u03C7\u03C1\u03CE\u03BC\u03B1\u03C4\u03BF\u03C2 \u03C6\u03CC\u03BD\u03C4\u03BF\u03C5";
    if(id=='217') return "\u0395\u03C0\u03B9\u03BB\u03BF\u03B3\u03AE \u03C7\u03C1\u03CE\u03BC\u03B1\u03C4\u03BF\u03C2 \u03BA\u03B5\u03B9\u03BC\u03AD\u03BD\u03BF\u03C5";
    if(id=='218') return "\u0395\u03C0\u03B9\u03BB\u03BF\u03B3\u03AE \u03C7\u03C1\u03CE\u03BC\u03B1\u03C4\u03BF\u03C2 \u03C6\u03CC\u03BD\u03C4\u03BF\u03C5";

    if(id=='301') return "\u0395\u03B9\u03C3\u03B1\u03B3\u03C9\u03B3\u03AE \u03B5\u03C4\u03B9\u03BA\u03AD\u03C4\u03B1\u03C2";
    if(id=='302') return "\u0395\u03B9\u03C3\u03B1\u03B3\u03C9\u03B3\u03AE \u03BA\u03BF\u03C5\u03BC\u03C0\u03B9\u03BF\u03CD";
    if(id=='303') return "\u0395\u03B9\u03C3\u03B1\u03B3\u03C9\u03B3\u03AE HTML input";
    if(id=='304') return "\u0395\u03B9\u03C3\u03B1\u03B3\u03C9\u03B3\u03AE HTML checkbox";
    if(id=='305') return "\u0395\u03B9\u03C3\u03B1\u03B3\u03C9\u03B3\u03AE HTML radio";
    if(id=='306') return "\u0395\u03B9\u03C3\u03B1\u03B3\u03C9\u03B3\u03AE \u03B1\u03BD\u03B1\u03C0\u03C4\u03C5\u03C3\u03C3\u03CC\u03BC\u03B5\u03BD\u03B7\u03C2 \u03BB\u03AF\u03C3\u03C4\u03B1\u03C2 \u03B5\u03C0\u03B9\u03BB\u03BF\u03B3\u03CE\u03BD";
    if(id=='307') return "\u0395\u03B9\u03C3\u03B1\u03B3\u03C9\u03B3\u03AE \u03BB\u03AF\u03C3\u03C4\u03B1\u03C2 \u03B5\u03C0\u03B9\u03BB\u03BF\u03B3\u03CE\u03BD";
    if(id=='308') return "\u0395\u03B9\u03C3\u03B1\u03B3\u03C9\u03B3\u03AE \u03C0\u03B5\u03C1\u03B9\u03BF\u03C7\u03AE\u03C2 \u03BA\u03B5\u03B9\u03BC\u03AD\u03BD\u03BF\u03C5";
    if(id=='309') return "\u0395\u03B9\u03C3\u03B1\u03B3\u03C9\u03B3\u03AE \u03C5\u03C0\u03BF\u03C3\u03B5\u03BB\u03AF\u03B4\u03B1\u03C2";
    if(id=='310') return "\u0395\u03B9\u03C3\u03B1\u03B3\u03C9\u03B3\u03AE container";
    if(id=='311') return "\u0391\u03C0\u03CC\u03BB\u03C5\u03C4\u03B7 \u03B8\u03AD\u03C3\u03B7";
    if(id=='312') return "\u0395\u03B9\u03C3\u03B1\u03B3\u03C9\u03B3\u03AE text module";
    if(id=='313') return "\u039D\u03AD\u03BF text module";
    if(id=='314') return "\u0391\u03C6\u03B1\u03AF\u03C1\u03B5\u03C3\u03B7 \u03B5\u03BD\u03B5\u03C1\u03B3\u03BF\u03CD text module";

    if(id=='401') return "\u0395\u03C0\u03B5\u03BE\u03B5\u03C1\u03B3\u03B1\u03C3\u03AF\u03B1";
    if(id=='402') return "HTML";
    if(id=='403') return "\u03A0\u03C1\u03BF\u03B5\u03C0\u03B9\u03C3\u03BA\u03CC\u03C0\u03B9\u03C3\u03B7";
    if(id=='404') return "\u0395\u03B9\u03C3\u03B1\u03B3\u03C9\u03B3\u03AE \u03B4\u03B9\u03B1\u03BA\u03BF\u03C0\u03AE\u03C2 \u03C3\u03B5\u03BB\u03AF\u03B4\u03B1\u03C2";
    if(id=='405') return "\u0395\u03C0\u03B9\u03BB\u03BF\u03B3\u03AE \u03CC\u03BB\u03C9\u03BD";
    if(id=='406') return "\u0395\u03B9\u03C3\u03B1\u03B3\u03C9\u03B3\u03AE HTML \u03C6\u03CC\u03C1\u03BC\u03B1\u03C2";
    if(id=='407') return "\u0395\u03B9\u03C3\u03B1\u03B3\u03C9\u03B3\u03AE HTML anchor";
    if(id=='408') return "\u0395\u03C0\u03B9\u03BA\u03CC\u03BB\u03BB\u03B7\u03C3\u03B7 \u03B1\u03C0\u03CC \u03C4\u03BF Word";
    if(id=='409') return "\u0395\u03B9\u03C3\u03B1\u03B3\u03C9\u03B3\u03AE \u03BA\u03B9\u03BD\u03BF\u03CD\u03BC\u03B5\u03BD\u03BF\u03C5 \u03BA\u03B5\u03B9\u03BC\u03AD\u03BD\u03BF\u03C5 (HTML Marquee)";
    if(id=='410') return "\u0395\u03B9\u03C3\u03B1\u03B3\u03C9\u03B3\u03AE \u03BA\u03C1\u03C5\u03C6\u03BF\u03CD \u03C0\u03B5\u03B4\u03AF\u03BF\u03C5 (HTML Hidden)";
    if(id=='411') return "\u03A3\u03C5\u03BB\u03BB\u03B1\u03B2\u03B9\u03C3\u03BC\u03CC\u03C2";

    if(id=='501') return " \u03A0\u03AF\u03BD\u03B1\u03BA\u03B1\u03C2";
    if(id=='502') return "\u0391\u03BA\u03CD\u03C1\u03C9\u03C3\u03B7";

    if(id=='601') return "\u03A0\u03B5\u03C1\u03B9\u03C3\u03C3\u03CC\u03C4\u03B5\u03C1\u03B1 \u03C7\u03C1\u03CE\u03BC\u03B1\u03C4\u03B1...";

    if(id=='701') return "\u0395\u03B9\u03C3\u03B1\u03B3\u03C9\u03B3\u03AE \u03B1\u03C1\u03C7\u03B5\u03AF\u03BF\u03C5 \u03B5\u03B9\u03BA\u03CC\u03BD\u03B1\u03C2 \u03B1\u03C0\u03CC \u03C4\u03BF\u03C0\u03B9\u03BA\u03CC \u03B4\u03AF\u03C3\u03BA\u03BF";
    if(id=='702') return "\u0395\u03B9\u03C3\u03B1\u03B3\u03C9\u03B3\u03AE \u03B1\u03C1\u03C7\u03B5\u03AF\u03BF\u03C5 \u03B5\u03B9\u03BA\u03CC\u03BD\u03B1\u03C2 \u03B1\u03C0\u03CC \u0394\u03B9\u03B1\u03B4\u03AF\u03BA\u03C4\u03C5\u03BF";
    if(id=='703') return "\u0395\u03B9\u03C3\u03B1\u03B3\u03C9\u03B3\u03AE \u03B1\u03C1\u03C7\u03B5\u03AF\u03BF\u03C5 \u03B5\u03B9\u03BA\u03CC\u03BD\u03B1\u03C2 \u03B1\u03C0\u03CC \u03B5\u03BE\u03C5\u03C0\u03B7\u03C1\u03B5\u03C4\u03B7\u03C4\u03AE";
    if(id=='704') return "\u0391\u03C0\u03BF\u03C3\u03C4\u03BF\u03BB\u03AE \u03C4\u03BF\u03C0\u03B9\u03BA\u03BF\u03CD \u03B1\u03C1\u03C7\u03B5\u03AF\u03BF\u03C5 \u03B5\u03B9\u03BA\u03CC\u03BD\u03B1\u03C2 \u03C3\u03C4\u03BF\u03BD \u03B5\u03BE\u03C5\u03C0\u03B7\u03C1\u03B5\u03C4\u03B7\u03C4\u03AE";

    if(id=='801') return "\u0395\u03B9\u03C3\u03B1\u03B3\u03C9\u03B3\u03AE \u03B3\u03C1\u03B1\u03BC\u03BC\u03AE\u03C2 \u03C0\u03C1\u03B9\u03BD \u03B1\u03C0\u03CC \u03C4\u03B7\u03BD \u03C4\u03C1\u03AD\u03C7\u03BF\u03C5\u03C3\u03B1 \u03B3\u03C1\u03B1\u03BC\u03BC\u03AE";
    if(id=='802') return "\u0395\u03B9\u03C3\u03B1\u03B3\u03C9\u03B3\u03AE \u03B3\u03C1\u03B1\u03BC\u03BC\u03AE\u03C2 \u03BC\u03B5\u03C4\u03AC \u03C4\u03B7\u03BD \u03C4\u03C1\u03AD\u03C7\u03BF\u03C5\u03C3\u03B1 \u03B3\u03C1\u03B1\u03BC\u03BC\u03AE";
    if(id=='803') return "\u0394\u03B9\u03B1\u03B3\u03C1\u03B1\u03C6\u03AE \u03B3\u03C1\u03B1\u03BC\u03BC\u03AE\u03C2";
    if(id=='804') return "\u0395\u03B9\u03C3\u03B1\u03B3\u03C9\u03B3\u03AE \u03BA\u03BF\u03BB\u03CE\u03BD\u03B1\u03C2 \u03C0\u03C1\u03B9\u03BD \u03B1\u03C0\u03CC \u03C4\u03B7\u03BD \u03C4\u03C1\u03AD\u03C7\u03BF\u03C5\u03C3\u03B1 \u03BA\u03BF\u03BB\u03CE\u03BD\u03B1";
    if(id=='805') return "\u0395\u03B9\u03C3\u03B1\u03B3\u03C9\u03B3\u03AE \u03BA\u03BF\u03BB\u03CE\u03BD\u03B1\u03C2 \u03BC\u03B5\u03C4\u03AC \u03C4\u03B7\u03BD \u03C4\u03C1\u03AD\u03C7\u03BF\u03C5\u03C3\u03B1 \u03BA\u03BF\u03BB\u03CE\u03BD\u03B1";
    if(id=='806') return "\u0394\u03B9\u03B1\u03B3\u03C1\u03B1\u03C6\u03AE \u03BA\u03BF\u03BB\u03CE\u03BD\u03B1\u03C2";
    if(id=='807') return "\u0394\u03B9\u03B1\u03B3\u03C1\u03B1\u03C6\u03AE \u03BA\u03B5\u03BB\u03B9\u03BF\u03CD";
    if(id=='808') return "\u03A3\u03C5\u03B3\u03C7\u03CE\u03BD\u03B5\u03C5\u03C3\u03B7 \u03BA\u03B5\u03BB\u03B9\u03BF\u03CD";
    if(id=='809') return "\u0394\u03B9\u03B1\u03C7\u03C9\u03C1\u03B9\u03C3\u03BC\u03CC\u03C2 \u03BA\u03B5\u03BB\u03B9\u03BF\u03CD";
    if(id=='810') return "\u0399\u03B4\u03B9\u03CC\u03C4\u03B7\u03C4\u03B5\u03C2 \u03BA\u03B5\u03BB\u03B9\u03BF\u03CD...";
    if(id=='811') return "\u0399\u03B4\u03B9\u03CC\u03C4\u03B7\u03C4\u03B5\u03C2 \u03C0\u03AF\u03BD\u03B1\u03BA\u03B1...";
    if(id=='812') return "\u0399\u03B4\u03B9\u03CC\u03C4\u03B7\u03C4\u03B5\u03C2 \u03C3\u03B5\u03BB\u03AF\u03B4\u03B1\u03C2...";
    if(id=='813') return "\u0399\u03B4\u03B9\u03CC\u03C4\u03B7\u03C4\u03B5\u03C2 \u03B5\u03B9\u03BA\u03CC\u03BD\u03B1\u03C2...";
    if(id=='814') return "\u03A4\u03BF\u03C0\u03BF\u03B8\u03AD\u03C4\u03B7\u03C3\u03B7 \u03C3\u03C4\u03BF \u03C0\u03C1\u03BF\u03C3\u03BA\u03AE\u03BD\u03B9\u03BF";
    if(id=='815') return "\u0399\u03B4\u03B9\u03CC\u03C4\u03B7\u03C4\u03B5\u03C2 Container...";
    if(id=='816') return "\u0399\u03B4\u03B9\u03CC\u03C4\u03B7\u03C4\u03B5\u03C2 \u03C5\u03C0\u03BF\u03C3\u03B5\u03BB\u03AF\u03B4\u03B1\u03C2...";
    if(id=='817') return "\u0399\u03B4\u03B9\u03CC\u03C4\u03B7\u03C4\u03B5\u03C2 \u03B5\u03C4\u03B9\u03BA\u03AD\u03C4\u03B1\u03C2...";

    if(id=='900') return "\u0391\u03BA\u03CD\u03C1\u03C9\u03C3\u03B7";
    if(id=='901') return "\u0399\u03B4\u03B9\u03CC\u03C4\u03B7\u03C4\u03B5\u03C2 \u03C0\u03AF\u03BD\u03B1\u03BA\u03B1";
    if(id=='902') return "\u03A0\u03BF\u03C3\u03BF\u03C3\u03C4\u03CC";
    if(id=='903') return "\u0391\u03C0\u03CC\u03BB\u03C5\u03C4\u03BF\u03C2 \u03B1\u03C1\u03B9\u03B8\u03BC\u03CC\u03C2";
    if(id=='904') return "\u0391\u03C0\u03CC\u03C3\u03C4\u03B1\u03C3\u03B7 \u03B3\u03CD\u03C1\u03C9 \u03B1\u03C0\u03CC \u03C4\u03BF \u03BA\u03B5\u03BB\u03AF (\u03C3\u03B5 \u03B5\u03B9\u03BA\u03BF\u03BD\u03BF\u03C3\u03C4\u03BF\u03B9\u03C7\u03B5\u03AF\u03B1/pixels):";
    if(id=='905') return "\u03A0\u03BB\u03AC\u03C4\u03BF\u03C2 \u03C0\u03B5\u03C1\u03B9\u03B8\u03C9\u03C1\u03AF\u03BF\u03C5:";
    if(id=='906') return "\u0391\u03C0\u03CC\u03C3\u03C4\u03B1\u03C3\u03B7 \u03BC\u03B5\u03C4\u03B1\u03BE\u03CD \u03BA\u03B5\u03BB\u03B9\u03CE\u03BD (\u03C3\u03B5 \u03B5\u03B9\u03BA\u03BF\u03BD\u03BF\u03C3\u03C4\u03BF\u03B9\u03C7\u03B5\u03AF\u03B1/pixels):";
    if(id=='907') return "\u03A7\u03C1\u03CE\u03BC\u03B1 \u03C0\u03B5\u03C1\u03B9\u03B8\u03C9\u03C1\u03AF\u03BF\u03C5:";
    if(id=='908') return "\u03A0\u03BB\u03AC\u03C4\u03BF\u03C2:";
    if(id=='909') return "\u03A7\u03C1\u03CE\u03BC\u03B1 \u03C6\u03CC\u03BD\u03C4\u03BF\u03C5:";
    if(id=='911') return "\u0399\u03B4\u03B9\u03CC\u03C4\u03B7\u03C4\u03B5\u03C2 \u03BA\u03B5\u03BB\u03B9\u03BF\u03CD";
    if(id=='912') return "\u039F\u03C1\u03B9\u03B6\u03CC\u03BD\u03C4\u03B9\u03B1 \u03B5\u03C5\u03B8\u03C5\u03B3\u03C1\u03AC\u03BC\u03BC\u03B9\u03C3\u03B7:";
    if(id=='913') return "\u038E\u03C8\u03BF\u03C2:";
    if(id=='914') return "\u039A\u03AC\u03B8\u03B5\u03C4\u03B7 \u03B5\u03C5\u03B8\u03C5\u03B3\u03C1\u03AC\u03BC\u03BC\u03B9\u03C3\u03B7:";
    if(id=='915') return "\u0395\u03C0\u03B1\u03BD\u03B1\u03B4\u03AF\u03C0\u03BB\u03C9\u03C3\u03B7 \u03BA\u03B5\u03B9\u03BC\u03AD\u03BD\u03BF\u03C5:";
    if(id=='916') return "\u03A7\u03C1\u03CE\u03BC\u03B1 \u03C0\u03B5\u03C1\u03B9\u03B8\u03C9\u03C1\u03AF\u03BF\u03C5:";
    if(id=='917') return "\u03A7\u03C1\u03CE\u03BC\u03B1 \u03C6\u03CC\u03BD\u03C4\u03BF\u03C5:";
    if(id=='918') return "\u03A0\u03BB\u03AC\u03C4\u03BF\u03C2:";
    if(id=='921') return "\u0399\u03B4\u03B9\u03BF\u03C4\u03AE\u03C4\u03B5\u03C2 \u03B5\u03B9\u03BA\u03CC\u03BD\u03B1\u03C2";
    if(id=='922') return "\u03A0\u03BB\u03AC\u03C4\u03BF\u03C2:";
    if(id=='923') return "\u039A\u03AC\u03B8\u03B5\u03C4\u03B7 \u03B5\u03C5\u03B8\u03C5\u03B3\u03C1\u03AC\u03BC\u03BC\u03B9\u03C3\u03B7:";
    if(id=='924') return "\u038E\u03C8\u03BF\u03C2:";
    if(id=='925') return "\u03A0\u03BB\u03AC\u03C4\u03BF\u03C2 \u03C0\u03B5\u03C1\u03B9\u03B8\u03C9\u03C1\u03AF\u03BF\u03C5:";
    if(id=='926') return "\u03A4\u03AF\u03C4\u03BB\u03BF\u03C2:";
    if(id=='927') return "\u03A7\u03C1\u03CE\u03BC\u03B1 \u03C0\u03B5\u03C1\u03B9\u03B8\u03C9\u03C1\u03AF\u03BF\u03C5:";
    if(id=='931') return "\u0399\u03B4\u03B9\u03CC\u03C4\u03B7\u03C4\u03B5\u03C2 \u03B5\u03C4\u03B9\u03BA\u03AD\u03C4\u03B1\u03C2";
    if(id=='932') return "\u03A0\u03BB\u03AC\u03C4\u03BF\u03C2:";
    if(id=='933') return "\u03A7\u03C1\u03CE\u03BC\u03B1 \u03C0\u03B5\u03C1\u03B9\u03B8\u03C9\u03C1\u03AF\u03BF\u03C5:";
    if(id=='934') return "\u038E\u03C8\u03BF\u03C2:";
    if(id=='935') return "\u03A7\u03C1\u03CE\u03BC\u03B1 \u03C6\u03CC\u03BD\u03C4\u03BF\u03C5:";
    if(id=='941') return "\u0399\u03B4\u03B9\u03CC\u03C4\u03B7\u03C4\u03B5\u03C2 HTML Container";
    if(id=='942') return "\u03A0\u03BB\u03AC\u03C4\u03BF\u03C2:";
    if(id=='943') return "\u03A7\u03C1\u03CE\u03BC\u03B1 \u03C0\u03B5\u03C1\u03B9\u03B8\u03C9\u03C1\u03AF\u03BF\u03C5:";
    if(id=='944') return "\u038E\u03C8\u03BF\u03C2:";
    if(id=='945') return "\u03A7\u03C1\u03CE\u03BC\u03B1 \u03C6\u03CC\u03BD\u03C4\u03BF\u03C5:";
    if(id=='951') return "\u0399\u03B4\u03B9\u03CC\u03C4\u03B7\u03C4\u03B5\u03C2 \u03C3\u03B5\u03BB\u03AF\u03B4\u03B1\u03C2";
    if(id=='952') return "\u03A0\u03B5\u03C1\u03B9\u03B8\u03CE\u03C1\u03B9\u03BF:";
    if(id=='953') return "\u03A7\u03C1\u03CE\u03BC\u03B1 \u03C6\u03CC\u03BD\u03C4\u03BF\u03C5:";
    if(id=='961') return "\u0395\u03C0\u03B9\u03BB\u03BF\u03B3\u03AE \u03C7\u03C1\u03CE\u03BC\u03B1\u03C4\u03BF\u03C2";
    if(id=='971') return "\u0399\u03B4\u03B9\u03CC\u03C4\u03B7\u03C4\u03B5\u03C2 \u03C5\u03C0\u03BF\u03C3\u03B5\u03BB\u03AF\u03B4\u03B1\u03C2";
    if(id=='972') return "\u03A0\u03BB\u03AC\u03C4\u03BF\u03C2:";
    if(id=='973') return "\u038E\u03C8\u03BF\u03C2:";
    if(id=='974') return "URL:";
    if(id=='981') return "\u0395\u03B9\u03C3\u03B1\u03B3\u03C9\u03B3\u03AE \u03B5\u03B9\u03BA\u03CC\u03BD\u03B1\u03C2 \u03B1\u03C0\u03CC \u0394\u03B9\u03B1\u03B4\u03AF\u03BA\u03C4\u03C5\u03BF";
    if(id=='982') return "\u0395\u03B9\u03C3\u03AC\u03B3\u03B5\u03C4\u03B5 URL:";
    if(id=='991') return "\u0391\u03BD\u03B1\u03B6\u03AE\u03C4\u03B7\u03C3\u03B7/\u0391\u03BD\u03C4\u03B9\u03BA\u03B1\u03C4\u03AC\u03C3\u03C4\u03B1\u03C3\u03B7";
    if(id=='992') return "\u0391\u03BD\u03B1\u03B6\u03AE\u03C4\u03B7\u03C3\u03B7:";
    if(id=='993') return "\u0391\u03BD\u03C4\u03B9\u03BA\u03B1\u03C4\u03AC\u03C3\u03C4\u03B1\u03C3\u03B7:";
    if(id=='994') return "\u0391\u03BD\u03B1\u03B6\u03AE\u03C4\u03B7\u03C3\u03B7";
    if(id=='995') return "\u0391\u03BD\u03C4\u03B9\u03BA\u03B1\u03C4\u03AC\u03C3\u03C4\u03B1\u03C3\u03B7";
    if(id=='996') return "\u03A0\u03B1\u03C1\u03B1\u03BA\u03B1\u03BB\u03CE \u03B5\u03B9\u03C3\u03AC\u03B3\u03B5\u03C4\u03B5 \u03AD\u03BD\u03B1 \u03B1\u03BB\u03C6\u03B1\u03C1\u03B9\u03B8\u03BC\u03B7\u03C4\u03B9\u03BA\u03CC \u03B3\u03B9\u03B1 \u03B1\u03BD\u03B1\u03B6\u03AE\u03C4\u03B7\u03C3\u03B7 !";
    if(id=='997') return "\u0392\u03C1\u03AD\u03B8\u03B7\u03BA\u03B5 \u03C4\u03BF \u03C4\u03AD\u03BB\u03BF\u03C2 \u03C4\u03BF\u03C5 \u03B5\u03B3\u03B3\u03C1\u03AC\u03C6\u03BF\u03C5 \u03AE \u03B4\u03B5 \u03B2\u03C1\u03AD\u03B8\u03B7\u03BA\u03B5 \u03C4\u03BF \u03B1\u03BB\u03C6\u03B1\u03C1\u03B9\u03B8\u03BC\u03B7\u03C4\u03B9\u03BA\u03CC \u03C0\u03BF\u03C5 \u03B1\u03BD\u03B1\u03B6\u03B7\u03C4\u03AE\u03C3\u03B1\u03C4\u03B5. \u0397 \u03B1\u03BD\u03B1\u03B6\u03AE\u03C4\u03B7\u03C3\u03B7 \u03B8\u03B1 \u03C3\u03C5\u03BD\u03B5\u03C7\u03B9\u03C3\u03C4\u03B5\u03AF \u03B1\u03C0\u03CC \u03C4\u03B7\u03BD \u03B1\u03C1\u03C7\u03AE \u03C4\u03BF\u03C5 \u03B5\u03B3\u03B3\u03C1\u03AC\u03C6\u03BF\u03C5 !";
    if(id=='998') return "\u03A0\u03B1\u03C1\u03B1\u03BA\u03B1\u03BB\u03CE \u03B5\u03B9\u03C3\u03AC\u03B3\u03B5\u03C4\u03B5 \u03AD\u03BD\u03B1 \u03B1\u03BB\u03C6\u03B1\u03C1\u03B9\u03B8\u03BC\u03B7\u03C4\u03B9\u03BA\u03CC \u03C0\u03C1\u03BF\u03C2 \u03B1\u03BD\u03C4\u03B9\u03BA\u03B1\u03C4\u03AC\u03C3\u03C4\u03B1\u03C3\u03B7 !";

    if(id=='1001') return "\u0394\u03B9\u03AC\u03BB\u03BF\u03B3\u03BF\u03C2 \u03B1\u03BD\u03AC\u03BA\u03C4\u03B7\u03C3\u03B7\u03C2/\u03B1\u03C0\u03BF\u03B8\u03AE\u03BA\u03B5\u03C5\u03C3\u03B7\u03C2";
    if(id=='1002') return "'\u0391\u03BD\u03BF\u03B9\u03B3\u03BC\u03B1";
    if(id=='1003') return "\u0391\u03C0\u03BF\u03B8\u03AE\u03BA\u03B5\u03C5\u03C3\u03B7";
    if(id=='1004') return "'\u0391\u03BD\u03BF\u03B9\u03B3\u03BC\u03B1";
    if(id=='1005') return "\u038C\u03BD\u03BF\u03BC\u03B1 \u03B1\u03C1\u03C7\u03B5\u03AF\u03BF\u03C5:";
    if(id=='1006') return "\u03A0\u03C1\u03BF\u03B5\u03C0\u03B9\u03C3\u03BA\u03CC\u03C0\u03B9\u03C3\u03B7:";

    if(id=='1101') return "\u0394\u03B7\u03BC\u03B9\u03BF\u03C5\u03C1\u03B3\u03AF\u03B1/\u03B5\u03C0\u03B5\u03BE\u03B5\u03C1\u03B3\u03B1\u03C3\u03AF\u03B1 HTML anchor";
    if(id=='1102') return "\u038C\u03BD\u03BF\u03BC\u03B1 HTML anchor:";
    if(id=='1103') return "\u0399\u03B4\u03B9\u03CC\u03C4\u03B7\u03C4\u03B5\u03C2 HTML anchor";
    if(id=='1104') return "\u0391\u03C6\u03B1\u03AF\u03C1\u03B5\u03C3\u03B7 HTML anchor";

    if(id=='1201') return "\u0399\u03B4\u03B9\u03CC\u03C4\u03B7\u03C4\u03B5\u03C2 \u03C6\u03CC\u03C1\u03BC\u03B1\u03C2";
    if(id=='1202') return "\u038C\u03BD\u03BF\u03BC\u03B1 HTML Anchor:";
    if(id=='1203') return "\u0399\u03B4\u03B9\u03CC\u03C4\u03B7\u03C4\u03B5\u03C2 \u03C6\u03CC\u03C1\u03BC\u03B1\u03C2";
    if(id=='1204') return "\u0391\u03C6\u03B1\u03AF\u03C1\u03B5\u03C3\u03B7 \u03C6\u03CC\u03C1\u03BC\u03B1\u03C2";

    if(id=='1301') return "\u039A\u03B1\u03C4\u03B1\u03C7\u03CE\u03C1\u03B7\u03C3\u03B7 \u03B9\u03B4\u03B9\u03BF\u03C4\u03AE\u03C4\u03C9\u03BD";
    if(id=='1302') return "\u0395\u03C0\u03B9\u03BB\u03BF\u03B3\u03AE \u03B9\u03B4\u03B9\u03BF\u03C4\u03AE\u03C4\u03C9\u03BD";
    if(id=='1303') return "\u039C\u03AD\u03B3\u03B5\u03B8\u03BF\u03C2";
    if(id=='1304') return "\u03A7\u03C1\u03CE\u03BC\u03B1";

    if(id=='2000') return "\u03A4\u03BF\u03C0\u03BF\u03B8\u03AD\u03C4\u03B7\u03C3\u03B7 \u03C3\u03C4\u03BF \u03C6\u03CC\u03BD\u03C4\u03BF";
    if(id=='2001') return "Todo";

    // new in 3.0
    // TODO
    if(id=='119') return "Save as";
    if(id=='3000') return "Left to right";
    if(id=='3001') return "Right to left";
    if(id=='3002') return "Insert Paragraph";
    if(id=='3003') return "Table highlight";
    if(id=='3004') return "Hor. Alignment:";
    if(id=='3005') return "Upload and Insert";
    if(id=='3006') return "Please select a text range first !";
    if(id=='3007') return "Name:";
    if(id=='3008') return "Create text module";
    if(id=='3009') return "Please enter a valid text module name !";
    if(id=='3010') return "Upload document";
    if(id=='3011') return "Upload";
    if(id=='3012') return "Please select a text first !";
    if(id=='3013') return "Set RETURN mode";
    if(id=='3014') return "Select Style Sheet";
    if(id=='3015') return "Select Format";
    if(id=='3016') return "Select Font";
    if(id=='3017') return "Select Fontsize";
    if(id=='3018') return "File";
    if(id=='3019') return "Edit";
    if(id=='3020') return "Insert";
    if(id=='3021') return "Format";
    if(id=='3022') return "Forms";
    if(id=='3023') return "Link properties";
    if(id=='3024') return "Remove Link";
    if(id=='3025') return "Path:";
    if(id=='3026') return "Type:";
    if(id=='3027') return "Url:";
    if(id=='3028') return "Target:";
    if(id=='3029') return "Title:";
    if(id=='3030') return "Select document";
    if(id=='3031') return "Input Properties";
    if(id=='3032') return "Select Properties";
    if(id=='3033') return "Textarea Properties";

    // new in 3.1
    if(id=='962') return "ID:";
    if(id=='963') return "Name:";
    if(id=='964') return "ID/Name";
    if(id=='899') return "OK";
    if(id=='1007') return "Path:";
    if(id=='3100') return "Make editable";
    if(id=='3101') return "No color";

    if(id=='3201') return "Upload";
    if(id=='3202') return "File Upload";
    if(id=='3203') return "Server Path:";
    if(id=='3204') return "File:";

    if(id=='3210') return "Name";
    if(id=='3211') return "Size";
    if(id=='3212') return "Type";
    if(id=='3213') return "Changed";

    return "\u0394\u03B5 \u03B2\u03C1\u03AD\u03B8\u03B7\u03BA\u03B5 \u03BB\u03B5\u03BA\u03C4\u03B9\u03BA\u03CC \u03B3\u03B9\u03B1 \u03C4\u03BF \u03C3\u03C5\u03B3\u03BA\u03B5\u03BA\u03C1\u03B9\u03BC\u03AD\u03BD\u03BF resource"
  }

  // portugese
  if(language=="PT") {
    if(id=='101') return "Novo";
    if(id=='102') return "Abrir";
    if(id=='103') return "Imprimir";
    if(id=='104') return "Recortar";
    if(id=='105') return "Copiar";
    if(id=='106') return "Colar";
    if(id=='107') return "Desfazer";
    if(id=='108') return "Refazer";
    if(id=='109') return "Inserir Link";
    if(id=='110') return "Inserir Imagem";
    if(id=='111') return "Inserir Tabela";
    if(id=='112') return "Inserir R�gua";
    if(id=='113') return "Procurar";
    if(id=='114') return "Ajuda";
    if(id=='115') return "Inserir Caracteres Especiais";
    if(id=='116') return "Inserir Data";
    if(id=='117') return "Inserir Hora";
    if(id=='118') return "Salvar";

    if(id=='201') return "Negrito";
    if(id=='202') return "It�lico";
    if(id=='203') return "Sublinhado";
    if(id=='204') return "Sobrescrito";
    if(id=='205') return "Subscrito";
    if(id=='206') return "Alinhar � Esquerda";
    if(id=='207') return "Centralizar";
    if(id=='208') return "Alinhar � Direita";
    if(id=='209') return "Justificar";
    if(id=='210') return "Numera��o";
    if(id=='211') return "Marcadores";
    if(id=='212') return "Diminuir Recuo";
    if(id=='213') return "Aumentar Recuo";
    if(id=='214') return "Remover formata��o";
    if(id=='215') return "Cor do Texto";
    if(id=='216') return "Cor do Plano de Fundo";
    if(id=='217') return "Selecione a Cor do Texto";
    if(id=='218') return "Selecione a Cor do Plano de Fundo";

    if(id=='301') return "Inserir label";
    if(id=='302') return "Inserir Bot�o";
    if(id=='303') return "Inserir input";
    if(id=='304') return "Inserir checkbox";
    if(id=='305') return "Inserir radio";
    if(id=='306') return "Inserir combobox";
    if(id=='307') return "Inserir listbox";
    if(id=='308') return "Inserir �rea de texto";
    if(id=='309') return "Inserir sub page";
    if(id=='310') return "Inserir container";
    if(id=='311') return "Posi��o Absoluta";
    if(id=='312') return "Inserir m�dulo de texto";
    if(id=='313') return "Novo m�dulo de Texto";
    if(id=='314') return "Remover m�dulo de Texto Ativo";

    if(id=='401') return "Editar";
    if(id=='402') return "HTML";
    if(id=='403') return "Visualizar";
    if(id=='404') return "Quebra de P�gina";
    if(id=='405') return "Selecionar Tudo";
    if(id=='406') return "Inserir Formul�rio";
    if(id=='407') return "Inserir Link";
    if(id=='408') return "Colar do Word";
    if(id=='409') return "Marca";
    if(id=='410') return "Inserir Campo Oculto";
    if(id=='411') return "Verificar Ortografia";

    if(id=='501') return "Tabela";
    if(id=='502') return "Cancelar";

    if(id=='601') return "Mais Cores...";

    if(id=='701') return "Inserir imagem local";
    if(id=='702') return "Inserir imagem  da web";
    if(id=='703') return "Inserir imagem do servidor";
    if(id=='704') return "Enviar imagem ao servidor";

    if(id=='801') return "Inserir linha acima";
    if(id=='802') return "Inserir linha abaixo";
    if(id=='803') return "Excluir linha";
    if(id=='804') return "Inserir coluna � esquerda";
    if(id=='805') return "Inserir coluna � direita";
    if(id=='806') return "Deletar coluna";
    if(id=='807') return "Delete c�lula";
    if(id=='808') return "Mesclar c�lulas";
    if(id=='809') return "Dividir c�lulas";
    if(id=='810') return "Propriedades da  c�lula...";
    if(id=='811') return "Propriedades da Tabela...";
    if(id=='812') return "Propriedades da P�gina...";
    if(id=='813') return "Propriedades da Imagem...";
    if(id=='814') return "Definir como primeiro plano";
    if(id=='815') return "Propriedades do Container...";
    if(id=='816') return "Propriedades da Sub P�gina...";
    if(id=='817') return "Propriedades do Label...";

    if(id=='900') return "Cancelar";
    if(id=='901') return "Propriedades da Tabela";
    if(id=='902') return "Porcentagem";
    if(id=='903') return "Absoluto";
    if(id=='904') return "Espa�amento da C�lula:";
    if(id=='905') return "Tamanho da Borda:";
    if(id=='906') return "Enchimento da C�lula:";
    if(id=='907') return "Cor da Borda:";
    if(id=='908') return "Largura:";
    if(id=='909') return "Cor do Plano de Fundo:";
    if(id=='911') return "Propriedades da C�lula";
    if(id=='912') return "Alinhamento Horizontal:";
    if(id=='913') return "Altura:";
    if(id=='914') return "Alinhamento Vertical:";
    if(id=='915') return "Quebra:";
    if(id=='916') return "Cor da Borda:";
    if(id=='917') return "Cor do Plano de Fundo:";
    if(id=='918') return "Largura:";
    if(id=='921') return "Propriedades da Imagem";
    if(id=='922') return "Largura:";
    if(id=='923') return "Alinhamento Vertical:";
    if(id=='924') return "Altura:";
    if(id=='925') return "Largura da Borda:";
    if(id=='926') return "T�tulo:";
    if(id=='927') return "Cor da Borda:";
    if(id=='931') return "Propriedades do Label";
    if(id=='932') return "Width:";
    if(id=='933') return "Border color:";
    if(id=='934') return "Height:";
    if(id=='935') return "Background color:";
    if(id=='941') return "Container Properties";
    if(id=='942') return "Largura:";
    if(id=='943') return "Cor da Borda:";
    if(id=='944') return "Altura:";
    if(id=='945') return "Cor de Fundo:";
    if(id=='951') return "Propriedades da P�gina";
    if(id=='952') return "Margem:";
    if(id=='953') return "Cor do Plano de Fundo:";
    if(id=='961') return "Selecione uma Cor";
    if(id=='971') return "Propriedades da Sub P�gina";
    if(id=='972') return "Largura:";
    if(id=='973') return "Altura:";
    if(id=='974') return "URL:";
    if(id=='981') return "Inserir imagem da web";
    if(id=='982') return "Insira a URL:";
    if(id=='991') return "Localizar/Substituir";
    if(id=='992') return "Lozalizar:";
    if(id=='993') return "Substituir:";
    if(id=='994') return "Lozalizar";
    if(id=='995') return "Substituir";
    if(id=='996') return "Por favor, insira uma palavra !";
    if(id=='997') return "O documento chegou ao fim ou a palavra n�o foi encontrada.  A busca continuar� no in�cio do documento.";
    if(id=='998') return "Por favor, insira uma palavra para substituir !";

    if(id=='1001') return "Abrir/Salvar";
    if(id=='1002') return "Abrir";
    if(id=='1003') return "Salvar";
    if(id=='1004') return "Abrir";
    if(id=='1005') return "Nome do Arquivo:";
    if(id=='1006') return "Visualizar:";

    if(id=='1101') return "Criar/editar link";
    if(id=='1102') return "Nome do link:";
    if(id=='1103') return "Propriedades do link";
    if(id=='1104') return "Remover link";

    if(id=='1201') return "Propriedades do formul�rio";
    if(id=='1202') return "Nome do link:";
    if(id=='1203') return "Propriedades do formul�rio";
    if(id=='1204') return "Remover formul�rio";

    if(id=='1301') return "Propriedades do Input";
    if(id=='1302') return "Propriedades da Select";
    if(id=='1303') return "Tamanho";
    if(id=='1304') return "Cor";

    if(id=='2000') return "Definir como plano de fundo";
    if(id=='2001') return "Todo";

    // new in 3.0
    // TODO
    if(id=='119') return "Save as";
    if(id=='3000') return "Left to right";
    if(id=='3001') return "Right to left";
    if(id=='3002') return "Insert Paragraph";
    if(id=='3003') return "Table highlight";
    if(id=='3004') return "Hor. Alignment:";
    if(id=='3005') return "Upload and Insert";
    if(id=='3006') return "Please select a text range first !";
    if(id=='3007') return "Name:";
    if(id=='3008') return "Create text module";
    if(id=='3009') return "Please enter a valid text module name !";
    if(id=='3010') return "Upload document";
    if(id=='3011') return "Upload";
    if(id=='3012') return "Please select a text first !";
    if(id=='3013') return "Set RETURN mode";
    if(id=='3014') return "Select Style Sheet";
    if(id=='3015') return "Select Format";
    if(id=='3016') return "Select Font";
    if(id=='3017') return "Select Fontsize";
    if(id=='3018') return "File";
    if(id=='3019') return "Edit";
    if(id=='3020') return "Insert";
    if(id=='3021') return "Format";
    if(id=='3022') return "Forms";
    if(id=='3023') return "Link properties";
    if(id=='3024') return "Remove Link";
    if(id=='3025') return "Path:";
    if(id=='3026') return "Type:";
    if(id=='3027') return "Url:";
    if(id=='3028') return "Target:";
    if(id=='3029') return "Title:";
    if(id=='3030') return "Select document";
    if(id=='3031') return "Input Properties";
    if(id=='3032') return "Select Properties";
    if(id=='3033') return "Textarea Properties";

    // new in 3.1
    if(id=='962') return "ID:";
    if(id=='963') return "Name:";
    if(id=='964') return "ID/Name";
    if(id=='899') return "OK";
    if(id=='1007') return "Path:";
    if(id=='3100') return "Make editable";
    if(id=='3101') return "No color";

    if(id=='3201') return "Upload";
    if(id=='3202') return "File Upload";
    if(id=='3203') return "Server Path:";
    if(id=='3204') return "File:";

    if(id=='3210') return "Name";
    if(id=='3211') return "Size";
    if(id=='3212') return "Type";
    if(id=='3213') return "Changed";

    return "Faltando"
  }

  // more languages ...

}
