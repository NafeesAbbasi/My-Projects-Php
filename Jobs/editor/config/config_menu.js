//------------------------------------------------------------------------------------------------------
// configuration file for pinEdit menu creation
//------------------------------------------------------------------------------------------------------

var objMenuBar = null;

function CreateMenu(userCode)
{
  if(userCode == "") {
    objMenuBar = new MenuBar();

    // the menu design names differ
    if(design == "")
      objMenuBar.design = "Office2000";
    if(design == "Office")
      objMenuBar.design = "OfficeXP";
    if(design == "Office2003")
      objMenuBar.design = "Office2003";

    //-----------------------------------------------------------------------------------------------------------
    // create file menu
    //-----------------------------------------------------------------------------------------------------------
    objMenuBarItem = new MenuBarItem(getLanguageString(language,3018));

    objMenu = new Menu("onMenuItemClick");
    //objMenu.add(new MenuItem(getLanguageString(language,101) ,"design/image/" + design + "/neu.gif","","NEW"));
    //objMenu.add(new MenuItem(getLanguageString(language,102) + "...","design/image/" + design + "/open.gif","","OPEN"));
    //objMenu.add(new MenuItem(getLanguageString(language,118) ,"design/image/" + design + "/save.gif","","SAVE"));
    //objMenu.add(new MenuItem(getLanguageString(language,119) + "...","design/image/" + design + "/saveas.gif","","SAVEAS"));
    //objMenu.add(new MenuSeparator());
    objMenu.add(new MenuItem(getLanguageString(language,403) + "...","design/image/" + design + "/preview.gif","","PREVIEW"));
    if(!browser.ns)
      objMenu.add(new MenuItem(getLanguageString(language,103) + "...","design/image/" + design + "/print.gif","","PRINT"));

    objMenuBarItem.setMenu(objMenu);
    objMenuBar.add(objMenuBarItem);

    //-----------------------------------------------------------------------------------------------------------
    // create edit menu
    //-----------------------------------------------------------------------------------------------------------
    objMenuBarItem = new MenuBarItem(getLanguageString(language,3019));

    objMenu = new Menu("onMenuItemClick");
    objMenu.add(new MenuItem(getLanguageString(language,107) ,"design/image/" + design + "/undo.gif","","UNDO"));
    objMenu.add(new MenuItem(getLanguageString(language,108),"design/image/" + design + "/redo.gif","","REDO"));
    objMenu.add(new MenuSeparator());
    objMenu.add(new MenuItem(getLanguageString(language,104) ,"design/image/" + design + "/cut.gif","","CUT"));
    objMenu.add(new MenuItem(getLanguageString(language,105) ,"design/image/" + design + "/copy.gif","","COPY"));
    objMenu.add(new MenuItem(getLanguageString(language,106) ,"design/image/" + design + "/paste.gif","","PASTE"));
    objMenu.add(new MenuItem(getLanguageString(language,408) ,"design/image/" + design + "/pasteword.gif","","PASTEWORD"));
    objMenu.add(new MenuSeparator());
    objMenu.add(new MenuItem(getLanguageString(language,405) ,"design/image/" + design + "/selectall.gif","","SELECTALL"));
    objMenu.add(new MenuItem(getLanguageString(language,113) ,"design/image/" + design + "/search.gif","","SEARCH"));
    
    objMenuBarItem.setMenu(objMenu);
    objMenuBar.add(objMenuBarItem);

    //-----------------------------------------------------------------------------------------------------------
    // create insert menu
    //-----------------------------------------------------------------------------------------------------------
    objMenuBarItem = new MenuBarItem(getLanguageString(language,3020));

    objMenu = new Menu("onMenuItemClick");
    
	objMenu.add(new MenuItem(getLanguageString(language,109),"design/image/" + design + "/link.gif","","LINK"));
	objMenu.add(new MenuSeparator());
	//objMenu.add(new MenuItem(getLanguageString(language,110),"design/image/" + design + "/image.gif","","IMAGE"));
//	objMenu.add(new MenuItem(getLanguageString(language,701),"design/image/" + design + "/image.gif","","IMAGE"));
//	objMenu.add(new MenuItem(getLanguageString(language,705),"design/image/" + design + "/libraryImage.gif","","INSERTIMG"));
//	objMenu.add(new MenuItem(getLanguageString(language,706),"design/image/" + design + "/libraryBackground.gif","","INSERTBACKIMG"));
//	objMenu.add(new MenuItem(getLanguageString(language,707),"design/image/" + design + "/Emot_Icon.gif","","INSERTEMOT"));
	//objMenu.add(new MenuSeparator());
//	objMenu.add(new MenuItem(getLanguageString(language,709),"design/image/" + design + "/SoundFile_Icon.gif","","SOUNDREC"));
//	objMenu.add(new MenuItem(getLanguageString(language,710),"design/image/" + design + "/Tag_Icon.gif","","INSERTTAG"));


	/*	
	objMenu.add(new MenuItem(getLanguageString(language,116) ,"design/image/" + design + "/date.gif","","DATE"));
    objMenu.add(new MenuItem(getLanguageString(language,117),"design/image/" + design + "/time.gif","","TIME"));
	objMenu.add(new MenuItem(getLanguageString(language,109),"design/image/" + design + "/link.gif","","LINK"));
	objMenu.add(new MenuItem(getLanguageString(language,407),"design/image/" + design + "/anchor.gif","","ANCHOR"));
    objMenu.add(new MenuSeparator());
    objMenu.add(new MenuItem(getLanguageString(language,112),"design/image/" + design + "/rule.gif","","RULE"));
    if(!browser.ns) {
      objMenu.add(new MenuItem(getLanguageString(language,409),"design/image/" + design + "/marquee.gif","","MARQUEE"));
    }
    objMenu.add(new MenuSeparator());
    if(!browser.ns) {
      objMenu.add(new MenuItem(getLanguageString(language,404),"design/image/" + design + "/pagebreak.gif","","PAGEBREAK"));
    }
    objMenu.add(new MenuItem(getLanguageString(language,3002),"design/image/" + design + "/paragraph.gif","","PARAGRAPH"));
    objMenu.add(new MenuItem(getLanguageString(language,3000),"design/image/" + design + "/ltr.gif","","LTR"));
    objMenu.add(new MenuItem(getLanguageString(language,3001),"design/image/" + design + "/rtl.gif","","RTL"));
	*/
    objMenuBarItem.setMenu(objMenu);
    objMenuBar.add(objMenuBarItem);

    //-----------------------------------------------------------------------------------------------------------
    // create format menu
    //-----------------------------------------------------------------------------------------------------------
    objMenuBarItem = new MenuBarItem(getLanguageString(language,3021));

    objMenu = new Menu("onMenuItemClick");
    objMenu.add(new MenuItem(getLanguageString(language,201) ,"design/image/" + design + "/bold.gif","","BOLD"));
    objMenu.add(new MenuItem(getLanguageString(language,202),"design/image/" + design + "/italic.gif","","ITALIC"));
    objMenu.add(new MenuItem(getLanguageString(language,203),"design/image/" + design + "/underline.gif","","UNDERLINE"));
    objMenu.add(new MenuItem(getLanguageString(language,204),"design/image/" + design + "/superscript.gif","","SUPERSCRIPT"));
    objMenu.add(new MenuItem(getLanguageString(language,205),"design/image/" + design + "/subscript.gif","","SUBSCRIPT"));
    objMenu.add(new MenuSeparator());
    objMenu.add(new MenuItem(getLanguageString(language,206),"design/image/" + design + "/left.gif","","JUSTIFYLEFT"));
    objMenu.add(new MenuItem(getLanguageString(language,207),"design/image/" + design + "/center.gif","","JUSTIFYCENTER"));
    objMenu.add(new MenuItem(getLanguageString(language,208),"design/image/" + design + "/right.gif","","JUSTIFYRIGHT"));
    objMenu.add(new MenuItem(getLanguageString(language,209),"design/image/" + design + "/block.gif","","JUSTIFYFULL"));
    objMenu.add(new MenuSeparator());
    objMenu.add(new MenuItem(getLanguageString(language,210),"design/image/" + design + "/orderedlist.gif","","INSERTORDEREDLIST"));
    objMenu.add(new MenuItem(getLanguageString(language,211),"design/image/" + design + "/unorderedlist.gif","","INSERTUNORDEREDLIST"));
    objMenu.add(new MenuSeparator());
    objMenu.add(new MenuItem(getLanguageString(language,212),"design/image/" + design + "/indent.gif","","INDENT"));
    objMenu.add(new MenuItem(getLanguageString(language,213),"design/image/" + design + "/outdent.gif","","OUTDENT"));

    objMenuBarItem.setMenu(objMenu);
    objMenuBar.add(objMenuBarItem);

    //-----------------------------------------------------------------------------------------------------------
    // create Signature menu
    //-----------------------------------------------------------------------------------------------------------
  /*  objMenuBarItem = new MenuBarItem(getLanguageString(language,315));

    objMenu = new Menu("onMenuItemClick");
	objMenu.add(new MenuItem(getLanguageString(language,312) ,"design/image/" + design + "/tm_add.gif","","TMADD"));
    objMenu.add(new MenuItem(getLanguageString(language,313),"design/image/" + design + "/tm_create.gif","","TMCREATE"));
    objMenu.add(new MenuItem(getLanguageString(language,314),"design/image/" + design + "/tm_remove.gif","","TMREMOVE"));
    //objMenu.add(new MenuSeparator());

    objMenuBarItem.setMenu(objMenu);
    objMenuBar.add(objMenuBarItem);
	*/
	//-----------------------------------------------------------------------------------------------------------
    // create forms menu
    //-----------------------------------------------------------------------------------------------------------
    /*objMenuBarItem = new MenuBarItem(getLanguageString(language,3022));

    objMenu = new Menu("onMenuItemClick");
    objMenu.width = 200;
    objMenu.add(new MenuItem(getLanguageString(language,406),"design/image/" + design + "/form.gif","","FORM"));
    if(browser.ie) {
      objMenu.add(new MenuItem(getLanguageString(language,301),"design/image/" + design + "/label.gif","","LABEL"));
    }
    objMenu.add(new MenuItem(getLanguageString(language,302),"design/image/" + design + "/button.gif","","BUTTON"));
    objMenu.add(new MenuItem(getLanguageString(language,303),"design/image/" + design + "/input.gif","","INPUT"));
    objMenu.add(new MenuItem(getLanguageString(language,304),"design/image/" + design + "/checkbox.gif","","CHECK"));
    objMenu.add(new MenuItem(getLanguageString(language,305),"design/image/" + design + "/radio.gif","","OPTION"));
    objMenu.add(new MenuItem(getLanguageString(language,306),"design/image/" + design + "/combobox.gif","","COMBO"));
    objMenu.add(new MenuItem(getLanguageString(language,307),"design/image/" + design + "/listbox.gif","","LISTBOX"));
    objMenu.add(new MenuItem(getLanguageString(language,308),"design/image/" + design + "/textarea.gif","","AREA"));
    objMenu.add(new MenuItem(getLanguageString(language,410),"design/image/" + design + "/hidden.gif","","HIDDEN"));
    objMenu.add(new MenuSeparator());
    objMenu.add(new MenuItem(getLanguageString(language,310),"design/image/" + design + "/div.gif","","DIV"));
    if(browser.ie) {
      objMenu.add(new MenuItem(getLanguageString(language,309),"design/image/" + design + "/iframe.gif","","IFRAME"));
    }
    objMenu.add(new MenuSeparator());
    objMenu.add(new MenuItem(getLanguageString(language,311),"design/image/" + design + "/position.gif","","POSITION"));

    objMenuBarItem.setMenu(objMenu);
    objMenuBar.add(objMenuBarItem);
	*/
    //-----------------------------------------------------------------------------------------------------------
    // create help menu
    //-----------------------------------------------------------------------------------------------------------
    /*
	objMenuBarItem = new MenuBarItem("?");

    objMenu = new Menu("onMenuItemClick");
    objMenu.add(new MenuItem(getLanguageString(language,114),"design/image/" + design + "/help.gif","","HELP"));

    objMenuBarItem.setMenu(objMenu);
    objMenuBar.add(objMenuBarItem);
	*/
  }

  //---------------------------------------------------------------------------------------------------
  // sample for a custmized menu
  //---------------------------------------------------------------------------------------------------
  if(userCode == "USER") {
    objMenuBar = new MenuBar();
    objMenuBar.design = "Office2003";

    objMenuBarItem = new MenuBarItem('MenuBar1');
    objMenu = new Menu("onMyMenuItemClick");
    objMenu.add(new MenuItem("MyItem 1" ,"design/image/" + design + "/neu.gif","","ITEM1"));
    objMenu.add(new MenuItem("MyItem 2" ,"design/image/" + design + "/print.gif","","ITEM2"));
    objMenuBarItem.setMenu(objMenu);
    objMenuBar.add(objMenuBarItem);

    objMenuBarItem = new MenuBarItem('MenuBar2');
    objMenu = new Menu("onMyMenuItemClick");
    objMenu.add(new MenuItem("MyItem 3" ,"design/image/" + design + "/neu.gif","","ITEM3"));
    objMenu.add(new MenuItem("MyItem 4" ,"design/image/" + design + "/print.gif","","ITEM4"));
    objMenuBarItem.setMenu(objMenu);
    objMenuBar.add(objMenuBarItem);
  }

  // show menu
  objMenuBar.show(0,0,500,22);

}

function onMenuItemClick(key)
{
  if(key == "NEW")
    editNew();
  if(key == "OPEN")
    editOpen(1);
  if(key == "SAVE")
    saveCommon();
  if(key == "SAVEAS")
    editSaveDialog();
  if(key == "PREVIEW")
    editPreview();
  if(key == "PRINT")
    editPrint();
  if(key == "UNDO")
    editUndo();
  if(key == "REDO")
    editRedo();
  if(key == "CUT")
    editCut();
  if(key == "COPY")
    editCopy();
  if(key == "PASTE")
    editPaste();
  if(key == "PASTEWORD")
    editPasteWord();
  if(key == "SELECTALL")
    editSelectAll();
  if(key == "SEARCH")
    editSearch();
  if(key == "DATE")
    editInsertDate();
  if(key == "TIME")
    editInsertTime();
  if(key == "LINK")
    editLink();
  if(key == "ANCHOR")
    editInsertObject('ANCHOR');
  if(key == "IMAGE")
	{
	  editInsertObject('IMAGE');
	  //  editOpen(3);
	}
  if(key == "INSERTIMG")
  {

	 var name = parent.document.getElementsByTagName("form").item(0).name;
	 if(name == "form1")
	 {
		var id = parent.document.form1.id.value;
		win=window.open("../../showlibrary.php?LibraryType=images&DesignId="+id,"img",'resizable=yes,scrollbars=yes,width=800,height=650,toolbar=no, menubar=no,location=no, directories=no,status=no');
		win.moveTo(100,100);
	 }
	 else
	 {
	 	  var id = parent.document.frmCompose.UserDesignId.value;
		  win=window.open("../../showlibrary.php?LibraryType=images&DesignId="+id+"&func=front","img_front",'resizable=yes,scrollbars=yes,width=800,height=650,toolbar=no, menubar=no,location=no, directories=no,status=no');
		  win.moveTo(100,100);
	 }
  }
  if(key == "INSERTBACKIMG")
  {
	 var name = parent.document.getElementsByTagName("form").item(0).name;
	 if(name == "form1")
	 {
		var id = parent.document.form1.id.value;
		win=window.open("../../showlibrary.php?LibraryType=backgrounds&DesignId="+id,"backimg",'resizable=yes,scrollbars=yes,width=800,height=650,toolbar=no, menubar=no,location=no, directories=no,status=no');
		win.moveTo(100,100);
	 }
	 else
	 {
	 	  var id = parent.document.frmCompose.UserDesignId.value;
		  win=window.open("../../showlibrary.php?LibraryType=backgrounds&DesignId="+id+"&func=front","backimg_front",'resizable=yes,scrollbars=yes,width=800,height=650,toolbar=no, menubar=no,location=no, directories=no,status=no');
		  win.moveTo(100,100);
	 }
  }
  if(key == "INSERTEMOT")
  {
	 var name = parent.document.getElementsByTagName("form").item(0).name;
	 if(name == "form1")
	 {
		var id = parent.document.form1.id.value;
		win=window.open("../../showlibrary.php?LibraryType=emoticons&DesignId="+id,"emot",'resizable=yes,scrollbars=yes,width=800,height=650,toolbar=no, menubar=no,location=no, directories=no,status=no');
		win.moveTo(100,100);
	 }
	 else
	 {
	 	  var id = parent.document.frmCompose.UserDesignId.value;
		 win= window.open("../../showlibrary.php?LibraryType=emoticons&DesignId="+id+"&func=front","emot_front",'resizable=yes,scrollbars=yes,width=800,height=650,toolbar=no, menubar=no,location=no, directories=no,status=no');
		 win.moveTo(100,100);
	 }
  }
  if(key == "SOUNDREC")
  {
	 var name = parent.document.getElementsByTagName("form").item(0).name;
	 if(name == "form1")
	 {
		var id = parent.document.form1.id.value;
		win=window.open("../../showlibrary.php?LibraryType=sound&DesignId="+id,"soundrec",'resizable=yes,scrollbars=yes,width=800,height=650,toolbar=no, menubar=no,location=no, directories=no,status=no');
		win.moveTo(100,100);
	 }
	 else
	 {
		 var id = parent.document.frmCompose.UserDesignId.value;
		 var attachment = parent.document.frmCompose.attachment.value;
		 var attachmentPath = parent.document.frmCompose.attachmentPath.value;
		 //alert("attachment----"+attachment+" attachmentPath----"+attachmentPath);
		 win=window.open("../../showlibrary.php?LibraryType=sound&DesignId="+id+"&func=front&oldAttachment="+attachment+"&oldAttachmentPath="+attachmentPath,"soundrec_front",'resizable=yes,scrollbars=yes,width=800,height=650,toolbar=no, menubar=no,location=no, directories=no,status=no');
		 win.moveTo(100,100);
	 }
  }
  if(key == "INSERTTAG")
  {
	 var name = parent.document.getElementsByTagName("form").item(0).name;
	 if(name == "form1")
	 {
		var id = parent.document.form1.id.value;
		win=window.open("../../tags.php?DesignId="+id,"tag",'resizable=yes,scrollbars=yes,width=800,height=650,toolbar=no, menubar=no,location=no, directories=no,status=no');
		win.moveTo(100,100);
	 }
	 else
	 {
		 var id = parent.document.frmCompose.UserDesignId.value;
		 win=window.open("../../tags.php?DesignId="+id+"&func=front","tag_front",'resizable=yes,scrollbars=yes,width=800,height=650,toolbar=no, menubar=no,location=no, directories=no,status=no');
		 win.moveTo(100,100);
	 }
  }
 
  if(key == "TMADD")
  {
	  onInsertTextModule();
  }
  if(key == "TMCREATE")
  {
	  editSetTextModule();
  }
  if(key == "TMREMOVE")
  {
	  onDeleteTextModule();
  }


  if(key == "RULE")
    editInsertObject('RULE');
  if(key == "MARQUEE")
    editInsertObject('MARQUEE');
  if(key == "PAGEBREAK")
    editInsertObject('PAGEBREAK');
  if(key == "PARAGRAPH")
    editParagraph();
  if(key == "LTR")
    editSetDirection('ltr');
  if(key == "RTL")
    editSetDirection('rtl');

  if(key == "BOLD")
    editBold();
  if(key == "ITALIC")
    editItalic();
  if(key == "UNDERLINE")
    editUnderline();
  if(key == "SUPERSCRIPT")
    editSuperscript();
  if(key == "SUBSCRIPT")
    editSubscript();
  if(key == "JUSTIFYLEFT")
    editJustifyLeft();
  if(key == "JUSTIFYCENTER")
    editJustifyCenter();
  if(key == "JUSTIFYRIGHT")
    editJustifyRight();
  if(key == "JUSTIFYFULL")
    editJustifyFull();
  if(key == "INSERTORDEREDLIST")
    editOrderedList();
  if(key == "INSERTUNORDEREDLIST")
    editUnorderedList();
  if(key == "INDENT")
    editIndent();
  if(key == "OUTDENT")
    editOutdent();

  if(key == "FORM")
    editInsertObject('FORM');
  if(key == "LABEL")
    editInsertObject('LABEL');
  if(key == "BUTTON")
    editInsertObject('BUTTON');
  if(key == "INPUT")
    editInsertObject('INPUT');
  if(key == "CHECK")
    editInsertObject('CHECK');
  if(key == "OPTION")
    editInsertObject('OPTION');
  if(key == "COMBO")
    editInsertObject('COMBO');
  if(key == "LISTBOX")
    editInsertObject('LISTBOX');
  if(key == "AREA")
    editInsertObject('AREA');
  if(key == "HIDDEN")
    editInsertObject('HIDDEN');
  if(key == "DIV")
    editInsertObject('DIV');
  if(key == "IFRAME")
    editInsertObject('IFRAME');
  if(key == "POSITION")
    editAbsolute();

  if(key == "HELP")
    onHelp();
}


function onMyMenuItemClick(key)
{
  alert("Menu clicked: " + key);
}