//------------------------------------------------------------
// Events fired from editor
// Don't remove the function prototypes
//------------------------------------------------------------
function eventOnInit()
{
	// add special caracter conversion
  // add the &amp first
	editAddSpecialCharReplacement("\x26","&amp;");
	editAddSpecialCharReplacement("<","&lt;");
	editAddSpecialCharReplacement(">","&gt;");
	editAddSpecialCharReplacement("\xA0","&nbsp;");
	editAddSpecialCharReplacement("\xA9","&copy;");
	editAddSpecialCharReplacement("\xAE","&reg;");

  // German special characters
  if(language == "DE") {
	  editAddSpecialCharReplacement("\xC4","&Auml;");
	  editAddSpecialCharReplacement("\xD6","&Ouml;");
	  editAddSpecialCharReplacement("\xDC","&Uuml;");
	  editAddSpecialCharReplacement("\xE4","&auml;");
	  editAddSpecialCharReplacement("\xF6","&ouml;");
	  editAddSpecialCharReplacement("\xFC","&uuml;");
	  editAddSpecialCharReplacement("\xDF","&szlig;");
  }

  // Swedish special characters
  if(language == "SE") {
	  editAddSpecialCharReplacement("\xC4","&Auml;");
	  editAddSpecialCharReplacement("\xD6","&Ouml;");
	  editAddSpecialCharReplacement("\xE4","&auml;");
	  editAddSpecialCharReplacement("\xF6","&ouml;");
	  editAddSpecialCharReplacement("\xC5","&Aring;");
	  editAddSpecialCharReplacement("\xE5","&aring;");
  }
  
  // French special characters
  if(language == "FR") {
	  editAddSpecialCharReplacement("\xE1","&aacute;");
	  editAddSpecialCharReplacement("\xC1","&Aacute;");
	  editAddSpecialCharReplacement("\xF3","&oacute;");
	  editAddSpecialCharReplacement("\xD3","&Oacute;");
	  editAddSpecialCharReplacement("\xE9","&eacute;");
	  editAddSpecialCharReplacement("\xC9","&Eacute;");
	  editAddSpecialCharReplacement("\xE0","&agrave;");
	  editAddSpecialCharReplacement("\xC0","&Agrave;");
	  editAddSpecialCharReplacement("\xE8","&egrave;");
	  editAddSpecialCharReplacement("\xC8","&Egrave;");
	  editAddSpecialCharReplacement("\xF2","&ograve;");
	  editAddSpecialCharReplacement("\xD2","&Ograve;");
	  editAddSpecialCharReplacement("\xE2","&acirc;");
	  editAddSpecialCharReplacement("\xC2","&Acirc;");
	  editAddSpecialCharReplacement("\xEA","&ecirc;");
	  editAddSpecialCharReplacement("\xCA","&Ecirc;");
	  editAddSpecialCharReplacement("\xF4","&ocirc;");
	  editAddSpecialCharReplacement("\xD4","&Ocirc;");
	  editAddSpecialCharReplacement("\xCE","&Icirc;");
	  editAddSpecialCharReplacement("\xEE","&icirc;");
 }
}


//------------------------------------------------------------------------------------------------------
// Events for EDIT area
//------------------------------------------------------------------------------------------------------
function eventOnBeforeNew()
{
	// do something before creating new document
}

function eventOnBeforeLoad()
{
	// do something before loading document
	
}

function eventOnMouseDown()
{
	try {
	  // used for external toolbar to close popups
	  if(globalSingleMode)
		  parent.editOnEditorMouseDown(window.frameElement.id);
    else	  
		  // for internal toolbar
		  editOnEditorMouseDown(window.frameElement.id);
	} catch(Error) {}
}

function eventOnKeyDown(e)
{
	// to cancel KeyDown return true
	//return true;
}

// this event is fired when the editor content has been changed
function eventOnChanged()
{
  //alert("Changed");
}

// this event is fired if Control+V is supressed
function eventControlV()
{
  // do something to replace ctrl+v
  // maybe call paste from Word (editPasteWord)
  alert("Control+V pressed");

}

function eventOnProcessTM(aData)
{
	try {
		// used for external toolbar 
	  if(globalSingleMode)
  		parent.editOnProcessTM(aData,window.frameElement.id);
  	else
  		editOnProcessTM(aData,window.frameElement.id);
	} catch(Error) {}
}

// if the status/combo of the toolbar has to be changed
function eventOnChangeToolbar(data)
{
	try {
		// used for pinToolbar 
	  if(globalSingleMode)
  		parent.editOnChangeToolbar(data,window.frameElement.id);
  	else
  		editOnChangeToolbar(data,window.frameElement.id);
	} catch(Error) {}
}

// if the status/combo of the toolbar has to be changed
function eventOnAddStyle(name,html)
{
	try {
		// used for pinToolbar to close popups
	  if(globalSingleMode)
  		parent.editOnAddStyle(name,html,window.frameElement.id);
  	else
  		editOnAddStyle(name,html,window.frameElement.id);
	} catch(Error) {}
	
	
}

// if a style sheet is read and contains Hx elements then the format combo has to be updated
function eventOnChangeFormat(name,html)
{
	try {
		// used for pinToolbar to change format
	  if(globalSingleMode)
  		parent.editOnChangeFormat(name,html,window.frameElement.id);
  	else
  		editOnChangeFormat(name,html,window.frameElement.id);
	} catch(Error) {}
}

// occurs if editor loose cursor
function eventOnBlur()
{
	try {
 		parent.editOnBlur(window.frameElement.id);
	} catch(Error) {}
}

// occurs if editor gets focus
function eventOnFocus()
{
}

// is called when editor is turned in read only mode
function eventOnReadOnly(value)
{
	try {
	  if(globalSingleMode)
  		parent.editOnReadOnly(value,window.frameElement.id);
  	else
  		editOnReadOnly(value,window.frameElement.id);
	} catch(Error) {}
}

// is raised when a file has been completely loaded
function eventOnFileLoaded()
{
  //alert(editGetFileUrl());
}

// dbl click in editor
function eventOnDblClick()
{
  //alert("DBL-CLick");
}

function eventOnBeforeSave(sContent)
{
	try {
 		// Execute custom saving function in parent document
 		// If returns true, stop default behavior (don't use pinEdit default saving behavior)
 		//return parent.editOnBeforeSave(window.frameElement.id, sContent);
 		
	} catch(Error) {}
	
	// Allow default behavior
	return false;
}

function eventOnBeforeCreateMenu(objMenuBar, userCode)
{
	try {
		return parent.editOnBeforeCreateMenu(objMenuBar, userCode);
	}	catch(Error){}
	
	// Allow default behavior
	return false;
}

function eventOnAfterCreateMenu(objMenuBar, userCode)
{
	try {
		parent.editOnAfterCreateMenu(objMenuBar, userCode);
	}	catch(Error){}
	
	// Allow default behavior
	return false;
}

// if an image,flash,media file has been inserted
// type:    IMAGE
//          FLASH
//          MEDIA
// obj:     The object that has been inserted
function eventOnAfterInsertObject(type,obj)
{
}



//------------------------------------------------------------------------------------------------------
// Events for HTML area
//------------------------------------------------------------------------------------------------------
function eventOnHtmlMouseDown(e)
{
}

function eventOnHtmlKeyDown(e)
{
  //alert(e.keyCode)
  
	// to cancel KeyDown return true
	//return true;
}




//-------------------------------------------------------------------------------------------------------
// Add-on events
//-------------------------------------------------------------------------------------------------------
function eventOnPDFCreated(name)
{
  // set status bar
  editStatusBarSetText("PDF file created");
  // build url
  var url = globalPDFOutputUrl  + name;
  // open PDF
  window.open(url);
}

