//------------------------------------------------------------------------------------------------------------------
// Interface for pinEdit with internal toolbar
//------------------------------------------------------------------------------------------------------------------

// those variables are needed in toolbar button click events
var toolbarsTop    = null;
var toolbarsBottom = null;
var border         = false;

// the toolbar design (3=default)
var tbdesign = "3";
  
// image path
var imagePath = "";

// these buttons are needed to update state quickly after mouse click or key press in editor
var btnBold = null;
var btnItalic = null;
var btnUnderline = null;
var btnSuperscript = null;
var btnSubscript = null;
var btnJustifyLeft = null;
var btnJustifyCenter = null;
var btnJustifyRight = null;
var btnJustifyFull = null;
var btnInsertOrderedList = null;
var btnInsertUnorderedList = null;
var btnColor = null;
var btnBackColor = null;

// those combos are needed to update content after mouse click or key press in editor
var cmbStyle = null;
var cmbFormat = null;
var cmbFont = null;
var cmbFontSize = null;


//---------------------------------------------------------------------------------------------
// Toolbar events
//---------------------------------------------------------------------------------------------
// objToolbars: root object of toolbar
// id:          id of IFRAME where the toolbar runs ( needed of there a re multiple objects)
//---------------------------------------------------------------------------------------------
function toolbarCreate(objToolbars, id)
{
  if(design == "")
    tbdesign = "1";
  if(design == "Office")
    tbdesign = "2";
  if(design == "Office2003")
    tbdesign = "3";
  if(design == "Office2003S")
    tbdesign = "4";

  imagePath = "design/image/style" + tbdesign + "/";

  //-------------------------------------------------------------------------------------
  // Create customized dynamic toolbars:
  // If you call pinEdit.html?uc=TB1
  // the parameter "TB1" can be get by using
  // var code = editGetUserCode()
  //-------------------------------------------------------------------------------------

	// which toolbar do we have
	if(id == "toolbar_top") {
		// we need this variable later
		toolbarsTop = objToolbars;
		toolbarsTop.language = language;
		//-----------------------------------------------------------------------------------------
		// Toolbar I
		//-----------------------------------------------------------------------------------------
		// create toolbar object
		var objToolbar1 = objToolbars.createToolbar();
		// define event handler
		objToolbar1.action = "onToolbarButtonClick";
		// set values
		objToolbar1.design = tbdesign;
		objToolbar1.border = border;
		objToolbar1.height = 27;

		// create a separator
		objToolbar1.add(objToolbars.createSeparator(imagePath + "tbbegin.gif"));

		// create menu button
		var objTemplate = objToolbars.createMenuButton("",imagePath + "new.gif",imagePath + "selector.gif","onNewClicked",getLanguageString(language,101),"New");
		objTemplate.popupwidth = "150";
		objTemplate.add(objToolbars.createMenuItem(getLanguageString(language,101),imagePath + "new.gif","","NEW"));
		objTemplate.add(objToolbars.createMenuItem(getLanguageString(language,3313),imagePath + "template.gif","","TEMPLATE"));
		// add to toolbar
		objToolbar1.add(objTemplate);

		objToolbar1.add(objToolbars.createButton("",imagePath + "open.gif","",getLanguageString(language,102),"OPEN"));
		objToolbar1.add(objToolbars.createButton("",imagePath + "save.gif","",getLanguageString(language,118),"SAVE"));
		objToolbar1.add(objToolbars.createButton("",imagePath + "saveas.gif","",getLanguageString(language,119),"SAVEAS"));
		objToolbar1.add(objToolbars.createButton("",imagePath + "savelocal.gif","",getLanguageString(language,3300),"SAVELOCAL"));
		objToolbar1.add(objToolbars.createSeparator(imagePath + "separator.gif"));
		objToolbar1.add(objToolbars.createButton("",imagePath + "search.gif","",getLanguageString(language,113),"SEARCH"));
		objToolbar1.add(objToolbars.createSeparator(imagePath + "separator.gif"));
		objToolbar1.add(objToolbars.createButton("",imagePath + "print.gif","",getLanguageString(language,103),"PRINT"));
		objToolbar1.add(objToolbars.createButton("",imagePath + "preview.gif","",getLanguageString(language,3314),"PREVIEW"));
		objToolbar1.add(objToolbars.createSeparator(imagePath + "separator.gif"));
		objToolbar1.add(objToolbars.createButton("",imagePath + "spell.gif","",getLanguageString(language,411),"SPELL"));

		// create menu button
		var objUpload = objToolbars.createMenuButton("",imagePath + "upload.gif",imagePath + "colorselect.gif","onMenuUploadClicked",getLanguageString(language,3011),"Upload");
		objUpload.popupwidth = "190";
		objUpload.add(objToolbars.createMenuItem(getLanguageString(language,704),imagePath + "upload.gif","","UPLOADIMG"));
		objUpload.add(objToolbars.createMenuItem(getLanguageString(language,3010),imagePath + "upload.gif","","UPLOADDOC"));
		// add to toolbar
		objToolbar1.add(objUpload);

		objToolbar1.add(objToolbars.createSeparator(imagePath + "separator.gif"));
		objToolbar1.add(objToolbars.createButton("",imagePath + "cut.gif","",getLanguageString(language,104),"CUT"));
		objToolbar1.add(objToolbars.createButton("",imagePath + "copy.gif","",getLanguageString(language,105),"COPY"));
		objToolbar1.add(objToolbars.createButton("",imagePath + "paste.gif","",getLanguageString(language,106),"PASTE"));
		objToolbar1.add(objToolbars.createButton("",imagePath + "pasteword.gif","",getLanguageString(language,408),"PASTEWORD"));
		objToolbar1.add(objToolbars.createButton("",imagePath + "pastetext.gif","",getLanguageString(language,3340),"PASTETEXT"));
		objToolbar1.add(objToolbars.createSeparator(imagePath + "separator.gif"));
		objToolbar1.add(objToolbars.createButton("",imagePath + "undo.gif","",getLanguageString(language,107),"UNDO"));
		objToolbar1.add(objToolbars.createButton("",imagePath + "redo.gif","",getLanguageString(language,108),"REDO"));

		if(tbdesign == "3" || tbdesign == "4") {
			objToolbar1.add(objToolbars.createSeparator(imagePath + "tbend.gif"));
			objToolbar1.add(objToolbars.createDistance(3,true));
			objToolbar1.add(objToolbars.createSeparator(imagePath + "tbbegin.gif"));
		} else {
			objToolbar1.add(objToolbars.createSeparator(imagePath + "separator.gif"));
		}

		objToolbar1.add(objToolbars.createButton("",imagePath + "link.gif","",getLanguageString(language,109),"LINK"));
		objToolbar1.add(objToolbars.createButton("",imagePath + "anchor.gif","",getLanguageString(language,407),"ANCHOR"));

		var objImage = objToolbars.createMenuButton("",imagePath + "image.gif",imagePath + "colorselect.gif","onImageClicked",getLanguageString(language,110),"Image");
		objImage.popupwidth = "180";
		if(browser.ie)
			objImage.add(objToolbars.createMenuItem(getLanguageString(language,701),imagePath + "image.gif","","INSERTLOC"));
		objImage.add(objToolbars.createMenuItem(getLanguageString(language,702),imagePath + "world.gif","","INSERTWEB"));
		objImage.add(objToolbars.createMenuItem(getLanguageString(language,703),imagePath + "server.gif","","INSERTSERVER"));
		objImage.add(objToolbars.createMenuItem(getLanguageString(language,3005),imagePath + "upload.gif","","INSERTUP"));
		objToolbar1.add(objImage);

		// the URL of the popup is absolute or is relative to this page URL
    var objPopupButton = objToolbars.createPopupButton("",imagePath + "table.gif","onCreateTable",getLanguageString(language,111),"","popup/table2.html");
    objPopupButton.popupwidth   = "120px";
    objPopupButton.popupheight  = "132px";
	  objToolbar1.add(objPopupButton);

		objToolbar1.add(objToolbars.createButton("",imagePath + "rule.gif","",getLanguageString(language,112),"RULE"));

		// the path to popup URL has to be relative to this page
    var objPopupButton = objToolbars.createPopupButton("",imagePath + "char.gif","onCharClicked",getLanguageString(language,115),"","popup/char2.html");
    objPopupButton.popupwidth   = "78px";
    objPopupButton.popupheight  = "86px";
	  objToolbar1.add(objPopupButton);

		objToolbar1.add(objToolbars.createButton("",imagePath + "date.gif","",getLanguageString(language,116),"DATE"));
		objToolbar1.add(objToolbars.createButton("",imagePath + "time.gif","",getLanguageString(language,117),"TIME"));
		objToolbar1.add(objToolbars.createButton("",imagePath + "marquee.gif","",getLanguageString(language,409),"MARQUEE"));
		objToolbar1.add(objToolbars.createButton("",imagePath + "pagebreak.gif","",getLanguageString(language,404),"PAGEBREAK"));
		objToolbar1.add(objToolbars.createButton("",imagePath + "paragraph.gif","",getLanguageString(language,3002),"PARAGRAPH"));
		objToolbar1.add(objToolbars.createButton("",imagePath + "flash.gif","",getLanguageString(language,3303),"FLASH"));
		objToolbar1.add(objToolbars.createButton("",imagePath + "media.gif","",getLanguageString(language,3304),"MEDIA"));

		objToolbar1.add(objToolbars.createSeparator(imagePath + "separator.gif"));
		objToolbar1.add(objToolbars.createButton("",imagePath + "help.gif","",getLanguageString(language,114),"HELP"));

		if(tbdesign == "3" || tbdesign == "4") 
			objToolbar1.add(objToolbars.createSeparator(imagePath + "tbend.gif"));

		// add toolbar to collection
		objToolbars.add(objToolbar1);

		//-----------------------------------------------------------------------------------------
		// Toolbar II
		//-----------------------------------------------------------------------------------------
		// create toolbar object
		var objToolbar2 = objToolbars.createToolbar();
		// define event handler
		objToolbar2.action = "onToolbarButtonClick";
		// set values
		objToolbar2.design = tbdesign;
		objToolbar2.border = border;
		objToolbar2.height = 27;

		// create separator
		objToolbar2.add(objToolbars.createSeparator(imagePath + "tbbegin.gif"));

		// create a style combo
		cmbStyle = objToolbars.createStyleCombo("onStyleComboChanged",getLanguageString(language,3014),"STYLE");
		// set combo width
		cmbStyle.width = "100";
		// set popup window width
		cmbStyle.popupwidth = "130";
		// set popup window height
		cmbStyle.popupheight = "";
		// define which of the properties shall be used for display 
		cmbStyle.displayValue = "value";
		// add item
		cmbStyle.add(objToolbars.createStyleComboItem("Standard","","","Standard"));
		// add to toolbar
		objToolbar2.add(cmbStyle);

		objToolbar2.add(objToolbars.createDistance(3,false));

		// create a style combo
		cmbFormat = objToolbars.createStyleCombo("onFormatComboChanged",getLanguageString(language,3015),"FORMAT");
		// set combo width
		cmbFormat.width = "80";
		// set popup window width
		cmbFormat.popupwidth = "200";
		// set popup window height
		cmbFormat.popupheight = "120";
		// define which of the properties shall be used for display 
		cmbFormat.displayValue = "tag1";
		// add item
		cmbFormat.add(objToolbars.createStyleComboItem("Normal","","","NORMAL","Normal"));
		cmbFormat.add(objToolbars.createStyleComboItem("<h1 unselectable='ON'>Heading 1</h1>","","","<h1>","Heading 1"));
		cmbFormat.add(objToolbars.createStyleComboItem("<h2 unselectable='ON'>Heading 2</h2>","","","<h2>","Heading 2"));
		cmbFormat.add(objToolbars.createStyleComboItem("<h3 unselectable='ON'>Heading 3</h3>","","","<h3>","Heading 3"));
		cmbFormat.add(objToolbars.createStyleComboItem("<h4 unselectable='ON'>Heading 4</h4>","","","<h4>","Heading 4"));
		cmbFormat.add(objToolbars.createStyleComboItem("<h5 unselectable='ON'>Heading 5</h5>","","","<h5>","Heading 5"));
		cmbFormat.add(objToolbars.createStyleComboItem("<h6 unselectable='ON'>Heading 6</h6>","","","<h6>","Heading 6"));
		cmbFormat.add(objToolbars.createStyleComboItem("<address unselectable='ON'>Address</address>","","","<address>","Address"));
		cmbFormat.add(objToolbars.createStyleComboItem("<dir unselectable='ON'>Directory List</dir>","","","<dir>","Directory List"));
		cmbFormat.add(objToolbars.createStyleComboItem("<pre unselectable='ON'>Formatted</pre>","","","<pre>","Formatted"));
		cmbFormat.add(objToolbars.createStyleComboItem("<menu unselectable='ON'>Menu List</menu>","","","<menu>","Menu List"));

		// add to toolbar
		objToolbar2.add(cmbFormat);

		objToolbar2.add(objToolbars.createDistance(3,false));

		// create a style combo
		cmbFont = objToolbars.createStyleCombo("onFontComboChanged",getLanguageString(language,3016),"");
		// set combo width
		cmbFont.width = "114";
		// set popup window width
		cmbFont.popupwidth = "120";
		// set popup window height
		cmbFont.popupheight = "80";
		// define which of the properties shall be used for display 
		cmbFont.displayValue = "value";
		// add item
		cmbFont.add(objToolbars.createStyleComboItem("<FONT unselectable='ON' face='Arial'>Arial</font>","","","Arial"));
		cmbFont.add(objToolbars.createStyleComboItem("<FONT unselectable='ON' face='Courier'>Courier</font>","","","Courier"));
		cmbFont.add(objToolbars.createStyleComboItem("<FONT unselectable='ON' face='Courier New'>Courier New</font>","","","Courier New"));
		cmbFont.add(objToolbars.createStyleComboItem("<FONT unselectable='ON' face='Tahoma'>Tahoma</font>","","","Tahoma"));
		cmbFont.add(objToolbars.createStyleComboItem("<FONT unselectable='ON' face='Times New Roman'>Times New Roman</font>","","","Times New Roman"));
		cmbFont.add(objToolbars.createStyleComboItem("<FONT unselectable='ON' face='Verdana'>Verdana</font>","","","Verdana"));
		// add to toolbar
		objToolbar2.add(cmbFont);

		objToolbar2.add(objToolbars.createDistance(3,false));

		// create a style combo
		cmbFontSize = objToolbars.createStyleCombo("onFontSizeComboChanged",getLanguageString(language,3017),"");
		// set combo width
		cmbFontSize.width = "35";
		// set popup window width
		cmbFontSize.popupwidth = "80";
		// set popup window height
		cmbFontSize.popupheight = "100";
		// define which of the properties shall be used for display 
		cmbFontSize.displayValue = "tag1";

		// add item
		cmbFontSize.add(objToolbars.createStyleComboItem("<FONT unselectable='ON' size=1>8</FONT>","","","1","8"));
		cmbFontSize.add(objToolbars.createStyleComboItem("<FONT unselectable='ON' size=2>10</FONT>","","","2","10"));
		cmbFontSize.add(objToolbars.createStyleComboItem("<FONT unselectable='ON' size=3>12</FONT>","","","3","12"));
		cmbFontSize.add(objToolbars.createStyleComboItem("<FONT unselectable='ON' size=4>14</FONT>","","","4","14"));
		cmbFontSize.add(objToolbars.createStyleComboItem("<FONT unselectable='ON' size=5>18</FONT>","","","5","18"));
		cmbFontSize.add(objToolbars.createStyleComboItem("<FONT unselectable='ON' size=6>24</FONT>","","","6","24"));
		cmbFontSize.add(objToolbars.createStyleComboItem("<FONT unselectable='ON' size=7>36</FONT>","","","7","36"));
		// add to toolbar
		objToolbar2.add(cmbFontSize);

		// those variables are set to improve performance during status update
		btnBold = objToolbars.createButton("",imagePath + "bold.gif","",getLanguageString(language,201),"BOLD");
		objToolbar2.add(btnBold);
		btnItalic = objToolbars.createButton("",imagePath + "italic.gif","",getLanguageString(language,202),"ITALIC");
		objToolbar2.add(btnItalic);
		btnUnderline = objToolbars.createButton("",imagePath + "underline.gif","",getLanguageString(language,203),"UNDERLINE")
		objToolbar2.add(btnUnderline);
		btnSuperscript = objToolbars.createButton("",imagePath + "superscript.gif","",getLanguageString(language,204),"SUPERSCRIPT")
		objToolbar2.add(btnSuperscript);
		btnSubscript = objToolbars.createButton("",imagePath + "subscript.gif","",getLanguageString(language,205),"SUBSCRIPT")
		objToolbar2.add(btnSubscript);
		objToolbar2.add(objToolbars.createSeparator(imagePath + "separator.gif"));
		btnJustifyLeft = objToolbars.createButton("",imagePath + "left.gif","",getLanguageString(language,206),"JUSTIFYLEFT")
		objToolbar2.add(btnJustifyLeft);
		btnJustifyCenter = objToolbars.createButton("",imagePath + "center.gif","",getLanguageString(language,207),"JUSTIFYCENTER")
		objToolbar2.add(btnJustifyCenter);
		btnJustifyRight = objToolbars.createButton("",imagePath + "right.gif","",getLanguageString(language,208),"JUSTIFYRIGHT")
		objToolbar2.add(btnJustifyRight);
		btnJustifyFull = objToolbars.createButton("",imagePath + "block.gif","",getLanguageString(language,209),"JUSTIFYFULL")
		objToolbar2.add(btnJustifyFull);
		objToolbar2.add(objToolbars.createSeparator(imagePath + "separator.gif"));
		btnInsertOrderedList = objToolbars.createButton("",imagePath + "orderedlist.gif","",getLanguageString(language,210),"INSERTORDEREDLIST")
		objToolbar2.add(btnInsertOrderedList);
		btnInsertUnorderedList = objToolbars.createButton("",imagePath + "unorderedlist.gif","",getLanguageString(language,211),"INSERTUNORDEREDLIST")
		objToolbar2.add(btnInsertUnorderedList);
		objToolbar2.add(objToolbars.createButton("",imagePath + "indent.gif","",getLanguageString(language,212),"INDENT"));
		objToolbar2.add(objToolbars.createButton("",imagePath + "outdent.gif","",getLanguageString(language,213),"OUTDENT"));
		objToolbar2.add(objToolbars.createSeparator(imagePath + "separator.gif"));
    btnColor = objToolbars.createColorButton(imagePath + "color.gif",imagePath + "colorselect.gif","onChangeTextColor",getLanguageString(language,215),getLanguageString(language,217),"");
		objToolbar2.add(btnColor);
		btnBackColor = objToolbars.createColorButton(imagePath + "backcolor.gif",imagePath + "colorselect.gif","onChangeBackgroundColor",getLanguageString(language,216),getLanguageString(language,218),"");
		objToolbar2.add(btnBackColor);

		if(tbdesign == "3" || tbdesign == "4")
			objToolbar2.add(objToolbars.createSeparator(imagePath + "tbend.gif"));

		// add toolbar to collection
		objToolbars.add(objToolbar2);

		//-----------------------------------------------------------------------------------------
		// Toolbar III
		//-----------------------------------------------------------------------------------------
		// create toolbar object
		var objToolbar3 = objToolbars.createToolbar();
		// define event handler
		objToolbar3.action = "onToolbarButtonClick";
		// set values
		objToolbar3.design = tbdesign;
		objToolbar3.border = border;
		objToolbar3.height = 27;

		objToolbar3.add(objToolbars.createSeparator(imagePath + "tbbegin.gif"));
		objToolbar3.add(objToolbars.createButton("",imagePath + "page.gif","",getLanguageString(language,3398),"PAGE"));
		objToolbar3.add(objToolbars.createButton("",imagePath + "form.gif","",getLanguageString(language,406),"FORM"));
		if(browser.ie)
			objToolbar3.add(objToolbars.createButton("",imagePath + "label.gif","",getLanguageString(language,301),"LABEL"));
		objToolbar3.add(objToolbars.createButton("",imagePath + "button.gif","",getLanguageString(language,302),"BUTTON"));
		objToolbar3.add(objToolbars.createButton("",imagePath + "input.gif","",getLanguageString(language,303),"INPUT"));
		objToolbar3.add(objToolbars.createButton("",imagePath + "checkbox.gif","",getLanguageString(language,304),"CHECK"));
		objToolbar3.add(objToolbars.createButton("",imagePath + "radio.gif","",getLanguageString(language,305),"OPTION"));
		objToolbar3.add(objToolbars.createButton("",imagePath + "combobox.gif","",getLanguageString(language,306),"COMBO"));
		objToolbar3.add(objToolbars.createButton("",imagePath + "listbox.gif","",getLanguageString(language,307),"LISTBOX"));
		objToolbar3.add(objToolbars.createButton("",imagePath + "textarea.gif","",getLanguageString(language,308),"AREA"));
		objToolbar3.add(objToolbars.createButton("",imagePath + "hidden.gif","",getLanguageString(language,410),"HIDDEN"));
		objToolbar3.add(objToolbars.createSeparator(imagePath + "separator.gif"));
		objToolbar3.add(objToolbars.createButton("",imagePath + "div.gif","",getLanguageString(language,310),"DIV"));
		if(browser.ie)
  		objToolbar3.add(objToolbars.createButton("",imagePath + "iframe.gif","",getLanguageString(language,309),"IFRAME"));
		objToolbar3.add(objToolbars.createSeparator(imagePath + "separator.gif"));
		objToolbar3.add(objToolbars.createButton("",imagePath + "position.gif","",getLanguageString(language,311),"POSITION"));

		if(tbdesign == "3" || tbdesign == "4") {
			objToolbar3.add(objToolbars.createSeparator(imagePath + "tbend.gif"));
			objToolbar3.add(objToolbars.createDistance(3,true));
			objToolbar3.add(objToolbars.createSeparator(imagePath + "tbbegin.gif"));
		} else {
			objToolbar3.add(objToolbars.createSeparator(imagePath + "separator.gif"));
		}
		// create a style combo
		cmbTM = objToolbars.createStyleCombo("",getLanguageString(language,3320),"TM");
		// set combo width
		cmbTM.width = "100";
		// set popup window width
		cmbTM.popupwidth = "120";
		// set popup window height
		cmbTM.popupheight = "";
		// define which of the properties shall be used for display 
		cmbTM.displayValue = "value";
		// add to toolbar
		objToolbar3.add(cmbTM);
		objToolbar3.add(objToolbars.createButton("",imagePath + "tm_add.gif","",getLanguageString(language,312),"TMADD"));
		objToolbar3.add(objToolbars.createButton("",imagePath + "tm_create.gif","",getLanguageString(language,313),"TMCREATE"));
		objToolbar3.add(objToolbars.createButton("",imagePath + "tm_remove.gif","",getLanguageString(language,314),"TMREMOVE"));

		if(tbdesign == "3" || tbdesign == "4") {
			objToolbar3.add(objToolbars.createSeparator(imagePath + "tbend.gif"));
			objToolbar3.add(objToolbars.createDistance(3,true));
			objToolbar3.add(objToolbars.createSeparator(imagePath + "tbbegin.gif"));
		} else {
			objToolbar3.add(objToolbars.createSeparator(imagePath + "separator.gif"));
		}
    // full size mode
		objToolbar3.add(objToolbars.createButton("",imagePath + "fullsize.gif","",getLanguageString(language,3397),"FULLSIZE"));
		// create cleaner menu button
		var objCleaner = objToolbars.createMenuButton("",imagePath + "new.gif",imagePath + "colorselect.gif","onCleanerClicked",getLanguageString(language,214),"CLEANER");
		objCleaner.popupwidth = "215";
		objCleaner.add(objToolbars.createMenuItem(getLanguageString(language,3315),imagePath + "removestyle.gif","","RMSTYLE"));
		objCleaner.add(objToolbars.createMenuItem(getLanguageString(language,3316),imagePath + "removeformat.gif","","RMFORMAT"));
		objCleaner.add(objToolbars.createMenuItem(getLanguageString(language,3317),imagePath + "removetag.gif","","RMEMPTY"));
		objCleaner.add(objToolbars.createMenuItem(getLanguageString(language,3318),imagePath + "removeword.gif","","RMWORD"));
    objCleaner.add(objToolbars.createMenuItem(getLanguageString(language,3396),imagePath + "removespan.gif","","RMSPAN"));
		// add to toolbar
		objToolbar3.add(objCleaner);
		objToolbar3.add(objToolbars.createButton("",imagePath + "bron.gif","",getLanguageString(language,3013),"RETURN"));
		objToolbar3.add(objToolbars.createButton("",imagePath + "tablehigh.gif","",getLanguageString(language,3003),"HIGHLIGHT"));
		objToolbar3.add(objToolbars.createButton("",imagePath + "selectall.gif","",getLanguageString(language,405),"SELECTALL"));
		objToolbar3.add(objToolbars.createButton("",imagePath + "edit.gif","",getLanguageString(language,3301),"EDITABLE"));

		// create a style combo
		cmbZoom = objToolbars.createStyleCombo("onZoomClicked",getLanguageString(language,3319),"");
		// set combo width
		cmbZoom.width = "50";
		// set popup window width
		cmbZoom.popupwidth = "40";
		// set popup window height
		cmbZoom.popupheight = "120";
		// define which of the properties shall be used for display 
		cmbZoom.displayValue = "value";
		cmbZoom.add(objToolbars.createStyleComboItem("10%","","","10%"));
		cmbZoom.add(objToolbars.createStyleComboItem("50%","","","50%"));
		cmbZoom.add(objToolbars.createStyleComboItem("100%","","","100%"));
		cmbZoom.add(objToolbars.createStyleComboItem("150%","","","150%"));
		cmbZoom.add(objToolbars.createStyleComboItem("200%","","","200%"));
		cmbZoom.selectedIndex  = 2;
		// add to toolbar
		objToolbar3.add(cmbZoom);
		if(tbdesign == "3" || tbdesign == "4")
			objToolbar3.add(objToolbars.createSeparator(imagePath + "tbend.gif"));

		// add toolbar to collection
		objToolbars.add(objToolbar3);
	}

	// which toolbar do we have
	if(id == "toolbar_bottom") {
		// we need this variable later
		toolbarsBottom = objToolbars;
		// create toolbar object
		var objToolbar1 = objToolbars.createToolbar();
		// define event handler
		objToolbar1.action = "onToolbarButtonClickBottom";
		// set values
		objToolbar1.design = tbdesign;
		objToolbar1.border = border;
		objToolbar1.height = 27;

		objToolbar1.add(objToolbars.createSeparator(imagePath + "tbbegin.gif"));

		objToolbar1.add(objToolbars.createCheckButton("Edit",imagePath + "edit.gif","","Edit","EDIT",true,"GROUP"));
		objToolbar1.add(objToolbars.createCheckButton("Html",imagePath + "html.gif","","Html","HTML",false,"GROUP"));
		objToolbar1.add(objToolbars.createCheckButton("Preview",imagePath + "preview.gif","","Preview","PREVIEW",false,"GROUP"));

		objToolbar1.add(objToolbars.createSeparator(imagePath + "tbend.gif"));

		// add toolbar to collection
		objToolbars.add(objToolbar1);
	}

	// now create toolbar
	objToolbars.create();

}


//---------------------------------------------------------------------------------------------
// Editor events
//---------------------------------------------------------------------------------------------
// is called after mouse down
function editOnEditorMouseDown()
{
	// if there are open popups they will be closed
	toolbarsTop.reset();
  if(objMenuBar) {
    var objMenu = objMenuBar.getActiveBarItem();
    if(objMenu)
      objMenu.reset();
  }

}

// Is called when Text Modules are changed
function editOnProcessTM(aData)
{			
	try {
		// get text module combo
		var combo = toolbarsTop.getElementByTag("TM");
		// clear first because all are readed
		combo.clear();
		// insert text modules
		for(var i=0;i<aData.length-1;i++) {
			var aText = aData[i].split("|");
			combo.add(toolbarsTop.createStyleComboItem(aText[0],"","",aText[0],aText[1]));
		}
		combo.setSelectedIndex(0);
	} catch(error) {}
}

// is called when toolbar status has changed (Buttons, Combo)
function editOnChangeToolbar(data)
{
	try {
		var aData = data.split(":");
		// set button status
		btnBold.setStatus(aData[0]>0 ? true:false);
		btnItalic.setStatus(aData[1]>0 ? true:false);
		btnUnderline.setStatus(aData[2]>0 ? true:false);
		btnSuperscript.setStatus(aData[3]>0 ? true:false);
		btnSubscript.setStatus(aData[4]>0 ? true:false);
		btnJustifyLeft.setStatus(aData[5]>0 ? true:false);
		btnJustifyCenter.setStatus(aData[6]>0 ? true:false);
		btnJustifyRight.setStatus(aData[7]>0 ? true:false);
		btnJustifyFull.setStatus(aData[8]>0 ? true:false);
		btnInsertOrderedList.setStatus(aData[9]>0 ? true:false);
		btnInsertUnorderedList.setStatus(aData[10]>0 ? true:false);

		// set combo
		if(cmbStyle)
			cmbStyle.setSelectedText(aData[11]);
		if(cmbFormat)
			cmbFormat.setSelectedText(aData[12]);
		if(cmbFont)
			cmbFont.setSelectedText(aData[13]);
		if(cmbFontSize)
			cmbFontSize.setSelectedText(aData[14]);
		if(btnColor)
			btnColor.setColor(aData[15]);
		if(btnBackColor)
			btnBackColor.setColor(aData[16]);
	} catch(error) {}
}

// called for each style that has to be set because the editor detected styles in document
function editOnAddStyle(name,html)
{
	try {
		// get style combo
		var combo = toolbarsTop.getElementByTag("STYLE");
		// clear combo
		if(name == "") {
			combo.clear();
		} else {
			combo.add(toolbarsTop.createStyleComboItem(html,"","",name));
		}
	} catch(error) {}
}

// called for each style settings with Hx elements. Therefore display has to be changed
function editOnChangeFormat(name,html)
{
	try {
		var combo = toolbarsTop.getElementByTag("FORMAT");
		var item = combo.getItemByValue("<" + name + ">");
		item.text = html + item.tag1 + "</span>";
	} catch(error) {}

}

function editOnReadOnly(value)
{
  if(value) {
		document.getElementById("toolbar_top").style.display = "none";  
		document.getElementById("toolbar_bottom").style.display = "none";  
  } else {
		document.getElementById("toolbar_top").style.display = "inline";  
		document.getElementById("toolbar_bottom").style.display = "inline";  
  }
}


//-------------------------------------------------------------------------------------------
// User defined Toolbar events
//-------------------------------------------------------------------------------------------
// called when button is clicked (independant of toolbar row)
function onToolbarButtonClick(id)
{
  // get object
  var button = toolbarsTop.getElementById(id);

	// 1. toolbar
  if(button.tag == "NEW")	                 editNew();  
  if(button.tag == "OPEN")	               editOpen(1);  
  if(button.tag == "SAVE")	               onSaveFile();  
  if(button.tag == "SAVEAS")	             editSaveDialog();  
  if(button.tag == "SAVELOCAL")	           editSaveLocal();  
  if(button.tag == "SEARCH")	             editSearch();  
  if(button.tag == "PRINT")	               editPrint();  
  if(button.tag == "PREVIEW")	             editPreview();  
  if(button.tag == "SPELL")	               editSpell();  
  if(button.tag == "CUT")	                 editCut();  
  if(button.tag == "COPY")	               editCopy();  
  if(button.tag == "PASTE")	               editPaste();  
  if(button.tag == "PASTEWORD")	           editPasteWord();  
  if(button.tag == "PASTETEXT")	           editPasteText();  
  if(button.tag == "UNDO")	               editUndo();  
  if(button.tag == "REDO")	               editRedo();  
  if(button.tag == "LINK")	               editLink();  
  if(button.tag == "ANCHOR")	             editInsertObject('ANCHOR');  
  if(button.tag == "RULE")	               editInsertObject('RULE');  
  if(button.tag == "DATE")	               editInsertDate();  
  if(button.tag == "TIME")	               editInsertTime();  
  if(button.tag == "MARQUEE")	             editInsertObject('MARQUEE');  
  if(button.tag == "PAGEBREAK")	           editInsertObject('PAGEBREAK');  
  if(button.tag == "PARAGRAPH")	           editParagraph();  
  if(button.tag == "EDITABLE")	           editSetEditable(false);  
  if(button.tag == "FLASH")	               editOpen(4);  
  if(button.tag == "MEDIA")	               editOpen(5);  
  if(button.tag == "HELP")	               onHelp();  

	// 2. toolbar
  if(button.tag == "BOLD")	               editBold();  
  if(button.tag == "ITALIC")	             editItalic();  
  if(button.tag == "UNDERLINE")	           editUnderline();  
  if(button.tag == "SUPERSCRIPT")	         editSuperscript();  
  if(button.tag == "SUBSCRIPT")	           editSubscript();  
  if(button.tag == "JUSTIFYLEFT")	         editJustifyLeft();  
  if(button.tag == "JUSTIFYCENTER")	       editJustifyCenter();  
  if(button.tag == "JUSTIFYRIGHT")	       editJustifyRight();  
  if(button.tag == "JUSTIFYFULL")	         editJustifyFull();  
  if(button.tag == "INSERTORDEREDLIST")	   editOrderedList();  
  if(button.tag == "INSERTUNORDEREDLIST")	 editUnorderedList();  
  if(button.tag == "INDENT")	             editIndent();  
  if(button.tag == "OUTDENT")	             editOutdent();  

  // 3. toolbar
  if(button.tag == "PAGE")	               editProperties(4);  
  if(button.tag == "FORM")	               editInsertObject('FORM');  
  if(button.tag == "LABEL")	               editInsertObject('LABEL');  
  if(button.tag == "BUTTON")	             editInsertObject('BUTTON');  
  if(button.tag == "INPUT")	               editInsertObject('INPUT');  
  if(button.tag == "CHECK")	               editInsertObject('CHECK');  
  if(button.tag == "OPTION")	             editInsertObject('OPTION');  
  if(button.tag == "COMBO")	               editInsertObject('COMBO');  
  if(button.tag == "LISTBOX")	             editInsertObject('LISTBOX');  
  if(button.tag == "AREA")	               editInsertObject('AREA');  
  if(button.tag == "HIDDEN")	             editInsertObject('HIDDEN');  
  if(button.tag == "DIV")	                 editInsertObject('DIV');  
  if(button.tag == "IFRAME")	             editInsertObject('IFRAME');  
  if(button.tag == "POSITION")	           editAbsolute();  
  if(button.tag == "TMADD")							   onInsertTextModule();  
  if(button.tag == "TMCREATE")						 editSetTextModule();  
  if(button.tag == "TMREMOVE")						 onDeleteTextModule();  
  if(button.tag == "RETURN")							 onReturnMode();  
  if(button.tag == "HIGHLIGHT")	           editTableHighlight();  
  if(button.tag == "SELECTALL")	           editSelectAll();  
  if(button.tag == "FULLSIZE")	           editFullSize();  
}

// for lower toolbar
function onToolbarButtonClickBottom(id)
{
  // get object
  var button = toolbarsBottom.getElementById(id);
  // hide top toolbar in HTML/PREVIEW
  if(button.tag == "HTML" || button.tag == "PREVIEW") {
		document.getElementById("toolbar_top").style.display = "none";  
  } else {
		document.getElementById("toolbar_top").style.display = "inline";  
  }

	editSetMode(button.tag);
  editSetActiveMode(button.tag);
}

// if NEW menu button item has been clicked
function onNewClicked(id)
{
  var menu = toolbarsTop.getElementById(id);
  if(menu.selectedItem.value == "NEW")      editNew();
  if(menu.selectedItem.value == "TEMPLATE") editOpen(6);
}

// if UPLOAD menu button item has been clicked
function onMenuUploadClicked(id)
{
  var menu = toolbarsTop.getElementById(id);
  if(menu.selectedItem.value == "UPLOADIMG") editUpload("","",0);
  if(menu.selectedItem.value == "UPLOADDOC") editUpload("","",1);
}

function onCreateTable(id)
{
  var popup = toolbarsTop.getElementById(id);
	// in value we have row:col (if cancel then value = "")
	if(popup.value != "") {
		var temp = popup.value.split(":");
		editCreateTable(temp[0],temp[1]);
	}
}

function onCharClicked(id)
{
  var popup = toolbarsTop.getElementById(id);
	// in value we have character
	if(popup.value != "")
		editInsertHtml(popup.value);
}

// if IMAGE menu button item has been clicked
function onImageClicked(id)
{
  var menu = toolbarsTop.getElementById(id);
  if(menu.selectedItem.value == "INSERTLOC")    editInsertObject('IMAGE');
  if(menu.selectedItem.value == "INSERTWEB")    editInsertObject('IMAGEWEB');
  if(menu.selectedItem.value == "INSERTSERVER") editOpen(3);
  if(menu.selectedItem.value == "INSERTUP")     editUpload("","",0,true);
}

// called if style combo changed
function onStyleComboChanged(id)
{
  var combo = toolbarsTop.getElementById(id);
  if(combo.selectedItem.value == "STANDARD")
    editSetStyle("");
  else
    editSetStyle(combo.selectedItem.value);
}

// called if format combo changed
function onFormatComboChanged(id)
{
  var combo = toolbarsTop.getElementById(id);
	var value = combo.selectedItem.value;
  if (value == 'NORMAL') {
    editFormat('Normal');
    editRemoveFormat();
  } else {
    editFormat(value);
  }
}

// called if font combo changed
function onFontComboChanged(id)
{
  var combo = toolbarsTop.getElementById(id);
	var value = combo.selectedItem.value;
	editFont(value);
}

// called if font size combo changed
function onFontSizeComboChanged(id)
{
  var combo = toolbarsTop.getElementById(id);
	var value = combo.selectedItem.value;
	editFontSize(value);
}

// called if CLEANER menu button item has been clicked
function onCleanerClicked(id)
{
  var menu = toolbarsTop.getElementById(id);
  if(menu.selectedItem.value == "RMSTYLE")    editClean(0);
  if(menu.selectedItem.value == "RMFORMAT")   editClean(1); 
  if(menu.selectedItem.value == "RMEMPTY")    editClean(2); 
  if(menu.selectedItem.value == "RMWORD")     editClean(3); 
  if(menu.selectedItem.value == "RMSPAN")     editRemoveTagWithoutContent('span'); 
}

// called if ZOOM menu button item has been clicked
function onZoomClicked(id)
{
  var menu = toolbarsTop.getElementById(id);
  editZoom(menu.selectedItem.value)
}

// called if COLOR has been changed
function onChangeTextColor(id)
{
  var button = toolbarsTop.getElementById(id);
  editColor(button.color)
}

function onChangeBackgroundColor(id)
{
  var button = toolbarsTop.getElementById(id);
  editBackColor(button.color)
}

// called if RETURNMODE has been changed
function onReturnMode()
{
  var button = toolbarsTop.getElementByTag("RETURN");
  if(editGetBr()) {
    editSetBr(false);
    button.setImage(imagePath + "broff.gif");
  } else {
    editSetBr(true);
    button.setImage(imagePath + "bron.gif");
  }
}

// id save button is clicked
function onSaveFile()
{
	// if no file is loaded then open save dialog
	if(editGetFileUrl() == "")
		editSaveDialog();
	else
		editSave();
}

// if a text snippet has to be inserted
function onInsertTextModule()
{
	var combo = toolbarsTop.getElementByTag("TM");
	editInsertHtml(combo.getSelected().tag1);
}

// if a text snippet has to be deleted
function onDeleteTextModule()
{
	var combo = toolbarsTop.getElementByTag("TM");
	editDeleteTextModule(combo.getSelected().text);  
}

// help function
function onHelp()
{
  var left = screen.width/2 - 400/2;
  var top = screen.height/2 - 600/2;
  window.open(__editGetEditorUrl() + "userhelp.html","","left=" + left + ",top=" + top + ",height=600,width=400,resizable=1,status=0,scrollbars=1");
}

