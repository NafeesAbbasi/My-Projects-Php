//------------------------------------------------------------------------------------------------
// This script creates toolbar from a toolbar configuration string passed in tab parameter i.e.
// pinEdit.html?tb=T010203;B5354
// 
// Full string:
// T01020365730405766466SE0607086177SE0910SE1160121314151617625767707172SE99;
// T195620212223242526SE27282930SE31323334SE3536;
// T7559383940414243444563SE4647SE48SE49505152SE783768695818;
// B535455
// 
// T means Toolbar Top
// B means Toolbar Bottom
//
//
// NEW       1
// OPEN      2
// SAVE      3
// SEARCH    4
// PRINT     5
// SPELL     64
// CUT       6
// COPY      7
// PASTE     8
// PASTEWORD 61
// UNDO      9
// REDO      10
// LINK      11
// ANCHOR    60
// IMAGE     12
// TABLE     13
// RULE      14
// CHAR      15
// DATE      16
// TIME      17
// MARQUEE   62
// PAGEBREAK 57
// ZOOM      18
// REMOVE    37
// SELALL    58

// STYLE     19
// FORMAT    56
// FONT      20
// FONTSIZE  21
// BOLD      22
// ITALIC    23
// UNDERLINE 24
// SUPER     25
// SUB       26
// LEFT      27
// CENTER    28
// RIGHT     29
// BLOCK     30
// ORDERED   31
// UNORDERED 32
// INDENT    33
// OUTDENT   34
// COLOR     35
// BACKCOLOR 36

// FORM      59
// LABEL     38
// BUTTON    39
// INPUT     40
// CHECKBOX  41
// RADIO     42
// COMBO     43
// LIST      44
// AREA      45
// HIDDEN    63
// DIV       46
// IFRAME    47
// ABSOLUTE  48
// TMLIST    49
// TMADD     50
// TMNEW     51
// TMREMOVE  52
// EDIT      53
// HTML      54
// PREVIEW   55

// SAVEAS    65
// UPLOAD    66
// PARAGRAPH 67
// RETURNMODE68
// HIGHLIGHT 69
// FLASH     70
// MEDIA     71
// EDITABLE  72
// SAVELOC   73
// TEMPLATE  74
// PAGE      75
// PRINTPREVIEW 76
// PASTETEXT 77
// FULLSIZE  78

// HELP      99


// those variables are needed in toolbar button click event

var toolbarsTop    = null;
var toolbarsBottom = null;

// the toolbar design (3 == default)
var tbdesign = "3";
  
// image path
var imagePath = "";

// these buttons are needed to update state quickly after mouse click or key press in editor
var btnBold = null;
var btnItalic = null;
var btnUnderline = null;
var btnSuperscript = null;
var btnSubscript = null;
var btnJustifyLeft = null;
var btnJustifyCenter = null;
var btnJustifyRight = null;
var btnJustifyFull = null;
var btnInsertOrderedList = null;
var btnInsertUnorderedList = null;
var btnColor = null;
var btnBackColor = null;

// those combos are needed to update content after mouse click or key press in editor
var cmbStyle = null;
var cmbFormat = null;
var cmbFont = null;
var cmbFontSize = null;

//---------------------------------------------------------------------------------------------
// Toolbar events
//---------------------------------------------------------------------------------------------
// objToolbars: root object of toolbar
// id:          id of IFRAME where the toolbar runs ( needed of there a re multiple objects)
//---------------------------------------------------------------------------------------------
function toolbarCreate(objToolbars, id)
{
  if(design == "")
    tbdesign = "1";
  if(design == "Office")
    tbdesign = "2";
  if(design == "Office2003")
    tbdesign = "3";
  if(design == "Office2003S")
    tbdesign = "4";
  imagePath = "design/image/style" + tbdesign + "/";
  
  var hasTop    = false;
  var hasBottom = false;
  var hasTM     = false;
  var hasEdit   = false;

  // search for 'B'
  var bottomPos = __editToolbarString.indexOf("B")

	// which toolbar do we have
	if(id == "toolbar_top") {
    toolbarsTop = objToolbars;
    if(__editToolbarString.indexOf("T") >= 0) {
      if(bottomPos > 0) {
        var config = __editToolbarString.substring(0,bottomPos-1);
      } else {
        var config = __editToolbarString;
      }
    } else {
      document.getElementById(id).style.height = 0;
      return;
    }
  }
	// which toolbar do we have
	if(id == "toolbar_bottom") {
    toolbarsBottom = objToolbars;
    if(bottomPos >= 0) {
      var config = __editToolbarString.substring(bottomPos);
    } else {
      document.getElementById(id).style.height = 0;
      return;
    }
  }

  if(config == "")
    return;
    
  // remove last ;
  if(config.substring(config.length-1,config.length) == ";")
    config = config.substring(0,config.length-1);
  
  // which toolbars do we have
  var aToolbars = config.split(";");

  // set height of toolbars  
  document.getElementById(id).style.height = aToolbars.length * 28;

  for(var i=0;i<aToolbars.length;i++) {
    var where = aToolbars[i].substring(0,1);
    if(where == "T") {
      hasTop = true;
    } else if( where == "B")  {
      hasBottom = true;
    } else {
      alert("Unknown toolbar key: " + where);
      return;
    }
    var buttons = aToolbars[i].substring(1,aToolbars[i].length);
    var aItems = new Array(buttons.length/2);
    for(var k=0;k<aItems.length;k++) {
      aItems[k] = buttons.substring(k*2,(k*2) +2);
    }
    var objToolbar = objToolbars.createToolbar();
    objToolbar.design = tbdesign;
    objToolbar.border = globalToolbarBorder;
    objToolbar.height = 27;

	  if(id == "toolbar_top")
		  objToolbar.action = "onToolbarButtonClick";
	  if(id == "toolbar_bottom")
		  objToolbar.action = "onToolbarButtonClickBottom";

		objToolbar.add(objToolbars.createSeparator(imagePath + "tbbegin.gif"));
    hasButton = false;
    for(var j=0;j<aItems.length;j++) {
      if(aItems[j] == "49")
        hasTM = true;
      if(aItems[j] == "53")
        hasEdit = true;
      if(__toolbar_addItem(objToolbars,objToolbar,aItems[j]))
        hasButton = true;
    }
    if(design =="Office2003")
      //objToolbar.add(new Separator("design/image/" + design + "/tbend.gif"));
  		objToolbar.add(objToolbars.createSeparator(imagePath + "tbend.gif"));
    if(hasButton)
      objToolbars.add(objToolbar);
  }

  objToolbars.create();

  if(hasEdit){
    btnEdit.setStatus(true);
  }

  if(hasTM) {
    // read available text modules from server
    editGetTextModule();
  }
  
}

function __toolbar_addItem(objToolbars,objToolbar, item)
{
  switch(item) {
    case "FM": //File Menu Title
		objToolbar.add(objToolbars.createSeparator(imagePath + "mFile_Title.gif"));
		return false;

	case "EM": //Edit Menu Title
		objToolbar.add(objToolbars.createSeparator(imagePath + "mEdit_Title.gif"));
		return false;
	
	case "IM": //Insert Menu Title
		objToolbar.add(objToolbars.createSeparator(imagePath + "mInsert_Title.gif"));
		return false;

	case "FR": //Format Menu Title
		//objToolbar.add(objToolbars.createSeparator(imagePath + "mFormat_Title.gif"));
		return false;
	
	case "SM": //Signature Menu Title
		objToolbar.add(objToolbars.createSeparator(imagePath + "mSignature_Title.gif"));
		return false;

	case "SP":  // Separator
   		objToolbar.add(objToolbars.createSeparator(imagePath + "mSeparator_line.gif"));
      return false;

	case "SE":  // Separator
   		objToolbar.add(objToolbars.createSeparator(imagePath + "separator.gif"));
      return false;

    case "01":  // New
		  var objNew = objToolbars.createMenuButton("",imagePath + "new.gif",imagePath + "selector.gif","onNewClicked",getLanguageString("",101),"New");
		  objNew.popupwidth = "150";
		  objNew.add(objToolbars.createMenuItem(getLanguageString("",101),imagePath + "new.gif","","NEW"));
		  objNew.add(objToolbars.createMenuItem(getLanguageString("",3313),imagePath + "template.gif","","TEMPLATE"));
		  // add to toolbar
		  objToolbar.add(objNew);
      return true;

    case "02": // Open
   		objToolbar.add(objToolbars.createButton("",imagePath + "open.gif","",getLanguageString(language,102),"OPEN"));
      return true;

    case "03":  // Save
		  objToolbar.add(objToolbars.createButton("",imagePath + "save.gif","",getLanguageString(language,118),"SAVE"));
      return true;

    case "65": // Save As
      objToolbar.add(objToolbars.createButton("",imagePath + "saveas.gif","",getLanguageString(language,119),"SAVEAS"));
      return true;

    case "73": // Save local
		  objToolbar.add(objToolbars.createButton("",imagePath + "savelocal.gif","",getLanguageString(language,3300),"SAVELOCAL"));
      return true;

    case "04": // Search
      objToolbar.add(objToolbars.createButton("",imagePath + "search.gif","",getLanguageString(language,113),"SEARCH"));
      return true;

    case "05": // Print
		  objToolbar.add(objToolbars.createButton("",imagePath + "print.gif","",getLanguageString(language,103),"PRINT"));
      return true;

    case "76": // Preview
		  objToolbar.add(objToolbars.createButton("",imagePath + "preview.gif","",getLanguageString(language,3314),"PREVIEW"));
      return true;

	case "64": // Spell
		  //objToolbar.add(objToolbars.createButton("",imagePath + "spell.gif","",getLanguageString(language,411),"SPELL"));
		  objToolbar.add(objToolbars.createButton("",imagePath + "spell.gif","",getLanguageString(language,411),"SPELL"));
      return true;

	//for frontend
	case "98": // Spell
		  //objToolbar.add(objToolbars.createButton("",imagePath + "spell.gif","",getLanguageString(language,411),"SPELL"));
		  objToolbar.add(objToolbars.createButton("",imagePath + "spell.gif","",getLanguageString(language,411),"SPELLFRONT"));
      return true;

    case "66": // Upload
		  var objUpload = objToolbars.createMenuButton("",imagePath + "upload.gif",imagePath + "colorselect.gif","onMenuUploadClicked",getLanguageString(language,3011),"Upload");
		  objUpload.popupwidth = "190";
		  objUpload.add(objToolbars.createMenuItem(getLanguageString(language,704),imagePath + "upload.gif","","UPLOADIMG"));
		  objUpload.add(objToolbars.createMenuItem(getLanguageString(language,3010),imagePath + "upload.gif","","UPLOADDOC"));
		  objToolbar.add(objUpload);
      return true;

    case "06":  // Cut
   		objToolbar.add(objToolbars.createButton("",imagePath + "cut.gif","",getLanguageString(language,104),"CUT"));
      return true;

    case "07":  // Copy
		  objToolbar.add(objToolbars.createButton("",imagePath + "copy.gif","",getLanguageString(language,105),"COPY"));
      return true;

    case "08":  // Paste
		  objToolbar.add(objToolbars.createButton("",imagePath + "paste.gif","",getLanguageString(language,106),"PASTE"));
      return true;

    case "61":  // Paste Word
		  objToolbar.add(objToolbars.createButton("",imagePath + "pasteword.gif","",getLanguageString(language,408),"PASTEWORD"));
      return true;

    case "77":  // Paste Text
		  objToolbar.add(objToolbars.createButton("",imagePath + "pastetext.gif","",getLanguageString(language,3340),"PASTETEXT"));
      return true;

    case "09":  // Undo
		  objToolbar.add(objToolbars.createButton("",imagePath + "undo.gif","",getLanguageString(language,107),"UNDO"));
      return true;

    case "10":  // Redo
		  objToolbar.add(objToolbars.createButton("",imagePath + "redo.gif","",getLanguageString(language,108),"REDO"));
      return true;

    case "11": // Link
   		objToolbar.add(objToolbars.createButton("",imagePath + "link.gif","",getLanguageString(language,109),"LINK"));
      return true;

    case "60": // Link
  		objToolbar.add(objToolbars.createButton("",imagePath + "anchor.gif","",getLanguageString(language,407),"ANCHOR"));
      return true;

    case "12": // Image
		  var objImage = objToolbars.createMenuButton("",imagePath + "image.gif",imagePath + "colorselect.gif","onImageClicked",getLanguageString(language,110),"Image");
		  objImage.popupwidth = "180";
		  if(browser.ie)
		objImage.add(objToolbars.createMenuItem(getLanguageString(language,705),imagePath + "libraryImage.gif","","INSERTIMG"));
		 // objImage.add(objToolbars.createMenuItem(getLanguageString(language,701),imagePath + "image.gif","","INSERTLOC"));
		  objImage.add(objToolbars.createMenuItem(getLanguageString(language,702),imagePath + "world.gif","","INSERTWEB"));
		  //objImage.add(objToolbars.createMenuItem(getLanguageString(language,703),imagePath + "server.gif","","INSERTSERVER"));
		  //objImage.add(objToolbars.createMenuItem(getLanguageString(language,3005),imagePath + "upload.gif","","INSERTUP"));
		  
		  //objImage.add(objToolbars.createMenuItem(getLanguageString(language,706),imagePath + "libraryBackground.gif","","INSERTBACKIMG"));
		  //objImage.add(objToolbars.createMenuItem(getLanguageString(language,707),imagePath + "Emot_Icon.gif","","INSERTEMOT"));
		  objToolbar.add(objImage);
      return true;

	//for frontend
	case "79": // Image
		  var objImage = objToolbars.createMenuButton("",imagePath + "image.gif",imagePath + "colorselect.gif","onImageClicked",getLanguageString(language,110),"Image");
		  objImage.popupwidth = "180";
		 
		  if(browser.ie)
		  objImage.add(objToolbars.createMenuItem(getLanguageString(language,705),imagePath + "libraryImage.gif","","INSERTIMGFRONT"));
		 // objImage.add(objToolbars.createMenuItem(getLanguageString(language,701),imagePath + "image.gif","","INSERTLOC"));
		  objImage.add(objToolbars.createMenuItem(getLanguageString(language,702),imagePath + "world.gif","","INSERTWEB"));
		
		  objToolbar.add(objImage);
      return true;

	//for frontend
	case "87": //Insert image from background
		objToolbar.add(objToolbars.createButton("",imagePath + "libraryBackground.gif","",getLanguageString(language,706),"INSERTBACKIMGFRONT"));
		return true;

	case "88": //Insert image from background
		objToolbar.add(objToolbars.createButton("",imagePath + "libraryBackground.gif","",getLanguageString(language,706),"INSERTBACKIMG"));
		return true;
	
	case "89": //Insert emoticons from library
		//objToolbar.add(objToolbars.createButton("",imagePath + "Emot_Icon.gif","",getLanguageString(language,707),"INSERTEMOT"));
		
		return true;

	case "94": //Insert various tags
		objToolbar.add(objToolbars.createButton("",imagePath + "Tag_Icon.gif","",getLanguageString(language,710),"INSERTTAG"));
		return true;
	
	//for frontend
	case "92": //Insert various tags
		objToolbar.add(objToolbars.createButton("",imagePath + "Tag_Icon.gif","",getLanguageString(language,710),"INSERTTAGFRONT"));
		return true;
	case "99": //Insert various tags
		objToolbar.add(objToolbars.createButton("",imagePath + "modifyimage.gif","",getLanguageString(language,114),"MODIFYPHOTO"));
		return true;

	//for frontend
	case "93": //Replace Image
		objToolbar.add(objToolbars.createButton("",imagePath + "ReplaceImg_Icon.gif","",getLanguageString(language,711),"REPLACEIMAGEFRONT"));
		return true;

	//for frontend
	case "84": //Audio Recorder
		//objToolbar.add(objToolbars.createButton("",imagePath + "SoundFile_Icon.gif","",getLanguageString(language,714),"AUDIO"));
		return true;

	//for frontend
	case "85": //Insert Printable Link
		objToolbar.add(objToolbars.createButton("",imagePath + "mPRNLink.gif","",getLanguageString(language,715),"PRNLINK"));
		return true;

	//for admin
	//for inserting the emoticons in editor
	case "90": // Emoticon
		  var objImage = objToolbars.createMenuButton("",imagePath + "Emot_Icon.gif",imagePath + "colorselect.gif","onImageClicked",getLanguageString(language,120),"Emoticons");
		  objImage.popupwidth = "25";
		  if(browser.ie)
			  objImage.add(objToolbars.createMenuItem(getLanguageString(language,708),imagePath + "Emot_Icon.gif","","EMOTHAPPY"));
			  objImage.add(objToolbars.createMenuItem(getLanguageString(language,712),imagePath + "emoticon_1.gif","","EMOT1"));
			  objImage.add(objToolbars.createMenuItem(getLanguageString(language,712),imagePath + "emoticon_2.gif","","EMOT2"));
			  objImage.add(objToolbars.createMenuItem(getLanguageString(language,712),imagePath + "emoticon_3.gif","","EMOT3"));
			  objImage.add(objToolbars.createMenuItem(getLanguageString(language,712),imagePath + "emoticon_4.gif","","EMOT4"));
			  objImage.add(objToolbars.createMenuItem(getLanguageString(language,712),imagePath + "emoticon_5.gif","","EMOT5"));
		  objToolbar.add(objImage);
      return true;

	//for frontend
	case "96": //Insert emoticons from library
//		objToolbar.add(objToolbars.createButton("",imagePath + "Emot_Icon.gif","",getLanguageString(language,707),"INSERTEMOTFRONT"));
		return true;
	
	//for frontend
	//for inserting the emoticons in editor
	/*case "97": // Emoticon
		  var objImage = objToolbars.createMenuButton("",imagePath + "Emot_Icon.gif",imagePath + "colorselect.gif","onImageClicked",getLanguageString(language,120),"Emoticons");
		  objImage.popupwidth = "25";
		  if(browser.ie)
			  objImage.add(objToolbars.createMenuItem(getLanguageString(language,708),imagePath + "Emot_Icon.gif","","EMOTHAPPYFRONT"));
			  objImage.add(objToolbars.createMenuItem(getLanguageString(language,712),imagePath + "emoticon_1.gif","","EMOT1FRONT"));
			  objImage.add(objToolbars.createMenuItem(getLanguageString(language,712),imagePath + "emoticon_2.gif","","EMOT2FRONT"));
			  objImage.add(objToolbars.createMenuItem(getLanguageString(language,712),imagePath + "emoticon_3.gif","","EMOT3FRONT"));
			  objImage.add(objToolbars.createMenuItem(getLanguageString(language,712),imagePath + "emoticon_4.gif","","EMOT4FRONT"));
			  objImage.add(objToolbars.createMenuItem(getLanguageString(language,712),imagePath + "emoticon_5.gif","","EMOT5FRONT"));
		  objToolbar.add(objImage);
      return true;
	*/
   
   //for inserting the emoticons in editor
   case "97":  // Emoticon
//	   	objToolbar.add(objToolbars.createButton("",imagePath + "Emot_Icon.gif","",getLanguageString(language,707),"INSERTEMOTFRONT"));
		/////////
/*		var objImage = objToolbars.createMenuButton("",imagePath + "image.gif",imagePath + "colorselect.gif","onImageClicked",getLanguageString(language,110),"Image");
		  objImage.popupwidth = "180";
		 
		  if(browser.ie)
		  objImage.add(objToolbars.createMenuItem(getLanguageString(language,705),imagePath + "libraryImage.gif","","INSERTIMGFRONT"));
		  objImage.add(objToolbars.createMenuItem(getLanguageString(language,701),imagePath + "image.gif","","INSERTLOC"));
		  objImage.add(objToolbars.createMenuItem(getLanguageString(language,702),imagePath + "world.gif","","INSERTWEB"));
		
		  objToolbar.add(objImage);*/
		//////////
		 // btnBackColor = objToolbars.createColorButton(imagePath + "Emot_Icon.gif",imagePath + "colorselect.gif","onToolbarButtonClick",getLanguageString(language,707),getLanguageString(language,120),"Emoticons");
		  //objToolbar.add(btnBackColor);
		  btnBackColor = objToolbars.createColorButton(imagePath + "Emot_Icon.gif",imagePath + "colorselect.gif","onToolbarButtonClick",getLanguageString(language,707),getLanguageString(language,120),"Emoticons");
		  objToolbar.add(btnBackColor);

      return true;

	 //for inserting the background color in editor
   case "82":  // Back-Ground Color
		  btnBackColor = objToolbars.createColorButton(imagePath + "mBackColor.gif",imagePath + "colorselect.gif","onImageClicked",getLanguageString(language,219),getLanguageString(language,220),"BGCOLOR");
		  objToolbar.add(btnBackColor);
      return true;

	//for admin
	case "81": //Insert emoticons from library
		objToolbar.add(objToolbars.createButton("",imagePath + "mAnimatedIcon.gif","",getLanguageString(language,713),"ANIMATION"));
		return true;

	//for frontend
	case "80": //Insert gif animations from library
		objToolbar.add(objToolbars.createButton("",imagePath + "mAnimatedIcon.gif","",getLanguageString(language,713),"ANIMATIONFRONT"));
		return true;

	case "91": // Audio Recorder
		objToolbar.add(objToolbars.createButton("",imagePath + "SoundFile_Icon.gif","",getLanguageString(language,709),"SOUNDREC"));
		return true;

/*	case "98": // Audio Recorder
		objToolbar.add(objToolbars.createButton("",imagePath + "","",getLanguageString(language,709),"IMAGETAGS"));
		return true;*/
	
	//for frontend
	case "95": // Audio Recorder
		objToolbar.add(objToolbars.createButton("",imagePath + "SoundFile_Icon.gif","",getLanguageString(language,709),"SOUNDRECFRONT"));
		return true;
	
    case "13":  // Table
      var objPopupButton = objToolbars.createPopupButton("",imagePath + "table.gif","onCreateTable",getLanguageString(language,111),"","popup/table2.html");
      objPopupButton.popupwidth   = "120px";
      objPopupButton.popupheight  = "132px";
	    objToolbar.add(objPopupButton);
      return true;

    case "14": // Rule
		  objToolbar.add(objToolbars.createButton("",imagePath + "rule.gif","",getLanguageString(language,112),"RULE"));
      return true;

    case "15": // Char
      var objPopupButton = objToolbars.createPopupButton("",imagePath + "char.gif","onCharClicked",getLanguageString(language,115),"","popup/char2.html");
      objPopupButton.popupwidth   = "78px";
      objPopupButton.popupheight  = "86px";
	    objToolbar.add(objPopupButton);
      return true;

    case "16": // Date
		  objToolbar.add(objToolbars.createButton("",imagePath + "date.gif","",getLanguageString(language,116),"DATE"));
      return true;

    case "17": // Time
		  objToolbar.add(objToolbars.createButton("",imagePath + "time.gif","",getLanguageString(language,117),"TIME"));
      return true;

    case "62": // Marquee
  		objToolbar.add(objToolbars.createButton("",imagePath + "marquee.gif","",getLanguageString(language,409),"MARQUEE"));
      return true;

    case "62": // Page break
  		objToolbar.add(objToolbars.createButton("",imagePath + "pagebreak.gif","",getLanguageString(language,404),"PAGEBREAK"));
      return true;

    case "67": // Paragraph
  		objToolbar.add(objToolbars.createButton("",imagePath + "paragraph.gif","",getLanguageString(language,3002),"PARAGRAPH"));
      return true;

    case "70": // Flash
  		objToolbar.add(objToolbars.createButton("",imagePath + "flash.gif","",getLanguageString(language,3303),"FLASH"));
      return true;

    case "71": // Media
  		objToolbar.add(objToolbars.createButton("",imagePath + "media.gif","",getLanguageString(language,3304),"MEDIA"));
      return true;

/*    case "99": // Help
		  objToolbar.add(objToolbars.createButton("",imagePath + "help.gif","",getLanguageString(language,114),"HELP"));
      return true;*/

    /*case "99": // Help
		  objToolbar.add(objToolbars.createButton("",imagePath + "help.gif","",getLanguageString(language,114),"IMAGTAGS"));
      return true;*/

    case "19":  // Styles
		 /* // create a style combo
		  cmbStyle = objToolbars.createStyleCombo("onStyleComboChanged",getLanguageString(language,3014),"STYLE");
		  // set combo width
		  cmbStyle.width = "100";
		  // set popup window width
		  cmbStyle.popupwidth = "130";
		  // set popup window height
		  cmbStyle.popupheight = "";
		  // define which of the properties shall be used for display 
		  cmbStyle.displayValue = "value";
		  // add item
		  cmbStyle.add(objToolbars.createStyleComboItem("Standard","","","Standard"));
		  // add to toolbar
		  objToolbar.add(cmbStyle);
   		objToolbar.add(objToolbars.createDistance(3,false));*/
      return true;

    case "56": // Format
		 cmbFormat = objToolbars.createStyleCombo("onFormatComboChanged",getLanguageString(language,3015),"FORMAT");
		  // set combo width
		  cmbFormat.width = "80";
		  // set popup window width
		  cmbFormat.popupwidth = "200";
		  // set popup window height
		  cmbFormat.popupheight = "";
		  // define which of the properties shall be used for display 
		  cmbFormat.displayValue = "tag1";
		  // add item
		  cmbFormat.add(objToolbars.createStyleComboItem("Normal","","","NORMAL","Normal"));
		  cmbFormat.add(objToolbars.createStyleComboItem("<h1 unselectable='ON'>Heading 1</h1>","","","<h1>","Heading 1"));
		  cmbFormat.add(objToolbars.createStyleComboItem("<h2 unselectable='ON'>Heading 2</h2>","","","<h2>","Heading 2"));
		  cmbFormat.add(objToolbars.createStyleComboItem("<h3 unselectable='ON'>Heading 3</h3>","","","<h3>","Heading 3"));
		  cmbFormat.add(objToolbars.createStyleComboItem("<h4 unselectable='ON'>Heading 4</h4>","","","<h4>","Heading 4"));
		  cmbFormat.add(objToolbars.createStyleComboItem("<h5 unselectable='ON'>Heading 5</h5>","","","<h5>","Heading 5"));
		  cmbFormat.add(objToolbars.createStyleComboItem("<h6 unselectable='ON'>Heading 6</h6>","","","<h6>","Heading 6"));
		  cmbFormat.add(objToolbars.createStyleComboItem("<address unselectable='ON'>Address</address>","","","<address>","Address"));
		  cmbFormat.add(objToolbars.createStyleComboItem("<dir unselectable='ON'>Directory List</dir>","","","<dir>","Directory List"));
		  cmbFormat.add(objToolbars.createStyleComboItem("<pre unselectable='ON'>Formatted</pre>","","","<pre>","Formatted"));
		  cmbFormat.add(objToolbars.createStyleComboItem("<menu unselectable='ON'>Menu List</menu>","","","<menu>","Menu List"));
		  objToolbar.add(cmbFormat);
   		objToolbar.add(objToolbars.createDistance(3,false));
      return true;

    case "20":  // Font
		  // create a style combo
		  cmbFont = objToolbars.createStyleCombo("onFontComboChanged",getLanguageString(language,3016),"");
		  // set combo width
		  cmbFont.width = "114";
		  // set popup window width
		  cmbFont.popupwidth = "120";
		  // set popup window height
		  cmbFont.popupheight = "";
		  // define which of the properties shall be used for display 
		  cmbFont.displayValue = "value";
		  // add item
		  cmbFont.add(objToolbars.createStyleComboItem("<FONT unselectable='ON' face='Arial'>Arial</font>","","","Arial"));
		  cmbFont.add(objToolbars.createStyleComboItem("<FONT unselectable='ON' face='Arial Black'>Arial Black</font>","","","Arial Black"));
		  cmbFont.add(objToolbars.createStyleComboItem("<FONT unselectable='ON' face='Arial Narrow'>Arial Narrow</font>","","","Arial Narrow"));
		  cmbFont.add(objToolbars.createStyleComboItem("<FONT unselectable='ON' face='Book Antiqua'>Book Antiqua</font>","","","Book Antiqua"));
		  cmbFont.add(objToolbars.createStyleComboItem("<FONT unselectable='ON' face='Bookman Old Style'>Bookman Old</font>","","","Bookman Old Style"));
		  cmbFont.add(objToolbars.createStyleComboItem("<FONT unselectable='ON' face='Century Gothic'>Century</font>","","","Century Gothic"));
		  cmbFont.add(objToolbars.createStyleComboItem("<FONT unselectable='ON' face='Comic Sans MS'>Comic Sans MS</font>","","","Comic Sans MS"));
		  cmbFont.add(objToolbars.createStyleComboItem("<FONT unselectable='ON' face='Courier New'>Courier New</font>","","","Courier New"));
		  cmbFont.add(objToolbars.createStyleComboItem("<FONT unselectable='ON' face='Monotype Corsiva'>Monotype Corsiva</font>","","","Monotype Corsiva"));
		  cmbFont.add(objToolbars.createStyleComboItem("<FONT unselectable='ON' face='System'>System</font>","","","System"));
		  cmbFont.add(objToolbars.createStyleComboItem("<FONT unselectable='ON' face='Tahoma'>Tahoma</font>","","","Tahoma"));
		  cmbFont.add(objToolbars.createStyleComboItem("<FONT unselectable='ON' face='Times New Roman'>Times New Roman</font>","","","Times New Roman"));
		  cmbFont.add(objToolbars.createStyleComboItem("<FONT unselectable='ON' face='Verdana'>Verdana</font>","","","Verdana"));
		  objToolbar.add(cmbFont);
		  objToolbar.add(objToolbars.createDistance(3,false));
      return true;

    case "21":  // Font size
		  // create a style combo
		  cmbFontSize = objToolbars.createStyleCombo("onFontSizeComboChanged",getLanguageString(language,3017),"");
		  // set combo width
		  cmbFontSize.width = "35";
		  // set popup window width
		  cmbFontSize.popupwidth = "80";
		  // set popup window height
		  cmbFontSize.popupheight = "";
		  // define which of the properties shall be used for display 
		  cmbFontSize.displayValue = "tag1";
		  // add item
		  cmbFontSize.add(objToolbars.createStyleComboItem("<FONT unselectable='ON' size=1>8</FONT>","","","1","8"));
		  cmbFontSize.add(objToolbars.createStyleComboItem("<FONT unselectable='ON' size=2>10</FONT>","","","2","10"));
		  cmbFontSize.add(objToolbars.createStyleComboItem("<FONT unselectable='ON' size=3>12</FONT>","","","3","12"));
		  cmbFontSize.add(objToolbars.createStyleComboItem("<FONT unselectable='ON' size=4>14</FONT>","","","4","14"));
		  cmbFontSize.add(objToolbars.createStyleComboItem("<FONT unselectable='ON' size=5>18</FONT>","","","5","18"));
		  cmbFontSize.add(objToolbars.createStyleComboItem("<FONT unselectable='ON' size=6>24</FONT>","","","6","24"));
		  cmbFontSize.add(objToolbars.createStyleComboItem("<FONT unselectable='ON' size=7>36</FONT>","","","7","36"));
   		  //cmbFontSize.add(objToolbars.createStyleComboItem("<FONT unselectable='ON' size=64>64</FONT>","","","2","64"));
		  objToolbar.add(cmbFontSize);
      return true;

    case "22":  // Bold
		  btnBold = objToolbars.createButton("",imagePath + "bold.gif","",getLanguageString(language,201),"BOLD");
		  objToolbar.add(btnBold);
      return true;

    case "23":  // Italic
		  btnItalic = objToolbars.createButton("",imagePath + "italic.gif","",getLanguageString(language,202),"ITALIC");
		  objToolbar.add(btnItalic);
      return true;

    case "24":  // Underline
		  btnUnderline = objToolbars.createButton("",imagePath + "underline.gif","",getLanguageString(language,203),"UNDERLINE")
		  objToolbar.add(btnUnderline);
      return true;

    case "25":  // Superscript
		  btnSuperscript = objToolbars.createButton("",imagePath + "superscript.gif","",getLanguageString(language,204),"SUPERSCRIPT")
		  objToolbar.add(btnSuperscript);
      return true;

    case "26":  // Subscript
		  btnSubscript = objToolbars.createButton("",imagePath + "subscript.gif","",getLanguageString(language,205),"SUBSCRIPT")
		  objToolbar.add(btnSubscript);
      return true;

    case "27":  // Justify left
		  btnJustifyLeft = objToolbars.createButton("",imagePath + "left.gif","",getLanguageString(language,206),"JUSTIFYLEFT")
		  objToolbar.add(btnJustifyLeft);
      return true;

    case "28":  // Justify center
		  btnJustifyCenter = objToolbars.createButton("",imagePath + "center.gif","",getLanguageString(language,207),"JUSTIFYCENTER")
		  objToolbar.add(btnJustifyCenter);
      return true;

    case "29":  // Justify right
		  btnJustifyRight = objToolbars.createButton("",imagePath + "right.gif","",getLanguageString(language,208),"JUSTIFYRIGHT")
		  objToolbar.add(btnJustifyRight);
      return true;

    case "30":  // Justify block
		  btnJustifyFull = objToolbars.createButton("",imagePath + "block.gif","",getLanguageString(language,209),"JUSTIFYFULL")
		  objToolbar.add(btnJustifyFull);
      return true;

    case "31":  // Ordered list
		  btnInsertOrderedList = objToolbars.createButton("",imagePath + "orderedlist.gif","",getLanguageString(language,210),"INSERTORDEREDLIST")
		  objToolbar.add(btnInsertOrderedList);
      return true;

    case "32":  // Unordered list
		  btnInsertUnorderedList = objToolbars.createButton("",imagePath + "unorderedlist.gif","",getLanguageString(language,211),"INSERTUNORDEREDLIST")
		  objToolbar.add(btnInsertUnorderedList);
      return true;

    case "33":  // Indent
		  objToolbar.add(objToolbars.createButton("",imagePath + "indent.gif","",getLanguageString(language,212),"INDENT"));
      return true;

    case "34":  // Outdent
		  objToolbar.add(objToolbars.createButton("",imagePath + "outdent.gif","",getLanguageString(language,213),"OUTDENT"));
      return true;

    case "35":  // Color
      btnColor = objToolbars.createColorButton(imagePath + "color.gif",imagePath + "colorselect.gif","onChangeTextColor",getLanguageString(language,215),getLanguageString(language,217),"");
		  objToolbar.add(btnColor);
      return true;

    case "36":  // Backcolor
		  btnBackColor = objToolbars.createColorButton(imagePath + "mTextBackground.gif",imagePath + "colorselect.gif","onChangeBackgroundColor",getLanguageString(language,216),getLanguageString(language,218),"");
		  objToolbar.add(btnBackColor);
      return true;

/*    case "75":  // Page
		  objToolbar.add(objToolbars.createButton("",imagePath + "page.gif","",getLanguageString(language,3398),"PAGE"));
      return true;*/
case "75":  // Page
		  objToolbar.add(objToolbars.createButton("",imagePath + "modifyimage.gif","",getLanguageString(language,114),"MODIFYPHOTOADMIN"));
      return true;

    case "59":  // Form
		  objToolbar.add(objToolbars.createButton("",imagePath + "form.gif","",getLanguageString(language,406),"FORM"));
      return true;

    case "38":  // Label
		  if(browser.ie)
			  objToolbar.add(objToolbars.createButton("",imagePath + "label.gif","",getLanguageString(language,301),"LABEL"));
      return true;

    case "39":  // Button
		  objToolbar.add(objToolbars.createButton("",imagePath + "button.gif","",getLanguageString(language,302),"BUTTON"));
      return true;

    case "40":  // Input
		  objToolbar.add(objToolbars.createButton("",imagePath + "input.gif","",getLanguageString(language,303),"INPUT"));
      return true;

    case "41":  // Checkbox
  		objToolbar.add(objToolbars.createButton("",imagePath + "checkbox.gif","",getLanguageString(language,304),"CHECK"));
      return true;

    case "42":  // Radio
		  objToolbar.add(objToolbars.createButton("",imagePath + "radio.gif","",getLanguageString(language,305),"OPTION"));
      return true;

    case "43":  // Combo
  		objToolbar.add(objToolbars.createButton("",imagePath + "combobox.gif","",getLanguageString(language,306),"COMBO"));
      return true;

    case "44":  // Listbox
		  objToolbar.add(objToolbars.createButton("",imagePath + "listbox.gif","",getLanguageString(language,307),"LISTBOX"));
      return true;

    case "45":  // Textarea
		  objToolbar.add(objToolbars.createButton("",imagePath + "textarea.gif","",getLanguageString(language,308),"AREA"));
      return true;

    case "63":  // Hidden
		  objToolbar.add(objToolbars.createButton("",imagePath + "hidden.gif","",getLanguageString(language,410),"HIDDEN"));
      return true;

    case "46":  // Div
  		objToolbar.add(objToolbars.createButton("",imagePath + "div.gif","",getLanguageString(language,310),"DIV"));
      return true;

    case "47":  // IFrame
		  if(browser.ie)
  		  objToolbar.add(objToolbars.createButton("",imagePath + "iframe.gif","",getLanguageString(language,309),"IFRAME"));
      return true;

    case "48":  // Position
  		objToolbar.add(objToolbars.createButton("",imagePath + "position.gif","",getLanguageString(language,311),"POSITION"));
      return true;

    case "49":  // TM
		  // create a style combo
		  cmbTM = objToolbars.createStyleCombo("",getLanguageString(language,3320),"TM");
		  // set combo width
		  cmbTM.width = "100";
		  // set popup window width
		  cmbTM.popupwidth = "120";
		  // set popup window height
		  cmbTM.popupheight = "";
		  // define which of the properties shall be used for display 
		  cmbTM.displayValue = "value";
		  // add to toolbar
		  objToolbar.add(cmbTM);
      return true;

    case "74":  // TM
/*		  // create a style combo
		  cmbTMCHP = objToolbars.createStyleCombo("",getLanguageString(language,3320),"TM");
		  // set combo width
		  cmbTMCHP.width = "100";
		  // set popup window width
		  cmbTMCHP.popupwidth = "120";
		  // set popup window height
		  cmbTMCHP.popupheight = "";
		  // define which of the properties shall be used for display 
		  cmbTMCHP.displayValue = "value";
		  cmbTMCHP.add(objToolbars.createStyleComboItem("INSERTBLANKTAG","","","INSERTBLANKTAG","INSERTBLANKTAG1"));
//		  cmbTMCHP.add(objToolbars.createStyleComboItem("PROPERTYPHOTO","","","PROPERTYPHOTO","PROPERTYPHOTO1"));
  //		  cmbTMCHP.add(objToolbars.createStyleComboItem("AGENTPHOTO","","","AGENTPHOTO","AGENTPHOTO1"));
		  cmbTMCHP.add(objToolbars.createStyleComboItem("AGENTLOGO","","","AGENTLOGO","AGENTLOGO1"));

	  // add to toolbar
		  objToolbar.add(cmbTMCHP);*/
		 cmbFormatCP = objToolbars.createStyleCombo("onImageTagChanged",getLanguageString(language,3034),"Tag");
		  // set combo width
		  cmbFormatCP.width = "80";
		  // set popup window width
		  cmbFormatCP.popupwidth = "200";
		  // set popup window height
		  cmbFormatCP.popupheight = "";
		  // define which of the properties shall be used for display 
		  cmbFormatCP.displayValue = "tag1";
		  // add item
		  cmbFormatCP.add(objToolbars.createStyleComboItem("INSERTBLANKTAG","","","INSERTBLANKTAG","INSERTBLANKTAG"));
		  cmbFormatCP.add(objToolbars.createStyleComboItem("PROPERTYPHOTO","","","PROPERTYPHOTO","PROPERTYPHOTO"));
		  cmbFormatCP.add(objToolbars.createStyleComboItem("AGENTPHOTO","","","AGENTPHOTO","AGENTPHOTO"));
		  cmbFormatCP.add(objToolbars.createStyleComboItem("AGENTLOGO","","","AGENTLOGO","AGENTLOGO"));
		  objToolbar.add(cmbFormatCP);
   		objToolbar.add(objToolbars.createDistance(1,false));
      return true;


    case "50":  // TM add
		  objToolbar.add(objToolbars.createButton("",imagePath + "tm_add.gif","",getLanguageString(language,312),"TMADD"));
      return true;

    case "51":  // TM Create
  		objToolbar.add(objToolbars.createButton("",imagePath + "tm_create.gif","",getLanguageString(language,313),"TMCREATE"));
      return true;

    case "52":  // TM Remove
  		objToolbar.add(objToolbars.createButton("",imagePath + "tm_remove.gif","",getLanguageString(language,314),"TMREMOVE"));
      return true;

    case "78":  // Full size
  		objToolbar.add(objToolbars.createButton("",imagePath + "fullsize.gif","",getLanguageString(language,3397),"FULLSIZE"));
      return true;

    case "37":  // Cleaner
		  // create cleaner menu button
		  var objCleaner = objToolbars.createMenuButton("",imagePath + "new.gif",imagePath + "colorselect.gif","onCleanerClicked",getLanguageString(language,214),"CLEANER");
		  objCleaner.popupwidth = "215";
		  objCleaner.add(objToolbars.createMenuItem(getLanguageString(language,3315),imagePath + "removestyle.gif","","RMSTYLE"));
		  objCleaner.add(objToolbars.createMenuItem(getLanguageString(language,3316),imagePath + "removeformat.gif","","RMFORMAT"));
		  objCleaner.add(objToolbars.createMenuItem(getLanguageString(language,3317),imagePath + "removetag.gif","","RMEMPTY"));
		  objCleaner.add(objToolbars.createMenuItem(getLanguageString(language,3318),imagePath + "removeword.gif","","RMWORD"));
		  objCleaner.add(objToolbars.createMenuItem(getLanguageString(language,3396),imagePath + "removespan.gif","","RMSPAN"));
		  // add to toolbar
		  objToolbar.add(objCleaner);
      return true;

    case "68":  // Return mode
  		if(globalIsBR)
    		objToolbar.add(objToolbars.createButton("",imagePath + "bron.gif","",getLanguageString(language,3013),"RETURN"));
    	else
    		objToolbar.add(objToolbars.createButton("",imagePath + "broff.gif","",getLanguageString(language,3013),"RETURN"));
      return true;

/*
      // set RETURN mode dependant of settings
      if(editGetBr())
        btnReturnMode = new Button("","","design/image/" + design + "/bron.gif","onReturnMode()",getLanguageString(language,3013),design,"");
      else
        btnReturnMode = new Button("","","design/image/" + design + "/broff.gif","onReturnMode()",getLanguageString(language,3013),design,"");
      objToolbar.add(btnReturnMode);
      return true;
*/

    case "69":  // Table hilight
  		objToolbar.add(objToolbars.createButton("",imagePath + "tablehigh.gif","",getLanguageString(language,3003),"HIGHLIGHT"));
      return true;

    case "58":  // Select all
  		objToolbar.add(objToolbars.createButton("",imagePath + "selectall.gif","",getLanguageString(language,405),"SELECTALL"));
      return true;

    case "72":  // Editable
  		//objToolbar.add(objToolbars.createButton("",imagePath + "mNonEditableIcon.gif","",getLanguageString(language,3301),"EDITABLE"));
      return true;
	
	case "83"://Rev Editable
		//objToolbar.add(objToolbars.createButton("",imagePath + "mEditableIcon.gif","",getLanguageString(language,3414),"REVEDITABLE"));
      return true;

    case "18":
      if(browser.ie) {
		    // create a style combo
		    cmbZoom = objToolbars.createStyleCombo("onZoomClicked",getLanguageString(language,3319),"");
		    // set combo width
		    cmbZoom.width = "50";
		    // set popup window width
		    cmbZoom.popupwidth = "40";
		    // set popup window height
		    cmbZoom.popupheight = "";
		    // define which of the properties shall be used for display 
		    cmbZoom.displayValue = "value";
		    cmbZoom.add(objToolbars.createStyleComboItem("10%","","","10%"));
		    cmbZoom.add(objToolbars.createStyleComboItem("50%","","","50%"));
		    cmbZoom.add(objToolbars.createStyleComboItem("100%","","","100%"));
		    cmbZoom.add(objToolbars.createStyleComboItem("150%","","","150%"));
		    cmbZoom.add(objToolbars.createStyleComboItem("200%","","","200%"));
		    cmbZoom.selectedIndex  = 2;
		    // add to toolbar
		    objToolbar.add(cmbZoom);
		    return true;
      } 

    case "53":  // Edit
  		objToolbar.add(objToolbars.createCheckButton("Edit",imagePath + "edit.gif","","Edit","EDIT",true,"GROUP"));
      return true;

    case "54":  // HTML
  		objToolbar.add(objToolbars.createCheckButton("Html",imagePath + "html.gif","","Html","HTML",false,"GROUP"));
      return true;

    case "55": // Preview
  		objToolbar.add(objToolbars.createCheckButton("Preview",imagePath + "preview.gif","","Preview","PREVIEW",false,"GROUP"));
      return true;
  }
}


//---------------------------------------------------------------------------------------------
// Editor events
//---------------------------------------------------------------------------------------------
function editOnEditorReady()
{
  // read text modules
  editGetTextModule();
  
}

// is called after mouse down
function editOnEditorMouseDown()
{
	// if there are open popups they will be closed
	toolbarsTop.reset();
  if(objMenuBar) {
    var objMenu = objMenuBar.getActiveBarItem();
    if(objMenu)
      objMenu.reset();
  }
  
}

// Is called when Text Modules are changed
function editOnProcessTM(aData)
{			
	try {
		// get text module combo
		var combo = toolbarsTop.getElementByTag("TM");
		// clear first because all are readed
		combo.clear();
		// insert text modules
		for(var i=0;i<aData.length-1;i++) {
			var aText = aData[i].split("|");
			combo.add(toolbarsTop.createStyleComboItem(aText[0],"","",aText[0],aText[1]));
		}
		combo.setSelectedIndex(0);
	} catch(error) {}
	
}

// is called when toolbar status has changed (Buttons, Combo)
function editOnChangeToolbar(data)
{
	try {
		var aData = data.split(":");
		// set button status
		if(btnBold)
		  btnBold.setStatus(aData[0]>0 ? true:false);
		if(btnItalic)  
		  btnItalic.setStatus(aData[1]>0 ? true:false);
		if(btnUnderline)
  		btnUnderline.setStatus(aData[2]>0 ? true:false);
    if(btnSuperscript)
		  btnSuperscript.setStatus(aData[3]>0 ? true:false);
		if(btnSubscript)
		  btnSubscript.setStatus(aData[4]>0 ? true:false);
		if(btnJustifyLeft)
		  btnJustifyLeft.setStatus(aData[5]>0 ? true:false);
		if(btnJustifyCenter)
		  btnJustifyCenter.setStatus(aData[6]>0 ? true:false);
		if(btnJustifyRight)
		  btnJustifyRight.setStatus(aData[7]>0 ? true:false);
		if(btnJustifyFull)
		  btnJustifyFull.setStatus(aData[8]>0 ? true:false);
		if(btnInsertOrderedList)
		  btnInsertOrderedList.setStatus(aData[9]>0 ? true:false);
		if(btnInsertUnorderedList)
  		btnInsertUnorderedList.setStatus(aData[10]>0 ? true:false);

	  // set combo
		if(cmbStyle) 
			cmbStyle.setSelectedText(aData[11]);
		if(cmbFormat)
			cmbFormat.setSelectedText(aData[12]);
		if(cmbFont)
			cmbFont.setSelectedText(aData[13]);
		if(cmbFontSize)
			cmbFontSize.setSelectedText(aData[14]);
		if(btnColor)
			btnColor.setColor(aData[15]);
		if(btnBackColor)
			btnBackColor.setColor(aData[16]);
	} catch(Error) {
	alert(Error)
	}
	
}

// called for each style that has to be set because the editor detected styles in document
function editOnAddStyle(name,html)
{

	try {
		// get style combo
		var combo = toolbarsTop.getElementByTag("STYLE");
		// clear combo
		if(name == "") {
			combo.clear();
		} else {
			combo.add(toolbarsTop.createStyleComboItem(html,"","",name));
		}
	} catch(error) {}
//	parent.loadImages();
}

// called for each style settings with Hx elements. Therefore display has to be changed
function editOnChangeFormat(name,html)
{
	try {
		var combo = toolbarsTop.getElementByTag("FORMAT");
		var item = combo.getItemByValue("<" + name + ">");
		item.text = html + item.tag1 + "</span>";
	} catch(error) {}
}

function editOnReadOnly(value)
{
  if(value) {
		document.getElementById("toolbar_top").style.display = "none";  
		document.getElementById("toolbar_bottom").style.display = "none";  
  } else {
		document.getElementById("toolbar_top").style.display = "inline";  
		document.getElementById("toolbar_bottom").style.display = "inline";  
  }

}


//-------------------------------------------------------------------------------------------
// User defined Toolbar events
//-------------------------------------------------------------------------------------------
// called when button is clicked (independant of toolbar row)

function onToolbarButtonClick(id)
{

  // get object
  var button = toolbarsTop.getElementById(id);


	// 1. toolbar
  if(button.tag == "NEW")	                 editNew();  
  if(button.tag == "OPEN")	               editOpen(1);  
  if(button.tag == "SAVE")	               onSaveFile();  
  if(button.tag == "SAVEAS")	             editSaveDialog();  
  if(button.tag == "SAVELOCAL")	           editSaveLocal();  
  if(button.tag == "SEARCH")	             editSearch();  
  if(button.tag == "PRINT")	               editPrint();  
  if(button.tag == "PREVIEW")	             editPreview(); 
  
  //if(button.tag == "SPELL")	               editSpell();  
  if(button.tag == "SPELL")
  {
	  parent.document.form1.textVal.value = editGetHtml();
	  //alert(parent.document.form1.textVal.value);

	  window.open("../../htmlspellchecker/spellcheck.php?spellcheckfield=admin","spell",'resizable=yes,scrollbars=yes,width=800,height=650,toolbar=no, menubar=no,location=no, directories=no,status=no');
  }
  if(button.tag == "SPELLFRONT")
  {
	  //alert(parent.document.frmCompose.name);
	  window.open("../../htmlspellchecker/spellcheck.php?spellcheckfield=front","spell_front",'resizable=yes,scrollbars=yes,width=800,height=450,toolbar=no, menubar=no,location=no, directories=no,status=no');
  }
  if(button.tag == "CUT")	                 editCut();  
  if(button.tag == "COPY")	               editCopy();  
  if(button.tag == "PASTE")	               editPaste();  
  if(button.tag == "PASTEWORD")	           editPasteWord();  
  if(button.tag == "PASTETEXT")	           editPasteText();  
  if(button.tag == "UNDO")	               editUndo();  
  if(button.tag == "REDO")	               editRedo();  
  if(button.tag == "LINK")	               editLink();  
  if(button.tag == "ANCHOR")	           editInsertObject('ANCHOR');  
  if(button.tag == "RULE")	               editInsertObject('RULE');  
  if(button.tag == "DATE")	               editInsertDate();  
  if(button.tag == "TIME")	               editInsertTime();  
  if(button.tag == "MARQUEE")	             editInsertObject('MARQUEE');  
  if(button.tag == "PAGEBREAK")	           editInsertObject('PAGEBREAK');  
  if(button.tag == "PARAGRAPH")	           editParagraph();  
  if(button.tag == "EDITABLE")	           editSetEditable(false);  
  if(button.tag == "REVEDITABLE")	       editSetRevEditable(true);
  if(button.tag == "FLASH")	               editOpen(4);  
  if(button.tag == "MEDIA")	               editOpen(5);  
  if(button.tag == "HELP")	               onHelp();  

  if(button.tag == "INSERTBACKIMG")     
  {  

	  var id = 0;
	  win=window.open("../../showlibrary.php?func=front&LibraryType=backgrounds&DesignId="+id,"backimg",'resizable=yes,scrollbars=yes,width=800,height=650,toolbar=no, menubar=no,location=no, directories=no,status=no');
	  win.moveTo(100,100);
  }
  if(button.tag == "INSERTEMOT")     
  {
	  
	  var id = parent.document.form1.id.value;
	  win=window.open("../../showlibrary.php?LibraryType=emoticons&DesignId="+id,"emot",'resizable=yes,scrollbars=yes,width=800,height=650,toolbar=no, menubar=no,location=no, directories=no,status=no');
	  win.moveTo(100,100);
  }
  
  if(button.tag == "INSERTEMOTFRONT")     
  {
	  
	  var id = parent.document.frmCompose.UserDesignId.value;
	 win= window.open("../../showlibrary.php?LibraryType=emoticons&DesignId="+id+"&func=front","emot_front",'resizable=yes,scrollbars=yes,width=800,height=650,toolbar=no, menubar=no,location=no, directories=no,status=no');
	 win.moveTo(100,100);
  }
  
  if(button.tag == "Emoticons")     
  {
			var id = 0;
		  win=window.open("../../showlibrary.php?LibraryType=emoticons&DesignId="+id+"&func=front","emot_front",'resizable=yes,scrollbars=yes,width=800,height=650,toolbar=no, menubar=no,location=no, directories=no,status=no');
		  win.moveTo(100,100);
	}
  
  if(button.tag == "ANIMATION")     
  {
	  var id = 0;
	  win=window.open("../../showlibrary.php?func=front&LibraryType=animated&DesignId="+id,"animation",'resizable=yes,scrollbars=yes,width=800,height=650,toolbar=no, menubar=no,location=no, directories=no,status=no');
	  win.moveTo(100,100);
  }
  if(button.tag == "ANIMATIONFRONT")     
  {
	  var id = parent.document.frmCompose.UserDesignId.value;
	 win= window.open("../../showlibrary.php?LibraryType=animated&DesignId="+id+"&func=front","animation",'resizable=yes,scrollbars=yes,width=800,height=650,toolbar=no, menubar=no,location=no, directories=no,status=no');
	 win.moveTo(100,100);
  }

  if(button.tag == "SOUNDREC")     
  {
	  var id = 0;
	 win= window.open("../../showlibrary.php?func=front&LibraryType=sound&DesignId="+id,"soundrec",'resizable=yes,scrollbars=yes,width=800,height=650,toolbar=no, menubar=no,location=no, directories=no,status=yes');
	 win.moveTo(100,100);
  }
  if(button.tag == "IMAGTAGS")     
  {
	  var id = parent.document.form1.id.value;

	 win= window.open("../../showlibrary.php?LibraryType=sound&DesignId="+id,"soundrec",'resizable=yes,scrollbars=yes,width=800,height=650,toolbar=no, menubar=no,location=no, directories=no,status=yes');
	 win.moveTo(100,100);
  }

  if(button.tag == "SOUNDRECFRONT")     
  {
	  var id = parent.document.frmCompose.UserDesignId.value;
	  var attachment = parent.document.frmCompose.attachment.value;
	  var attachmentPath = parent.document.frmCompose.attachmentPath.value;
	  //alert("attachment----"+attachment+" attachmentPath----"+attachmentPath);
	 
	 win= window.open("../../showlibrary.php?LibraryType=sound&DesignId="+id+"&func=front&oldAttachment="+attachment+"&oldAttachmentPath="+attachmentPath,"soundrec_front",'resizable=yes,scrollbars=yes,width=800,height=650,toolbar=no, menubar=no,location=no, directories=no,status=yes');
	 win.moveTo(100,100);
  }
  
  if(button.tag == "INSERTBACKIMGFRONT")     
  {

	  var id = parent.document.frmCompose.UserDesignId.value;
	  //window.showModalDialog("http://apnew-mohit.com/workshop/modules/emailer/showlibrary.php?LibraryType=backgrounds&DesignId="+id+"&func=front","0",'resizable=yes,scrollbars=yes,width=800,height=650,toolbar=no, menubar=no,location=no, directories=no,status=no');
	 win= window.open("../../showlibrary.php?LibraryType=backgrounds&DesignId="+id+"&func=front","backimg_front",'resizable=yes,scrollbars=yes,width=800,height=650,toolbar=no, menubar=no,location=no, directories=no,status=no');
	 win.moveTo(100,100);
  }
  if(button.tag == "INSERTTAG")     
  {
	  var id = parent.document.form1.id.value;
	  win=window.open("../../tags.php?DesignId="+id,"tag",'resizable=yes,scrollbars=yes,width=800,height=650,toolbar=no, menubar=no,location=no, directories=no,status=no');
	  win.moveTo(100,100);
  }
  if(button.tag == "INSERTTAGFRONT")     
  {
	  var id = parent.document.frmCompose.UserDesignId.value;
	  win=window.open("../../tags.php?DesignId="+id+"&func=front","tag_front",'resizable=yes,scrollbars=yes,width=800,height=650,toolbar=no, menubar=no,location=no, directories=no,status=no');
	  win.moveTo(100,100);
  }
  if(button.tag == "MODIFYPHOTO")     
  {
	  
	  var id = parent.document.frmCompose.UserDesignId.value;

	  if(editGetActiveObject().tagName=="IMG")
	  {	
		  image=editGetActiveObject().src;
		  win=window.open("../../../printables/imaging/index.php?url="+image+"&func=emailer&nofl=0&id="+id,"ModifyImage",'resizable=yes,scrollbars=yes,width=800,height=650,toolbar=no, menubar=no,location=no, directories=no,status=no,left=100,top=50');
	  }
	  else
	  {
		alert("Please select image");
	  }
  }
  
  if(button.tag == "MODIFYPHOTOADMIN")     
  {
	  
	  var id = parent.document.form1.id.value;

	  if(editGetActiveObject().tagName=="IMG")
	  {	
		  image=editGetActiveObject().src;
		 
		  win=window.open("../../../printables/imaging/index.php?url="+image+"&func=emailer&funcFront=1&nofl=1&id="+id,"ModifyImage",'resizable=yes,scrollbars=yes,width=800,height=650,toolbar=no, menubar=no,location=no, directories=no,status=no,left=100,top=50');
	  }
	  else
	  {
		alert("Please select image");
	  }
  }
  
  if(button.tag == "AUDIO")     
  {
	  var id = parent.document.frmCompose.UserDesignId.value;
	 win= window.open("../../audiorecorder.php?UserDesignId="+id,"audio",'resizable=yes,scrollbars=yes,width=800,height=300,toolbar=no, menubar=no,location=no, directories=no,status=no');
	 win.moveTo(100,100);
  }
  if(button.tag == "PRNLINK")     
  {
	  var id = parent.document.frmCompose.UserDesignId.value;
	  var method = parent.document.frmCompose.PRNMethod.value;
	  var link = parent.document.frmCompose.PRNLink.value;
	  if(editGetSelectedText() == "")
		  alert("Please select the text first");
	  else
	  {
		  var text = editGetSelectedText();
		  //alert(link)
		  var arr = link.split("Your");
		  //alert(arr[0]);
		  var newText = arr[0]+text+"</font></a>";
		  //alert(newText);
		  editInsertHtml(newText);
	  }
  }
  if(button.tag == "REPLACEIMAGEFRONT")     
  {
	  var id = parent.document.frmCompose.UserDesignId.value;
	  var propertyID = parent.document.frmCompose.propertyID.value;
	  var oldWidth = editGetActiveObject().width;
	  var oldHeight = editGetActiveObject().height;
	  var path = editGetActiveObject().src;
	  var fullBody = editGetHtmlBody();
	  //alert(path);
	  if(path == undefined)
	  {
		  alert("Please choose a image first");
	  }
	  else
	  {
		  window.open("../../replaceimage.php?DesignId="+id+"&func=front&propertyID="+propertyID+"&path="+path+"&oldWidth="+oldWidth+"&oldHeight="+oldHeight,"repimg",'resizable=yes,scrollbars=yes,width=800,height=650,toolbar=no, menubar=no,location=no, directories=no,status=no');
	  }
  }


	// 2. toolbar
  if(button.tag == "BOLD")	               editBold();  
  if(button.tag == "ITALIC")	             editItalic();  
  if(button.tag == "UNDERLINE")	           editUnderline();  
  if(button.tag == "SUPERSCRIPT")	         editSuperscript();  
  if(button.tag == "SUBSCRIPT")	           editSubscript();  
  if(button.tag == "JUSTIFYLEFT")	         editJustifyLeft();  
  if(button.tag == "JUSTIFYCENTER")	       editJustifyCenter();  
  if(button.tag == "JUSTIFYRIGHT")	       editJustifyRight();  
  if(button.tag == "JUSTIFYFULL")	         editJustifyFull();  
  if(button.tag == "INSERTORDEREDLIST")	   editOrderedList();  
  if(button.tag == "INSERTUNORDEREDLIST")	 editUnorderedList();  
  if(button.tag == "INDENT")	             editIndent();  
  if(button.tag == "OUTDENT")	             editOutdent();  

  // 3. toolbar
  if(button.tag == "PAGE")	               editProperties(4);  
  if(button.tag == "FORM")	               editInsertObject('FORM');  
  if(button.tag == "LABEL")	               editInsertObject('LABEL');  
  if(button.tag == "BUTTON")	             editInsertObject('BUTTON');  
  if(button.tag == "INPUT")	               editInsertObject('INPUT');  
  if(button.tag == "CHECK")	               editInsertObject('CHECK');  
  if(button.tag == "OPTION")	             editInsertObject('OPTION');  
  if(button.tag == "COMBO")	               editInsertObject('COMBO');  
  if(button.tag == "LISTBOX")	             editInsertObject('LISTBOX');  
  if(button.tag == "AREA")	               editInsertObject('AREA');  
  if(button.tag == "HIDDEN")	             editInsertObject('HIDDEN');  
  if(button.tag == "DIV")	                 editInsertObject('DIV');  
  if(button.tag == "IFRAME")	             editInsertObject('IFRAME');  
  if(button.tag == "POSITION")	           editAbsolute();  
  if(button.tag == "TMADD")							   onInsertTextModule();  
  if(button.tag == "TMCREATE")						 editSetTextModule();  
  if(button.tag == "TMREMOVE")						 onDeleteTextModule();  
  if(button.tag == "RETURN")							 onReturnMode();  
  if(button.tag == "HIGHLIGHT")	           editTableHighlight();  
  if(button.tag == "SELECTALL")	           editSelectAll();  
  if(button.tag == "FULLSIZE")	           editFullSize();  
}

// for lower toolbar
function onToolbarButtonClickBottom(id)
{
  // get object
  var button = toolbarsBottom.getElementById(id);
  // hide top toolbar in HTML/PREVIEW
  if(button.tag == "HTML" || button.tag == "PREVIEW") {
		document.getElementById("toolbar_top").style.display = "none";  
  } else {
		document.getElementById("toolbar_top").style.display = "inline";  
  }

	editSetMode(button.tag);
  editSetActiveMode(button.tag);
}

// if NEW menu button item has been clicked
function onNewClicked(id)
{
  var menu = toolbarsTop.getElementById(id);
  if(menu.selectedItem.value == "NEW")      editNew();
  if(menu.selectedItem.value == "TEMPLATE") editOpen(6);
}

// if UPLOAD menu button item has been clicked
function onMenuUploadClicked(id)
{
  var menu = toolbarsTop.getElementById(id);
  if(menu.selectedItem.value == "UPLOADIMG") editUpload("","",0);
  if(menu.selectedItem.value == "UPLOADDOC") editUpload("","",1);
}

function onCreateTable(id)
{

  var popup = toolbarsTop.getElementById(id);
	// in value we have row:col (if cancel then value = "")
	if(popup.value != "") {
		var temp = popup.value.split(":");
		editCreateTable(temp[0],temp[1]);
	}
}

function onCharClicked(id)
{
  var popup = toolbarsTop.getElementById(id);
	// in value we have character
	if(popup.value != "")
		editInsertHtml(popup.value);
}

// if IMAGE menu button item has been clicked
function onImageClicked(id)
{

  var menu = toolbarsTop.getElementById(id);

 if(id==133)
	{

		//	var idd = parent.document.form1.id.value;

	  win=window.open("../../showlibrary.php?LibraryType=emoticons&DesignId="+id,"emot",'resizable=yes,scrollbars=yes,width=800,height=650,toolbar=no, menubar=no,location=no, directories=no,status=no');
	  win.moveTo(100,100);
	}
  if(menu.selectedItem.value == "INSERTLOC")    editInsertObject('IMAGE');
  if(menu.selectedItem.value == "INSERTWEB")    editInsertObject('IMAGEWEB');
  //if(menu.selectedItem.value == "INSERTSERVER") editOpen(3);
  //if(menu.selectedItem.value == "INSERTUP")     editUpload("","",0,true);

 
  if(menu.selectedItem.value == "INSERTIMG")     
  {  

	  var id = 0;
	  win=window.open("../../showlibrary.php?LibraryType=images&func=front&DesignId="+id,"img",'resizable=yes,scrollbars=yes,width=800,height=650,toolbar=no, menubar=no,location=no, directories=no,status=no');
	  win.moveTo(100,100);
  }
  if(menu.selectedItem.value == "INSERTIMGFRONT")     
  {

	  indexP="-1";
		indexL="-1";	
	  var id = parent.document.frmCompose.UserDesignId.value;
		if(editGetActiveObject().tagName=="IMG"){
		  	str=editGetActiveObject().src;
			indexP=str.indexOf("PHOTO");
			indexL=str.indexOf("LOGO");
		}
	if(indexP!="-1"){
		win=window.open("../../showlibrary.php?type=PHOTO&LibraryType=images&DesignId="+id+"&func=front","img_front",'resizable=yes,scrollbars=yes,width=800,height=650,toolbar=no, menubar=no,location=no, directories=no,status=no');
		win.moveTo(100,100);
	}
	else if(indexL!="-1"){
		win=window.open("../../showlibrary.php?type=LOGO&LibraryType=images&DesignId="+id+"&func=front","img_front",'resizable=yes,scrollbars=yes,width=800,height=650,toolbar=no, menubar=no,location=no, directories=no,status=no');
		win.moveTo(100,100);
	}
	else{
		win=window.open("../../showlibrary.php?LibraryType=images&DesignId="+id+"&func=front","img_front",'resizable=yes,scrollbars=yes,width=800,height=650,toolbar=no, menubar=no,location=no, directories=no,status=no');
		win.moveTo(100,100);
	}
  }
  if(menu.selectedItem.value == "EMOTHAPPY")     
  {  
	  var id = parent.document.form1.id.value;
	  path = imagePath+"Emot_Icon.gif";
	 win= window.open("../../insertemot.php?LibraryType=emoticons&DesignId="+id+"&func=front&path="+path,"emothappy",'resizable=yes,scrollbars=yes,width=10,height=10,toolbar=no, menubar=no,location=no, directories=no,status=no');
	 win.moveTo(100,100);
  }
  if(menu.selectedItem.value == "EMOT1")     
  {  
	  var id = parent.document.form1.id.value;
	  path = imagePath+"emoticon_1.gif";
	 win= window.open("../../insertemot.php?LibraryType=emoticons&DesignId="+id+"&func=front&path="+path,"emot1",'resizable=yes,scrollbars=yes,width=10,height=10,toolbar=no, menubar=no,location=no, directories=no,status=no');
	 win.moveTo(100,100);
  }
  if(menu.selectedItem.value == "EMOT2")     
  {  
	  var id = parent.document.form1.id.value;
	  path = imagePath+"emoticon_2.gif";
	 win= window.open("../../insertemot.php?LibraryType=emoticons&DesignId="+id+"&func=front&path="+path,"emot2",'resizable=yes,scrollbars=yes,width=10,height=10,toolbar=no, menubar=no,location=no, directories=no,status=no');
	 win.moveTo(100,100);
  }
  if(menu.selectedItem.value == "EMOT3")     
  {  
	  var id = parent.document.form1.id.value;
	  path = imagePath+"emoticon_3.gif";
	 win= window.open("../../insertemot.php?LibraryType=emoticons&DesignId="+id+"&func=front&path="+path,"emot3",'resizable=yes,scrollbars=yes,width=10,height=10,toolbar=no, menubar=no,location=no, directories=no,status=no');
	 win.moveTo(100,100);
  }
  if(menu.selectedItem.value == "EMOT4")     
  {  
	  var id = parent.document.form1.id.value;
	  path = imagePath+"emoticon_4.gif";
	  win=window.open("../../insertemot.php?LibraryType=emoticons&DesignId="+id+"&func=front&path="+path,"emot4",'resizable=yes,scrollbars=yes,width=10,height=10,toolbar=no, menubar=no,location=no, directories=no,status=no');
	  win.moveTo(100,100);
  }
  if(menu.selectedItem.value == "EMOT5")     
  {  
	  var id = parent.document.form1.id.value;
	  path = imagePath+"emoticon_5.gif";
	 win= window.open("../../insertemot.php?LibraryType=emoticons&DesignId="+id+"&func=front&path="+path,"emot5",'resizable=yes,scrollbars=yes,width=10,height=10,toolbar=no, menubar=no,location=no, directories=no,status=no');
	 win.moveTo(100,100);
  }

  if(menu.selectedItem.value == "EMOTHAPPYFRONT")     
  {  
	  var id = parent.document.frmCompose.UserDesignId.value;
	  path = imagePath+"Emot_Icon.gif";
	  //window.open("../../insertemot.php?LibraryType=emoticons&DesignId="+id+"&func=front&path="+path,"emothappy_front",'resizable=yes,scrollbars=yes,width=10,height=10,toolbar=no, menubar=no,location=no, directories=no,status=no');
  }
  if(menu.selectedItem.value == "EMOT1FRONT")     
  {  
	  var id = parent.document.frmCompose.UserDesignId.value;
	  path = imagePath+"emoticon_1.gif";
	 win= window.open("../../insertemot.php?LibraryType=emoticons&DesignId="+id+"&func=front&path="+path,"emot1_front",'resizable=yes,scrollbars=yes,width=10,height=10,toolbar=no, menubar=no,location=no, directories=no,status=no');
	 win.moveTo(100,100);
  }
  if(menu.selectedItem.value == "EMOT2FRONT")     
  {  
	  var id = parent.document.frmCompose.UserDesignId.value;
	  path = imagePath+"emoticon_2.gif";
	 win= window.open("../../insertemot.php?LibraryType=emoticons&DesignId="+id+"&func=front&path="+path,"emot2_front",'resizable=yes,scrollbars=yes,width=10,height=10,toolbar=no, menubar=no,location=no, directories=no,status=no');
	 win.moveTo(100,100);
  }
  if(menu.selectedItem.value == "EMOT3FRONT")     
  {  
	  var id = parent.document.frmCompose.UserDesignId.value;
	  path = imagePath+"emoticon_3.gif";
	 win= window.open("../../insertemot.php?LibraryType=emoticons&DesignId="+id+"&func=front&path="+path,"emot3_front",'resizable=yes,scrollbars=yes,width=10,height=10,toolbar=no, menubar=no,location=no, directories=no,status=no');
	 win.moveTo(100,100);
  }
  if(menu.selectedItem.value == "EMOT4FRONT")     
  {  
	  var id = parent.document.frmCompose.UserDesignId.value;
	  path = imagePath+"emoticon_4.gif";
	 win= window.open("../../insertemot.php?LibraryType=emoticons&DesignId="+id+"&func=front&path="+path,"emot4_front",'resizable=yes,scrollbars=yes,width=10,height=10,toolbar=no, menubar=no,location=no, directories=no,status=no');
	 win.moveTo(100,100);
  }
  if(menu.selectedItem.value == "EMOT5FRONT")     
  {  
	  var id = parent.document.frmCompose.UserDesignId.value;
	  path = imagePath+"emoticon_5.gif";
	 win= window.open("../../insertemot.php?LibraryType=emoticons&DesignId="+id+"&func=front&path="+path,"emot5_front",'resizable=yes,scrollbars=yes,width=10,height=10,toolbar=no, menubar=no,location=no, directories=no,status=no');
	 win.moveTo(100,100);
  }
}

// called if style combo changed
function onStyleComboChanged(id)
{
  var combo = toolbarsTop.getElementById(id);
  if(combo.selectedItem.value == "STANDARD")
    editSetStyle("");
  else
    editSetStyle(combo.selectedItem.value);
}

// called if format combo changed
function onFormatComboChanged(id)
{
  var combo = toolbarsTop.getElementById(id);
	var value = combo.selectedItem.value;
  if (value == 'NORMAL') {
    editFormat('Normal');
    editRemoveFormat();
  } else {
    editFormat(value);
  }
}
function onImageTagChanged(id)
{

  var combo = toolbarsTop.getElementById(id);
	var value = combo.selectedItem.value;
	if(editGetActiveObject().tagName=="IMG")
	{

		editGetActiveObject().alt="{"+value+"}";
		alert(value+" Tag has been successfully set.");
	}
	else
	{
		alert("Please select image.");
	}

/*  var combo = toolbarsTop.getElementById(id);
	var value = combo.selectedItem.value;
  if (value == 'NORMAL') {
    editFormat('Normal');
    editRemoveFormat();
  } else {
    editFormat(value);
  }*/
}

// called if font combo changed
function onFontComboChanged(id)
{
  var combo = toolbarsTop.getElementById(id);
	var value = combo.selectedItem.value;
	editFont(value);
}

// called if font size combo changed
function onFontSizeComboChanged(id)
{
  var combo = toolbarsTop.getElementById(id);
	var value = combo.selectedItem.value;
	editFontSize(value);
}

// called if CLEANER menu button item has been clicked
function onCleanerClicked(id)
{
  var menu = toolbarsTop.getElementById(id);
  if(menu.selectedItem.value == "RMSTYLE")    editClean(0);
  if(menu.selectedItem.value == "RMFORMAT")   editClean(1); 
  if(menu.selectedItem.value == "RMEMPTY")    editClean(2); 
  if(menu.selectedItem.value == "RMWORD")     editClean(3); 
  if(menu.selectedItem.value == "RMSPAN")     editRemoveTagWithoutContent('span'); 
}

// called if ZOOM menu button item has been clicked
function onZoomClicked(id)
{
  var menu = toolbarsTop.getElementById(id);
  editZoom(menu.selectedItem.value)
}

// called if COLOR has been changed
function onChangeTextColor(id)
{
  var button = toolbarsTop.getElementById(id);
  editColor(button.color)
}

function onChangeBackgroundColor(id)
{
  var button = toolbarsTop.getElementById(id);
  editBackColor(button.color)
}

// called if RETURNMODE has been changed
function onReturnMode()
{
  var button = toolbarsTop.getElementByTag("RETURN");
  if(editGetBr()) {
    editSetBr(false);
    button.setImage(imagePath + "broff.gif");
  } else {
    editSetBr(true);
    button.setImage(imagePath + "bron.gif");
  }

}

// id save button is clicked
function onSaveFile()
{
	// if no file is loaded then open save dialog
	if(editGetFileUrl() == "")
		editSaveDialog();
	else
		editSave();
}

// if a text snippet has to be inserted
function onInsertTextModule()
{
	var combo = toolbarsTop.getElementByTag("TM");
	//alert(combo.getSelected().tag1);
	editInsertHtml(combo.getSelected().tag1);

}

// if a text snippet has to be deleted
function onDeleteTextModule()
{
	var combo = toolbarsTop.getElementByTag("TM");
	//alert(combo.getSelected().text)
	editDeleteTextModule(combo.getSelected().text);  
}

// help function
function onHelp()
{
  var left = screen.width/2 - 400/2;
  var top = screen.height/2 - 600/2;
  window.open(__editGetEditorUrl() + "userhelp.html","","left=" + left + ",top=" + top + ",height=600,width=400,resizable=1,status=0,scrollbars=1");
}

