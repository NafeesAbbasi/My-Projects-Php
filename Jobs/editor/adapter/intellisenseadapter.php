<?php

    $sMode = "";
    $sData = "";
    $sPara1 = "";
    $sTemp = "";

    //------------------------------------------------------------------------------
    // This sample returns data after clicking the intellisense key
    // The return string must be
    // image &&: displaytext &&: html &&; ...
    //------------------------------------------------------------------------------
    $sMode = $_REQUEST["key"];
    $sPara1 = $_REQUEST["para"];

    // pinEdit wants to read the text modules
    If($sMode == "INTELLI") {
      $sSep1 = "&&:";
      $sSep2 = "&&;";

      // here we find the entered word before the key
      If($sPara1 == "name") {
        $sData = $sData . "design/image/user.gif" . $sSep1 . "Miller" . $sSep1 . "<font color=red><b>John Miller</b></font>" . $sSep2;
        $sData = $sData . "design/image/user.gif" . $sSep1 . "Howard" . $sSep1 . "<font color=blue><b>Jim Howard</b></font>" . $sSep2;
        $sData = $sData . "design/image/user.gif" . $sSep1 . "Perez"  . $sSep1 . "<font color=green><b>Rosie Perez</b></font>" . $sSep2;
        $sData = $sData . "design/image/user.gif" . $sSep1 . "Connor" . $sSep1 . "<font color=yellow><b>James Connor</b></font>" . $sSep2;
        $sData = $sData . "design/image/user.gif" . $sSep1 . "Grant"  . $sSep1 . "<font color=brown><b>Jamie Grant</b></font>" . $sSep2;
        $sData = $sData . "design/image/user.gif" . $sSep1 . "Stanton" . $sSep1 . "<font color=gray><b>Max Stanton</b></font>" . $sSep2;
      }
      If($sPara1 == "city") {
        $sData = $sData . "design/image/closed.gif" . $sSep1 . "New York" . $sSep1 . "<font color=red><b>New York</b></font>" . $sSep2;
        $sData = $sData . "design/image/closed.gif" . $sSep1 . "Chicago" . $sSep1 . "<font color=blue><b>Chicago</b></font>" . $sSep2;
        $sData = $sData . "design/image/closed.gif" . $sSep1 . "Los Angeles" . $sSep1 . "<font color=green><b>Los Angeles</b></font>" . $sSep2;
        $sData = $sData . "design/image/closed.gif" . $sSep1 . "Boston" . $sSep1 . "<font color=yellow><b>Boston</b></font>" . $sSep2;
        $sData = $sData . "design/image/closed.gif" . $sSep1 . "Miami" . $sSep1 . "<font color=brown><b>Miami</b></font>" . $sSep2;
        $sData = $sData . "design/image/closed.gif" . $sSep1 . "San Francisco" . $sSep1 . "<font color=gray><b>San Francisco</b></font>" . $sSep2;
	}
    }

    // Ignore this code. You don't have to understand it.
    // --> But don't remove it.
    $sTemp = $sTemp . "<html><body onload=\"javascript: parent.__data = document.frmData.data.value;parent.__comm.receive('" . $sMode . "');\"><form id='frmData' name='frmData'><TEXTAREA id='data' onclick='alert();'>" . $sData . "</TEXTAREA></form></body></html>";
    print $sTemp;

?>
