<?php

    $html = "";
    $url = "";
    $temp = "";
    $baseUrl  = "";
    $basePath  = "";
    $data = "OK";  // return value if everything is OK, otherwise error text

    //------------------------------------------------------------------------------
    // This sample saves the current html content under the path, the file
    // has been loaded in the editor by
    // - file property
    // - dialog
    // - Javascript function
    // In other cases there will be no URL
    //------------------------------------------------------------------------------
    if(isset($_REQUEST["html"])) {
      // get the html content
      $html   = $_REQUEST["html"];
    }
    if(isset($_REQUEST["url"])) {
      // get the url
      $url   = $_REQUEST["url"];
    }
    if(isset($_REQUEST["baseurl"])) {
      // get the url
      $baseUrl   = $_REQUEST["baseurl"];
    }
    if(isset($_REQUEST["basepath"])) {
      // get the url
      $basePath   = $_REQUEST["basepath"];
    }

    // Save only if we have an URL
    if($url != "") {
      //----------------------------------------------------------------------------------------------------
      // Sample implementation for file system
      //----------------------------------------------------------------------------------------------------
      $content = stripslashes($html);
      //$content = $html;

      // get the relative path
			$relPath = substr($url, strlen($baseUrl));
      // build path
			$filePath = $basePath . $relPath;

      // now save file
      if (!$handle = fopen($filePath, "w")) {
        $data = "An error occured while saving file to: " . $filePath;
      } else {
        $handle = fopen($filePath, "w");
        fwrite($handle,$content);
        // Only for debugging
        $data = "File saved to " . $filePath;
      }
      fclose($handle);

      /*
        // db sample
        // 
        $link = mysql_connect("mysql_host", "mysql_user", "mysql_password") or die("No connection: " . mysql_error());
        mysql_select_db("myDatabase") or die("Auswahl der Datenbank fehlgeschlagen");

        // write html
        $query = "UPDATE tDATA SET HTML='" . $content . "' WHERE ...";
        $result = mysql_query($query) or die("Error: " . mysql_error());

        mysql_free_result($result);

        // close connection
        mysql_close($link);
      */

    } else {
      $data = "File can't be saved: No URL specified !";
    }

    // Ignore this code. You don't have to understand it.
    $temp = "<html><body onload=\"javascript: parent.__data = document.frmData.data.value;parent.__comm.receive('SAVE');\"><form id='frmData' name='frmData'><TEXTAREA id='data'>" . $data . "</TEXTAREA></form></body></html>";
    print $temp;
?>