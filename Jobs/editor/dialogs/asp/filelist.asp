<%
	function isFilter(objFile)
		Dim temp
		for i=0 to ubound(filter)-1
			temp = filter(i)
			if right(Ucase(objFile.name),len(temp)) = ucase(temp) then
				isFilter = true
				exit function
			end if
		next
		isfilter = false
	end function
	
  function getSize(size)
    Dim sizeSmall 
    Dim l

    sizeSmall = size / 1024
    if sizeSmall < 1 then
      sizeSmall = 1
    end if
    l = cstr(clng(sizeSmall)) + " KB&nbsp;"
    getSize = l
	end function
	  
  Dim objFSO
	Dim filter
	language = request.QueryString("language")
  rootPath = request.QueryString("pathabs")
  rootUrl  = request.QueryString("urlabs")
	filters  = request.QueryString("filter")

	' prepare filter
	filter = split(filters,";")

	' if reloaded  
  if request.form("path") <> "" then
		path = request.form("path")
  else
    path = rootPath 
  end if

	on error resume next
	set objFSO = server.CreateObject("Scripting.FileSystemObject")
	Set objFolder = objFSO.GetFolder(path)
	if err.number > 0 then
		response.Write "The current path is not valid('" + path + "') !"
		response.end
	end if
%>
<html>
	<head>
		<script>
var selectedRow = -1;
var selectedType = "";
var language = "<%=language %>";
var abspath = "<%=rootPath %>";
var absurl  = "<%=rootUrl %>";

function load()
{
  document.getElementById("lblName").innerHTML = parent.getLanguageString(language,3210);
  document.getElementById("lblSize").innerHTML = parent.getLanguageString(language,3211);
  document.getElementById("lblType").innerHTML = parent.getLanguageString(language,3212);
  document.getElementById("lblChanged").innerHTML = parent.getLanguageString(language,3213);
}

function clickRow(row,i,type)
{
  selectedRow = i;
  selectedType = type;

  if(type == 'FILE') {
    parent.selectPath(getSelectedUrl());
	  parent.setFileName(getFileName());
  }

  var curtable = row.parentNode;
  try {
    for(var i=1;i<curtable.rows.length;i++) {
      curtable.rows[i].cells(1).style.background = "";
      curtable.rows[i].cells(1).style.color = "black";
    }
    row.cells(1).style.background = "Highlight";
    row.cells(1).style.color = "HighlightText";
  } catch(Error) {}

}

function dblClick(row,type)
{
  if(type == 'FOLDER') {
    setCurrentPath(row);
    document.frmSelect.submit();
  }
  if(type == 'FILE') {
    parent.RowDblClick()
  }

}

// if go back button is pressed
function goBack()
{
  var curPath = getCurrentPath();
  curPath = curPath.replace(/\\/gi, "/");

  if(curPath.substring(curPath.length-1,curPath.length) == "/")
    curPath = curPath.substring(0,curPath.length-1);

  var i = curPath.length - 1;
	// remove last folder
  while(curPath.substring(i,i+1) != "/") {
    i=i-1;
  }
  var newPath = curPath.substring(0,i);

  // check if we are on the "root"
  if(newPath.length >= (abspath.length - 1) ) {
    document.getElementById("path").value = curPath.substring(0,i) + "/";
    document.frmSelect.submit();
  }
}

// returns the URL of the current selected file/image
function getSelectedUrl()
{
  if(selectedRow >=0) {
    // this is the path
    var path = document.getElementById("hidRow" + selectedRow).value;
		// now build the URL
		var temp = path.substring(abspath.length,path.length);
    temp = temp.replace(/\\/gi, "/");
		path = absurl + temp;
    return path;
  } else {
    return "";
	}
}

// returns current folder URL (without file name)
function getCurrentUrl()
{
	var path = getCurrentPath();
	// now build the URL
	var temp = path.substring(abspath.length,path.length);
  temp = temp.replace(/\\/gi, "/");
	path = absurl + temp;
  return path;
}

// sets the current absolute folder path
function setCurrentPath(row)
{
  document.getElementById("path").value = document.getElementById("hidRow" + row).value;
}

// gets the current absolute folder path
function getCurrentPath()
{
  return document.getElementById("path").value;
}

function getSelectedType()
{
  return selectedType;
}

function getSelectedRow()
{
  return selectedRow;
}

function getFileName()
{
  var curPath = document.getElementById("hidRow" + selectedRow).value;
  curPath = curPath.replace(/\\/gi, "/");
  
  var i=curPath.length -1 ;

  while(curPath.substring(i,i+1) != "/") {
    i=i-1;
  }
  if(curPath.substring(i,i+1) == "/") {
    var ret = curPath.substring(i+1,curPath.length);
    return ret;
  }
}

		</script>
	</head>
	<body style="margin: 2px">
		<table style="cursor: Hand;padding-left: 2px; padding-right: 2px;font-family: arial; font-size:11px; font-weight:normal"
			border="0" cellspacing="0" cellpadding="1" width="100%">
			<form name="frmSelect" id="frmSelect" method="POST" action="filelist.asp?pathabs=<%=rootPath %>&urlabs=<%=rootUrl %>&language=<%=language %>&filter=<%=filters %>">
				<input type="hidden" id="path" name="path" value="<%=path %>">
			</form>
			<td colspan="2" align="left" bgcolor="#ECE9D8">Name</td>
			<td align="right" bgcolor="#ECE9D8">Size</td>
			<td align="left" bgcolor="#ECE9D8">Type</td>
			<td align="left" bgcolor="#ECE9D8">Changed</td>
			<%
    j=0
		For Each subFolder In objFolder.SubFolders
%>
			<tr onclick="clickRow(this,<%=j%>,'FOLDER');" ondblclick="dblClick(<%=j%>,'FOLDER')" onmouseover="this.style.background='buttonface';" onmouseout="this.style.background='white';">
				<input type="hidden" id="hidRow<%=j%>" value="<% = subFolder.path %>">
				<td width="1px"><img src="../../design/image/folder_close.gif" border="0"></td>
				<td align="left"><% = subFolder.name %></td>
				<td align="left"></td>
				<td align="left">Folder</td>
				<td nowrap width="1%" align="left"><% = subFolder.datelastmodified %></td>
			</tr>
			<%      j = j+ 1 %>
			<%  next %>
			<%
  For Each objFile In objFolder.Files
		if isFilter(objFile) then
%>
			<tr  onclick="clickRow(this,<%=j%>,'FILE');" ondblclick="dblClick(<%=j%>,'FILE')" onmouseover="this.style.background='buttonface'" onmouseout="this.style.background='white'">
				<input type="hidden" id="hidRow<%=j%>" value="<%=objFile.path %>">
<% 
	Dim image
	Dim ext
	
  ext = right(Ucase(objFile.name),3)
	if ext = "DOC" then
		image = "word.gif"
	elseif ext = "PDF" then
		image = "pdf.gif"
	else
		image = "html.gif"
	end if
%>

				<td width="1px"><img src="../../design/image/<%=image %>" border="0"></td>
				<td align="left"><%= objFile.name %></td>
				<td align="right"><% =getSize(objFile.size)  %></td>
				<td align="left">File</td>
				<td nowrap width="1%" align="left"><%= objFile.datelastmodified %></td>
			</tr>
			<%      j = j + 1 %>
			<%   end if %>
			<% next %>
			</td></tr>
		</table>
	</body>
</html>
