<%@ Page Language="VB" validateRequest="false" %>

<script runat="server" >

Public language As String = ""
Public rootPath As String = ""
Public rootUrl As String = ""
Public sHtml As String = ""
Public filter() As String
Public filters As String = ""

Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

  Dim path As String = ""
  Dim i As Integer
  Dim j As Integer
  Dim sName As String = ""
  Dim sTemp As String = ""
  Dim sPath As String = ""
  Dim sModified As Date

  Try
    language = Request.QueryString("language")
    rootPath = Request.QueryString("pathabs")
    rootUrl = Request.QueryString("urlabs")
    filters = Request.QueryString("filter")

    ' prepare filter
    filter = Split(filters, ";")

    ' if reloaded  
    If Request.Form("path") <> "" Then
      path = Request.Form("path")
    Else
      path = rootPath
    End If

    Dim objDirInfo As New System.IO.DirectoryInfo(path)
    Dim objDirectories() As System.IO.DirectoryInfo = objDirInfo.GetDirectories()

    sTemp = sTemp + "<table style='cursor: Hand;padding-left: 2px; padding-right: 2px;font-family: arial; font-size:11px; font-weight:normal"
    sTemp = sTemp + " border=0 cellspacing=0 cellpadding=1 width='100%'>"
    sTemp = sTemp + "<form name='frmSelect' id='frmSelect' method='POST' action='filelist.aspx?pathabs=" + rootPath + "&urlabs=" + rootUrl + "&language=" + language + "&filter=" + filters + "'>"
    sTemp = sTemp + "<input type='hidden' id='path' name='path' value='" + path + "'>"
    sTemp = sTemp + "</form>"
    sTemp = sTemp + "<tr>"
    sTemp = sTemp + "<td colspan='2' align='left' bgcolor='#ECE9D8'>Name</td>"
    sTemp = sTemp + "<td align='right' bgcolor='#ECE9D8'>Size</td>"
    sTemp = sTemp + "<td align='left' bgcolor='#ECE9D8'>Type</td>"
    sTemp = sTemp + "<td align='left' bgcolor='#ECE9D8'>Changed</td>"
    sTemp = sTemp + "</tr>"

    For i = 0 To objDirectories.Length - 1
      sName = objDirectories(i).Name
      sPath = objDirectories(i).FullName
      sModified = objDirectories(i).LastWriteTime
      sTemp = sTemp + "<tr onclick=""clickRow(this," + CStr(j) + ",'FOLDER');"" ondblclick=""dblClick(" + CStr(j) + ",'FOLDER')"" onmouseover=""this.style.background='buttonface';"" onmouseout=""this.style.background='white';"">"
      sTemp = sTemp + "<input type='hidden' id='hidRow" + CStr(j) + "' value='" + sPath + "'>"
      sTemp = sTemp + "<td width='1px'><img src='../../design/image/folder_close.gif' border='0'></td>"
      sTemp = sTemp + "<td align='left'>" + sName + "</td>"
      sTemp = sTemp + "<td align='left'></td>"
      sTemp = sTemp + "<td align='left'>Folder</td>"
      sTemp = sTemp + "<td nowrap width='1%' align='left'>" + sModified.ToShortDateString + "</td>"
      sTemp = sTemp + "</tr>"
      j = j + 1
    Next

    'list files
    Dim objFiles() As System.IO.FileInfo = objDirInfo.GetFiles()
    For i = 0 To objFiles.Length - 1
      sName = objFiles(i).Name
      sModified = objFiles(i).LastWriteTime
      sPath = objFiles(i).FullName

      If isFilter(objFiles(i).Extension) Then
        Dim sSize As String = getSize(objFiles(i).Length)
        sTemp = sTemp + "<tr  onclick=""clickRow(this," + CStr(j) + ",'FILE');"" ondblclick=""dblClick(" + CStr(j) + ",'FILE')"" onmouseover=""this.style.background='buttonface'"" onmouseout=""this.style.background='white'"">"
        sTemp = sTemp + "<input type=""hidden"" id=""hidRow" + CStr(j) + """ value=""" + sPath + """>"

        Dim image
        Dim ext

        ext = Right(UCase(objFiles(i).Name), 3)
        If ext = "DOC" Then
          image = "word.gif"
        ElseIf ext = "PDF" Then
          image = "pdf.gif"
        Else
          image = "html.gif"
        End If

        sTemp = sTemp + "<td width='1px'><img src='../../design/image/" + image + "' border=0></td>"
        sTemp = sTemp + "<td align='left'>" + sName + "</td>"
        sTemp = sTemp + "<td align='right'>" + sSize + "</td>"
        sTemp = sTemp + "<td align='left'>File</td>"
        sTemp = sTemp + "<td nowrap width='1%' align='left'>" + sModified.ToShortDateString + "</td>"
        sTemp = sTemp + "</tr>"
        sTemp = sTemp + "</td></tr>"
        j = j + 1
      End If
    Next

    sTemp = sTemp + "</table>"
  Catch ex As Exception
    sTemp = "Path " + path + " is not valid !"
  End Try

  sHtml = sTemp

end sub

Function isFilter(ByVal sExt As String)

  Dim sTemp As String = ""
  Dim i As Integer = 0

  For i = 0 To filter.Length - 1
    sTemp = "." + filter(i)
    If sExt.ToUpper = UCase(sTemp) Then
      isFilter = True
      Exit Function
    End If
  Next
  isFilter = False

End Function

Function getSize(ByVal size As Long) As String

  Dim sizeSmall As Double
  Dim l As Long

  sizeSmall = size / 1024
  If sizeSmall < 1 Then
    sizeSmall = 1
  End If
  Return CStr(CInt(sizeSmall)) + " KB&nbsp;"

End Function

</script>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<script>
var selectedRow = -1;
var selectedType = "";
var language = "<%=language %>";
var abspath = "<%=rootPath %>";
var absurl  = "<%=rootUrl %>";

function load()
{
  document.getElementById("lblName").innerHTML = parent.getLanguageString(language,3210);
  document.getElementById("lblSize").innerHTML = parent.getLanguageString(language,3211);
  document.getElementById("lblType").innerHTML = parent.getLanguageString(language,3212);
  document.getElementById("lblChanged").innerHTML = parent.getLanguageString(language,3213);
}

function clickRow(row,i,type)
{
  selectedRow = i;
  selectedType = type;

  if(type == 'FILE') {
    parent.selectPath(getSelectedUrl());
	  parent.setFileName(getFileName());
  }

  var curtable = row.parentNode;
  try {
    for(var i=1;i<curtable.rows.length;i++) {
      curtable.rows[i].cells(1).style.background = "";
      curtable.rows[i].cells(1).style.color = "black";
    }
    row.cells(1).style.background = "Highlight";
    row.cells(1).style.color = "HighlightText";
  } catch(Error) {}

}

function dblClick(row,type)
{
  if(type == 'FOLDER') {
    setCurrentPath(row);
    document.frmSelect.submit();
  }
  if(type == 'FILE') {
    parent.RowDblClick()
  }

}

// if go back button is pressed
function goBack()
{
  var curPath = getCurrentPath();
  curPath = curPath.replace(/\\/gi, "/");

  if(curPath.substring(curPath.length-1,curPath.length) == "/")
    curPath = curPath.substring(0,curPath.length-1);

  var i = curPath.length - 1;
	// remove last folder
  while(curPath.substring(i,i+1) != "/") {
    i=i-1;
  }
  var newPath = curPath.substring(0,i);

  // check if we are on the "root"
  if(newPath.length >= (abspath.length - 1) ) {
    document.getElementById("path").value = curPath.substring(0,i) + "/";
    document.frmSelect.submit();
  }
}

// returns the URL of the current selected file/image
function getSelectedUrl()
{
  if(selectedRow >=0) {
    // this is the path
    var path = document.getElementById("hidRow" + selectedRow).value;
		// now build the URL
		var temp = path.substring(abspath.length,path.length);
    temp = temp.replace(/\\/gi, "/");
		path = absurl + temp;
    return path;
  } else {
    return "";
	}
}

// returns current folder URL (without file name)
function getCurrentUrl()
{
	var path = getCurrentPath();
	// now build the URL
	var temp = path.substring(abspath.length,path.length);
  temp = temp.replace(/\\/gi, "/");
	path = absurl + temp;
  return path;
}

// sets the current absolute folder path
function setCurrentPath(row)
{
  document.getElementById("path").value = document.getElementById("hidRow" + row).value;
}

// gets the current absolute folder path
function getCurrentPath()
{
  return document.getElementById("path").value;
}

function getSelectedType()
{
  return selectedType;
}

function getSelectedRow()
{
  return selectedRow;
}

function getFileName()
{
  var curPath = document.getElementById("hidRow" + selectedRow).value;
  curPath = curPath.replace(/\\/gi, "/");
  
  var i=curPath.length -1 ;

  while(curPath.substring(i,i+1) != "/") {
    i=i-1;
  }
  if(curPath.substring(i,i+1) == "/") {
    var ret = curPath.substring(i+1,curPath.length);
    return ret;
  }
}
</script>
</HEAD>
<body style="MARGIN: 0px">
<%= sHtml %>
</body>
</HTML>
