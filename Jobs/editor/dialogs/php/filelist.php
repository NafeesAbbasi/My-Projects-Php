<?php
	  
$path     = "";
$filter   = null;
$filters  = $_REQUEST["filter"];
$language = $_REQUEST["language"];
$rootPath = "";
$rootUrl  = $_REQUEST["urlabs"];

	// prepare filter
	$filter = split(";",$filters);
  
  if(isset($_REQUEST["pathabs"])) {
    $rootPath = $_REQUEST["pathabs"];
  }

  if(isset($_REQUEST["path"])) {
    $path   = $_REQUEST["path"];
  }
  if($path == "") {
    $path = $rootPath;
  }

?>

<html>
	<head>
		<script>
var selectedRow = -1;
var selectedType = "";
var language = "<?php print $language; ?>";
var abspath = "<?php print $rootPath; ?>";
var absurl  = "<?php print $rootUrl; ?>";

function load()
{
  document.getElementById("lblName").innerHTML = parent.getLanguageString(language,3210);
  document.getElementById("lblSize").innerHTML = parent.getLanguageString(language,3211);
  document.getElementById("lblType").innerHTML = parent.getLanguageString(language,3212);
  document.getElementById("lblChanged").innerHTML = parent.getLanguageString(language,3213);
}

function clickRow(row,i,type)
{
  selectedRow = i;
  selectedType = type;

  if(type == 'FILE') {
    parent.selectPath(getSelectedUrl());
	  parent.setFileName(getFileName());
  }

  var curtable = row.parentNode;
  try {
    for(var i=1;i<curtable.rows.length;i++) {
      curtable.rows[i].cells(1).style.background = "";
      curtable.rows[i].cells(1).style.color = "black";
    }
    row.cells(1).style.background = "Highlight";
    row.cells(1).style.color = "HighlightText";
  } catch(Error) {}

}

function dblClick(row,type)
{
  if(type == 'FOLDER') {
    setCurrentPath(row);
    document.frmSelect.submit();
  }
  if(type == 'FILE') {
    parent.RowDblClick()
  }

}

// if go back button is pressed
function goBack()
{
  var curPath = getCurrentPath();
  curPath = curPath.replace(/\\/gi, "/");

  if(curPath.substring(curPath.length-1,curPath.length) == "/")
    curPath = curPath.substring(0,curPath.length-1);

  var i = curPath.length - 1;
	// remove last folder
  while(curPath.substring(i,i+1) != "/") {
    i=i-1;
  }
  var newPath = curPath.substring(0,i);

  // check if we are on the "root"
  if(newPath.length >= (abspath.length - 1) ) {
    document.getElementById("path").value = curPath.substring(0,i) + "/";
    document.frmSelect.submit();
  }
}

// returns the URL of the current selected file/image
function getSelectedUrl()
{
  if(selectedRow >=0) {
    // this is the path
    var path = document.getElementById("hidRow" + selectedRow).value;
		// now build the URL
		var temp = path.substring(abspath.length,path.length);
    temp = temp.replace(/\\/gi, "/");
		path = absurl + temp;
    return path;
  } else {
    return "";
	}
}

// returns current folder URL (without file name)
function getCurrentUrl()
{
	var path = getCurrentPath();
	// now build the URL
	var temp = path.substring(abspath.length,path.length);
  temp = temp.replace(/\\/gi, "/");
	path = absurl + temp;
  return path;
}

// sets the current absolute folder path
function setCurrentPath(row)
{
  document.getElementById("path").value = document.getElementById("hidRow" + row).value;
}

// gets the current absolute folder path
function getCurrentPath()
{
  return document.getElementById("path").value;
}

function getSelectedType()
{
  return selectedType;
}

function getSelectedRow()
{
  return selectedRow;
}

function getFileName()
{
  var curPath = document.getElementById("hidRow" + selectedRow).value;
  curPath = curPath.replace(/\\/gi, "/");
  
  var i=curPath.length -1 ;

  while(curPath.substring(i,i+1) != "/") {
    i=i-1;
  }
  if(curPath.substring(i,i+1) == "/") {
    var ret = curPath.substring(i+1,curPath.length);
    return ret;
  }
}

		</script>
	</head>


	<body style="margin: 2px">
		<table style="cursor: Hand;padding-left: 2px; padding-right: 2px;font-family: arial; font-size:11px; font-weight:normal"
			border="0" cellspacing="0" cellpadding="1" width="100%">
			<form name="frmSelect" id="frmSelect" method="POST" action="filelist.php?pathabs=<?php print $rootPath ; ?>&urlabs=<?php print $rootUrl; ?>&language=<?php print $language ; ?>&filter=<?php print $filters ; ?>">
				<input type="hidden" id="path" name="path" value="<?php print $path ; ?>">
			</form>
			<td colspan="2" align="left" bgcolor="#ECE9D8">Name</td>
			<td align="right" bgcolor="#ECE9D8">Size</td>
			<td align="left" bgcolor="#ECE9D8">Type</td>
			<td align="left" bgcolor="#ECE9D8">Changed</td>
<?php
    $j = 0;

    $handle = opendir($path);
    while ($file = readdir ($handle)) {
      if ($file != "." && $file != "..") {
        if (is_dir($path . $file)) {
          $subFolderPath = $path . $file;
          $subFolderName = $file;
          $subFolderDate = date("m/d/Y H:i:s", filemtime($subFolderPath));
?>
        <tr onclick="clickRow(this,<?php print $j; ?>,'FOLDER');" ondblclick="dblClick(<?php print $j; ?>,'FOLDER')" onmouseover="this.style.background='buttonface';" onmouseout="this.style.background='white';">
          <input type="hidden" id="hidRow<?php print $j;  ?>" value="<?php print $subFolderPath; ?>">
          <td width="1px"><img src="../../design/image/folder_close.gif" border=0></td>
          <td align="left"><?php print $subFolderName; ?></td>
          <td align="left"></td>
          <td align="left">Folder</td>
          <td nowrap width="1%" align="left"><?php print $subFolderDate; ?></td>
        </tr>
<?php
          $j++;
        }
      }
    }
    closedir($handle);
?>


<?php
    if(substr($path ,strlen($path )-1,strlen($path )) != "/") {
      $path = $path . "/";
    }
    $handle = opendir($path);
    while ($file = readdir ($handle)) {
      if ($file != "." && $file != "..") {
        if(is_file($path . $file)) {
          $subFilePath = $path . $file;
          $subFileName = $file;
          $subFileDate = date("m/d/Y H:i:s", filemtime($subFilePath));
          $ext = strtolower(substr($file, strrpos($file, '.') + 1));
	  if(in_array($ext,$filter)) {
?>
        <tr  onclick="clickRow(this,<?php print $j;  ?>,'FILE');" ondblclick="dblClick(<?php print $j;  ?>,'FILE')" onmouseover="this.style.background='buttonface'" onmouseout="this.style.background='white'">
          <input type="hidden" id="hidRow<?php print $j;  ?>" value="<?php print $subFilePath;  ?>">
          <td width="1px"><img src="../../design/image/html.gif" border=0></td>
          <td align="left"><?php print $subFileName;  ?></td>
          <td align="right"><?php print filesize($subFilePath);  ?></td>
          <td align="left">File</td>
          <td nowrap width="1%" align="left"><?php print $subFileDate;  ?></td>
        </tr>
<?php
            $j++;
          }
        }
      }
    }
    closedir($handle);
?>


  </td></tr>
</table>

</body>
</html>
