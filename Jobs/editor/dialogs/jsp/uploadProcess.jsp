<%@ page import="com.jspsmart.upload.*" %>
<jsp:useBean id="mySmartUpload" scope="page" class="com.jspsmart.upload.SmartUpload" />

<%
  String path         = request.getParameter("path");
  String insert       = request.getParameter("insert");
  String sErrorDescription = "";

  boolean isError = false;

  try {
    if(path != null) {
      // Initialization
      mySmartUpload.initialize(pageContext);
      // limit size
      mySmartUpload.setTotalMaxFileSize(500000);
      // Upload
      mySmartUpload.upload();
      // save file
      mySmartUpload.save(path);

      if(mySmartUpload.getFiles().getFile(0).isMissing()) {
        isError = true;
        //String fileName = mySmartUpload.getFiles().getFile(0).getFileName();
        //byte[] binary = mySmartUpload.getFiles().getFile(0).getContentString().getBytes();
      }
    }
  } catch(Exception e) {
    isError = true;
    sErrorDescription = e.getMessage();
  }

%>
<html>
<% if(!isError) { %>
<%   if(insert.equals("1")) { %>
       <body onload="window.opener.callback_editUpload('<%= mySmartUpload.getFiles().getFile(0).getFileName() %>');window.close()">
<%   } else { %>
       <body onload="window.close()">
<%   } %>
<% } else { %>
       <body>
       <%= "The following error occured while uploading to " + path + ": " + sErrorDescription %>
<% } %>

</body>
<html>