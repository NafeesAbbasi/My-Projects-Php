<?php



class Model_users extends CI_Model



{



	var $tbl = "job_users";	



	



	function __construct()



    {



        parent::__construct();



    }



	function count_users_rec($where_criteria)



	{

	

		if($where_criteria=='')

		{

		

	 $query = $this->db->query("Select job_users.user_id,

		 								

									   job_users.username,

									   

									    job_users.Password,



									   job_users.firstname,



									   job_users.lastname,  



									   job_users.address, 

									   

									   job_users.city,

									   

									   job_users.state,

									   

									   job_users.zipcode,

									   

									   job_users.country,

									   

									   job_users.phonenum,

									   

									   job_users.faxnum ,



									   job_users.Email,

									   

									   job_users.status,

									   

									   job_users.designation,

									   

									    job_users.role,



									   tbl_lm_states.name, 



									   tbl_lm_states.id							



									   FROM job_users 



									   left join tbl_lm_states on tbl_lm_states.abbr=job_users.state

									   

									   where admin_id='".$this->session->userdata('super_admin')."'					



									   $where_criteria");

									   

			}

			else

			{

			

			 $query = $this->db->query("Select job_users.user_id,

		 								

									   job_users.username,

									   

									    job_users.Password,



									   job_users.firstname,



									   job_users.lastname,  



									   job_users.address, 

									   

									   job_users.city,

									   

									   job_users.state,

									   

									   job_users.zipcode,

									   

									   job_users.country,

									   

									   job_users.phonenum,

									   

									   job_users.faxnum ,



									   job_users.Email,

									   

									   job_users.status,

									   

									   job_users.designation,

									   

									    job_users.role,



									   tbl_lm_states.name, 



									   tbl_lm_states.id							



									   FROM job_users 



									   left join tbl_lm_states on tbl_lm_states.abbr=job_users.state

									  

									   $where_criteria and admin_id='".$this->session->userdata('super_admin')."'");

			}						



	  return $query->num_rows();



	}



	function get_users_list($where_criteria,$limit = NULL, $offset = NULL){



		if($where_criteria=='')

		{
		
				if ($this->session->userdata('role')=='admin')
				{
					 $query = $this->db->query("Select job_users.user_id,

		 								

									   job_users.username,

									   

									    job_users.Password,



									   job_users.firstname,



									   job_users.lastname,  



									   job_users.address, 

									   

									   job_users.city,

									   

									   job_users.state,

									   

									   job_users.zipcode,

									   

									   job_users.country,

									   

									   job_users.phonenum,

									   

									   job_users.faxnum ,



									   job_users.Email,

									   

									   job_users.status,

									   

									   job_users.designation,

									   

									   job_users.role,



									   tbl_lm_states.name, 



									   tbl_lm_states.id							



									   FROM job_users 



									   left join tbl_lm_states on tbl_lm_states.abbr=job_users.state

									   

									   where admin_id='".$this->session->userdata('login_id')."'

									   

									   $where_criteria



									   LIMIT {$offset} , {$limit}")->result_array();

				
				}
				else
				{
				 $query = $this->db->query("Select job_users.user_id,

		 								

									   job_users.username,

									   

									    job_users.Password,



									   job_users.firstname,



									   job_users.lastname,  



									   job_users.address, 

									   

									   job_users.city,

									   

									   job_users.state,

									   

									   job_users.zipcode,

									   

									   job_users.country,

									   

									   job_users.phonenum,

									   

									   job_users.faxnum ,



									   job_users.Email,

									   

									   job_users.status,

									   

									   job_users.designation,

									   

									   job_users.role,



									   tbl_lm_states.name, 



									   tbl_lm_states.id							



									   FROM job_users 



									   left join tbl_lm_states on tbl_lm_states.abbr=job_users.state

									   

									   where admin_id='".$this->session->userdata('super_admin')."'

									   

									   $where_criteria



									   LIMIT {$offset} , {$limit}")->result_array();

				
				}	
	
			
				}

			else

			{
					if ($this->session->userdata('role')=='admin')
					{
						 $query = $this->db->query("Select job_users.user_id,

		 								

									   job_users.username,

									   

									    job_users.Password,



									   job_users.firstname,



									   job_users.lastname,  



									   job_users.address, 

									   

									   job_users.city,

									   

									   job_users.state,

									   

									   job_users.zipcode,

									   

									   job_users.country,

									   

									   job_users.phonenum,

									   

									   job_users.faxnum ,



									   job_users.Email,

									   

									   job_users.status,

									   

									   job_users.designation,

									   

									   job_users.role,



									   tbl_lm_states.name, 



									   tbl_lm_states.id							



									   FROM job_users 



									   left join tbl_lm_states on tbl_lm_states.abbr=job_users.state

									   

									   $where_criteria

									   

									   and admin_id='".$this->session->userdata('admin_id')."'



									   LIMIT {$offset} , {$limit}")->result_array();
					}
					else
					{
						 $query = $this->db->query("Select job_users.user_id,

		 								

									   job_users.username,

									   

									    job_users.Password,



									   job_users.firstname,



									   job_users.lastname,  



									   job_users.address, 

									   

									   job_users.city,

									   

									   job_users.state,

									   

									   job_users.zipcode,

									   

									   job_users.country,

									   

									   job_users.phonenum,

									   

									   job_users.faxnum ,



									   job_users.Email,

									   

									   job_users.status,

									   

									   job_users.designation,

									   

									   job_users.role,



									   tbl_lm_states.name, 



									   tbl_lm_states.id							



									   FROM job_users 



									   left join tbl_lm_states on tbl_lm_states.abbr=job_users.state

									   

									   $where_criteria

									   

									   and admin_id='".$this->session->userdata('super_admin')."'



									   LIMIT {$offset} , {$limit}")->result_array();
					}
				
			}



        return $query;



	}





	function get_user($id)



	{



	 $query = $this->db->query("Select job_users.user_id,

		 								

									   job_users.username,

									   

									    job_users.Password,



									   job_users.firstname,



									   job_users.lastname,  



									   job_users.address, 

									   

									   job_users.city,

									   

									   job_users.state,

									   

									   job_users.zipcode,

									   

									   job_users.country,

									   

									   job_users.phonenum,

									   

									   job_users.faxnum ,



									   job_users.Email,

									   

									   job_users.status,

									   

									   job_users.designation,
									   


									   job_users.admin_id,
									   


									   job_users.creator,
									   
									   
									   
									   job_users.role,
									   


									   tbl_lm_states.name, 



									   tbl_lm_states.id,

									   

									   tblmstcountry.Name, 



									   tblmstcountry.CountryId								



									   FROM job_users 



									   left join tbl_lm_states on tbl_lm_states.id=job_users.state

									   

									   left join tblmstcountry on tblmstcountry.CountryId=job_users.country								



									   where job_users.user_id=$id");



	  $result =  $query->result_array();



	  if($query->num_rows()>0){



	  return $result[0];



	  }



	  else



	  	return false;



	  



	}



	function get_states($state_code=NULL)



	{



	  $this->db->select('*');



	  if($state_code){



	  	 $this->db->where('Code', $state_code);



	  }



	  $result = $this->db->get('tbl_lm_states')->result_array();



	   if(count($result)>0){



	  	return $result;



	  }



	  else



	  	return 0;



	}



	function update_user($data)



	{



		$this->db->where('user_id', $data['user_id']);



		return $this->db->update($this->tbl, $data); 



	 }



	 function delete_user($id)



	{



	 	if(!$id) return false;



		$this->db->where('user_id', $id);



		return $this->db->delete($this->tbl); 



	 }

	 

	 function get_country()

	 {

	 	return $this->db->select('tblmstcountry.*')->get('tblmstcountry')->result_array();

	 }

	 

	 function add_user($data)

	 {

	

	  $this->db->insert('job_users', $data); 

	 }



	function get_state_by_country($id)

	{

		return $this->db->select('tbl_lm_states.*')->where('countryid',$id)->get('tbl_lm_states')->result_array();

	}

	

	function role_exists($email)

	{

	

    $this->db->where('Email',$email);

	

    $query = $this->db->get('job_users');

	

    if ($query->num_rows() > 0)

	{

        return true;

	}

		

	else

	{

		return false;

	}

	

	}

	

	function forgotpass($email)



	{

		 $this->db->where('Email', $email);



		 $this->db->select('job_users.*');



    	 return $this->db->get('job_users')->result_array();



	}

	

	function updatepass($pass, $email)



	{



		  $this->db->set('Password' , $pass);



		  $this->db->where('Email', $email);



		  $this->db->update('job_users');



	



	}



	



}











?>