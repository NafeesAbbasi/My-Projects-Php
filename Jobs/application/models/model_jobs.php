<?php



class Model_jobs extends CI_Model

{

	var $tbl = "job_jobs";	


	function __construct()


    {

        parent::__construct();

    }



	function count_jobs_rec($where_criteria)

	{

		if($where_criteria=='')

		{

	 $query = $this->db->query("Select * from job_jobs where admin_id='".$this->session->userdata('login_id')."' $where_criteria  ORDER BY job_jobs.job_id DESC");

		}

		else

		{

		 $query = $this->db->query("Select * from job_jobs $where_criteria and admin_id='".$this->session->userdata('login_id')."'  ORDER BY job_jobs.job_id DESC");

		}						

	  return $query->num_rows();



	}



	function get_jobs_list($where_criteria,$limit = NULL, $offset = NULL){



		if($where_criteria=='')

		{
			if($this->session->userdata('role')=='admin')
			{
				 $query = $this->db->query("Select * from job_jobs where admin_id='".$this->session->userdata('login_id')."' $where_criteria ORDER BY job_jobs.job_id DESC")->result_array();
			}
			else
			{
				 $query = $this->db->query("Select * from job_jobs where admin_id='".$this->session->userdata('super_admin')."' $where_criteria ORDER BY job_jobs.job_id")->result_array();
			
			}

		}

		else

		{
			if($this->session->userdata('role')=='admin')
			{
			
				$query = $this->db->query("Select * from job_jobs $where_criteria  and admin_id='".$this->session->userdata('login_id')."' ORDER BY job_jobs.job_id DESC")->result_array();
			
			}
			else
			{
			
				$query = $this->db->query("Select * from job_jobs $where_criteria  and admin_id='".$this->session->userdata('super_admin')."' ORDER BY job_jobs.job_id DESC")->result_array();
			
			}
			
		}

        return $query;
	}


	function get_job($id)
	{

	 $query = $this->db->query("Select * from job_jobs where job_jobs.job_id=$id");

	  $result =  $query->result_array();

	  if($query->num_rows()>0){

	  return $result[0];

	  }

	  else

	  	return false;
}

	//Update Job By Job Id

	public function UpdateJobById($id,$date_selec)
	{
		$this->db->set('date',$date_selec)->where('job_id',$id)->update('job_jobs');
	}


	function update_job($data)

	{

		$this->db->where('job_id', $data['job_id']);

		return $this->db->update($this->tbl, $data); 

	 }


	 function delete_job($id)

	{

	 	if(!$id) return false;

		$this->db->where('job_id', $id);

		return $this->db->delete($this->tbl); 

	 }
	 

	 function add_job($data)

	 {	

	  $this->db->insert('job_jobs', $data); 

	 }
	 
	  function unique_num()
	 {
	 
	 return mt_rand(100000, 999999);
	 
	 }
	 
	 function get_alljobs()
	{
	 /*$query = $this->db->query("Select * from job_jobs where admin_id='".$this->session->userdata('login_id')."' and status=1")->result_array();

	
	  	return $query;*/
		
		
		$results = $this->db->get('job_jobs')->result_array();
		
		$subfield = 'date';
		
		foreach ($results as $key => $row)
		{
			$string_value = $row[$subfield];
			
			$dater = explode('/',$string_value);
			
			$string_value = $dater[1]."/".$dater[0]."/".$dater[2];
			
			$time_value  = strtotime($string_value);
			
			$converted_date = date('Y-m-d',$time_value);
			
			$this->db->set('time_date',$converted_date)->where('job_id',$row['job_id'])->update('job_jobs');
			
			$sortarray[$key] = $row;
		}
	}
	 
	 function get_equipments()
	{

	 $query = $this->db->query("Select * from job_equipment where admin_id='".$this->session->userdata('login_id')."' and status=1")->result_array();

	
	  	return $query;
	}
	
	
	function get_users()
	{
	 $query = $this->db->query("Select * from job_users where admin_id='".$this->session->userdata('login_id')."' and status=1")->result_array();

	
	  	return $query;	
	}
	
	function get_jobs_list_sorted($id)
	{
		$res= explode('-', $id);
		
		//echo "Select * from job_jobs  where admin_id='".$this->session->userdata('login_id')."' ORDER BY job_jobs.".$res[0]." ".$res[1]."";
		
		if($this->session->userdata('role')=='admin')
		{
		$query = $this->db->query("Select * from job_jobs  where admin_id='".$this->session->userdata('login_id')."' ORDER BY job_jobs.".$res[0]." ".$res[1]."")->result_array();
		}
		else
		{
			$query = $this->db->query("Select * from job_jobs  where admin_id='".$this->session->userdata('super_admin')."' ORDER BY job_jobs.".$res[0]." ".$res[1]."")->result_array();
		}
		

	
	  	return $query;	
	
	}
	
	function filter_by_date($fromdate, $todate)
	{
		 if($this->session->userdata('role')=='admin')
		 {
		
		 $query = $this->db->query("SELECT * FROM `job_jobs` WHERE time_date BETWEEN '".$fromdate."' and '".$todate."' and admin_id='".$this->session->userdata('login_id')."' ORDER BY time_date")->result_array();
		 }
		 else
		 {
		  
		 $query = $this->db->query("SELECT * FROM `job_jobs` WHERE time_date BETWEEN '".$fromdate."' and '".$todate."' and admin_id='".$this->session->userdata('super_admin')."' ORDER BY time_date")->result_array();
		 }
		
	  	return $query;	
	}
	
	function filter_by_status($status)
	{
		 if($this->session->userdata('role')=='admin')
		 {
		
		 $query = $this->db->query("SELECT * FROM `job_jobs` WHERE status='".$status."' and admin_id='".$this->session->userdata('login_id')."'")->result_array();
		 }
		 else
		 {
		  
		 $query = $this->db->query("SELECT * FROM `job_jobs` WHERE status='".$status."' and admin_id='".$this->session->userdata('super_admin')."'")->result_array();
		 }
		
	  	return $query;	
	}
	
	function filter_by_priority($priority)
	{
		 if($this->session->userdata('role')=='admin')
		 {
		
		 $query = $this->db->query("SELECT * FROM `job_jobs` WHERE priority='".$priority."' and admin_id='".$this->session->userdata('login_id')."'")->result_array();
		 }
		 else
		 {
		  
		 $query = $this->db->query("SELECT * FROM `job_jobs` WHERE priority='".$priority."' and admin_id='".$this->session->userdata('super_admin')."'")->result_array();
		 }
		
	  	return $query;	
	}
	 

}

?>