<?php



class Model_equipment extends CI_Model



{



	var $tbl = "job_equipment";	


	function __construct()


    {

        parent::__construct();

    }



	function count_equipment_rec($where_criteria)

	{

		if($where_criteria=='')

		{

	 $query = $this->db->query("Select * from job_equipment where admin_id='".$this->session->userdata('login_id')."' $where_criteria");

		}

		else

		{

		 $query = $this->db->query("Select * from job_equipment $where_criteria and admin_id='".$this->session->userdata('login_id')."'");

		}						



	  return $query->num_rows();



	}



	function get_equipment_list($where_criteria,$limit = NULL, $offset = NULL){



		if($where_criteria=='')

		{
			if($this->session->userdata('role')=='admin')
			{
				 $query = $this->db->query("Select * from job_equipment where admin_id='".$this->session->userdata('login_id')."' $where_criteria LIMIT {$offset} , {$limit}")->result_array();
			
			}
			else
			{
				 $query = $this->db->query("Select * from job_equipment where admin_id='".$this->session->userdata('super_admin')."' $where_criteria LIMIT {$offset} , {$limit}")->result_array();
			
			}
			
			}

			else

			{
			
			if($this->session->userdata('role')=='admin')
			{
				$query = $this->db->query("Select * from job_equipment $where_criteria  and admin_id='".$this->session->userdata('login_id')."' LIMIT {$offset} , {$limit}")->result_array();
				
			}
			else
			{
				$query = $this->db->query("Select * from job_equipment $where_criteria  and admin_id='".$this->session->userdata('super_admin')."' LIMIT {$offset} , {$limit}")->result_array();
			
			}

				 

			

			}

        return $query;
	}


	function get_equipment($id)
	{

	 $query = $this->db->query("Select * from job_equipment where job_equipment.equipment_id=$id");

	  $result =  $query->result_array();

	  if($query->num_rows()>0){

	  return $result[0];

	  }

	  else

	  	return false;
}


	function update_equipment($data)

	{

		$this->db->where('equipment_id', $data['equipment_id']);

		return $this->db->update($this->tbl, $data); 

	 }


	 function delete_equipment($id)

	{

	 	if(!$id) return false;

		$this->db->where('equipment_id', $id);

		return $this->db->delete($this->tbl); 

	 }
	 

	 function add_equipment($data)

	 {	

	  $this->db->insert('job_equipment', $data); 

	 }
	 
	  function unique_num()
	 {
	 
	 return mt_rand(100000, 999999);
	 
	 }
	 

}

?>