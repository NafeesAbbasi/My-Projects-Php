<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_email extends CI_Model
{

	function __construct()
    {
        parent::__construct();
    }
	
	function getEmailList()
	{		
		return $this->db->from('tbl_lm_default_emails')->get()->result_array();		
	}
	
	function getEmailById($email_id)
	{
		if($email_id==0)
			$email = array_fill_keys($this->db->list_fields('tbl_lm_default_emails'), '');
		else
			$email = $this->db->where('email_id', $email_id)->get('tbl_lm_default_emails')->row_array();
			
		return $email;
	}
	function getEmailByTitle($title)
	{
		/*if($email_id==0)
			$email = array_fill_keys($this->db->list_fields('tbl_lm_default_emails'), '');
		else*/
			$email = $this->db->where('Title', $title)->get('tbl_lm_default_emails')->row_array();
			
		return $email;
	}
	
	function save_email($email_id)
	{
		//$this->form_validation->set_rules('from', 'From', 'trim|required|valid_email');
		$this->form_validation->set_rules('subject', 'Subject', 'trim|required');
		$this->form_validation->set_rules('content', 'Message', 'trim|required');
		if($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('message', validation_errors());
			return false;
		}
		else
		{
			if ($email_id == 0)
			{
				$this->db->set('email_id', '');
				$this->db->insert('tbl_lm_default_emails');
				$email_id = $this->db->insert_id();
			}
			
			//$this->db->set('from', $this->input->post('from'));
			$this->db->set('subject', $this->input->post('subject'));
			$this->db->set('content', $this->input->post('content'));
			
			$this->db->where('email_id', $email_id);
			$this->db->update('tbl_lm_default_emails');
			
			$this->session->set_flashdata('message', 'Email saved.');
			return true;
		}	
	}
	
	function get($name)
	{
		return $this->db->where('name',$name)->get('tbl_lm_default_emails')->row_array();
	}	
	function deleteEmail($id)
	{
		return $this->db->where('email_id',$id)->delete('tbl_lm_default_emails');
	}
}
/* End of file outbound_email_model.php */
/* Location: ./application/models/outbound_email_model.php */
