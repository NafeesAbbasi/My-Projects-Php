<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');







class Users extends CI_Controller {







  function __construct()



   {



		parent::__construct();



		// Your own constructor code



		if($this->session->userdata('login_id')=='')

		{



			redirect($this->config->item('base_url'),'Location');



		}

		$this->load->model('model_user');

		$this->load->model('model_email');

		$this->load->model('model_users');

		$this->data = array();		



   }



	function index()



	{		



		$this->data['meta'] = getMetaContent('Admin- Members Management','title+description+keywords');



		$this->data['body'] = 'users_management';



		$this->load->view('structure', $this->data);



	}



	function get_users()



	{	

		$where= '';

		



		if($this->input->post('first_name')!='')



			$where = "Where job_users.firstname  like '%".trim($this->input->post('first_name'))."%' "; 



		



		if($this->input->post('last_name')!='')



		{



			if($where =='')



				$where = "Where  job_users.lastname like '%".trim($this->input->post('last_name'))."%' ";



			else



				$where.= " and  job_users.last_name like '%".trim($this->input->post('last_name'))."%' ";



		}



		if($this->input->post('email')!='')



		{



			if($where =='')



				$where = "Where job_users.Email like '%".trim($this->input->post('email'))."%' ";



			else



				$where.= " and job_users.Email like '%".trim($this->input->post('email'))."%' ";



		}



		if($this->input->post('username')!='')



		{



			if($where =='')



				$where = "Where job_users.username like '%".trim($this->input->post('username'))."%' ";



			else



				$where.= " and job_users.username like '%".trim($this->input->post('username'))."%' ";



		}



		



		//echo $where;



		$per_page =15;



		if($this->uri->segment(3)=='')



			$start_point = 0;



		else



			$start_point = $this->uri->segment(3);



			$total = $this->model_users->count_users_rec($where);  //total post



			$this->data['users_list'] = $this->model_users->get_users_list($where,$per_page,$start_point);



			$config['base_url'] = $this->config->item('base_url').'users/get_users';



			$config['uri_segment'] = '3';



			$config['total_rows'] = $total;



			$config['per_page'] =   $per_page;



			$config['first_link'] = '';



			$config['last_link'] = '';



			$config['next_link'] = '<img src="../images/arrow_right_off.png" style="width: 15px;"/>';



			$config['prev_link'] = '<img src="../images/arrow_left_off.png" style="width: 15px;"/>';



			$config['full_tag_open']  = '<p>';



  			$config['full_tag_close'] = '</p>';



			$this->pagination->initialize($config);	



			$this->data['pagination'] = $this->pagination->create_links();



			$this->load->view('users/index', $this->data);



	}



	function view_users()



	{		



		$this->data['user_data'] = $this->model_users->get_user($this->uri->segment(3));

		

		$this->data['country'] = $this->model_users->get_country();



		$this->data['states'] = $this->model_users->get_states();



		//$this->data['states'] = $this->model_users->get_states($this->uri->segment(3));



		$this->load->view('users/view_user', $this->data);



	}



	function edit_users()



	{		



		$this->data['user_data'] = $this->model_users->get_user($this->uri->segment(3));

		

		$this->data['country'] = $this->model_users->get_country();



		$this->data['states'] = $this->model_users->get_states();



		$this->load->view('users/edit_user', $this->data);



	}



	function edit_user_validation()
	{		

			$this->form_validation->set_rules('username', 'User Name','required');

			$this->form_validation->set_rules('first_name', 'First Name','required');

			$this->form_validation->set_rules('last_name', 'Last Name', 'required');

			$this->form_validation->set_rules('email', 'Email', 'required');

			$this->form_validation->set_rules('address', 'Address','trim|required');

			$this->form_validation->set_rules('country', 'Country','required');

			//$this->form_validation->set_rules('state_code', 'State', 'required');

			$this->form_validation->set_rules('city', 'City', 'required');

			$this->form_validation->set_rules('zipcode','Zip Code','required|trim');

			$this->form_validation->set_rules('phone_number','Phone Number','required|trim');

			$this->form_validation->set_rules('fax_number','Fax Number','required|trim');

			$this->form_validation->set_rules('status','Status','required|trim');

			$this->form_validation->set_rules('designation','Designation','required|trim');
	

		if ($this->form_validation->run() == FALSE) 

		{

			$result['errors'] = array();

			$result['hidden'] = array();

			$result['success'] = false;

			$fields = array('username', 'first_name', 'last_name', 'email','address', 'country', 'zipcode', 'phone_number', 'fax_number', 'status','designation');

			foreach ($fields as $field)

			{

			

				if (form_error($field)) 

				{

				$result['errors'][$field] = strip_tags(form_error($field));

				}

				else

				{

				$result['hidden'][] = $field;

				}

			}

		}

		else

		{

				
							
					$data = array( 'user_id'=>$this->input->post('user_id'),

					
								'admin_id' => $this->input->post('admin_id'),
					

								  'creator'   => $this->input->post('creator'),
								  
						
								 'firstname'=>$this->input->post('first_name'),



							    'lastname'=>$this->input->post('last_name'),

							   

							    'username'=>$this->input->post('username'), 



							   'Email'=>$this->input->post('email'),



							   

							   'city'=>$this->input->post('city'),

							   

							   'country'=>$this->input->post('country'),

							   

							   'zipcode'=>$this->input->post('zipcode'),



							   'address'=>$this->input->post('address'),



							   'phonenum'=>$this->input->post('phone_number'),

							   

							   'faxnum'=>$this->input->post('fax_number'),



							   'status'=>$this->input->post('status'),

							   

							   'designation'=>$this->input->post('designation'),

							   

							   'role'=>$this->input->post('role'),



							   'Password'=>base64_encode($this->input->post('password'))



							);
						
						

									

				$this->model_users->update_user($data);

				$result['success'] = 1;

		}

		  echo json_encode($result);



	}



	function users_delete($id){





		$result= $this->model_users->delete_user($id);

		

		echo "success";

		

	}

	

	function add_user()

	{
				/*echo '<pre>';
				print_r($_SESSION);
				echo '</pre>';*/ 
		$this->data['country'] = $this->model_users->get_country();

		

		$this->load->view('users/add_user',$this->data);

	}

	

	function add_user_validation()

	{

			$this->form_validation->set_rules('first_name', 'First Name', 'trim|required|max_length[255]');



			$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required|max_length[255]');



			$this->form_validation->set_rules('username', 'User Name', 'trim|required|max_length[255]');

			

			$this->form_validation->set_rules('password', 'Password', 'required');

			

			$this->form_validation->set_rules('email', 'Email', 'required|valid_email');

			

			$this->form_validation->set_rules('address', 'Address', 'required');

			

			$this->form_validation->set_rules('zipcode', 'Zip Code', 'trim|required|max_length[255]');



			$this->form_validation->set_rules('phone_number', 'Phone Number', 'trim|required|max_length[255]');



			$this->form_validation->set_rules('fax_number', 'Fax Number', 'trim|required|max_length[25]');

			

			$this->form_validation->set_rules('city', 'City', 'trim|required|max_length[225]');

			

			$this->form_validation->set_rules('status', 'Status', 'required');

			

			$this->form_validation->set_rules('designation', 'Designation', 'required');



			$this->form_validation->set_rules('role', 'Role', 'required');

			if ($this->form_validation->run())

			{

				if($this->session->userdata('role')=='admin')
				{
					
				$data = array('admin_id' => $this->session->userdata('login_id'),

				

								'firstname'=>$this->input->post('first_name'),



							   'lastname'=>$this->input->post('last_name'),

							   

							   'username'=>$this->input->post('username'), 



							   'Email'=>$this->input->post('email'),



							  // 'state'=>$this->input->post('state_code'),

							   

							   'city'=>$this->input->post('city'),

							   

							   'country'=>$this->input->post('country'),

							   

							   'zipcode'=>$this->input->post('zipcode'),



							   'address'=>$this->input->post('address'),



							   'phonenum'=>$this->input->post('phone_number'),

							   

							   'faxnum'=>$this->input->post('fax_number'),



							   'status'=>$this->input->post('status'),

							   

							   'designation'=>$this->input->post('designation'),

							   

							   'role'=>$this->input->post('role'),



							   'Password'=>base64_encode($this->input->post('password'))



							);

						}	
						
						else
						{
						
						
						
							
					$data = array('admin_id' => $this->session->userdata('super_admin'),

								'creator'   => $this->session->userdata('login_id'),
						
								'firstname'=>$this->input->post('first_name'),



							    'lastname'=>$this->input->post('last_name'),

							   

							    'username'=>$this->input->post('username'), 



							   'Email'=>$this->input->post('email'),



							  // 'state'=>$this->input->post('state_code'),

							   

							   'city'=>$this->input->post('city'),

							   

							   'country'=>$this->input->post('country'),

							   

							   'zipcode'=>$this->input->post('zipcode'),



							   'address'=>$this->input->post('address'),



							   'phonenum'=>$this->input->post('phone_number'),

							   

							   'faxnum'=>$this->input->post('fax_number'),



							   'status'=>$this->input->post('status'),

							   

							   'designation'=>$this->input->post('designation'),

							   

							   'role'=>$this->input->post('role'),



							   'Password'=>base64_encode($this->input->post('password'))



							);
						
						}

							

							

							$result_user = $this->model_users->add_user($data);

							

							/*$email_user= $this->input->post('email'); 

				

							$this->load->library('email');



							$this->email->from($this->config->item('email_noreply_address'), $this->config->item('email_noreply_name'));

					

							$this->email->to($email_user);

		

							$this->email->subject('Your account has been created');

			

		$msg = "Congratulations! Your account has been created Please Login using this information: <br /> Email : ".$email_user."<br />Password :".$this->input->post('password')."<br />";

		

		

							$this->email->message($msg);

			

							$this->email->send();*/

				

							$result['success'] = 1;



							$this->session->set_flashdata('success_message', 'User added successfuly.');

				}

				else

				{

				$result['errors'] = array();



				$result['hidden'] = array();

				

				$result['success'] = false;

	   

				 $fields = array('first_name', 'last_name', 'username', 'email', 'address', 'phone_number', 'fax_number', 'city', 'status','designation','role');

				 

				foreach ($fields as $field) {

	

				if (form_error($field)) 

				{

				 $result['errors'][$field] = strip_tags(form_error($field));

				}

				else

				{

				 $result['hidden'][] = $field;

				}



            }

		}



       echo json_encode($result);



	

	}

	

	function get_state_by_country($id)

	{

		$id = str_replace('-',' ',$id);

   		header('Content-Type: application/x-json; charset=utf-8');

		echo(json_encode($this->model_users->get_state_by_country($id)));

	}

	

	function logout()

	{

	

		$this->session->unset_userdata(array('login_id'=>'','login_username'=>''));



		$this->session->set_flashdata('message', 'You have successfully logged out.</div>');



		redirect($this->config->item('base_url'),'Location');



	}

	

	

	

	

}?>