<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Equipment extends CI_Controller {


  function __construct()

   {

		parent::__construct();

		// Your own constructor code

		if($this->session->userdata('login_id')=='')

		{

			redirect($this->config->item('base_url'),'Location');

		}

	
		$this->load->model('model_email');

		$this->load->model('model_equipment');

		$this->data = array();		

   }

	function index()

	{		

		$this->data['meta'] = getMetaContent('Admin- Equipment Management','title+description+keywords');

		$this->data['body'] = 'users_management';

		$this->load->view('structure', $this->data);

	}


	function get_equipments()

	{	

		$where= '';

		
		if($this->input->post('equipment_name')!='')


			$where = "Where job_equipment.equipment_name  like '%".trim($this->input->post('equipment_name'))."%' "; 


		//echo $where;

		$per_page =15;

		if($this->uri->segment(3)=='')

			$start_point = 0;

		else

			$start_point = $this->uri->segment(3);

			$total = $this->model_equipment->count_equipment_rec($where);  //total post

			$this->data['equipment_list'] = $this->model_equipment->get_equipment_list($where,$per_page,$start_point);

			$config['base_url'] = $this->config->item('base_url').'equipment/get_equipments';

			$config['uri_segment'] = '3';

			$config['total_rows'] = $total;

			$config['per_page'] =   $per_page;

			$config['first_link'] = '';

			$config['last_link'] = '';

			$config['next_link'] = '<img src="../images/arrow_right_off.png" style="width: 15px;"/>';

			$config['prev_link'] = '<img src="../images/arrow_left_off.png" style="width: 15px;"/>';

			$config['full_tag_open']  = '<p>';

			$config['full_tag_close'] = '</p>';

			$this->pagination->initialize($config);	

			$this->data['pagination'] = $this->pagination->create_links();

			$this->load->view('equipment/index', $this->data);

	}

	function view_equipment()
	{		

		$this->data['equipment_data'] = $this->model_equipment->get_equipment($this->uri->segment(3));
		
		
		$this->load->view('equipment/view_equipment', $this->data);

	}

	function edit_equipment()

	{		
		$this->data['equipment_data'] = $this->model_equipment->get_equipment($this->uri->segment(3));

		$this->load->view('equipment/edit_equipment', $this->data);

	}

	function edit_equipment_validation()

	{		

		$this->form_validation->set_rules('equipment_name', 'Equipment Name','required');

		$this->form_validation->set_rules('equipment_description', 'Equipment Description','required');

		if ($this->form_validation->run() == FALSE) 

		{

			$result['errors'] = array();

			$result['hidden'] = array();

			$result['success'] = false;

			$fields = array('equipment_name', 'equipment_description');

			foreach ($fields as $field)

			{
				if (form_error($field)) 

				{

				$result['errors'][$field] = strip_tags(form_error($field));

				}

				else

				{

				$result['hidden'][] = $field;

				}

			}

		}

		else

		{
				if($this->session->userdata('role')=='admin')
				{
					
					$data = array('equipment_id' => $this->input->post('equipment_id'),
				
							  'equipment_name' => $this->input->post('equipment_name'),

							  'equipment_description'=> $this->input->post('equipment_description'),
							 
							  'status' =>  $this->input->post('status'),
							  
							  'admin_id' =>$this->session->userdata('login_id')
							  
							  );
									
				}
				else
				{
					$data = array('equipment_id' => $this->input->post('equipment_id'),
				
							  'equipment_name' => $this->input->post('equipment_name'),

							  'equipment_description'=> $this->input->post('equipment_description'),
							 
							  'status' =>  $this->input->post('status'),
							  
							  'admin_id' =>$this->session->userdata('super_admin'),
							  
							  'creator' =>$this->session->userdata('login_id'),
							  );
									
				}
			

				$this->model_equipment->update_equipment($data);

				$result['success'] = 1;

		}

		  echo json_encode($result);

	}


	function equipment_delete($id){

		$result= $this->model_equipment->delete_equipment($id);

		echo "success";

	}


	function add_equipment()
	{

		$this->load->view('equipment/add_equipment',$this->data);

	}

	

	function add_equipment_validation()

	{

		$this->form_validation->set_rules('equipment_name', 'Equipment Name','required');

		$this->form_validation->set_rules('equipment_description', 'Equipment Description','required');


			if ($this->form_validation->run())

			{
				if($this->session->userdata('role')=='admin')
				{
					$data = array('equipment_name' => $this->input->post('equipment_name'),

							  'equipment_description'=> $this->input->post('equipment_description'),
							 
							  'status' =>  $this->input->post('status'),
							  
							  'admin_id' =>$this->session->userdata('login_id')
							  
							  );
				}
				else
				{
					$data = array('equipment_name' => $this->input->post('equipment_name'),

							  'equipment_description'=> $this->input->post('equipment_description'),
							 
							  'status' =>  $this->input->post('status'),
							  
							  'admin_id' =>$this->session->userdata('super_admin'),
							  
							  'creator' =>$this->session->userdata('login_id')
							  
							  );
				
				}
				
				
							  
							   $result_equipment = $this->model_equipment->add_equipment($data);
							
							   $result['success'] = 1;

							$this->session->set_flashdata('success_message', 'Equipment added successfully.');

			}

				else

				{

				$result['errors'] = array();

				$result['hidden'] = array();

				$result['success'] = false;
	   
				$fields = array('equipment_name', 'equipment_description');
				 
				foreach ($fields as $field) {

				if (form_error($field)) 

				{

				 $result['errors'][$field] = strip_tags(form_error($field));

				}

				else

				{

				 $result['hidden'][] = $field;

				}

			}

		}

	   echo json_encode($result);

	}
	

}?>