<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');







class Home extends CI_Controller {







  function __construct()



   {



		parent::__construct();



		if($this->session->userdata('login_id')=='')



		{



			redirect($this->config->item('base_url'),'Location');



		}



		// Your own constructor code

		$this->load->library('googlemaps');

		$this->load->model('model_user');



		$this->load->model('model_email');



		$this->load->model('model_home');

		
		$this->load->model('model_jobs');
		
		$this->data = array();		



   }



	function index()
	{		
		
		if ($this->session->userdata('role')=='admin')
		{
			$this->data['not_started']= $this->model_home->not_started($this->session->userdata('login_id'));
		
		}
		else
		{
			$this->data['not_started']= $this->model_home->not_started($this->session->userdata('super_admin'));
		
		}
		
		
		
		if ($this->session->userdata('role')=='admin')
		{
			$this->data['in_progress_jobs']= $this->model_home->in_progress_jobs($this->session->userdata('login_id'));
		
		}
		else
		{
			$this->data['in_progress_jobs']= $this->model_home->in_progress_jobs($this->session->userdata('super_admin'));
		
		}
		
		if ($this->session->userdata('role')=='admin')
		{
		
			$this->data['completed_jobs']= $this->model_home->completed_jobs($this->session->userdata('login_id'));
		
		}
		else
		{
		
			$this->data['completed_jobs']= $this->model_home->completed_jobs($this->session->userdata('super_admin'));
		
		}
	
		
		if ($this->session->userdata('role')=='admin')
		{
	
			$this->data['total_jobs']= $this->model_home->total_jobs($this->session->userdata('login_id'));
		}
		else
		{
			
			$this->data['total_jobs']= $this->model_home->total_jobs($this->session->userdata('super_admin'));
		}
		
		if ($this->session->userdata('role')=='admin')
		{
			$this->data['over_due']= $this->model_home->over_due($this->session->userdata('login_id'));
		
		}
		else
		{
			$this->data['over_due']= $this->model_home->over_due($this->session->userdata('super_admin'));
		
		}
		
			$users  = $this->model_jobs->get_users(); 
		
		$jobs= $this->model_home->get_all_jobs();
		
		$config['center'] = '37.4419, -122.1419';
		$config['zoom'] = 'auto';
		$this->googlemaps->initialize($config);
		foreach($jobs as $job)
		{
		$marker = array();
		$marker['position'] = $job['lon'].','.- $job['lat']; //'37.429, -122.1519';
		$desc='';
		if(strlen($job['job_description'])>15){
		 	$desc= @substr($job['job_description'],0,15).'...';
		 }else{  $desc= @$job['job_description'];
		  }
		  $assigned='';
		 foreach($users as $user)
		 {
		 	if(@$user['user_id']==@$job['assigned_to'])
			{
				 $assigned=@$user['username'];
			}
		 } 
		 
		
			if(@$job['priority']=='low')
			{
			
				 if(@$job['status']=='1')
				{ 
				$marker['infowindow_content'] ="<div style='overflow: hidden;'>".$job['job_title']."<br>".$desc."<br>".$assigned."<br>".$job['assigned_by'].'<div id="job_priority_low" style="width:85px; margin-left: 174px;">Low</div><div id="job_status_1" style="width:85px; margin-left: 174px;">Not Started</div></div>';
				 }
				else if(@$job['status']=='2')
				{
					$marker['infowindow_content'] ="<div style='overflow: hidden;'>".$job['job_title']."<br>".$desc."<br>".$assigned."<br>".$job['assigned_by'].'<div id="job_priority_low" style="width:85px; margin-left: 174px;">Low</div><div id="job_status_2" style="width:85px; margin-left: 174px;">In Progress</div></div>';
				 }
				else if(@$job['status']=='3')
				{
					$marker['infowindow_content'] ="<div style='overflow: hidden;'>".$job['job_title']."<br>".$desc."<br>".$assigned."<br>".$job['assigned_by'].'<div id="job_priority_low" style="width:85px; margin-left: 174px;">Low</div><div id="job_status_3" style="width:85px; margin-left: 174px;">Completed</div></div>';
				 }
				else
				{
			
				$marker['infowindow_content'] ="<div style='overflow: hidden;'>".$job['job_title']."<br>".$desc."<br>".$assigned."<br>".$job['assigned_by'].'<div id="job_priority_low" style="width:85px; margin-left: 174px;">Low</div><div id="job_status_4" style="width:85px; margin-left: 174px;">Overdue</div></div>';
				 }
			
		
			}
			else if(@$job['priority']=='medium')
			{ 
				
			 if(@$job['status']=='1')
				{ 
				$marker['infowindow_content'] ="<div style='overflow: hidden;'>".$job['job_title']."<br>".$desc."<br>".$assigned."<br>".$job['assigned_by'].'<div id="job_priority_medium" style="width:85px; margin-left: 174px;">Medium</div><div id="job_status_1" style="width:85px; margin-left: 174px;">Not Started</div></div>';
				 }
				else if(@$job['status']=='2')
				{
					$marker['infowindow_content'] ="<div style='overflow: hidden;'>".$job['job_title']."<br>".$desc."<br>".$assigned."<br>".$job['assigned_by'].'<div id="job_priority_medium" style="width:85px; margin-left: 174px;">Medium</div><div id="job_status_2" style="width:85px; margin-left: 174px;">In Progress</div></div>';
				 }
				else if(@$job['status']=='3')
				{
					$marker['infowindow_content'] ="<div style='overflow: hidden;'>".$job['job_title']."<br>".$desc."<br>".$assigned."<br>".$job['assigned_by'].'<div id="job_priority_medium" style="width:85px; margin-left: 174px;">Medium</div><div id="job_status_3" style="width:85px; margin-left: 174px;">Completed</div></div>';
				 }
				else
				{
			
				$marker['infowindow_content'] ="<div style='overflow: hidden;'>".$job['job_title']."<br>".$desc."<br>".$assigned."<br>".$job['assigned_by'].'<div id="job_priority_medium" style="width:85px; margin-left: 174px;">Medium</div><div id="job_status_4" style="width:85px; margin-left: 174px;">Overdue</div></div>';
				 }	
				
	
			}
			else
			{
			 if(@$job['status']=='1')
				{ 
				$marker['infowindow_content'] ="<div style='overflow: hidden;'>".$job['job_title']."<br>".$desc."<br>".$assigned."<br>".$job['assigned_by'].'<div id="job_priority_high" style="width:85px; margin-left: 174px;">High</div><div id="job_status_1" style="width:85px; margin-left: 174px;">Not Started</div></div>';
				 }
				else if(@$job['status']=='2')
				{
					$marker['infowindow_content'] ="<div style='overflow: hidden;'>".$job['job_title']."<br>".$desc."<br>".$assigned."<br>".$job['assigned_by'].'<div id="job_priority_high" style="width:85px; margin-left: 174px;">High</div><div id="job_status_2" style="width:85px; margin-left: 174px;">In Progress</div></div>';
				 }
				else if(@$job['status']=='3')
				{
					$marker['infowindow_content'] ="<div style='overflow: hidden;'>".$job['job_title']."<br>".$desc."<br>".$assigned."<br>".$job['assigned_by'].'<div id="job_priority_high" style="width:85px; margin-left: 174px;">High</div><div id="job_status_3" style="width:85px; margin-left: 174px;">Completed</div></div>';
				 }
				else
				{
			
				$marker['infowindow_content'] ="<div style='overflow: hidden;'>".$job['job_title']."<br>".$desc."<br>".$assigned."<br>".$job['assigned_by'].'<div id="job_priority_high" style="width:85px; margin-left: 174px;">High</div><div id="job_status_4" style="width:85px; margin-left: 174px;">Overdue</div></div>';
				 }	
		
			}
		
	
		$marker['icon'] = 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=A|008000|FCF9F9';
		$this->googlemaps->add_marker($marker);
		}
		
		
		$this->data['map'] = $this->googlemaps->create_map();
		//print_r($this->data['map']); exit;
		
		$this->data['meta'] = getMetaContent('Admin- Home','title+description+keywords');
		
		$this->data['body'] = 'home';

		$this->load->view('structure', $this->data);

	}

	function map_heartbeat()
	{
		
		
		$users  = $this->model_jobs->get_users(); 
		
		$jobs= $this->model_home->get_all_jobs();
		
		$config['center'] = '37.4419, -122.1419';
		$config['zoom'] = 'auto';
		$this->googlemaps->initialize($config);
		foreach($jobs as $job)
		{
		$marker = array();
		$marker['position'] = $job['lon'].','.- $job['lat']; //'37.429, -122.1519';
		$desc='';
		if(strlen($job['job_description'])>15){
		 	$desc= @substr($job['job_description'],0,15).'...';
		 }else{  $desc= @$job['job_description'];
		  }
		  $assigned='';
		 foreach($users as $user)
		 {
		 	if(@$user['user_id']==@$job['assigned_to'])
			{
				 $assigned=@$user['username'];
			}
		 } 
		 
		
			if(@$job['priority']=='low')
			{
			
				 if(@$job['status']=='1')
				{ 
				$marker['infowindow_content'] ="<div style='overflow: hidden;'>".$job['job_title']."<br>".$desc."<br>".$assigned."<br>".$job['assigned_by'].'<div id="job_priority_low" style="width:85px; margin-left: 174px;">Low</div><div id="job_status_1" style="width:85px; margin-left: 174px;">Not Started</div></div>';
				 }
				else if(@$job['status']=='2')
				{
					$marker['infowindow_content'] ="<div style='overflow: hidden;'>".$job['job_title']."<br>".$desc."<br>".$assigned."<br>".$job['assigned_by'].'<div id="job_priority_low" style="width:85px; margin-left: 174px;">Low</div><div id="job_status_2" style="width:85px; margin-left: 174px;">In Progress</div></div>';
				 }
				else if(@$job['status']=='3')
				{
					$marker['infowindow_content'] ="<div style='overflow: hidden;'>".$job['job_title']."<br>".$desc."<br>".$assigned."<br>".$job['assigned_by'].'<div id="job_priority_low" style="width:85px; margin-left: 174px;">Low</div><div id="job_status_3" style="width:85px; margin-left: 174px;">Completed</div></div>';
				 }
				else
				{
			
				$marker['infowindow_content'] ="<div style='overflow: hidden;'>".$job['job_title']."<br>".$desc."<br>".$assigned."<br>".$job['assigned_by'].'<div id="job_priority_low" style="width:85px; margin-left: 174px;">Low</div><div id="job_status_4" style="width:85px; margin-left: 174px;">Overdue</div></div>';
				 }
		
			}
			else if(@$job['priority']=='medium')
			{ 
				
			 if(@$job['status']=='1')
				{ 
				$marker['infowindow_content'] ="<div style='overflow: hidden;'>".$job['job_title']."<br>".$desc."<br>".$assigned."<br>".$job['assigned_by'].'<div id="job_priority_medium" style="width:85px; margin-left: 174px;">Medium</div><div id="job_status_1" style="width:85px; margin-left: 174px;">Not Started</div></div>';
				 }
				else if(@$job['status']=='2')
				{
					$marker['infowindow_content'] ="<div style='overflow: hidden;'>".$job['job_title']."<br>".$desc."<br>".$assigned."<br>".$job['assigned_by'].'<div id="job_priority_medium" style="width:85px; margin-left: 174px;">Medium</div><div id="job_status_2" style="width:85px; margin-left: 174px;">In Progress</div></div>';
				 }
				else if(@$job['status']=='3')
				{
					$marker['infowindow_content'] ="<div style='overflow: hidden;'>".$job['job_title']."<br>".$desc."<br>".$assigned."<br>".$job['assigned_by'].'<div id="job_priority_medium" style="width:85px; margin-left: 174px;">Medium</div><div id="job_status_3" style="width:85px; margin-left: 174px;">Completed</div></div>';
				 }
				else
				{
			
				$marker['infowindow_content'] ="<div style='overflow: hidden;'>".$job['job_title']."<br>".$desc."<br>".$assigned."<br>".$job['assigned_by'].'<div id="job_priority_medium" style="width:85px; margin-left: 174px;">Medium</div><div id="job_status_4" style="width:85px; margin-left: 174px;">Overdue</div></div>';
				 }	
				
			}
			else
			{
			 if(@$job['status']=='1')
				{ 
				$marker['infowindow_content'] ="<div style='overflow: hidden;'>".$job['job_title']."<br>".$desc."<br>".$assigned."<br>".$job['assigned_by'].'<div id="job_priority_high" style="width:85px; margin-left: 174px;">High</div><div id="job_status_1" style="width:85px; margin-left: 174px;">Not Started</div></div>';
				 }
				else if(@$job['status']=='2')
				{
					$marker['infowindow_content'] ="<div style='overflow: hidden;'>".$job['job_title']."<br>".$desc."<br>".$assigned."<br>".$job['assigned_by'].'<div id="job_priority_high" style="width:85px; margin-left: 174px;">High</div><div id="job_status_2" style="width:85px; margin-left: 174px;">In Progress</div></div>';
				 }
				else if(@$job['status']=='3')
				{
					$marker['infowindow_content'] ="<div style='overflow: hidden;'>".$job['job_title']."<br>".$desc."<br>".$assigned."<br>".$job['assigned_by'].'<div id="job_priority_high" style="width:85px; margin-left: 174px;">High</div><div id="job_status_3" style="width:85px; margin-left: 174px;">Completed</div></div>';
				 }
				else
				{
			
				$marker['infowindow_content'] ="<div style='overflow: hidden;'>".$job['job_title']."<br>".$desc."<br>".$assigned."<br>".$job['assigned_by'].'<div id="job_priority_high" style="width:85px; margin-left: 174px;">High</div><div id="job_status_4" style="width:85px; margin-left: 174px;">Overdue</div></div>';
				 }	
		
			}
		
	
		$marker['icon'] = 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=A|008000|FCF9F9';
		
		$this->googlemaps->add_marker($marker);
		}
		
		
		$this->data['map'] = $this->googlemaps->create_map();
		//print_r($this->data['map']); exit;
		$this->load->view('map', $this->data);
	}	

	function welcome()

	{		


		$this->data['meta'] = getMetaContent('Admin- Home','title+description+keywords');

		
		$this->load->view('home/index');

	}



	function editprofile()

	{
	

		$this->data['profile_data'] = $this->model_home->get_user($this->session->userdata('login_id'), $this->session->userdata('role'))->result();


		$this->load->view('home/editprofile', $this->data);



	}



	function updateprofile()



	{



		$this->form_validation->set_rules('first_name', 'First Name', 'trim|required');



		$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');



		$this->form_validation->set_rules('Email', 'Email Address', 'trim|required|valid_email|unique[ceteksa_users.email]');

		

		$this->form_validation->set_rules('designation', 'Designation', 'trim|required');



		if($this->form_validation->run($this) == true)



		{



			if($this->input->post('Password')!=''){

				

				$data = array('firstname'=>$this->input->post('first_name'),



							   'lastname'=>$this->input->post('last_name'),



							   'Email'=>$this->input->post('Email'),

							   

							   'designation'=>$this->input->post('designation'),



							   'status'=>$this->input->post('IsActive'),



							   'Password'=>base64_encode($this->input->post('password'))



							);



			}



			else{



				$data = array('firstname'=>$this->input->post('first_name'),



						   'lastname'=>$this->input->post('last_name'),



						   'Email'=>$this->input->post('Email'),

						   

						   'designation'=>$this->input->post('designation'),



						   'status'=>$this->input->post('IsActive')



						);

						

						

				



			}



			$this->data['profile_data'] = $this->model_home->update_users($data,$this->input->post('user_id'), $this->input->post('role'));

			

			

			$this->session->set_flashdata('success_message', '<div id="message">Profile Updated Sucessfully</div>'); 



			$rerutn_data = array('success'=>1);



		}



		else{



				$rerutn_data['errors'] = array();



				$rerutn_data['hidden'] = array();



				$rerutn_data['success'] = false;



				$fields = array('Email,first_name,last_name');



				foreach ($fields as $field)



				{



				



					if (form_error($field)) 



					{



						$rerutn_data['errors'][$field] = strip_tags(form_error($field));



					}



					else



					{



						$rerutn_data['hidden'][] = $field;



					}



				}



		}



		echo json_encode($rerutn_data);



	}



	function changepassword()



	{



		$this->data['profile_data'] = $this->model_home->get_user($this->session->userdata('login_id'), $this->session->userdata('role'))->result();



		//$this->data['body'] = ;



		$this->load->view('home/changepassword', $this->data);



	}



	function update_password()

	{

		if($this->input->post('password')!=''){



			$data = array( 'Password'=>base64_encode($this->input->post('password'))



						);



		$this->data['profile_data'] = $this->model_home->update_users($data,$this->input->post('user_id'), $this->session->userdata('role'));



		$this->session->set_flashdata('success_message', '<div id="message">Password Updated Sucessfully</div>'); 



		}



		else{



			$this->session->set_flashdata('success_message', '<div id="message">Password Updated Sucessfully</div>'); 



		}



		$rerutn_data = array('success'=>1);



		echo json_encode($rerutn_data);



	}



	



	



}?>