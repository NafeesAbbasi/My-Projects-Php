<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Welcome extends CI_Controller {



	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -  

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in 

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see http://codeigniter.com/user_guide/general/urls.html

	 */

	private $_tgroups = "groups";

	private $_tusers = "users";

	private $_tmembers = "members";

	private $_usergroups = "user_group";

	private $data;

	function __construct()

    {

		parent::__construct();

		// Your own constructor code

		$this->load->model('model_user');

		$this->data['title'] = "Limoverse Admin";

		$this->data['meta_desc'] = "Limoverse Admin";

		$this->data['meta_keywords'] = "Limoverse Admin";	

		$this->data['page_title'] = "Limoverse Admin";

		$this->data['author'] = "Limoverse Developer";								//load member model

    }

	public function index()

	{

		if($this->session->userdata('login_id')!='')

		{

			redirect($this->config->item('base_url').'home');

		}

		else

		{

			$this->load->view('login',$this->data);

		}

	}

}



/* End of file welcome.php */

/* Location: ./application/controllers/welcome.php */