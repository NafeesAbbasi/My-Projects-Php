<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account_Settings extends CI_Controller {

  function __construct()

   {

		parent::__construct();



		// Your own constructor code



		if($this->session->userdata('login_id')=='')

		{

			redirect($this->config->item('base_url'),'Location');

		}
		
		$this->load->model('model_home');

		$this->load->model('model_user');

		$this->load->model('model_email');

		$this->load->model('model_users');

		$this->data = array();		

   }

	public function index()



	{
		$this->data['meta'] = getMetaContent('Admin- Account Settings','title+description+keywords');



		$this->data['body'] = 'account_settings';



		$this->load->view('structure', $this->data);
		
	}
}