<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jobs extends CI_Controller {


  function __construct()

   {

		parent::__construct();

		// Your own constructor code

		if($this->session->userdata('login_id')=='')

		{

			redirect($this->config->item('base_url'),'Location');

		}

	
		$this->load->model('model_email');

		$this->load->model('model_jobs');

		$this->data = array();		

   }

	function index($year = null, $month = null)

	{		
		$where= '';

		$this->data['meta'] = getMetaContent('Admin- Jobs Management','title+description+keywords');

		$this->data['body'] = 'jobs_manage';
		
		$per_page =20;

		if($this->uri->segment(3)=='')

			$start_point = 0;

		else

			$start_point = $this->uri->segment(3);

			$total = $this->model_jobs->count_jobs_rec($where);  //total post

			$this->data['jobs_list'] = $this->model_jobs->get_jobs_list($where);
			
			$this->data['equipments']  = $this->model_jobs->get_equipments(); 
			
			$this->data['users']  = $this->model_jobs->get_users(); 
			
			$config['base_url'] = $this->config->item('base_url').'jobs/index';

			$config['uri_segment'] = '3';

			$config['total_rows'] = $total;

			$config['per_page'] =   $per_page;

			$config['first_link'] = '';

			$config['last_link'] = '';

			$config['next_link'] = '<img src="../images/arrow_right_off.png" style="width: 15px;"/>';

			$config['prev_link'] = '<img src="../images/arrow_left_off.png" style="width: 15px;"/>';

			$config['full_tag_open']  = '<p>';

			$config['full_tag_close'] = '</p>';

			$this->pagination->initialize($config);	

			$this->data['pagination'] = $this->pagination->create_links();
			
			$this->load->view('structure', $this->data);

	}
	
	function side_bar_sorting($id)
	{
		
		$this->data['jobs_list'] = $this->model_jobs->get_jobs_list_sorted($id);
		
		$checker = explode('-',$id);
		
		//sorting By
		$column = $checker[0];
		//sort Type
		$control = $checker[1];
		
		//Creating Sortted Array
		
		$count_results = count($this->data['jobs_list']);
		
		//$resulting_array = $this->sort_array_of_array($this->data['jobs_list'], 'date', $control);
		
		
		//print_r($resulting_array);exit;
		
		/*for($i = 0; $i<$count_results; $i++)
		{
			if($column == 'date')
			{
				
			}
		}*/
		
		
		
		$this->data['equipments']  = $this->model_jobs->get_equipments(); 
			
		$this->data['users']  = $this->model_jobs->get_users(); 
		
		
		$this->load->view('jobs/jobs_listing',$this->data);
	}
	
	//Get Ajaxed Calender
	
	public function getCalender($target)
	{
		$resutling = explode('-',$target);
		
		$this->data['month'] 	= $resutling[0];
		$this->data['year'] 	= $resutling[1];
	
		$this->load->view('jobs/ajax_calender',$this->data);
	
	}
	
	


	function get_jobs()

	{	

		$where= '';

		
		/*if($this->input->post('equipment_name')!='')


			$where = "Where job_equipment.equipment_name  like '%".trim($this->input->post('equipment_name'))."%' "; */


		//echo $where;

		$per_page =15;

		if($this->uri->segment(3)=='')

			$start_point = 0;

		else

			$start_point = $this->uri->segment(3);

			$total = $this->model_jobs->count_jobs_rec($where);  //total post

			$this->data['jobs_list'] = $this->model_jobs->get_jobs_list($where,$per_page,$start_point);
			echo '<pre>';
			print_r($this->data['jobs_list']);//die();
			
			$this->data['equipments']  = $this->model_jobs->get_equipments(); 
			
			$this->data['users']  = $this->model_jobs->get_users(); 
			
			$config['base_url'] = $this->config->item('base_url').'jobs/get_jobs';

			$config['uri_segment'] = '3';

			$config['total_rows'] = $total;

			$config['per_page'] =   $per_page;

			$config['first_link'] = '';

			$config['last_link'] = '';

			$config['next_link'] = '<img src="../images/arrow_right_off.png" style="width: 15px;"/>';

			$config['prev_link'] = '<img src="../images/arrow_left_off.png" style="width: 15px;"/>';

			$config['full_tag_open']  = '<p>';

			$config['full_tag_close'] = '</p>';

			$this->pagination->initialize($config);	

			$this->data['pagination'] = $this->pagination->create_links();

			$this->load->view('jobs/index', $this->data);

	}
	
	function get_jobs_sidebar()

	{	

			
		$where= '';

		$this->data['meta'] = getMetaContent('Admin- Jobs Management','title+description+keywords');

		$this->data['body'] = 'jobs_manage';
		
		$per_page =20;

		if($this->uri->segment(3)=='')

			$start_point = 0;

		else

			$start_point = $this->uri->segment(3);

			$total = $this->model_jobs->count_jobs_rec($where);  //total post

			$this->data['jobs_list'] = $this->model_jobs->get_jobs_list($where);
			
			$this->data['equipments']  = $this->model_jobs->get_equipments(); 
			
			$this->data['users']  = $this->model_jobs->get_users(); 
			
			$config['base_url'] = $this->config->item('base_url').'jobs/index';

			$config['uri_segment'] = '3';

			$config['total_rows'] = $total;

			$config['per_page'] =   $per_page;

			$config['first_link'] = '';

			$config['last_link'] = '';

			$config['next_link'] = '<img src="../images/arrow_right_off.png" style="width: 15px;"/>';

			$config['prev_link'] = '<img src="../images/arrow_left_off.png" style="width: 15px;"/>';

			$config['full_tag_open']  = '<p>';

			$config['full_tag_close'] = '</p>';

			$this->pagination->initialize($config);	

			$this->data['pagination'] = $this->pagination->create_links();
			

			$this->load->view('jobs/jobs_listing', $this->data);

	}

	function view_jobs()
	{		

		$this->data['job_data'] = $this->model_jobs->get_job($this->uri->segment(3));
		
		$this->data['equipments']  = $this->model_jobs->get_equipments(); 
			
		$this->data['users']  = $this->model_jobs->get_users(); 
		
		$this->load->view('jobs/view_job', $this->data);

	}

	function edit_jobs()

	{		
		$this->data['job_data'] = $this->model_jobs->get_job($this->uri->segment(3));
		
		$this->data['equipments']  = $this->model_jobs->get_equipments(); 
			
		$this->data['users']  = $this->model_jobs->get_users(); 

		$this->load->view('jobs/edit_job', $this->data);

	}
	
	//Dragable Job Item On Calender Dorped In Another Data Update The Job Date Here
	
	public function savejoblocation($job_id,$date_choosen)
	{
		$date_sel = str_replace('-','/',$date_choosen);
		
		//Update The Job Info
		$this->model_jobs->UpdateJobById($job_id, $date_sel);
		
		
	}

	function edit_job_validation()

	{		

		$this->form_validation->set_rules('job_title', 'Job Title','required');

		$this->form_validation->set_rules('date', 'Date','required');
		
	/*	$this->form_validation->set_rules('assigned_to', 'Assigned To','required');*/

		$this->form_validation->set_rules('status', 'Status','required');
		
		$this->form_validation->set_rules('priority', 'Priority','required');

		$this->form_validation->set_rules('equipment', 'Equipment','required');
		
		$this->form_validation->set_rules('job_desc', 'Job Description','required');

		if ($this->form_validation->run() == FALSE) 

		{

			$result['errors'] = array();

			$result['hidden'] = array();

			$result['success'] = false;

			$fields = array('job_title', 'date','status','priority','equipment','job_desc');

			foreach ($fields as $field)

			{
				if (form_error($field)) 

				{

				$result['errors'][$field] = strip_tags(form_error($field));

				}

				else

				{

				$result['hidden'][] = $field;

				}

			}

		}

		else

		{

				$data = array('job_id' =>$this->input->post('job_id'),
				
							  'job_title' => $this->input->post('job_title'),

							  'date'=> $this->input->post('date'),
							 
							  'assigned_to' =>  $this->input->post('assigned_to'),
							  
							  'assigned_by' => $this->input->post('assigned_by'),

							  'status'=> $this->input->post('status'),
							 
							  'priority' =>  $this->input->post('priority'),
							  
							  'equipment_id' =>  $this->input->post('equipment'),
							  
							  'job_description' =>  $this->input->post('job_desc'),
							  
							  'admin_id' =>$this->session->userdata('login_id')
							  
							  );
									

				$this->model_jobs->update_job($data);

				$result['success'] = 1;

		}

		  echo json_encode($result);

	}


	function jobs_delete($id){

		$result= $this->model_jobs->delete_job($id);

		echo "success";

	}


	function add_job_validation()

	{

		$this->form_validation->set_rules('job_title', 'Job Title','required');

		$this->form_validation->set_rules('date', 'Date','required');
		
	/*	$this->form_validation->set_rules('assigned_to', 'Assigned To','required');*/

		$this->form_validation->set_rules('status', 'Status','required');
		
		$this->form_validation->set_rules('priority', 'Priority','required');

		$this->form_validation->set_rules('equipment', 'Equipment','required');
		
		$this->form_validation->set_rules('job_desc', 'Job Description','required');



			if ($this->form_validation->run())

			{
				$dater = explode('/',$this->input->post('date'));
				
				$string_value = @$dater[1]."/".@$dater[0]."/".@$dater[2];
				
				$time_value  = strtotime($string_value);
				
				$converted_date = date('Y-m-d',$time_value);
					
					
				if ($this->session->userdata('role')=='admin')
				{
				
				$data = array('job_title' => $this->input->post('job_title'),

							  'date'=> $this->input->post('date'),
							  
							  'time_date'=> $converted_date,
							 
							  'assigned_to' =>  $this->input->post('assigned_to'),
							  
							  'assigned_by' => $this->input->post('assigned_by'),

							  'status'=> $this->input->post('status'),
							 
							  'priority' =>  $this->input->post('priority'),
							  
							  'equipment_id' =>  $this->input->post('equipment'),
							  
							  'job_description' =>  $this->input->post('job_desc'),
							  
							  'admin_id' =>$this->session->userdata('login_id')
							  
							  );
					}
					else
					{
					//convet date
					
					
					
					$data =	array('job_title' => $this->input->post('job_title'),

							  'date'=> $this->input->post('date'),
							  
							  'time_date'=> $converted_date,
							 
							  'assigned_to' =>  $this->input->post('assigned_to'),
							  
							  'assigned_by' => $this->input->post('assigned_by'),

							  'status'=> $this->input->post('status'),
							 
							  'priority' =>  $this->input->post('priority'),
							  
							  'equipment_id' =>  $this->input->post('equipment'),
							  
							  'job_description' =>  $this->input->post('job_desc'),
							  
							  'admin_id' =>$this->session->userdata('super_admin'),
							  
							  'creator' =>$this->session->userdata('login_id')
							  
							  );
					
					}
							   $result_job = $this->model_jobs->add_job($data);
							
							   $result['success'] = 1;

							   $this->session->set_flashdata('success_message', 'Job added successfully.');

			}

				else

				{

				$result['errors'] = array();

				$result['hidden'] = array();

				$result['success'] = false;
	   
				$fields = array('job_title', 'date','status','priority','equipment','job_desc');
				 
				foreach ($fields as $field) {

				if (form_error($field)) 

				{

				 $result['errors'][$field] = strip_tags(form_error($field));

				}

				else

				{

				 $result['hidden'][] = $field;

				}

			}

		}

	   echo json_encode($result);

	}
	
	function filter_by_date($fromdate=NULL, $todate=NULL)
	{
		$fromdate= str_replace('-','/',$fromdate);
		$todate= str_replace('-','/',$todate);
		
		
		$this->data['jobs_list']  = $this->model_jobs->filter_by_date($fromdate, $todate); 
				
		$this->data['equipments']  = $this->model_jobs->get_equipments(); 
			
		$this->data['users']  = $this->model_jobs->get_users(); 
		
		if($this->data['jobs_list']!=NULL){
		
		$this->load->view('jobs/jobs_listing',$this->data);
		}
		
	}
	
	function filter_by_status($status=NULL)
	{
		
		$this->data['jobs_list']  = $this->model_jobs->filter_by_status($status); 
		
		$this->data['equipments']  = $this->model_jobs->get_equipments(); 
			
		$this->data['users']  = $this->model_jobs->get_users(); 
		
		$this->load->view('jobs/jobs_listing',$this->data);
	}
	
	
	function filter_by_priority($priority=NULL)
	{
		
		$this->data['jobs_list']  = $this->model_jobs->filter_by_priority($priority); 
		
		$this->data['equipments']  = $this->model_jobs->get_equipments(); 
			
		$this->data['users']  = $this->model_jobs->get_users(); 
		
		$this->load->view('jobs/jobs_listing',$this->data);
	}
	
	//Return Sorted Array
	
	/*function sort_array_of_array(&$array, $subfield, $cont)
	{
		$sortarray = array();
		foreach ($array as $key => $row)
		{
		
			//var_dump($row);exit;
			
			$string_value = $row[$subfield];
			
			$dater = explode('/',$string_value);
			
			$string_value = $dater[1]."/".$dater[0]."/".$dater[2];
			
			//var_dump($string_value);
			
			$time_value  = strtotime($string_value);
			
			//var_dump($time_value);
			
			$row[$subfield] = $time_value;
			
			var_dump($time_value);
			
			$sortarray[$key] = $row;
		}
	
		array_multisort($sortarray, SORT_ASC, $array);
		return $sortarray;
	}*/
	
	/*function updatetimedate()
	{
		$all_jobs= $this->model_jobs->get_alljobs();
	
	}*/
	

}?>