<div>

<p style="font-weight:normal; font-family:Arial,Helvetica,sans-serif; font-size:27px; color:#666666">Edit Equipment</p>

</div>

<div>

<form id="add_form_cust" class="add_form">

<input name="equipment_id" id="equipment_id"  value="<?php if(isset($equipment_data['equipment_id'])&& $equipment_data['equipment_id']!=''){ echo $equipment_data['equipment_id'];}?>" type="hidden" />

<input name="equipment_name" id="equipment_name" class="input-block-level" value="<?php if(isset($equipment_data['equipment_name'])&& $equipment_data['equipment_name']!=''){ echo $equipment_data['equipment_name'];}?>" type="text" style="width:300px;margin-right:300px;" placeholder="Equipment Name">


<textarea id="equipment_description" name="equipment_description" placeholder="Description" style="width: 286px;"> <?php if(isset($equipment_data['equipment_description'])&& $equipment_data['equipment_description']!=''){ echo $equipment_data['equipment_description'];}?></textarea>

<br />
<select name="status" id="status">

	<option value="">Select Status</option>

	<option value="1" <?php if(isset($equipment_data['status'])&& $equipment_data['status']=='1'){?> selected="selected"<?php }?>>Active</option>

	<option value="0" <?php if(isset($equipment_data['status'])&& $equipment_data['status']=='0'){?> selected="selected"<?php }?>>Inactive</option>

</select><br />

<button class="btn btn-large btn-primary" type="" onclick="update_equipment();return false;">Update &raquo;</button>

<button class="btn btn-large btn-primary" type="" onclick="GetEquipmentPage();return false;">Cancel</button>

</form>

</div>