
          <ul class="breadcrumb large"><li><a href="<?=base_url()?>equipment/">Equipment Management</a> <span class="divider">&raquo;</span></li> <li><a href="javascript:void(0);">List Equipments</a><span class="divider">&raquo;</span></li></ul> 

		  <?php

			if(validation_errors()){

			echo '<div class="warning" style="width:500px;">'.validation_errors().'</div>';

			}

			if(!empty($msg))

			{?>

			<div class="success"><?php echo $msg;?></div>

			<?php	}

			if($this->session->flashdata('error_message'))

			echo '<div class="warning">'.@$this->session->flashdata('error_message').'</div>';

			if($this->session->flashdata('success_message'))

			echo '<div class="success">'.@$this->session->flashdata('success_message').'</div>';

			$this->load->model('model_users');

			

			?>   
  <th colspan="12">

<a id="add_equipment" class="btn btn-primary pull-right" href="javascript:void(0);" style="margin-bottom: 10px;">Add New Equipment &raquo;</a>

					</th>
				
            <table class="table table-striped table-bordered table-condensed">

                <thead>

                  <tr>

				<tr>
				

                    <th colspan="12">

						<form name="search_operator_orders" id="search_operator_orders" action="" method="post">

						<input type="text" name="equipment_name" id="equipment_name" placeholder="Equipment Name" value="<?=@$this->input->post('equipment_name');?>" />

						<input type="button" class="btn" style="margin-top:-10px" onclick="search_equipment();return false;" value="Search">

						</form>

					</th>

                  </tr>

				  <tr style="background:#F5F5F5; color:#0088CC">

                    <th>#</th>
					
					

                    <th>Equipment Name</th>
  
					<th>Status</th>

                    <th colspan="5">Actions</th>

                  </tr>

				  <?php
				 
				   if(isset($equipment_list) && $equipment_list!=NULL){

				  $srno=1;

				  	foreach($equipment_list as $key=>$val){ ?>

					<tr>

						<td><?=@$srno++;?></td>
						

						<td><?=@$val['equipment_name'];?></td>

						<td><?php if(@$val['status']=='1'){
						echo "Active";
						}
						else
						{
						echo "Inactive";
						}
						
						?></td>

						<td colspan="5"><a id="view" load="equipment" rel="equipment" name="<?php echo @$val['equipment_id'];?>" href="javascript:void(0);">View</a> | 

					<a id="edit" load="equipment" rel="equipment" name="<?php echo @$val['equipment_id'];?>" href="javascript:void(0);">Edit</a> | 

<a id="delete_pop_ancor" rel="equipment" title="Delete Equipment" load="equipment" name="<?php echo @$val['equipment_id'];?>" href="javascript:void(0);">Remove</a></td>

					</tr>

					<? }

				  }else{?>

				  <tr>

                    <th colspan="8" style="text-align:center;"> You Have No Equipments Yet </th>

                  </tr>

				  <?php }?>

                </thead>

              </table><!--/row-->

			 <div class="pagination" align="right" style="float: left; width: 998px;">

			<?php echo @$pagination; ?>

			</div>