
   <div class="container">

      <form name="forget_form" id="forget_form" class="form-signin" method="post" action="<?=base_url()?>login">

        <h2 class="form-signin-heading">Forgot Password</h2>

		<?php

			if(validation_errors()){

			echo '<div class="warning" style="width:500px;">'.validation_errors().'</div>';

			}

			if(!empty($msg))

			{?>

			<div class="success"><?php echo $msg;?></div>

			<?php	}

			if($this->session->flashdata('error_message'))

			echo '<div class="warning">'.@$this->session->flashdata('error_message').'</div>';

			?>

        <input type="text" class="input-block-level" name="email" id="email" placeholder="Email address" <?php if(@$email){ echo @$email;}else { set_value('email'); }?>>

        <button class="btn btn-large btn-primary" type="submit" onClick="forget_password();return false;">Submit</button>

	</form>



    </div> <!-- /container --> 