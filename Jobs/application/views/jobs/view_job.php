

<p class="para-job" style="display: block; width: 500px; z-index: 1002; font-size: 27px; margin-left: 12px;">View Job</p>
<div id="line-r"></div>


<table style="font-size: 14px; width: 90%; margin: 28px;">
<tbody>
	<tr>
		<td class="left_title">Job Title :</td>
		<td style="font-size: 20px;"><?php if(isset($job_data['job_title'])&& $job_data['job_title']!=''){ echo $job_data['job_title'];}?></td>
	</tr>
	
	
	<tr>
	<td class="left_title">Job Description :</td>
	<td style="font-size: 20px;"><?php if(isset($job_data['job_description'])&& $job_data['job_description']!=''){ echo $job_data['job_description'];}?></td>
	</tr>
	
	<tr>
	<td class="left_title">Date :</td>
	<td style="font-size: 20px;"><?php if(isset($job_data['date'])&& $job_data['date']!=''){ echo $job_data['date'];}?></td>
	</tr>
	
	<tr>
	<td class="left_title">Assgined To :</td>
	<td style="font-size: 20px;"><?php 
							if($users) 
							{
								foreach ($users as $user)
								{
									if(@$user['user_id']==@$job_data['assigned_to'])
									{
										echo @$user['firstname'].' '.@$user['lastname'];
									}
							
								}
							}
							?></td>
	</tr>
	
	<tr>
	<td class="left_title">Assigned By :</td>
	<td style="font-size: 20px;"><?php if(isset($job_data['assigned_by'])&& $job_data['assigned_by']!=''){ echo $job_data['assigned_by'];}?></td>
	</tr>
	
	<tr>
	<td class="left_title">Status :</td>
	<td style="font-size: 20px;"><?php 
							
							if(@$job_data['status']=='1')
							{
								echo "Not Started";	
							}
							else if(@$job_data['status']=='2')
							{
								echo "In Progress";	
							}
							else
							{
								echo "Completed";
							}
							?></td>
	</tr>
	
	<tr>
	<td class="left_title">Priority :</td>
	<td style="font-size: 20px;"><?php if(@$job_data['priority']=='low')
							{
								echo "Low";
							}
							else if(@$job_data['priority']=='medium')
							{
								echo "Medium";
							}
							else
							{
								echo "High";
							}
							?></td>
	</tr>
	
	<tr>
	<td class="left_title">Equipment  :</td>
	<td style="font-size: 20px;"><?php if($equipments) 
								{
								foreach ($equipments as $equipment)
								{
									if(@$equipment['equipment_id']==@$job_data['equipment_id'])
									{
										echo @$equipment['equipment_name'];
									}
							
								}
							}?></td>
	</tr>
	
	
	
 </tbody>	

</table>

<div style="text-align: center;">
	<button class="btn btn-large btn-primary" type="" id="closepop">Cancel</button>
</div>