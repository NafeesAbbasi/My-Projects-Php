<style>
#ui-datepicker-div {
    font-size: 12px;
    left: 462.5px;
    position: absolute;
    top: 174.833px;
    width: 237px;
    z-index: 1;
}
</style>
<ul class="breadcrumb large"><li><a href="<?=base_url()?>jobs/">Jobs Management</a> <span class="divider">&raquo;</span></li> <li><a href="javascript:void(0);">List Jobs</a><span class="divider">&raquo;</span></li></ul> 

		  <?php

			if(validation_errors()){

			echo '<div class="warning" style="width:500px;">'.validation_errors().'</div>';

			}

			if(!empty($msg))

			{?>

			<div class="success"><?php echo $msg;?></div>

			<?php	}

			if($this->session->flashdata('error_message'))

			echo '<div class="warning">'.@$this->session->flashdata('error_message').'</div>';

			if($this->session->flashdata('success_message'))

			echo '<div class="success">'.@$this->session->flashdata('success_message').'</div>';

			$this->load->model('model_users');

			

			?> 
			  
  <th colspan="12">

<a id="add_job" onmouseover="create_job()" class="btn btn-primary pull-right" href="javascript:void(0);" style="margin-bottom: 10px;">Add New Job &raquo;</a>

					</th>
				
            <table class="table table-striped table-bordered table-condensed">

                <thead>

                  <tr>

				<tr>
				

                    <th colspan="12">

						<form name="search_operator_orders" id="search_operator_orders" action="" method="post">

						<input type="text" name="first_name" id="first_name" placeholder="First Name" value="<?=@$this->input->post('first_name');?>" />

						<input type="text" name="last_name" id="last_name" placeholder="Last Name" value="<?=@$this->input->post('last_name');?>" />

						<input type="text" name="username" id="username" placeholder="Username"  value="<?=@$this->input->post('username');?>"/>

						<input type="text" name="email" id="email" placeholder="Email" value="<?=@$this->input->post('email');?>" />

						<input type="button" class="btn" style="margin-top:-10px" onclick="search_users();return false;" value="Search">

						</form>

					</th>

                  </tr>

				  <tr style="background:#F5F5F5; color:#0088CC">

                    <th>#</th>

                    <th>Job Title</th>

                    <th>Assigned To</th>
					
					 <th>Equipment</th>

                    <th>Assigned By</th>

					<th>Status</th>
					
					<th>Priority</th>

					<th>Date</th>

                    <th colspan="5">Actions</th>

                  </tr>

				  <?php if(isset($jobs_list) && $jobs_list!=NULL){

				  $srno=1;

				  	foreach($jobs_list as $key=>$val){ ?>

					<tr>

						<td><?=@$srno++;?></td>

						<td><?=@$val['job_title'];?></td>

						<td><?php 
						if($users) 
						{
							foreach ($users as $user)
							{
								if(@$user['user_id']==@$val['assigned_to'])
								{
								echo @$user['firstname'].' '.@$user['lastname'];
								}
						
							}
						}
						?></td>
								
						<td><?php 
						if($equipments) 
						{
							foreach ($equipments as $equipment)
							{
								if(@$equipment['equipment_id']==@$val['equipment_id'])
								{
									echo @$equipment['equipment_name'];
								}
						
							}
						}?></td>		

						<td><?=@$val['assigned_by'];?></td>

						<td><?php 
						
						if(@$val['status']=='1')
						{
							echo "Not Started";	
						}
						else if(@$val['status']=='2')
						{
							echo "In Progress";	
						}
						else
						{
							echo "Completed";
						}
						?></td>
						
						<td><?php if(@$val['priority']=='low')
						{
							echo "Low";
						}
						else if(@$val['priority']=='medium')
						{
							echo "Medium";
						}
						else
						{
							echo "High";
						}
						?></td>

						<td><?=@$val['date'];?></td>

					

	<td colspan="5"><a id="view" load="jobs" rel="jobs" name="<?php echo @$val['job_id'];?>" href="javascript:void(0);">View</a> | 

	<a id="edit" load="jobs" rel="jobs" name="<?php echo @$val['job_id'];?>" href="javascript:void(0);">Edit</a> | 

<a id="delete_pop_ancor" rel="jobs" title="Delete Job" load="jobs" name="<?php echo @$val['job_id'];?>" href="javascript:void(0);">Remove</a></td>

					</tr>

					<? }

				  }else{?>

				  <tr>

                    <th colspan="8" style="text-align:center;"> You Have No Jobs Yet </th>

                  </tr>

				  <?php }?>

                </thead>

              </table><!--/row-->

			 <div class="pagination" align="right" style="float: left; width: 998px;">

			<?php echo @$pagination; ?>

			</div>
<div style="display:none;">			
<div class="widget">

<div id="job-lightbox">
<p class="para-job">Create a New Job</p>
<div id="line-r"></div>
<form id="add_new_job">

	<fieldset id="w1first" class="step ui-formwizard-content">

                        <div class="formRow">

                            <div class="grid9" style="margin-top: 10px; width: 240px; margin-left: 10px;">

							<input name="job_title" id="job_title" class="input-block-level input_me" value="" type="text" style="margin-right:300px;" placeholder="Job Title">

							
							 <div class="grid9" style="width: 240px; margin-left: 287px; margin-top: -40px;"><input name="date" id="date" class="input-block-level input_me" value="" type="text" style="margin-right:300px;" placeholder="Date"></div>
								<script>
								$( "#date" ).datepicker({ 

								defaultDate: +7,
						
								showOtherMonths:true,
						
								autoSize: true,
						
								dateFormat: 'd/mm/yy'
						
							});	
						
						
								</script>						

							</div>

				
							<div class="clear"></div>

                         </div>

						 <div class="formRow">
	 
						 <div class="grid9" style="margin-top: 10px; width: 250px; margin-left: 10px;">
						 <select name="assigned_to" id="assigned_to" class="input_me" style=" width: 240px;">

							<option value="">Assigned To</option>

						

							<?php if($users){

								foreach($users as $user){ ?>

								<option value="<?php echo $user['user_id']; ?>"><?php echo $user['firstname']; ?> <?php echo $user['lastname']; ?></option>						

							<?php	}

					
							} ?>
						</select>
						 
							</div>

                           <div class="grid9" style="width: 240px; margin-left: 300px; margin-top: -40px;">

							<input name="assigned_by" id="assigned_by" class="input-block-level input_me" value="<?=@$this->session->userdata('firstname') ?> <?=@$this->session->userdata('lastname') ?>" readonly="" type="text" style="margin-right:300px;" placeholder="Assigned By">

							</div>


							<div class="clear"></div>

                         </div>
						
						 <div class="formRow">
						  <div class="grid9" style="margin-top: 10px; width: 250px; margin-left: 10px;">
						  <select id="status" name="status" style=" width: 240px;" class="input_me" >
							<option value="">Select Status</option>
							<option value="1">Not Started</option>
							<option value="2">In Progress</option>
							<option value="3">Completed</option>
							</select></div>

                           

						    <div class="grid9" style="width: 250px; margin-left: 300px; margin-top: -40px;">
							<select id="priority" name="priority" style=" width: 240px;" class="input_me" >
							<option value="">Select Priority</option>
							<option value="low">Low</option>
							<option value="medium">Medium</option>
							<option value="high">High</option>
							</select>
							
							</div>

							

							<div class="clear"></div>

                         </div>

						 <div class="formRow">
						<div class="grid9" style="margin-top: 10px; width: 250px; margin-left: 10px;">
						 <select name="equipment" id="equipment" class="input_me" style=" width: 240px;">

							<option value="">Select Equipment</option>

						

							<?php if($equipments){

								foreach($equipments as $equipment){ ?>

								<option value="<?php echo $equipment['equipment_id']; ?>"><?php echo $equipment['equipment_name']; ?></option>						

							<?php	}

					
							} ?>
						</select>
						 
							

							</div>
						

                           <div class="grid9" style="width: 250px; margin-left: 300px; margin-top: -40px;">
	<textarea name="job_desc" id="job_desc" class="input-block-level input_me"  style="margin-right:300px; width: 238px;"  placeholder="Job Description"></textarea>
							

							</div>

								<div class="clear"></div>

                         </div>

                    </fieldset>


<br />

<button class="btn btn-large btn-primary" type="" onClick="add_job();return false;" style="margin-left:25px;">&nbsp;&nbsp;Add Job &raquo;</button>

<button class="btn btn-large btn-primary" type="button" id="popover_close">Cancel</button>



</form>

</div>
</div>
</div>			
			
