<script type="text/javascript" >
	$(function() {	
	if (window.Touch) {
        var $gallery = $( ".group" );
		$( ".update_status", $gallery ).live("touchstart", function(){
			 var $this = $(this);
			  if(!$this.is(':data(draggable)')) {
			    $this.draggable({
			     	helper: "clone",
					containment: $( "#demo-frame" ).length ? "#demo-frame" : "document", 
					cursor: "move"
			    });
			  }
		});
		$( "span", $gallery ).live("touchstart", function(){
			 var $this = $(this);
			  if(!$this.is(':data(draggable)')) {
			    $this.draggable({
			     	helper: "clone",
					containment: $( "#demo-frame" ).length ? "#demo-frame" : "document", 
					cursor: "move"
			    });
			  }
		});

					   /* $elem.bind('touchstart', function(){
			 var $this = $(this);
			  if(!$this.is(':data(draggable)')) {
			    $this.draggable({
			     	helper: "clone",
					containment: $( "#demo-frame" ).length ? "#demo-frame" : "document", 
					cursor: "move"
			    });
			  }
		});
                        $elem.bind('touchmove', jQuery.event.special.tabOrClick.onTouchMove);
                        $elem.bind('touchend', jQuery.event.special.tabOrClick.onTouchEnd);
     */               } else {
                       /* $elem.bind('click', jQuery.event.special.tabOrClick.click);
                    }*/
		// Initiate draggable for public and groups
		var $gallery = $( ".group" );
		$( ".update_status", $gallery ).live("mouseenter", function(){
			 var $this = $(this);
			  if(!$this.is(':data(draggable)')) {
			    $this.draggable({
			     	helper: "clone",
					containment: $( "#demo-frame" ).length ? "#demo-frame" : "document", 
					cursor: "move"
			    });
			  }
		});
		
		}
		// Initiate Droppable for groups
		// Adding members into groups
		// Removing members from groups
		// Shift members one group to another
		
		$(".group").livequery(function(){
			var casePublic = false;
			$(this).droppable({
				activeClass: "ui-state-highlight",
				drop: function( event, ui ) {
					var m_id = $(ui.draggable).attr('rel');
					//alert(m_id);
					if(!m_id)
						{
							casePublic = true;
							var m_id = $(ui.draggable).attr("id");
							m_id = parseInt(m_id.substring(3));
						}					
					var g_id = $(this).attr('id');
					dropPublic(m_id, g_id, casePublic);
					$("#mem"+m_id).hide();
					$( "<li></li>" ).html( ui.draggable ).appendTo( this );
				},
				 out: function(event, ui) {
				 	var m_id = $(ui.draggable).attr('rel');
					var g_id = $(this).attr('id');			 	
				 	$(ui.draggable).hide("explode", 1000);
				 	//removeMember(g_id,m_id);
				 }
			});
		
		});
		
		// Add or shift members from groups
		function dropPublic(m_id, g_id,caseFrom)
			{
				//alert(m_id+' '+g_id+' '+caseFrom);
				$.ajax({
					type:"GET",
					beforeSend 	: fnLoadStart,
					url:"<?=$this->config->item('base_url')?>jobs/savejoblocation/"+m_id+"/"+g_id,
					cache:false,
					success:function(response){
						res = g_id.split('-');
						targets = res[1]+'-'+res[2];	
								 $.ajax({
														url  		: ""+base_url+"jobs/getCalender/"+targets,
														type 		: 'POST',
														complete	: fnLoadStop,
														success 	: function(data)
														{
															$('#listings_forcalender').slideUp(200);
															$('#listings_forcalender').slideDown(400);
															 
															$('#listings_forcalender').html(data);
															location.reload();
															//top.location =""+base_url+"ads";
														}
						
								});
					}
				});
			}
		// Remove memebers from groups
		// It will restore into public groups or non grouped members
		function removeMember(g_id,m_id)
			{
				$.ajax({
					type:"GET",
					url:"<?=$this->config->item('base_url')?>welcome/removeMember/"+m_id,
					cache:false,
					success:function(response){
						$("#removed"+g_id).animate({"opacity" : "10" },10);
						$("#removed"+g_id).show();
						$("#removed"+g_id).animate({"margin-top": "-50px"}, 450);
						$("#removed"+g_id).animate({"margin-top": "0px","opacity" : "0" }, 450);
						//$.get("groups.php?reload", function(data){ $("#public").html(data); });
						$.get("<?=$this->config->item('base_url')?>welcome/getMembers_reload/", function(data){ $("#public").html(data); });
					}
				});
			}	
		
	});
	</script>
	
	<script>
		(function($){
			$(window).load(function(){
				$("body,.content").mCustomScrollbar({
					scrollButtons:{
						enable:true
					}
				});
			});
		})(jQuery);
		
		$(function() {
		$( document ).tooltip({
			position: {
				my: "center bottom-20",
				at: "center top",
				using: function( position, feedback ) {
					$( this ).css( position );
					$( "<div>" )
						.addClass( "arrow" )
						.addClass( feedback.vertical )
						.addClass( feedback.horizontal )
						.appendTo( this );
				}
			}
		});
	});
	</script>
<?php
$monthNames = Array("January", "February", "March", "April", "May", "June", "July","August", "September","October", "November","December");?>

<?php

if (!isset($month)) 
$_REQUEST["month"] = date("n");
else
$_REQUEST["month"] = $month;
if (!isset($year)) 
$_REQUEST["year"] = date("Y");
else
$_REQUEST["year"] = $year;
//if(isset($id)) { $_REQUEST["month"] = date("n",strtotime($apps['arrival'])); $_REQUEST["year"] = date("Y",strtotime($apps['arrival'])); }

//var_dump($_REQUEST["month"]);
//exit();
?>

<?php

$cMonth = $_REQUEST["month"];
$cYear = $_REQUEST["year"];

$prev_year = $cYear;
$next_year = $cYear;
$prev_month = $cMonth-1;
$next_month = $cMonth+1;

if ($prev_month == 0 ) {
$prev_month = 12;
$prev_year = $cYear - 1;
}
if ($next_month == 13 ) {
$next_month = 1;
$next_year = $cYear + 1;
}

?>
	   	<div class="calender_controls">
			<div class="current_day">
				
			</div>
			<div class="slections" style="margin-top: -35px;">
			
			<a href="javascript:" id="calender_actions_changings" style=" margin-right: 8px;" class="previous" rel="<?php
			if($prev_month<9) {
				echo '0'.$prev_month."-".$prev_year;
			}
			else {
			echo $prev_month."-".$prev_year;
			} ?>">
				<img src="#" alt="" style="padding: 7px;" />
			</a>
			
					<select name="month_drop" id="month_drop" class="month" style="margin-top: 7px;" >
						<option <?php if(isset($month) && $month == '01'){ echo 'selected="selected"';}?> value="01">January</option>
						<option <?php if(isset($month) && $month == '02'){ echo 'selected="selected"';}?> value="02">Feburary</option>
						<option <?php if(isset($month) && $month == '03'){ echo 'selected="selected"';}?> value="03">March</option>
						<option <?php if(isset($month) && $month == '04'){ echo 'selected="selected"';}?> value="04">April</option>
						<option <?php if(isset($month) && $month == '05'){ echo 'selected="selected"';}?> value="05">May</option>
						<option <?php if(isset($month) && $month == '06'){ echo 'selected="selected"';}?> value="06">June</option>
						<option <?php if(isset($month) && $month == '07'){ echo 'selected="selected"';}?> value="07">July</option>
						<option <?php if(isset($month) && $month == '08'){ echo 'selected="selected"';}?> value="08">August</option>
						<option <?php if(isset($month) && $month == '09'){ echo 'selected="selected"';}?> value="09">September</option>
						<option <?php if(isset($month) && $month == '10'){ echo 'selected="selected"';}?> value="10">October</option>
						<option <?php if(isset($month) && $month == '11'){ echo 'selected="selected"';}?> value="11">November</option>
						<option <?php if(isset($month) && $month == '12'){ echo 'selected="selected"';}?> value="12">December</option>
					</select>
					<select name="year_drop" id="year_drop" class="year"  style="margin-top: 7px;">
						<?php 
						$strting_year = '2012';
						$current_year = date('Y',time());
						for($i = $strting_year; $i<=$current_year;$i++)
						{
						?>						
						<option <?php if(isset($year) && $year == $i){ echo 'selected="selected"'; } ?> value="<?php echo $i;?>"><?php echo $i;?></option>
						<?php 
						}
						?>
					</select>
			
			<a href="javascript:" id="calender_actions_changings" class="next" style="margin-left: 8px;" rel="<?php
				if($next_month<9) {
					echo '0'.$next_month."-".$next_year;
				}
				else {
				echo $next_month."-".$next_year;
				} 
			 ?>">
				<img src="#" alt="" style="padding: 7px;" />
			</a>	
				<div class="month_heading" style="font-size:28px; font-weight:bolder;"><?php echo $monthNames[$cMonth-1].' '.$cYear; ?></div>
			</div> 
		</div>
	   	<table class="calender" style="">
			<tbody>
				<tr class="weekdays" style="">
					<td>Sun</td>
					<td>Mon</td>
					<td>Tue</td>
					<td>Wed</td>
					<td>Thu</td>
					<td>Fri</td>
					<td>Sat</td>
				</tr>
			</tbody>
			<tbody>
			
			<?php
			$timestamp = mktime(0,0,0,$cMonth,1,$cYear);
			$maxday = date("t",$timestamp);
			$thismonth = getdate ($timestamp);
			$startday = $thismonth['wday'];
			
		
			for ($i=0; $i<($maxday+$startday); $i++) {
			 $day= ($i - $startday + 1);
				 $cMonth = $_REQUEST["month"];
				 $cYear = $_REQUEST["year"];
				 if($cMonth <= 9)
							 {
							 	$cMonth = $cMonth;
							 }
							 
							
				$job_date_match= $day.'/'.$cMonth.'/'.$cYear;
				
				$tr_date = $day.'-'.$cMonth.'-'.$cYear;
			
				if(($i % 7) == 0 ) { echo "<tr class='days'>"; }
				if($i < $startday) { echo "<td></td>"; }
				
				
				else { echo "<td id='".$tr_date."' class='group'>
							<div class='key_day'>". ($i - $startday + 1) . "</div>";
					}		
							
							 
							 
							
							$date_status_match= Match_Job($job_date_match, $this->session->userdata('login_id'));
							//print_r($job_date_match);
							//print_r($date_status_match);
							
							if(count($date_status_match))
							{
							//print_r($date_status_match); exit;
							echo "<div style='min-height: 20px; width: 96px; margin-top: 2px;' id='set_status_div'>";
								foreach($date_status_match as $match)
								{
							
	 $title = $match['job_title']."<br />".$match['job_description']."<br />".$match['assigned_to']."<br />".$match['assigned_by'];
	$title= str_replace("'","`", $title);
	//$title = $match['job_description'];
							
							if($match['status']==1)
							 {
							 	echo "<div id='".$job_date_match."' rel='".$match['job_id']."' class='app_indication_yellow update_status' onmouseover='show_prompt(this);' onmouseout='hide_prompt(this);' lang='$title'></div>";
							 }
							 else if ($match['status']==2)
							 {
								echo "<div id='".$job_date_match."' rel='".$match['job_id']."' class='app_indication_green update_status' onmouseover='show_prompt(this);' onmouseout='hide_prompt(this);' lang='$title'></div>";
							 }
							 else if ($match['status']==3)
							 {
								echo "<div id='".$job_date_match."' rel='".$match['job_id']."' class='app_indication_blue update_status'  onmouseover='show_prompt(this);' onmouseout='hide_prompt(this);' lang='$title'></div>";
							 }
							 else 
							 {
							 	echo "<div id='".$job_date_match."' rel='".$match['job_id']."' class='app_indication_red update_status' onmouseover='show_prompt(this);' onmouseout='hide_prompt(this);' lang='$title'></div>";
							 }
							 
								
								}
							 echo "</div>";
							}
							echo "</td>";
							
							
				if(($i % 7) == 6 ) echo "</tr>";
			}
			?>
				
			</tbody>
		</table> 
		
		<script type="text/javascript">

	$(document).ready(function(){
	
		$( ".draggable_yellow" ).draggable();
		$( ".draggable_green" ).draggable();
		$( ".draggable_blue" ).draggable();
		$( ".draggable_red" ).draggable();
	
	});

</script>
<div class="arrow-left" style="display:none;"></div>
<div style="display:none;position: absolute; left: 907px; top: 50px; background-color: black; padding: 14px; width: 201px; border: 7px solid gray; border-radius: 8px 8px 8px 8px; font-size: 10px;color:white;" class="prompt"></div>


<style>

#arrow-left {
	width: 0; 
	height: 0; 
	border-top: 10px solid transparent;
	border-bottom: 10px solid transparent; 
	
	border-right:10px solid gray; 
}
</style>