<style>

.assigned_to {
    margin-top: 5px;
    min-height: 20px;
}

</style>

<p class="para-job" style="display: block; width: 500px; z-index: 1002; font-size: 27px; margin-left: 12px;">Edit Job</p>
<div id="line-r"></div>

<form id="add_form_cust" class="add_form">

<input name="job_id" id="job_id"  value="<?php if(isset($job_data['job_id'])&& $job_data['job_id']!=''){ echo $job_data['job_id'];}?>" type="hidden" />
<table style="font-size: 14px; width: 90%; margin: 28px;">
<tbody>
	<tr>
		<!--<td class="left_title">Job Title :</td>-->
		<td style="font-size: 20px;"><input name="job_title" id="job_title" class="input-block-level" value="<?php if(isset($job_data['job_title'])&& $job_data['job_title']!=''){ echo $job_data['job_title'];}?>" type="text" style="width:300px;margin-right:300px;" placeholder="Job Title"></td>
	</tr>
	
	
	<tr>
	<!--<td class="left_title">Job Description :</td>-->
	<td style="font-size: 20px;"><textarea name="job_desc" id="job_desc" class="input-block-level input_me"  style="width: 300px; margin-left: 0px;"  placeholder="Job Description"><?php if(isset($job_data['job_description'])&& $job_data['job_description']!=''){ echo $job_data['job_description'];}?></textarea></td>
	</tr>
	
	<tr>
	<!--<td class="left_title">Date :</td>-->
	<td style="font-size: 20px;"><input name="date" id="date" class="input-block-level" value="<?php if(isset($job_data['date'])&& $job_data['date']!=''){ echo $job_data['date'];}?>" type="text" style="width:300px;margin-right:300px;" placeholder="Date"></td>
	</tr>
	
	<tr>
	<!--<td class="left_title">Assgined To :</td>-->
	<td style="font-size: 20px;"><select id="assigned_to" name="assigned_to" class="assigned_to" style="margin-left:0px; width:300px; margin-top: 5px; min-height: 20px;">
								<option value="">Assigned To</option>
											<?php if($users){
								
											foreach($users as $user){ ?>
								
											<option value="<?php echo $user['user_id']; ?>" <?php if($job_data['assigned_to']==$user['user_id']) {echo 'selected="selected"';} ?>><?php echo $user['firstname']; ?> <?php echo $user['lastname']; ?></option>						
								
										<?php	}
								
								
										} ?>
</select></td>
	</tr>
	
	<tr>
	<!--<td class="left_title">Assigned By :</td>-->
	<td style="font-size: 20px;"><input name="assigned_by" id="assigned_by"  class="input-block-level" value="<?php if(isset($job_data['assigned_by'])&& $job_data['assigned_by']!=''){ echo $job_data['assigned_by'];}?>" type="text" style=" height: 30px; margin-left: 0; margin-right: 300px; width: 300px;" placeholder="Assigned By" readonly=""></td>
	</tr>
	
	<tr>
	<!--<td class="left_title">Status :</td>-->
	<td style="font-size: 20px;"> <select id="status" name="status"  class="input_me">
							<option value="">Select Status</option>
							<option value="1" <?php if($job_data['status']=='1') { echo 'selected="selected"'; } ?>>Not Started</option>
							<option value="2" <?php if($job_data['status']=='2') { echo 'selected="selected"'; } ?>>In Progress</option>
							<option value="3" <?php if($job_data['status']=='3') { echo 'selected="selected"'; } ?>>Completed</option>
							</select></td>
	</tr>
	
	<tr>
	<!--<td class="left_title">Priority :</td>-->
	<td style="font-size: 20px;"><select id="priority" name="priority"  class="input_me">
							<option value="">Select Priority</option>
							<option value="low"	   <?php if($job_data['priority']=='low') 	 { echo 'selected="selected"'; } ?>>Low</option>
							<option value="medium" <?php if($job_data['priority']=='medium') { echo 'selected="selected"'; } ?>>Medium</option>
							<option value="high"   <?php if($job_data['priority']=='high') 	 { echo 'selected="selected"'; } ?>>High</option>
							</select></td>
	</tr>
	
	<tr>
	<!--<td class="left_title">Equipment  :</td>-->
	<td style="font-size: 20px;"><select id="equipment" name="equipment">
								<option value="">Select Equipment</option>
			<?php if($equipments) 
						{
							foreach ($equipments as $equipment)
							{?>
							<option value="<?php echo $equipment['equipment_id']; ?>" <?php if($job_data['equipment_id']==$equipment['equipment_id']) {echo 'selected="selected"';} ?>><?php echo $equipment['equipment_name']; ?></option>		
							
								
					<?php	}
						}?>
</select></td>
	</tr>
	
	
	
 </tbody>	

</table>


<div style="text-align: center;">

<button class="btn btn-large btn-primary" type="" onclick="update_job();return false;">Update &raquo;</button>

<button class="btn btn-large btn-primary" type="button" id="delete_pop_ancor" rel="jobs" title="Delete Job" load="jobs" name="<?php echo @$job_data['job_id'];?>">Delete &raquo;</button>


<button class="btn btn-large btn-primary" type="button" id="closepop_edit">Cancel</button>

</form>
</div>
</div>