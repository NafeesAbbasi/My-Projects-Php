    <div class="container">

      <form name="reset_pass_form" id="reset_pass_form" class="form-signin" method="post" action="<?=base_url()?>login">

        <h2 class="form-signin-heading">Reset Password</h2>

		

		<?php

		if(!$is_valid_token){

			echo '<div class="warning" style="width:500px;"><b>Sorry! </b>your token is not valid. Please try again</div>';

		}

		else{

			if(validation_errors()){

			echo '<div class="warning" style="width:500px;">'.validation_errors().'</div>';

			}

			if(!empty($msg))

			{?>

			<div class="success"><?php echo $msg;?></div>

			<?php	}

			if($this->session->flashdata('error_message'))

			echo '<div class="warning">'.@$this->session->flashdata('error_message').'</div>';

			?>

        <input type="password" class="input-block-level" name="password" id="password" placeholder="New Password" <?php if(@$email){ echo @$email;}else { set_value('email'); }?>>

        <input type="password" class="input-block-level" placeholder="Repeat Password" name="re_password" id="re_password" <?php if(@$re_password){ echo @$re_password;}else { set_value('re_password'); }?>>

		 <input type="hidden" name="reset_password_token" id="reset_password_token" <?php echo @$reset_password_token;?>>

		

        <button class="btn btn-large btn-primary" type="submit" onClick="reset_password_update();return false;">Sign in</button>

		<?php } ?>

	</form>



    </div> <!-- /container --> 