    <div class="container">

      <form name="login_form" id="login_form" class="form-signin" method="post" action="<?=base_url()?>login">

        <h2 class="form-signin-heading">Please sign in</h2>

		<?php

			if(validation_errors()){

			echo '<div class="warning" style="width:500px;">'.validation_errors().'</div>';

			}

			if(!empty($msg))

			{?>

			<div class="success"><?php echo $msg;?></div>

			<?php	}

			if($this->session->flashdata('error_message'))

			echo '<div class="warning">'.@$this->session->flashdata('error_message').'</div>';

			?>
		<!--<select id="usertype" name="usertype">
		<option value="">Select User Type</option>
		<option value="admin">Admin</option>
		<option value="employee">Employee</option>
		</select>-->	

        <input type="text" class="input-block-level" name="email" id="email" placeholder="Email address" <?php if(@$email){ echo @$email;}else { set_value('email'); }?>>

        <input type="password" class="input-block-level" placeholder="Password" name="password" id="password" <?php if(@$password){ echo @$password;}else { set_value('password'); }?>>

        <label class="checkbox">

          <input type="checkbox" value="rememberme"> Remember me

        </label>

        <button class="btn btn-large btn-primary" type="submit" onClick="admin_login();return false;">Sign in</button>

		<a href="<?php echo $this->config->item('base_url');?>login/signup" style="float:right" >Signup</a><br />
		
		<a href="<?php echo $this->config->item('base_url');?>login/forgot_pass" style="float:right" >Forget Password</a><br />

	      </form>

    </div> <!-- /container --> 