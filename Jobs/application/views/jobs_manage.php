<style>

.sorting_date {
    margin-left: 4px;
    margin-top: 2px;
}
.arrowup {
    background: url("../images/sort_uparrow.png") no-repeat scroll 0 0 transparent;
    color: #FFFFFF;
    float: left;
    height: 6px;
    text-decoration: none;
    width: 7px;
}
.arrowdown {
    background: url("../images/sort_downarrow.png") no-repeat scroll 0 0 transparent;
    color: #FFFFFF;
    float: left;
    height: 6px;
    margin-left: -7px;
    margin-top: 10px;
    text-decoration: none;
    width: 7px;
}

.search_by_date
{
	background-color: #006DCC;
    background-image: linear-gradient(to bottom, #0088CC, #0044CC);
    background-repeat: repeat-x;
    border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
    color: #FFFFFF;
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
	float:left;
	border-radius: 4px 4px 4px 4px;
    height: 24px;
    margin-top: -18px;
	margin-left:4px;
	
}

.search_by_status
{
	background-color: #006DCC;
    background-image: linear-gradient(to bottom, #0088CC, #0044CC);
    background-repeat: repeat-x;
    border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
    color: #FFFFFF;
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
	float:left;
	border-radius: 4px 4px 4px 4px;
    height: 24px;
    margin-top: -18px;
	margin-left:4px;
	
}

.search_by_priority
{
	background-color: #006DCC;
    background-image: linear-gradient(to bottom, #0088CC, #0044CC);
    background-repeat: repeat-x;
    border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
    color: #FFFFFF;
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
	float:left;
	border-radius: 4px 4px 4px 4px;
    height: 24px;
    margin-top: -18px;
	margin-left:4px;
	
}

.ui-datepicker
{
	margin-top: -24px;
}



</style>

	<!--<link href="<?=base_url();?>css/jquery.ui.tooltip.css" rel="stylesheet">-->
	<link href="<?=base_url();?>css/jquery.mCustomScrollbar.css" rel="stylesheet" />
	<!--<script src="<?php echo base_url();?>js/js/ui/jquery.ui.core.js"></script>
	<script src="<?php echo base_url();?>js/js/ui/jquery.ui.widget.js"></script>
	<script src="<?php echo base_url();?>js/js/ui/jquery.ui.position.js"></script>
	<script src="<?php echo base_url();?>js/js/ui/jquery.ui.tooltip.js"></script>
	<script src="<?php echo base_url();?>js/js/ui/jquery.ui.mouse.js"></script>-->
	<!--<script src="<?php echo base_url();?>js/js/ui/jquery.ui.draggable.js"></script>-->
	<script src="<?php echo base_url();?>js/js/jquery.mCustomScrollbar.concat.min.js"></script>
	
	
	<script src="<?php echo base_url();?>js/js/jquery.ui.touch-punch.min.js"></script>
	<script src="<?php echo base_url();?>js/js/jquery.livequery.min.js"></script>
	
	<script type="text/javascript" >
	$(function() {	
	if (window.Touch) {
        var $gallery = $( ".group" );
		$( ".update_status", $gallery ).live("touchstart", function(){
			 var $this = $(this);
			  if(!$this.is(':data(draggable)')) {
			    $this.draggable({
			     	helper: "clone",
					containment: $( "#demo-frame" ).length ? "#demo-frame" : "document", 
					cursor: "move"
			    });
			  }
		});
		$( "span", $gallery ).live("touchstart", function(){
			 var $this = $(this);
			  if(!$this.is(':data(draggable)')) {
			    $this.draggable({
			     	helper: "clone",
					containment: $( "#demo-frame" ).length ? "#demo-frame" : "document", 
					cursor: "move"
			    });
			  }
		});

					   /* $elem.bind('touchstart', function(){
			 var $this = $(this);
			  if(!$this.is(':data(draggable)')) {
			    $this.draggable({
			     	helper: "clone",
					containment: $( "#demo-frame" ).length ? "#demo-frame" : "document", 
					cursor: "move"
			    });
			  }
		});
                        $elem.bind('touchmove', jQuery.event.special.tabOrClick.onTouchMove);
                        $elem.bind('touchend', jQuery.event.special.tabOrClick.onTouchEnd);
     */               } else {
                       /* $elem.bind('click', jQuery.event.special.tabOrClick.click);
                    }*/
		// Initiate draggable for public and groups
		var $gallery = $( ".group" );
		$( ".update_status", $gallery ).live("mouseenter", function(){
			 var $this = $(this);
			  if(!$this.is(':data(draggable)')) {
			    $this.draggable({
			     	helper: "clone",
					containment: $( "#demo-frame" ).length ? "#demo-frame" : "document", 
					cursor: "move"
			    });
			  }
		});
		
		}
		// Initiate Droppable for groups
		// Adding members into groups
		// Removing members from groups
		// Shift members one group to another
		
		$(".group").livequery(function(){
			var casePublic = false;
			$(this).droppable({
				activeClass: "ui-state-highlight",
				drop: function( event, ui ) {
					var m_id = $(ui.draggable).attr('rel');
					//alert(m_id);
					if(!m_id)
						{
							casePublic = true;
							var m_id = $(ui.draggable).attr("id");
							m_id = parseInt(m_id.substring(3));
						}					
					var g_id = $(this).attr('id');
					dropPublic(m_id, g_id, casePublic);
					$("#mem"+m_id).hide();
					$( "<li></li>" ).html( ui.draggable ).appendTo( this );
				},
				 out: function(event, ui) {
				 	var m_id = $(ui.draggable).attr('rel');
					var g_id = $(this).attr('id');			 	
				 	$(ui.draggable).hide("explode", 1000);
				 	//removeMember(g_id,m_id);
				 }
			});
		
		});
		
		// Add or shift members from groups
		function dropPublic(m_id, g_id,caseFrom)
			{
				//alert(m_id+' '+g_id+' '+caseFrom);
				$.ajax({
					type		:"GET",
					beforeSend 	: fnLoadStart,
					url			:"<?=$this->config->item('base_url')?>jobs/savejoblocation/"+m_id+"/"+g_id,
					cache:false,
					success:function(response){
					
						res = g_id.split('-');
						targets = res[1]+'-'+res[2];	
								 $.ajax({
														url  		: ""+base_url+"jobs/getCalender/"+targets,
														type 		: 'POST',
														complete 	: fnLoadStop,
														success 	: function(data)
														{
															$('#listings_forcalender').slideUp(200);
															$('#listings_forcalender').slideDown(400);
															$('#listings_forcalender').html(data);
															location.reload();
															//top.location =""+base_url+"ads";
														}
						
								});
					}
				});
			}
		// Remove memebers from groups
		// It will restore into public groups or non grouped members
		function removeMember(g_id,m_id)
			{
				$.ajax({
					type:"GET",
					url:"<?=$this->config->item('base_url')?>welcome/removeMember/"+m_id,
					cache:false,
					success:function(response){
						$("#removed"+g_id).animate({"opacity" : "10" },10);
						$("#removed"+g_id).show();
						$("#removed"+g_id).animate({"margin-top": "-50px"}, 450);
						$("#removed"+g_id).animate({"margin-top": "0px","opacity" : "0" }, 450);
						//$.get("groups.php?reload", function(data){ $("#public").html(data); });
						$.get("<?=$this->config->item('base_url')?>welcome/getMembers_reload/", function(data){ $("#public").html(data); });
					}
				});
			}	
		
	});
	</script>
	
	<script>
		(function($){
			$(window).load(function(){
				$("body,.content").mCustomScrollbar({
					scrollButtons:{
						enable:true
					}
				});
			});
		})(jQuery);
		
		$(function() {
		$( document ).tooltip({
			position: {
				my: "center bottom-20",
				at: "center top",
				using: function( position, feedback ) {
					$( this ).css( position );
					$( "<div>" )
						.addClass( "arrow" )
						.addClass( feedback.vertical )
						.addClass( feedback.horizontal )
						.appendTo( this );
				}
			}
		});
	});
	</script>
	
<div style="margin-right: 18px;">
<a id="add_job" onmouseover="create_job()" class="btn btn-primary pull-right" href="javascript:void(0);" style="margin-bottom: 10px;">Add New Job &raquo;</a>
</div>

    <div class="container-fluid">

      <div class="row-fluid">

        <div class="span3" style="margin-top: -9px; background: none repeat scroll 0% 0% white; margin-left: -20px; width: 322px;">

		<div class="sidebar-nav ">

          <div class="well2 sidebar-nav hidden-phone" style="width: 363px;">
			
			<div id="filters" style="margin-top: 22px;">
			<select id="filter" name="filter" style="margin-left: 7px; width: 92px; height: 30px; float: left; margin-bottom: 5px; margin-top: -22px;">
			<option>Select -</option>
			<option value="by_date">By Date</option>
			<option value="by_status">By Status</option>
			<option value="by_priority">By Priority</option>
			</select>
			</div>
			
			<div id="bydate" style="display:none;">
			<form id="search_calls" name="search_calls" action="" method="post">
			<input type="text" id="from_date" name="from_date" placeholder="From Date" style="float: left; margin-left: 3px; width: 82px; margin-top:-21px;" />
			<input type="text" id="to_date" name="to_date" placeholder="To Date" style="float: left; margin-left: 3px; width: 82px; margin-top:-21px;" />
			
			<a href="javascript:void(0);" class="search_voice_calls"><input type="button" value="Search" class="search_by_date"></a>
			</form>
			</div>
			
			<div id="bystatus" style="display:none;">
			<form id="search_calls" name="search_calls" action="" method="post">
			<select id="status" name="status" style="float: left; margin-top: -22px; margin-left: 10px; width: 148px;" class="input_me" >
			<option value="">Select Status</option>
			<option value="1">Not Started</option>
			<option value="2">In Progress</option>
			<option value="3">Completed</option>
			<option value="4">Overdue</option>
			</select>
			
			<a href="javascript:void(0);" class="search_voice_calls"><input type="button" value="Search" class="search_by_status"></a>
			</form>
			</div>
			
			<div id="bypriority" style="display:none;">
			<form id="search_calls" name="search_calls" action="" method="post">
			<select id="priority" name="priority" style="float: left; margin-top: -22px; margin-left: 10px; width: 148px;" class="input_me" >
			<option value="">Select Priority</option>
			<option value="low">Low</option>
			<option value="medium">Medium</option>
			<option value="high">High</option>
			</select>
			
			
			<a href="javascript:void(0);" class="search_voice_calls"><input type="button" value="Search" class="search_by_priority"></a>
			</form>
			</div>
			
          </div>
		  
		  
		  
		  <div class="well1 sidebar-nav hidden-phone">
			
			<div id="sorting">
			<div class="sorting_date" style="margin-top: -9px;">
			<a class="arrowup sorting_col_sup" href="javascript:void(0)" name="time_date-DESC"></a>
			<a class="arrowdown sorting_col_sup" href="javascript:void(0)" name="time_date-ASC"></a>
			</div>
			<div id="Date">Date </div>
			
			<div class="sorting_pri" style="margin-top: -9px; margin-left: 94px;">
			<a class="arrowup sorting_col_sup" href="javascript:void(0)" name="priority-DESC"></a>
			<a class="arrowdown sorting_col_sup" href="javascript:void(0)" name="priority-ASC"></a>
			</div>
			<div id="job_prioirty">Priority</div>
			
			<div class="sorting_status" style="margin-top: -9px; margin-left: 206px;">
			<a class="arrowup sorting_col_sup" href="javascript:void(0)" name="status-DESC"></a>
			<a class="arrowdown sorting_col_sup" href="javascript:void(0)" name="status-ASC"></a>
			</div>
			<div id="job_status">Status</div>
			
			
			</div>
			
          </div><!--/.well -->
		  <div class="content">
		  <div id="listing_sub">
<?php if(isset($jobs_list) && $jobs_list!=NULL){

				  	

				  	foreach($jobs_list as $key=>$val){ ?>
		 <a id="<?php echo @$val['job_id'];?>" class="edit_job" style="color:black; text-decoration:none;" href="javascript:void(0);">
		 <div id="jobs">
		  <div id="job_date"><?=@$val['date'];?></div>
		 <div id="job_title1"><?php echo @$val['job_id'];?> - <?=@$val['job_title'];?></div>
		 
		 <div id="job_desc">
		 <?php if(strlen($val['job_description'])>45){
		 	echo @substr($val['job_description'],0,45).'...';
		 }else{ echo @$val['job_description'];
		  }?> </div>
		
		<div id="" style="margin-top: -58px;">
		
		<!--<a id="<?php echo @$val['job_id'];?>" class="view_job" href="javascript:void(0);">
		
		<img src="../images/viewIcon.png" class="view_image" />
		
		</a>-->
		</div>
		<div id=""><!--<a id="<?php echo @$val['job_id'];?>" class="edit_job" href="javascript:void(0);"><img src="../images/editIcon.png" class="edit_image" /></a>--></div>
		 <div id="assigned_to">Assigned To: <span style="color:#390982;">
					 <?php 
						if($users) 
						{
							foreach ($users as $user)
							{
								if(@$user['user_id']==@$val['assigned_to'])
								{
								echo @$user['username'];
								}
						
							}
						}
						?></span></div>
		 <div id="assigned_by">Assigned By: <span style="color:#390982;"><?php echo @$val['assigned_by'];?></span></div>
		 <?php if(@$val['priority']=='low')
						{?>
						
							<div id="job_priority_low">Low</div>
						<?php
						}
						else if(@$val['priority']=='medium')
						{ ?>
							 <div id="job_priority_medium">Medium</div>
						<?php 
						}
						else
						{?>
							 <div id="job_priority_high">High</div>
						<?php 
						}
						?>
		 				<?php 
						
						if(@$val['status']=='1')
						{ ?>
							<div id="job_status_1">Not Started</div>
						<?php }
						else if(@$val['status']=='2')
						{ ?>
							<div id="job_status_2">In Progress</div>
						<?php }
						else if(@$val['status']=='3')
						{ ?>
							<div id="job_status_3">Completed</div>
						<?php }
						else
						{
						?>
						<div id="job_status_4">Overdue</div>
						<?php }
						?>
		 
		 
		 </div></a>
		<?php }  }
		?>
			
		  </div></div>
		  </div>
		 <div class="pagination" align="left" style="float: left; width: 998px;">

		
			</div>	
			


        </div><!--/span-->

<!-- Calender Setting -->

<?php
$monthNames = Array("January", "February", "March", "April", "May", "June", "July","August", "September","October", "November","December");?>

<?php

if (!isset($_REQUEST["month"])) $_REQUEST["month"] = date("n");
if (!isset($_REQUEST["year"])) $_REQUEST["year"] = date("Y");

$cMonth = $_REQUEST["month"];
$cYear = $_REQUEST["year"];

$prev_year = $cYear;
$next_year = $cYear;
$prev_month = $cMonth-1;
$next_month = $cMonth+1;

if ($prev_month == 0 ) {
$prev_month = 12;
$prev_year = $cYear - 1;
}
if ($next_month == 13 ) {
$next_month = 1;
$next_year = $cYear + 1;
}

?>


       <div class="span9" id="listings_forcalender" style="width: 804px; margin-top: 43px; margin-left: 57px;">
	   	<div class="calender_controls">
			<div class="current_day">
				
			</div>
			<div class="slections" style="margin-top: -35px;">
			
			<a href="javascript:" id="calender_actions_changings" style=" margin-right: 8px;" class="previous" rel="<?php
			if($prev_month<9) {
				echo '0'.$prev_month."-".$prev_year;
			}
			else {
			echo $prev_month."-".$prev_year;
			} ?>">
				<img src="#" alt="" style="padding: 7px;" />
			</a>
			
					<select name="month_drop" id="month_drop" class="month" style="margin-top: 7px;" >
						<option <?php if(isset($month) && $month == '01'){ echo 'selected="selected"';} else if(date('F',time()) == 'January') { echo "selected='selected'"; }?> value="01">January</option>
						<option <?php if(isset($month) && $month == '02'){ echo 'selected="selected"';} else if(date('F',time()) == 'Feburary') { echo "selected='selected'"; }?> value="02">Feburary</option>
						<option <?php if(isset($month) && $month == '03'){ echo 'selected="selected"';} else if(date('F',time()) == 'March') { echo "selected='selected'"; }?> value="03">March</option>
						<option <?php if(isset($month) && $month == '04'){ echo 'selected="selected"';} else if(date('F',time()) == 'April') { echo "selected='selected'"; }?> value="04">April</option>
						<option <?php if(isset($month) && $month == '05'){ echo 'selected="selected"';} else if(date('F',time()) == 'May') { echo "selected='selected'"; }?> value="05">May</option>
						<option <?php if(isset($month) && $month == '06'){ echo 'selected="selected"';} else if(date('F',time()) == 'June') { echo "selected='selected'"; }?> value="06">June</option>
						<option <?php if(isset($month) && $month == '07'){ echo 'selected="selected"';} else if(date('F',time()) == 'July') { echo "selected='selected'"; }?> value="07">July</option>
						<option <?php if(isset($month) && $month == '08'){ echo 'selected="selected"';} else if(date('F',time()) == 'August') { echo "selected='selected'"; }?> value="08">August</option>
						<option <?php if(isset($month) && $month == '09'){ echo 'selected="selected"';} else if(date('F',time()) == 'September') { echo "selected='selected'"; }?> value="09">September</option>
						<option <?php if(isset($month) && $month == '10'){ echo 'selected="selected"';} else if(date('F',time()) == 'October') { echo "selected='selected'"; }?> value="10">October</option>
						<option <?php if(isset($month) && $month == '11'){ echo 'selected="selected"';} else if(date('F',time()) == 'November') { echo "selected='selected'"; }?> value="11">November</option>
						<option <?php if(isset($month) && $month == '12'){ echo 'selected="selected"';} else if(date('F',time()) == 'December') { echo "selected='selected'"; }?> value="12">December</option>
					</select>
					<select name="year_drop" id="year_drop" class="year"  style="margin-top: 7px;">
						<?php 
						$strting_year = '2012';
						$current_year = date('Y',time());
						for($i = $strting_year; $i<=$current_year;$i++)
						{
						?>						
						<option <?php if(isset($year) && $year == $i){ echo 'selected="selected"';} else if(date('Y',time()) == $i) { echo "selected='selected'"; }?> value="<?php echo $i;?>"><?php echo $i;?></option>
						<?php 
						}
						?>
					</select>
			
			<a href="javascript:" id="calender_actions_changings" class="next" style="margin-left: 8px;" rel="<?php
			if($next_month<9) {
				echo '0'.$next_month."-".$next_year;
			}
			else {
			echo $next_month."-".$next_year;
			} 
			 ?>">
				<img src="#" alt="" style="padding: 7px;" />
			</a>	
				<div class="month_heading" style="font-size:28px; font-weight:bolder;"><?php echo $monthNames[$cMonth-1].' '.$cYear; ?></div>
			</div> 
		</div>
		
		


<div class="demo-description">

	   	<table class="calender" style="">
			<tbody>
				<tr class="weekdays" style="">
					<td>Sun</td>
					<td>Mon</td>
					<td>Tue</td>
					<td>Wed</td>
					<td>Thu</td>
					<td>Fri</td>
					<td>Sat</td>
				</tr>
			</tbody>
			<tbody>
			
			<?php
			$timestamp = mktime(0,0,0,$cMonth,1,$cYear);
			$maxday = date("t",$timestamp);
			$thismonth = getdate ($timestamp);
			$startday = $thismonth['wday'];
			
		
			for ($i=0; $i<($maxday+$startday); $i++) {
			 $day= ($i - $startday + 1);
				 $cMonth = $_REQUEST["month"];
				 $cYear = $_REQUEST["year"];
				 if($cMonth <= 9)
							 {
							 	$cMonth = '0'.$cMonth;
							 }
							 
							
				$job_date_match= $day.'/'.$cMonth.'/'.$cYear;
				
				$tr_date = $day.'-'.$cMonth.'-'.$cYear;
			
				if(($i % 7) == 0 ) { echo "<tr class='days'>"; }
				if($i < $startday) { echo "<td></td>"; }
				
				
				else { echo "<td id='".$tr_date."' class='group'>
							<div class='key_day'>". ($i - $startday + 1) . "</div>";
					}		
							
							 
							 
							
							$date_status_match= Match_Job($job_date_match, $this->session->userdata('login_id'));
							//print_r($job_date_match);
							//print_r($date_status_match);
							
							if(count($date_status_match))
							{
							//print_r($date_status_match); exit;
							echo "<div style='min-height: 20px; width: 96px; margin-top: 2px;' id='set_status_div'>";
								foreach($date_status_match as $match)
								{
									$res= get_assigned_to($match['assigned_to']);
									
	 $title = $match['job_title']."<br />".$match['job_description']."<br />".$res['username']."<br />".$match['assigned_by'];
	$title= str_replace("'","`", $title);
	//$title = $match['job_description'];
							
							if($match['status']==1)
							 {
							 	echo "<div id='".$job_date_match."' rel='".$match['job_id']."' class='app_indication_yellow update_status' onmouseover='show_prompt(this);' onmouseout='hide_prompt(this);' lang='$title'>".$match['job_id']."</div>";
							 }
							 else if ($match['status']==2)
							 {
								echo "<div id='".$job_date_match."' rel='".$match['job_id']."' class='app_indication_green update_status' onmouseover='show_prompt(this);' onmouseout='hide_prompt(this);' lang='$title'>".$match['job_id']."</div>";
							 }
							 else if ($match['status']==3)
							 {
								echo "<div id='".$job_date_match."' rel='".$match['job_id']."' class='app_indication_blue update_status'  onmouseover='show_prompt(this);' onmouseout='hide_prompt(this);' lang='$title'>".$match['job_id']."</div>";
							 }
							 else 
							 {
							 	echo "<div id='".$job_date_match."' rel='".$match['job_id']."' class='app_indication_red update_status' onmouseover='show_prompt(this);' onmouseout='hide_prompt(this);' lang='$title'>".$match['job_id']."</div>";
							 }
							 
								
								}
							 echo "</div>";
							}
							echo "</td>";
							
							
				if(($i % 7) == 6 ) echo "</tr>";
			}
		
			?>
				
			</tbody>
		</table> 
	   
	   </div>

<?php //echo @$calendar; ?>

	</div><!--/row-->



	</div>




<div style="display:none;">			
<div class="widget">

<div id="job-lightbox">
<p class="para-job">Create a New Job</p>
<div id="line-r"></div>
<form id="add_new_job">

	<fieldset id="w1first" class="step ui-formwizard-content">

                        <div class="formRow">

                            <div class="grid9" style="margin-top: 10px; width: 240px; margin-left: 10px;">

							<input name="job_title" id="job_title" class="input-block-level input_me" value="" type="text" style="margin-right:300px;" placeholder="Job Title">

							
							 <div class="grid9" style="width: 240px; margin-left: 287px; margin-top: -40px;"><input name="date" id="date" class="input-block-level input_me" value="" type="text" style="margin-right:300px;" placeholder="Date"></div>
								<script>
								$( "#date" ).datepicker({ 

								defaultDate: +7,
						
								showOtherMonths:true,
						
								autoSize: true,
						
								dateFormat: 'd/mm/yy'
						
							});	
						
						
								</script>						

							</div>

				
							<div class="clear"></div>

                         </div>

						 <div class="formRow">
	 
						 <div class="grid9" style="margin-top: 10px; width: 250px; margin-left: 10px;">
						 <select name="assigned_to" id="assigned_to" class="input_me" style=" width: 240px; margin-left: 2px; margin-top: -7px;">

							<option value="">Assigned To</option>

						

							<?php if($users){

								foreach($users as $user){ ?>

								<option value="<?php echo $user['user_id']; ?>"><?php echo $user['username']; ?></option>						

							<?php	}

					
							} ?>
						</select>
						 
							</div>

                           <div class="grid9" style="width: 240px; margin-left: 300px; margin-top: -40px;">

							<input name="assigned_by" id="assigned_by" class="input-block-level input_me" value="<?=@$this->session->userdata('firstname') ?> <?=@$this->session->userdata('lastname') ?>" readonly="" type="text" style="margin-right:300px; height:28px; width:230px; " placeholder="Assigned By">

							</div>


							<div class="clear"></div>

                         </div>
						
						 <div class="formRow">
						  <div class="grid9" style="margin-top: 10px; width: 250px; margin-left: 10px;">
						  <select id="status" name="status" style=" width: 240px;" class="input_me" >
							<option value="">Select Status</option>
							<option value="1">Not Started</option>
							<option value="2">In Progress</option>
							<option value="3">Completed</option>
							<option value="4">Overdue</option>
							</select></div>

                           

						    <div class="grid9" style="width: 250px; margin-left: 300px; margin-top: -40px;">
							<select id="priority" name="priority" style=" width: 240px;" class="input_me" >
							<option value="">Select Priority</option>
							<option value="low">Low</option>
							<option value="medium">Medium</option>
							<option value="high">High</option>
							</select>
							
							</div>

							

							<div class="clear"></div>

                         </div>

						 <div class="formRow">
						<div class="grid9" style="margin-top: 10px; width: 250px; margin-left: 10px;">
						 <select name="equipment" id="equipment" class="input_me" style=" width: 240px;">

							<option value="">Select Equipment</option>

						

							<?php if($equipments){

								foreach($equipments as $equipment){ ?>

								<option value="<?php echo $equipment['equipment_id']; ?>"><?php echo $equipment['equipment_name']; ?></option>						

							<?php	}

					
							} ?>
						</select>
						 
							

							</div>
						

                           <div class="grid9" style="width: 250px; margin-left: 300px; margin-top: -40px;">
	<textarea name="job_desc" id="job_desc" class="input-block-level input_me"  style="margin-right:300px; width: 238px;  margin-left: 6px;"  placeholder="Job Description"></textarea>
							

							</div>

								<div class="clear"></div>

                         </div>

                    </fieldset>


<br />

<button class="btn btn-large btn-primary" type="" onClick="add_job(); return false;" style="margin-left:25px;">&nbsp;&nbsp;Add Job &raquo;</button>

<button class="btn btn-large btn-primary" type="button" id="popover_close">Cancel</button>



</form>

</div>
</div>
</div>
<div id="view_membership_message" style="position:fixed; top:164px; left:600px;display:none"></div>

<div style="display:none;">
	<div id="view_job_popup" >
	</div>
</div>
<div style="display:none;">
	<div id="edit_job_popup" style="display: block; left: 50%; margin-left: -308px; z-index: 1002; width: 600px;">
	</div>
</div>

<div class="arrow-left"  style="display:none;"></div>
<div style="display:none;position: absolute; left: 907px; top: 50px; background-color: black; padding: 14px; width: 201px; border: 7px solid gray; border-radius: 8px 8px 8px 8px; font-size: 10px;color:white;" class="prompt"></div>


<style>

.arrow-left {
	width: 0; 
	height: 0; 
	border-top: 10px solid transparent;
	border-bottom: 10px solid transparent; 
	
	border-right:10px solid gray; 
}
</style>